<!DOCTYPE html>
<html class="no-icon-fonts">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="canonical" href="http://www.espn.com/golf/leaderboard" />
	<title>World Golf Championships-Mexico Championship Golf Leaderboard and Results- ESPN</title>
	<meta name="description" content="ESPN's up-to-the-minute leaderboard of the World Golf Championships-Mexico Championship golf tournament"
	/>
	<meta name="news_keywords" content="" />
	<meta name="keywords" content="" />
	<meta property="fb:app_id" content="116656161708917" />
	<meta property="og:site_name" content="ESPN.com" />
	<meta property="og:url" content="http://www.espn.com/golf/leaderboard" />
	<meta property="og:title" content="World Golf Championships-Mexico Championship Golf Leaderboard and Results- ESPN" />
	<meta property="og:description" content="ESPN's up-to-the-minute leaderboard of the World Golf Championships-Mexico Championship golf tournament"
	/>
	<meta property="og:image" content="http://a1.espncdn.com/combiner/i?img=%2Fi%2Fespn%2Fespn_logos%2Fespn_red.png" />
	<meta property="og:type" content="website" />
	<meta name="twitter:site" content="espn" />
	<meta name="twitter:url" content="http://www.espn.com/golf/leaderboard" />
	<meta name="twitter:title" content="World Golf Championships-Mexico Championship Golf Leaderboard and Results- ESPN" />
	<meta name="twitter:description" content="ESPN's up-to-the-minute leaderboard of the World Golf Championships-Mexico Championship golf tournament"
	/>
	<meta name="twitter:image" content="http://a1.espncdn.com/combiner/i?img=%2Fi%2Fespn%2Fespn_logos%2Fespn_red.png" />
	<meta name="twitter:card" content="summary">
	<meta name="twitter:app:name:iphone" content="ESPN" />
	<meta name="twitter:app:id:iphone" content="317469184" />
	<meta name="twitter:app:name:googleplay" content="ESPN" />
	<meta name="twitter:app:id:googleplay" content="com.espn.score_center" />
	<meta name="title" content="World Golf Championships-Mexico Championship Golf Leaderboard and Results- ESPN" />
	<meta name="medium" content="website" />

	<!-- Indicate preferred brand name for Google to display -->
	<script type="application/ld+json">
		{
		"@context": "http://schema.org",
		"@type":    "WebSite",
		"name":     "ESPN",
		"url":      "http://www.espn.com/"
	}
	</script>

	<!--
<PageMap>
	<DataObject type="document">
		<Attribute name="title">World Golf Championships-Mexico Championship Golf Leaderboard and Results- ESPN</Attribute>
	</DataObject>
	<DataObject type="thumbnail">
		<Attribute name="src" value="http://a1.espncdn.com/combiner/i?img=%2Fi%2Fespn%2Fespn_logos%2Fespn_red.png" />
		<Attribute name="width" value="1200" />
		<Attribute name="height" value="630" />
	</DataObject>
</PageMap>
-->
	<link rel="alternate" hreflang="en-us" href="http://www.espn.com/golf/leaderboard" />
	<link rel="alternate" hreflang="en" href="http://www.espn.com/golf/leaderboard" />
	<link rel="alternate" hreflang="en-gb" href="http://www.espn.co.uk/golf/leaderboard" />
	<link rel="alternate" hreflang="en-au" href="http://www.espn.com.au/golf/leaderboard" />
	<link rel="alternate" hreflang="en-in" href="http://www.espn.in/golf/leaderboard" />
	<link rel="alternate" hreflang="es-us" href="http://espndeportes.espn.com/golf/lideres" />
	<link rel="alternate" hreflang="es-es" href="http://espndeportes.espn.com/golf/lideres" />
	<link rel="alternate" hreflang="es" href="http://espndeportes.espn.com/golf/lideres" />
	<link rel="alternate" hreflang="es-ar" href="http://www.espn.com.ar/golf/lideres" />
	<link rel="alternate" hreflang="es-pe" href="http://www.espn.com.ar/golf/lideres" />
	<link rel="alternate" hreflang="es-cl" href="http://www.espn.cl/golf/lideres" />
	<link rel="alternate" hreflang="es-co" href="http://www.espn.com.co/golf/lideres" />
	<link rel="alternate" hreflang="es-mx" href="http://www.espn.com.mx/golf/lideres" />
	<link rel="alternate" hreflang="es-ve" href="http://www.espn.com.ve/golf/lideres" />
	<link rel="alternate" hreflang="es-cr" href="http://www.espn.com.ve/golf/lideres" />
	<link rel="alternate" hreflang="es-gt" href="http://www.espn.com.ve/golf/lideres" />
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js"></script>
	<script type="text/javascript">
		; (function () {
			function rc(a) { for (var b = a + "=", c = document.cookie.split(";"), d = 0; d < c.length; d++) { for (var e = c[d]; " " === e.charAt(0);)e = e.substring(1, e.length); if (0 === e.indexOf(b)) return e.substring(b.length, e.length) } return null } var _nr = !1, _nrCookie = rc("_nr"); null !== _nrCookie ? "1" === _nrCookie && (_nr = !0) : Math.floor(100 * Math.random()) + 1 === 13 ? (_nr = !0, document.cookie = "_nr=1; path=/") : (_nr = !1, document.cookie = "_nr=0; path=/"), _nr && (window.NREUM || (NREUM = {}), __nr_require = function (t, e, n) { function r(n) { if (!e[n]) { var o = e[n] = { exports: {} }; t[n][0].call(o.exports, function (e) { var o = t[n][1][e]; return r(o || e) }, o, o.exports) } return e[n].exports } if ("function" == typeof __nr_require) return __nr_require; for (var o = 0; o < n.length; o++)r(n[o]); return r }({ 1: [function (t, e, n) { function r(t) { try { c.console && console.log(t) } catch (e) { } } var o, i = t("ee"), a = t(19), c = {}; try { o = localStorage.getItem("__nr_flags").split(","), console && "function" == typeof console.log && (c.console = !0, o.indexOf("dev") !== -1 && (c.dev = !0), o.indexOf("nr_dev") !== -1 && (c.nrDev = !0)) } catch (s) { } c.nrDev && i.on("internal-error", function (t) { r(t.stack) }), c.dev && i.on("fn-err", function (t, e, n) { r(n.stack) }), c.dev && (r("NR AGENT IN DEVELOPMENT MODE"), r("flags: " + a(c, function (t, e) { return t }).join(", "))) }, {}], 2: [function (t, e, n) { function r(t, e, n, r, o) { try { h ? h -= 1 : i("err", [o || new UncaughtException(t, e, n)]) } catch (c) { try { i("ierr", [c, (new Date).getTime(), !0]) } catch (s) { } } return "function" == typeof f && f.apply(this, a(arguments)) } function UncaughtException(t, e, n) { this.message = t || "Uncaught error with no additional information", this.sourceURL = e, this.line = n } function o(t) { i("err", [t, (new Date).getTime()]) } var i = t("handle"), a = t(20), c = t("ee"), s = t("loader"), f = window.onerror, u = !1, h = 0; s.features.err = !0, t(1), window.onerror = r; try { throw new Error } catch (p) { "stack" in p && (t(12), t(11), "addEventListener" in window && t(6), s.xhrWrappable && t(13), u = !0) } c.on("fn-start", function (t, e, n) { u && (h += 1) }), c.on("fn-err", function (t, e, n) { u && (this.thrown = !0, o(n)) }), c.on("fn-end", function () { u && !this.thrown && h > 0 && (h -= 1) }), c.on("internal-error", function (t) { i("ierr", [t, (new Date).getTime(), !0]) }) }, {}], 3: [function (t, e, n) { t("loader").features.ins = !0 }, {}], 4: [function (t, e, n) { function r() { N++ , S = y.hash, this[u] = Date.now() } function o() { N-- , y.hash !== S && i(0, !0); var t = Date.now(); this[l] = ~~this[l] + t - this[u], this[h] = t } function i(t, e) { x.emit("newURL", ["" + y, e]) } function a(t, e) { t.on(e, function () { this[e] = Date.now() }) } var c = "-start", s = "-end", f = "-body", u = "fn" + c, h = "fn" + s, p = "cb" + c, d = "cb" + s, l = "jsTime", m = "fetch", v = "addEventListener", w = window, y = w.location; if (w[v]) { var g = t(9), b = t(10), x = t(8), E = t(6), T = t(12), O = t(7), P = t(13), R = t("ee"), D = R.get("tracer"); t(14), t("loader").features.spa = !0; var S, M = w[v], N = 0; R.on(u, r), R.on(p, r), R.on(h, o), R.on(d, o), R.buffer([u, h, "xhr-done", "xhr-resolved"]), E.buffer([u]), T.buffer(["setTimeout" + s, "clearTimeout" + c, u]), P.buffer([u, "new-xhr", "open-xhr" + c]), O.buffer([m + c, m + "-done", m + f + c, m + f + s]), x.buffer(["newURL"]), g.buffer([u]), b.buffer(["propagate", p, d, "executor-err", "resolve" + c]), D.buffer([u, "no-" + u]), a(P, "open-xhr" + c), a(R, "xhr-resolved"), a(R, "xhr-done"), a(O, m + c), a(O, m + "-done"), x.on("pushState-end", i), x.on("replaceState-end", i), M("hashchange", i, !0), M("load", i, !0), M("popstate", function () { i(0, N > 1) }, !0) } }, {}], 5: [function (t, e, n) { function r(t) { } if (window.performance && window.performance.timing && window.performance.getEntriesByType) { var o = t("ee"), i = t("handle"), a = t(12), c = t(11), s = "learResourceTimings", f = "addEventListener", u = "resourcetimingbufferfull", h = "bstResource", p = "resource", d = "-start", l = "-end", m = "fn" + d, v = "fn" + l, w = "bstTimer", y = "pushState"; t("loader").features.stn = !0, t(8); var g = NREUM.o.EV; o.on(m, function (t, e) { var n = t[0]; n instanceof g && (this.bstStart = Date.now()) }), o.on(v, function (t, e) { var n = t[0]; n instanceof g && i("bst", [n, e, this.bstStart, Date.now()]) }), a.on(m, function (t, e, n) { this.bstStart = Date.now(), this.bstType = n }), a.on(v, function (t, e) { i(w, [e, this.bstStart, Date.now(), this.bstType]) }), c.on(m, function () { this.bstStart = Date.now() }), c.on(v, function (t, e) { i(w, [e, this.bstStart, Date.now(), "requestAnimationFrame"]) }), o.on(y + d, function (t) { this.time = Date.now(), this.startPath = location.pathname + location.hash }), o.on(y + l, function (t) { i("bstHist", [location.pathname + location.hash, this.startPath, this.time]) }), f in window.performance && (window.performance["c" + s] ? window.performance[f](u, function (t) { i(h, [window.performance.getEntriesByType(p)]), window.performance["c" + s]() }, !1) : window.performance[f]("webkit" + u, function (t) { i(h, [window.performance.getEntriesByType(p)]), window.performance["webkitC" + s]() }, !1)), document[f]("scroll", r, !1), document[f]("keypress", r, !1), document[f]("click", r, !1) } }, {}], 6: [function (t, e, n) { function r(t) { for (var e = t; e && !e.hasOwnProperty(u);)e = Object.getPrototypeOf(e); e && o(e) } function o(t) { c.inPlace(t, [u, h], "-", i) } function i(t, e) { return t[1] } var a = t("ee").get("events"), c = t(21)(a), s = t("gos"), f = XMLHttpRequest, u = "addEventListener", h = "removeEventListener"; e.exports = a, "getPrototypeOf" in Object ? (r(document), r(window), r(f.prototype)) : f.prototype.hasOwnProperty(u) && (o(window), o(f.prototype)), a.on(u + "-start", function (t, e) { if (t[1]) { var n = t[1]; if ("function" == typeof n) { var r = s(n, "nr@wrapped", function () { return c(n, "fn-", null, n.name || "anonymous") }); this.wrapped = t[1] = r } else "function" == typeof n.handleEvent && c.inPlace(n, ["handleEvent"], "fn-") } }), a.on(h + "-start", function (t) { var e = this.wrapped; e && (t[1] = e) }) }, {}], 7: [function (t, e, n) { function r(t, e, n) { var r = t[e]; "function" == typeof r && (t[e] = function () { var t = r.apply(this, arguments); return o.emit(n + "start", arguments, t), t.then(function (e) { return o.emit(n + "end", [null, e], t), e }, function (e) { throw o.emit(n + "end", [e], t), e }) }) } var o = t("ee").get("fetch"), i = t(19); e.exports = o; var a = window, c = "fetch-", s = c + "body-", f = ["arrayBuffer", "blob", "json", "text", "formData"], u = a.Request, h = a.Response, p = a.fetch, d = "prototype"; u && h && p && (i(f, function (t, e) { r(u[d], e, s), r(h[d], e, s) }), r(a, "fetch", c), o.on(c + "end", function (t, e) { var n = this; e ? e.clone().arrayBuffer().then(function (t) { n.rxSize = t.byteLength, o.emit(c + "done", [null, e], n) }) : o.emit(c + "done", [t], n) })) }, {}], 8: [function (t, e, n) { var r = t("ee").get("history"), o = t(21)(r); e.exports = r, o.inPlace(window.history, ["pushState", "replaceState"], "-") }, {}], 9: [function (t, e, n) { var r = t("ee").get("mutation"), o = t(21)(r), i = NREUM.o.MO; e.exports = r, i && (window.MutationObserver = function (t) { return this instanceof i ? new i(o(t, "fn-")) : i.apply(this, arguments) }, MutationObserver.prototype = i.prototype) }, {}], 10: [function (t, e, n) { function r(t) { var e = a.context(), n = c(t, "executor-", e), r = new f(n); return a.context(r).getCtx = function () { return e }, a.emit("new-promise", [r, e], e), r } function o(t, e) { return e } var i = t(21), a = t("ee").get("promise"), c = i(a), s = t(19), f = NREUM.o.PR; e.exports = a, f && (window.Promise = r, ["all", "race"].forEach(function (t) { var e = f[t]; f[t] = function (n) { function r(t) { return function () { a.emit("propagate", [null, !o], i), o = o || !t } } var o = !1; s(n, function (e, n) { Promise.resolve(n).then(r("all" === t), r(!1)) }); var i = e.apply(f, arguments), c = f.resolve(i); return c } }), ["resolve", "reject"].forEach(function (t) { var e = f[t]; f[t] = function (t) { var n = e.apply(f, arguments); return t !== n && a.emit("propagate", [t, !0], n), n } }), f.prototype["catch"] = function (t) { return this.then(null, t) }, f.prototype = Object.create(f.prototype, { constructor: { value: r } }), s(Object.getOwnPropertyNames(f), function (t, e) { try { r[e] = f[e] } catch (n) { } }), a.on("executor-start", function (t) { t[0] = c(t[0], "resolve-", this), t[1] = c(t[1], "resolve-", this) }), a.on("executor-err", function (t, e, n) { t[1](n) }), c.inPlace(f.prototype, ["then"], "then-", o), a.on("then-start", function (t, e) { this.promise = e, t[0] = c(t[0], "cb-", this), t[1] = c(t[1], "cb-", this) }), a.on("then-end", function (t, e, n) { this.nextPromise = n; var r = this.promise; a.emit("propagate", [r, !0], n) }), a.on("cb-end", function (t, e, n) { a.emit("propagate", [n, !0], this.nextPromise) }), a.on("propagate", function (t, e, n) { this.getCtx && !e || (this.getCtx = function () { if (t instanceof Promise) var e = a.context(t); return e && e.getCtx ? e.getCtx() : this }) }), r.toString = function () { return "" + f }) }, {}], 11: [function (t, e, n) { var r = t("ee").get("raf"), o = t(21)(r), i = "equestAnimationFrame"; e.exports = r, o.inPlace(window, ["r" + i, "mozR" + i, "webkitR" + i, "msR" + i], "raf-"), r.on("raf-start", function (t) { t[0] = o(t[0], "fn-") }) }, {}], 12: [function (t, e, n) { function r(t, e, n) { t[0] = a(t[0], "fn-", null, n) } function o(t, e, n) { this.method = n, this.timerDuration = "number" == typeof t[1] ? t[1] : 0, t[0] = a(t[0], "fn-", this, n) } var i = t("ee").get("timer"), a = t(21)(i), c = "setTimeout", s = "setInterval", f = "clearTimeout", u = "-start", h = "-"; e.exports = i, a.inPlace(window, [c, "setImmediate"], c + h), a.inPlace(window, [s], s + h), a.inPlace(window, [f, "clearImmediate"], f + h), i.on(s + u, r), i.on(c + u, o) }, {}], 13: [function (t, e, n) { function r(t, e) { h.inPlace(e, ["onreadystatechange"], "fn-", c) } function o() { var t = this, e = u.context(t); t.readyState > 3 && !e.resolved && (e.resolved = !0, u.emit("xhr-resolved", [], t)), h.inPlace(t, v, "fn-", c) } function i(t) { w.push(t), l && (g = -g, b.data = g) } function a() { for (var t = 0; t < w.length; t++)r([], w[t]); w.length && (w = []) } function c(t, e) { return e } function s(t, e) { for (var n in t) e[n] = t[n]; return e } t(6); var f = t("ee"), u = f.get("xhr"), h = t(21)(u), p = NREUM.o, d = p.XHR, l = p.MO, m = "readystatechange", v = ["onload", "onerror", "onabort", "onloadstart", "onloadend", "onprogress", "ontimeout"], w = []; e.exports = u; var y = window.XMLHttpRequest = function (t) { var e = new d(t); try { u.emit("new-xhr", [e], e), e.addEventListener(m, o, !1) } catch (n) { try { u.emit("internal-error", [n]) } catch (r) { } } return e }; if (s(d, y), y.prototype = d.prototype, h.inPlace(y.prototype, ["open", "send"], "-xhr-", c), u.on("send-xhr-start", function (t, e) { r(t, e), i(e) }), u.on("open-xhr-start", r), l) { var g = 1, b = document.createTextNode(g); new l(a).observe(b, { characterData: !0 }) } else f.on("fn-end", function (t) { t[0] && t[0].type === m || a() }) }, {}], 14: [function (t, e, n) { function r(t) { var e = this.params, n = this.metrics; if (!this.ended) { this.ended = !0; for (var r = 0; r < h; r++)t.removeEventListener(u[r], this.listener, !1); if (!e.aborted) { if (n.duration = (new Date).getTime() - this.startTime, 4 === t.readyState) { e.status = t.status; var i = o(t, this.lastSize); if (i && (n.rxSize = i), this.sameOrigin) { var a = t.getResponseHeader("X-NewRelic-App-Data"); a && (e.cat = a.split(", ").pop()) } } else e.status = 0; n.cbTime = this.cbTime, f.emit("xhr-done", [t], t), c("xhr", [e, n, this.startTime]) } } } function o(t, e) { var n = t.responseType; if ("json" === n && null !== e) return e; var r = "arraybuffer" === n || "blob" === n || "json" === n ? t.response : t.responseText; return l(r) } function i(t, e) { var n = s(e), r = t.params; r.host = n.hostname + ":" + n.port, r.pathname = n.pathname, t.sameOrigin = n.sameOrigin } var a = t("loader"); if (a.xhrWrappable) { var c = t("handle"), s = t(15), f = t("ee"), u = ["load", "error", "abort", "timeout"], h = u.length, p = t("id"), d = t(18), l = t(17), m = window.XMLHttpRequest; a.features.xhr = !0, t(13), f.on("new-xhr", function (t) { var e = this; e.totalCbs = 0, e.called = 0, e.cbTime = 0, e.end = r, e.ended = !1, e.xhrGuids = {}, e.lastSize = null, d && (d > 34 || d < 10) || window.opera || t.addEventListener("progress", function (t) { e.lastSize = t.loaded }, !1) }), f.on("open-xhr-start", function (t) { this.params = { method: t[0] }, i(this, t[1]), this.metrics = {} }), f.on("open-xhr-end", function (t, e) { "loader_config" in NREUM && "xpid" in NREUM.loader_config && this.sameOrigin && e.setRequestHeader("X-NewRelic-ID", NREUM.loader_config.xpid) }), f.on("send-xhr-start", function (t, e) { var n = this.metrics, r = t[0], o = this; if (n && r) { var i = l(r); i && (n.txSize = i) } this.startTime = (new Date).getTime(), this.listener = function (t) { try { "abort" === t.type && (o.params.aborted = !0), ("load" !== t.type || o.called === o.totalCbs && (o.onloadCalled || "function" != typeof e.onload)) && o.end(e) } catch (n) { try { f.emit("internal-error", [n]) } catch (r) { } } }; for (var a = 0; a < h; a++)e.addEventListener(u[a], this.listener, !1) }), f.on("xhr-cb-time", function (t, e, n) { this.cbTime += t, e ? this.onloadCalled = !0 : this.called += 1, this.called !== this.totalCbs || !this.onloadCalled && "function" == typeof n.onload || this.end(n) }), f.on("xhr-load-added", function (t, e) { var n = "" + p(t) + !!e; this.xhrGuids && !this.xhrGuids[n] && (this.xhrGuids[n] = !0, this.totalCbs += 1) }), f.on("xhr-load-removed", function (t, e) { var n = "" + p(t) + !!e; this.xhrGuids && this.xhrGuids[n] && (delete this.xhrGuids[n], this.totalCbs -= 1) }), f.on("addEventListener-end", function (t, e) { e instanceof m && "load" === t[0] && f.emit("xhr-load-added", [t[1], t[2]], e) }), f.on("removeEventListener-end", function (t, e) { e instanceof m && "load" === t[0] && f.emit("xhr-load-removed", [t[1], t[2]], e) }), f.on("fn-start", function (t, e, n) { e instanceof m && ("onload" === n && (this.onload = !0), ("load" === (t[0] && t[0].type) || this.onload) && (this.xhrCbStart = (new Date).getTime())) }), f.on("fn-end", function (t, e) { this.xhrCbStart && f.emit("xhr-cb-time", [(new Date).getTime() - this.xhrCbStart, this.onload, e], e) }) } }, {}], 15: [function (t, e, n) { e.exports = function (t) { var e = document.createElement("a"), n = window.location, r = {}; e.href = t, r.port = e.port; var o = e.href.split("://"); !r.port && o[1] && (r.port = o[1].split("/")[0].split("@").pop().split(":")[1]), r.port && "0" !== r.port || (r.port = "https" === o[0] ? "443" : "80"), r.hostname = e.hostname || n.hostname, r.pathname = e.pathname, r.protocol = o[0], "/" !== r.pathname.charAt(0) && (r.pathname = "/" + r.pathname); var i = !e.protocol || ":" === e.protocol || e.protocol === n.protocol, a = e.hostname === document.domain && e.port === n.port; return r.sameOrigin = i && (!e.hostname || a), r } }, {}], 16: [function (t, e, n) { function r() { } function o(t, e, n) { return function () { return i(t, [(new Date).getTime()].concat(c(arguments)), e ? null : this, n), e ? void 0 : this } } var i = t("handle"), a = t(19), c = t(20), s = t("ee").get("tracer"), f = NREUM; "undefined" == typeof window.newrelic && (newrelic = f); var u = ["setPageViewName", "setCustomAttribute", "setErrorHandler", "finished", "addToTrace", "inlineHit"], h = "api-", p = h + "ixn-"; a(u, function (t, e) { f[e] = o(h + e, !0, "api") }), f.addPageAction = o(h + "addPageAction", !0), e.exports = newrelic, f.interaction = function () { return (new r).get() }; var d = r.prototype = { createTracer: function (t, e) { var n = {}, r = this, o = "function" == typeof e; return i(p + "tracer", [Date.now(), t, n], r), function () { if (s.emit((o ? "" : "no-") + "fn-start", [Date.now(), r, o], n), o) try { return e.apply(this, arguments) } finally { s.emit("fn-end", [Date.now()], n) } } } }; a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","), function (t, e) { d[e] = o(p + e) }), newrelic.noticeError = function (t) { "string" == typeof t && (t = new Error(t)), i("err", [t, (new Date).getTime()]) } }, {}], 17: [function (t, e, n) { e.exports = function (t) { if ("string" == typeof t && t.length) return t.length; if ("object" == typeof t) { if ("undefined" != typeof ArrayBuffer && t instanceof ArrayBuffer && t.byteLength) return t.byteLength; if ("undefined" != typeof Blob && t instanceof Blob && t.size) return t.size; if (!("undefined" != typeof FormData && t instanceof FormData)) try { return JSON.stringify(t).length } catch (e) { return } } } }, {}], 18: [function (t, e, n) { var r = 0, o = navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/); o && (r = +o[1]), e.exports = r }, {}], 19: [function (t, e, n) { function r(t, e) { var n = [], r = "", i = 0; for (r in t) o.call(t, r) && (n[i] = e(r, t[r]), i += 1); return n } var o = Object.prototype.hasOwnProperty; e.exports = r }, {}], 20: [function (t, e, n) { function r(t, e, n) { e || (e = 0), "undefined" == typeof n && (n = t ? t.length : 0); for (var r = -1, o = n - e || 0, i = Array(o < 0 ? 0 : o); ++r < o;)i[r] = t[e + r]; return i } e.exports = r }, {}], 21: [function (t, e, n) { function r(t) { return !(t && "function" == typeof t && t.apply && !t[a]) } var o = t("ee"), i = t(20), a = "nr@original", c = Object.prototype.hasOwnProperty, s = !1; e.exports = function (t) { function e(t, e, n, o) { function nrWrapper() { var r, a, c, s; try { a = this, r = i(arguments), c = "function" == typeof n ? n(r, a) : n || {} } catch (u) { h([u, "", [r, a, o], c]) } f(e + "start", [r, a, o], c); try { return s = t.apply(a, r) } catch (p) { throw f(e + "err", [r, a, p], c), p } finally { f(e + "end", [r, a, s], c) } } return r(t) ? t : (e || (e = ""), nrWrapper[a] = t, u(t, nrWrapper), nrWrapper) } function n(t, n, o, i) { o || (o = ""); var a, c, s, f = "-" === o.charAt(0); for (s = 0; s < n.length; s++)c = n[s], a = t[c], r(a) || (t[c] = e(a, f ? c + o : o, i, c)) } function f(e, n, r) { if (!s) { s = !0; try { t.emit(e, n, r) } catch (o) { h([o, e, n, r]) } s = !1 } } function u(t, e) { if (Object.defineProperty && Object.keys) try { var n = Object.keys(t); return n.forEach(function (n) { Object.defineProperty(e, n, { get: function () { return t[n] }, set: function (e) { return t[n] = e, e } }) }), e } catch (r) { h([r]) } for (var o in t) c.call(t, o) && (e[o] = t[o]); return e } function h(e) { try { t.emit("internal-error", e) } catch (n) { } } return t || (t = o), e.inPlace = n, e.flag = a, e } }, {}], ee: [function (t, e, n) { function r() { } function o(t) { function e(t) { return t && t instanceof r ? t : t ? c(t, a, i) : i() } function n(n, r, o) { t && t(n, r, o); for (var i = e(o), a = p(n), c = a.length, s = 0; s < c; s++)a[s].apply(i, r); var u = f[v[n]]; return u && u.push([w, n, r, i]), i } function h(t, e) { m[t] = p(t).concat(e) } function p(t) { return m[t] || [] } function d(t) { return u[t] = u[t] || o(n) } function l(t, e) { s(t, function (t, n) { e = e || "feature", v[n] = e, e in f || (f[e] = []) }) } var m = {}, v = {}, w = { on: h, emit: n, get: d, listeners: p, context: e, buffer: l }; return w } function i() { return new r } var a = "nr@context", c = t("gos"), s = t(19), f = {}, u = {}, h = e.exports = o(); h.backlog = f }, {}], gos: [function (t, e, n) { function r(t, e, n) { if (o.call(t, e)) return t[e]; var r = n(); if (Object.defineProperty && Object.keys) try { return Object.defineProperty(t, e, { value: r, writable: !0, enumerable: !1 }), r } catch (i) { } return t[e] = r, r } var o = Object.prototype.hasOwnProperty; e.exports = r }, {}], handle: [function (t, e, n) { function r(t, e, n, r) { o.buffer([t], r), o.emit(t, e, n) } var o = t("ee").get("handle"); e.exports = r, r.ee = o }, {}], id: [function (t, e, n) { function r(t) { var e = typeof t; return !t || "object" !== e && "function" !== e ? -1 : t === window ? 0 : a(t, i, function () { return o++ }) } var o = 1, i = "nr@id", a = t("gos"); e.exports = r }, {}], loader: [function (t, e, n) { function r() { if (!g++) { var t = y.info = NREUM.info, e = u.getElementsByTagName("script")[0]; if (t && t.licenseKey && t.applicationID && e) { s(v, function (e, n) { t[e] || (t[e] = n) }); var n = "https" === m.split(":")[0] || t.sslForHttp; y.proto = n ? "https://" : "http://", c("mark", ["onload", a()], null, "api"); var r = u.createElement("script"); r.src = y.proto + t.agent, e.parentNode.insertBefore(r, e) } } } function o() { "complete" === u.readyState && i() } function i() { c("mark", ["domContent", a()], null, "api") } function a() { return (new Date).getTime() } var c = t("handle"), s = t(19), f = window, u = f.document, h = "addEventListener", p = "attachEvent", d = f.XMLHttpRequest, l = d && d.prototype; NREUM.o = { ST: setTimeout, CT: clearTimeout, XHR: d, REQ: f.Request, EV: f.Event, PR: f.Promise, MO: f.MutationObserver }, t(16); var m = "" + location, v = { beacon: "bam.nr-data.net", errorBeacon: "bam.nr-data.net", agent: "js-agent.newrelic.com/nr-spa-974.min.js" }, w = d && l && l[h] && !/CriOS/.test(navigator.userAgent), y = e.exports = { offset: a(), origin: m, features: {}, xhrWrappable: w }; u[h] ? (u[h]("DOMContentLoaded", i, !1), f[h]("load", r, !1)) : (u[p]("onreadystatechange", o), f[p]("onload", r)), c("mark", ["firstbyte", a()], null, "api"); var g = 0 }, {}] }, {}, ["loader", 2, 14, 5, 3, 4]), NREUM.info = { beacon: "bam.nr-data.net", errorBeacon: "bam.nr-data.net", licenseKey: "d1734eda45", applicationID: "3785502", sa: 1 });
		})();
	</script>
	<script>
		// load new edition settings
		var espn = espn || {};
		espn.i18n = espn.i18n || {};
		espn.i18n.lang = "en";
		espn.i18n.siteId = "1";
		espn.i18n.site = "espn";
		espn.i18n.editionKey = "espn-en";
		espn.i18n.personalization = true;
		espn.i18n.country = "us";
		espn.i18n.domain = "www.espn.com";
		espn.i18n.searchUrl = "http://search.espn.com/";
		espn.i18n.hasSearch = true;
		espn.i18n.nowFeed = false;
		espn.i18n.temperature = { scale: "fahrenheit", symbol: "&deg; F" };
		espn.i18n.facebook = { appId: "116656161708917", locale: "en_US" };
		espn.i18n.outbrain = { "mobile": { "compliantId": "MB_4", "nonCompliantId": "MB_5" }, "desktop": { "compliantId": "AR_15", "nonCompliantId": "AR_12" }, "video": { "mobile": { "compliantId": "MB_6" }, "desktop": { "compliantId": "AR_16" } }, "recap": { "mobile": { "compliantId": "MB_9" }, "desktop": { "compliantId": "AR_30" } } }
		espn.i18n.betting = { show: true, provider: "" };
		espn.i18n.tickets = { "enabled": true, "provider": "VividSeats", "baseUrl": "https://www.vividseats.com", "callToAction": "Buy on Vivid Seats", "trackSection": "vivid" };
		espn.i18n.translations = {};
		espn.i18n.environment = "prod";
		espn.i18n.sportBranding = { "cricket": "logos/logo-uk-cricinfo.png", "womenbb": "logos/ESPNcom-powerby-espnw.png", "ncaaw": "logos/ESPNcom-powerby-espnw.png", "ncw": "logos/ESPNcom-powerby-espnw.png", "rugby": "logos/logo-uk-scrum.png", "wnba": "logos/ESPNcom-powerby-espnw.png", "soccer": "logos/logo-uk-fc.png" };
		espn.i18n.sportToUrl = { "cricket": "http://www.espncricinfo.com", "womenbb": "http://espnw.com", "ncaaw": "http://espnw.com", "ncw": "http://espnw.com", "wnba": "http://espnw.com", "soccer": "http://www.espnfc.us" };
		espn.i18n.showWatch = true;

		espn.i18n.showFCContent = false;
		espn.i18n.showCricInfoContent = false;
		espn.i18n.showInsider = true;
		espn.i18n.indexAutoStart = false;
		espn.i18n.videoAutoStart = { index: false, scoreboard: false };
		espn.i18n.customPrimaryNav = false;
		espn.i18n.singleSportNav = false;
		espn.i18n.sportReplacements = "null";

		espn.i18n.uriRewrites = { "paramKeys": { "toEdition": {}, "toEnglish": {} }, "pathSegments": { "toEdition": {}, "toEnglish": {} }, "roots": { "toEdition": { "/horse/": "/horse-racing/", "/nascar/": "/racing/nascar/", "/ncaa/": "/college-sports/", "/ncb/": "/mens-college-basketball/", "/ncf/": "/college-football/", "/oly/": "/olympics/", "/rpm/": "/racing/", "/womenbb/": "/womens-basketball/", "/flb/": "/fantasy/baseball/", "/fba/": "/fantasy/basketball/", "/ffl/": "/fantasy/football/", "/fhl/": "/fantasy/hockey/" }, "toEnglish": { "/oly/summer/gymnastics/": "/oly/summer/gymnastics/", "/oly/summer/cycling/": "/oly/summer/cycling/", "/racing/nascar/": "/nascar/", "/racing/": "/rpm/", "/college-football/": "/ncf/", "/college-football/rumors": "/college-football/rumors", "/mens-college-basketball/": "/ncb/", "/mens-college-basketball/rumors": "/mens-college-basketball/rumors", "/womens-college-basketball/": "/ncw/", "/womens-basketball/": "/womenbb/", "/olympics/": "/oly/", "/cycling/": "/oly/", "/figure-skating/": "/oly/", "/college-sports/": "/ncaa/", "/gymnastics/": "/oly/", "/skiing/": "/oly/", "/horse-racing/": "/horse/", "/sports/womenbb/": "/womenbb/", "/sports/horse/": "/horse/", "/sports/endurance/": "/endurance/", "/losangeles/": "/los-angeles/", "/newyork/": "/new-york/", "/espn/onenacion/": "/onenacion/", "/fantasy/baseball/": "/flb/", "/fantasy/basketball/": "/fba/", "/fantasy/football/": "/ffl/", "/fantasy/hockey/": "/fhl/" } }, "urls": { "toEdition": {}, "toEnglish": {} }, "paramValues": { "toEdition": {}, "toEnglish": {} } };

		try {
			var translations = { "year.mobile.filters": "year", "removedFromYourFavorites": "<p>You've removed<\/p><h1>${title}<\/h1>${subTitle}<p>as a suggested favorite<\/p>", "preferences.sport_labels.700": "English Premier League", "preferences.sport_labels.850": "Tennis", "heliumdown": "Login Temporarily Unavailable", "newsletters": "Newsletters", "no": "No", "pageTitle.Scores_%{leagueOrSport}": "${leagueOrSport} Scores", "score": "Score", "preferences.sport_labels.46": "NBA", "video.next.text": "Up Next", "message.leaguemanager": "Message LM", "favoritesmgmt.manualSortSelected": "You have chosen to manually order how your favorites will appear across ESPN products. At any time, you may return to having ESPN order your favorites by selecting the \"Auto\" option.", "onefeed.suggested": "Suggested Favorites", "favoritesmgmt.favoriteEntity": "${entity} - Favorite", "video.messages.deviceRestricted": "Video is not available for this device.", "preferences.sport_labels.41": "NCAAM", "manageSettingInPersonalSettings": "You can manage this setting in the future under your <a href=\"#\">Personal Settings<\/a>", "hide": "Hide", "search": "Search", "preferences.sport_labels.1700": "Track and Field", "welcomeToESPN": "Welcome to the new ESPN.com", "tweet": "Tweet", "to_be_determined.abbrev": "TBD", "favoriteadded": "Favorite Added", "over/under.abbrev": "O/U", "preferences.sport_labels.8300": "College Sports", "move": "Move", "preferences.sport_labels.5501": "FIFA Club World Cup", "preferences.sport_labels.3700": "Olympic Sports", "preferences.sport_labels.8319": "Snooker", "alert": "Alert", "preferences.sport_labels.8318": "Darts", "favoritesmgmt.confirmHideFavorite": "Hide this from my favorites?", "preferences.sport_labels.59": "WNBA", "season.mobile.filters": "season", "reactivate": "Reactivate", "position.abbrev": "POS", "favoritesmgmt.alertType": "Alert Type", "preferences.sport_labels.54": "NCAAW", "createAccount": "Sign Up", "viewall": "View All", "favoritesmgmt.suggestedHeader": "Suggestions for your location", "home": "Home", "footerText": "ESPN Internet Ventures. <a href=\"http://disneytermsofuse.com/\" rel=\"nofollow\">Terms of Use<\/a>, <a href=\"http://disneyprivacycenter.com/\" rel=\"nofollow\">Privacy Policy<\/a>, <a href=\"https://disneyprivacycenter.com/notice-to-california-residents/\" rel=\"nofollow\">Your California Privacy Rights<\/a>, <a href=\"https://disneyprivacycenter.com/kids-privacy-policy/english/\">Children's Online Privacy Policy<\/a> and <a href=\"http://preferences-mgr.truste.com/?type=espn&affiliateId=148\">Interest-Based Ads<\/a> are applicable to you. All rights reserved.<\/span><span class=\"link-text-short\">Footer<\/span>", "remove": "Remove", "preferences.sport_labels.3918": "English FA Cup", "preferences.sport_labels.28": "NFL", "preferences.sport_labels.3301": "MMA", "preferences.sport_labels.1652": "WWE", "opponent.abbrev": "OPP", "preferences.sport_labels.1650": "Wrestling", "preferences.sport_labels.23": "NCAAF", "subscribe": "Subscribe", "preferences.sport_labels.1200": "Horse Racing", "pop-out": "Pop-out", "inprogress": "In Progress", "connectedfacebook": "Connected to Facebook", "manageMy": "Manage my", "disableVideoDockingPermanently": "Disable video docking permanently", "preferences.sport_labels.3520": "Poker", "suggested": "Suggested", "insider.pickcenter.login_message": "To get exclusive PickCenter analysis, you must be an ESPN Insider", "preferences.sport_labels.3920": "English Capital One Cup", "yes": "Yes", "register": "Register", "preferences.sport_labels.1000": "Boxing", "preferences.sport_labels.33": "CFL", "preferences.sport_labels.8098": "X Games", "reset.mobile.filters": "reset", "favorites.tooManyTeamsToAdd": "Maximum favorite teams limit reached. Please remove at least one team prior to adding additional teams.", "Soccer Scores": "Football Scores", "confirm": "Confirm", "onefeed.scheduleDraft": "Schedule Draft", "comments": "Comments", "viewAllResultsBySearchTerm": "View all results for '${search_term}'", "insiderSubscription": "Insider Subscription", "favoritesmgmt.suggestedHeaderReset": "Suggestions", "purse": "Purse", "undo": "Undo", "manageFavoritesSignIn": "To manage favorites please sign-in or create an ESPN account", "sports": "Sports", "show": "Show", "cancel": "Cancel", "accountInformation": "Account Information", "preferences.sport_labels.2030": "Formula 1", "resize": "Resize", "nflBye": "Bye", "close": "Close", "redesignWelcomeText": "We've redesigned the site with some new and exciting features. You have been selected as part of a limited set of fans who get to experience our new site and give it feedback before it launches!", "favoritesmgmt.reorderSports": "Reorder Sports", "scores": "Scores", "thingsHaveChanged": "As you may notice, things have definitely changed", "favoritesmgmt.noFavorites": "You have not chosen any favorites yet", "preferences.sport_labels.3170": "College Sports", "thereAreNoEventsByDisplayNameByDate": "There are no ${displayName} events for ${readableDate}", "preferences.sport_labels.800": "Field Hockey", "signOut": "Log Out", "video.nowPlaying.text": "Now Playing", "onefeed.draft": "Draft", "filter.mobile.filters": "filter", "videoSettings": "Video Settings", "favorites.draftingNow": "Drafting Now", "noTeamsInFavorites": "No teams in your favorites yet", "preferences.sport_labels.90": "NHL", "favorites.carousel.espnapp.link": "http://www.espn.com/espn/mobile/products/products/_/id/6857590", "enter": "Enter", "preferences.sport_labels.2020": "NASCAR", "favorites": "Favorites", "onefeed.insider.manage": "Manage", "video.messages.geoRestricted": "Video is not available in your country.", "welcometext": "Welcome", "submit.mobile.filters": "submit", "favorites.streakLabel": "Current Streak:", "email": "Email", "pts": "Pts", "favoritesmgmt.sportTeam": "${sportLabel} Team", "preferences.sport_labels.10": "MLB", "onefeed.draftNow": "Draft Now", "preferences.sport_labels.200": "Cricket", "preferences.sport_labels.1300": "Cycling", "preferences.sport_labels.770": "MLS", "preferences.sport_labels.775": "UEFA Champions League", "preferences.sport_labels.300": "Rugby", "today": "Today", "preferences.sport_labels.776": "UEFA Europa League", "add": "Add", "preferences.sport_labels.8367": "Esports", "videoDockingDisabled": "Video docking disabled", "connectfacebook": "Connect with Facebook", "facebook.conversation.account_policy": "Use a <a href=\"https://www.facebook.com/r.php\" rel=\"nofollow\">Facebook account<\/a> to add a comment, subject to Facebook's <a href=\"https://www.facebook.com/policies/\" rel=\"nofollow\">Terms of Service<\/a> and <a href=\"https://www.facebook.com/about/privacy/\" rel=\"nofollow\">Privacy Policy<\/a>. Your Facebook name, photo & other personal information you make public on Facebook will appear with your comment, and may be used on ESPN's media platforms. <a href=\"http://espn.com/espn/story/_/id/8756098/espn-facebook-comments-faq\">Learn more<\/a>.", "earnings": "Earnings", "preferences.sport_labels.781": "European Championship", "addSomeForQuickAccess": "Add some for quick access", "edit": "Edit", "preferences.sport_labels.2000": "Racing", "preferences.sport_labels.2040": "IndyCar", "activateInsider": "Subscribe", "favoritesmgmt.noSuggestedFavorites": "Additional Suggested Favorites are not available at this time", "favoritesmgmt.confirmRemoveFavorite": "Remove Favorite?", "addfavorite": "Add Favorite", "preferences.sport_labels.606": "Football", "signIn": "Log In", "preferences.sport_labels.8374": "Kabaddi", "preferences.sport_labels.8373": "Badminton", "addFavorites": "Add favorites", "preferences.sport_labels.600": "Soccer", "preferences.sport_labels.8372": "Chess", "preferences.sport_labels.1106": "Golf" };
			if (Object.keys(translations).length > 0) {
				espn.i18n.translations = translations;
			}
		} catch (err) {
			window.console.log('Error in espn.i18n loading translations', err);
		}

		espn.i18n.dateTime = {
			dateFormats: {
				time1: "h:mm A",
				date1: "MMM D, YYYY",
				date2: "M/D/YYYY",
				date3: "MM/DD/YYYY",
				date4: "MMDDYYYY",
				date5: "MMMM Do YYYY",
				date6: "dddd, MMMM Do YYYY",
				date7: "ddd, MMM D YYYY",
				date8: "M/D",
				date9: "ddd",
				date10: "dddd, MMMM Do",
				date11: "ddd, MMMM D",
				date12: "MMMM D, YYYY",
				date13: "dddd, M/D",
				date14: "MMM D",
				date15: "ddd, M/D"

			},


			firstDayOfWeek: "Sunday",
			timeZoneBucket: "America/New_York",
			dayNamesShort: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], dayNamesMedium: ["Sun", "Mon", "Tues", "Wed", "Thu", "Fri", "Sat"],

			dayNamesLong: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],

			monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],

			monthNamesLong: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
		};
	</script>
	<script>
				!function () { function n(n, e, i) { var t; if (i) { var r = new Date; r.setTime(r.getTime() + 24 * i * 60 * 60 * 1e3), t = "; expires=" + r.toGMTString() } else t = ""; document.cookie = n + "=" + e + t + "; path=/" } function e(n) { for (var e = n + "=", i = document.cookie.split(";"), t = 0; t < i.length; t++) { for (var r = i[t]; " " === r.charAt(0);)r = r.substring(1, r.length); if (0 === r.indexOf(e)) return r.substring(e.length, r.length) } return null } function i() { var i = e("country"); if ("in" != i && window.location.href.match(/(\?|\&)(src|country-view)=/)) { var t = e("country-view"); null === t && n("country-view", espn.i18n.country, 30) } else if ("en" === espn.i18n.lang) { var t = e("country-view"), r = t || i, o = "|" + r + "|"; if ("|bd|bt|in|mv|np|pk|af|lk|".indexOf(o) > -1 ? r = "in" : "|as|au|ck|fj|pf|ki|mh|fm|nu|pw|pg|tk|to|tv|ws|".indexOf(o) > -1 ? r = "au" : "|uk|gb|fk|gg|gi|gs|im|io|je|sh|".indexOf(o) > -1 && (r = "gb"), null !== r && ("au" == r || "gb" == r || "in" == r) && espn.i18n.country !== r) for (var a = document.querySelectorAll("link[hreflang]"), f = 0; f < a.length; f++) { var u = a[f], c = u.getAttribute("hreflang"), l = new RegExp(espn.i18n.lang + "-" + r, "i"); if (void 0 !== c && c.match(l)) { var g = u.getAttribute("href"); if (void 0 !== g) { var s = g; window.location.href = s } } } } } espn && espn.i18n && (window.location.href.match(/(https?:\/\/insider.|\/insider)/) || i()) }();
	</script>
	<!--
	<link href='http://a.espncdn.com' rel='preconnect' crossorigin>
	<link href='http://a1.espncdn.com' rel='preconnect' crossorigin>
	<link href='http://a2.espncdn.com' rel='preconnect' crossorigin>
	<link href='http://a3.espncdn.com' rel='preconnect' crossorigin>
	<link href='http://a4.espncdn.com' rel='preconnect' crossorigin>
	<link href='http://tredir.go.com' rel='preconnect' crossorigin>
	<link href='https://cdn.registerdisney.go.com' rel='preconnect' crossorigin>
	<link href='http://fan.api.espn.com' rel='preconnect' crossorigin>
	<link href='http://cdn.espn.com' rel='preconnect' crossorigin>
-->
	<link rel="mask-icon" sizes="any" href="http://a.espncdn.com/prod/assets/icons/E.svg" color="#990000">
	<link rel="shortcut icon" href="http://a.espncdn.com/favicon.ico" />
	<link rel="apple-touch-icon" href="http://a.espncdn.com/wireless/mw5/r1/images/bookmark-icons/espn_icon-57x57.min.png" />
	<link rel="apple-touch-icon-precomposed" href="http://a.espncdn.com/wireless/mw5/r1/images/bookmark-icons/espn_icon-57x57.min.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://a.espncdn.com/wireless/mw5/r1/images/bookmark-icons/espn_icon-72x72.min.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://a.espncdn.com/wireless/mw5/r1/images/bookmark-icons/espn_icon-114x114.min.png">
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="http://a.espncdn.com/wireless/mw5/r1/images/bookmark-icons/espn_icon-152x152.min.png">
	<link id="font-link" rel="stylesheet" href="./fonts/base64-benton-woff.css">
	<link rel="stylesheet" href="./css/shell-desktop.css" />
	<link rel="stylesheet" href="./css/page.css">
	<link class="page-type-include" rel="stylesheet" href="./css/game-package-golf.css">
	<script src="./js/espn-head.js"></script>

	<script src="./js/lazysizes.js"></script>

	<script type="text/javascript">
		/*var googletag = googletag || {};
		googletag.cmd = googletag.cmd || [];
		(function () {
			var gads = document.createElement("script");
			gads.async = true;
			gads.type = "text/javascript";
			gads.src = "https://www.googletagservices.com/tag/js/gpt.js";
			var node = document.getElementsByTagName("script")[0];
			node.parentNode.insertBefore(gads, node);
		})();
		*/
	</script>

	<script type="text/javascript">
		var espn = espn || {};
		espn.ads = espn.ads || {};
		espn.ads.config = { "selector": ".ad-slot", "sizes": { "native": { "defaultSize": [1, 4], "mappings": [{ "viewport": [0, 0], "slot": [[1, 4]] }] }, "incontent2": { "defaultSize": [300, 250], "mappings": [{ "viewport": [0, 0], "slot": [[300, 250]] }] }, "incontent": { "defaultSize": [300, 250], "mappings": [{ "viewport": [1024, 0], "slot": [[300, 250], [300, 600]] }] }, "overlay": { "defaultSize": [0, 0], "mappings": [{ "viewport": [0, 0], "slot": [[0, 0]] }] }, "presby": { "defaultSize": [112, 62], "mappings": [{ "viewport": [0, 0], "slot": [[112, 62]] }] }, "wallpaper": { "defaultSize": [1680, 1050], "mappings": [{ "viewport": [1280, 0], "slot": [[1680, 1050]] }, { "viewport": [1024, 0], "slot": [[1280, 455]] }, { "viewport": [0, 0], "slot": [] }] }, "exclusions": { "defaultSize": [1, 2], "mappings": [{ "viewport": [0, 0], "slot": [[1, 2]] }] }, "instream": { "defaultSize": [1, 3], "mappings": [{ "viewport": [0, 0], "slot": [[1, 3]] }] }, "presentedbylogo": { "defaultSize": [110, 28], "mappings": [{ "viewport": [1024, 0], "slot": [[110, 28]] }, { "viewport": [0, 0], "slot": [[90, 20]] }] }, "midpage": { "defaultSize": [320, 250], "mappings": [{ "viewport": [1280, 0], "slot": [[700, 400]] }, { "viewport": [1024, 0], "slot": [[440, 330]] }, { "viewport": [768, 0], "slot": [[320, 250]] }, { "viewport": [0, 0], "slot": [[320, 250]] }] }, "incontentstrip": { "defaultSize": [298, 50], "mappings": [{ "viewport": [1024, 0], "slot": [298, 50] }, { "viewport": [0, 0], "slot": [] }] }, "banner": { "defaultSize": [970, 66], "mappings": [{ "viewport": [1280, 0], "slot": [[1280, 100]] }, { "viewport": [1024, 0], "slot": [[970, 66], [970, 250]] }, { "viewport": [768, 0], "slot": [[728, 90]] }, { "viewport": [0, 0], "slot": [[320, 50]] }] } }, "delayInPageAdSlots": true, "refreshOnBreakpointChange": true, "kvps": [{ "name": "ed", "value": "us" }, { "name": "sp", "value": "golf" }, { "name": "league", "value": "pga" }, { "name": "pgtyp", "value": "leaderboard" }], "network": "6444", "supportDynamicPageLoad": true, "id": 12129264, "level": "espn.com/golf/leaderboard", "incontentPositions": { "index": { "nfl": {}, "top": { "favorites": -1 } }, "defaults": { "news": 4, "now": 4, "favorites": -1 } }, "base": "espn.com", "dynamicKeyValues": { "profile": { "key": "prof" } }, "load": { "story": { "tablet": "window.load", "desktop": "window.load", "mobile": "window.load" }, "frontpage": { "tablet": "init", "desktop": "init", "mobile": "window.load" }, "schedule": { "tablet": "window.load", "desktop": "window.load", "mobile": "window.load" }, "index": { "tablet": "window.load", "desktop": "window.load", "mobile": "window.load" }, "defaults": { "tablet": "init", "desktop": "init", "mobile": "window.load" }, "standings": { "tablet": "window.load", "desktop": "window.load", "mobile": "window.load" }, "scoreboard": { "tablet": "window.load", "desktop": "window.load", "mobile": "window.load" } }, "breakpoints": { "s": [0, 767], "xl": [1280], "l": [1024, 1279], "m": [768, 1023] }, "disabled": "false" };
		espn.ads.configPre = JSON.parse(JSON.stringify(espn.ads.config))
	</script>


	<!-- optimizely js -->
	<!--<script type="text/javascript" src="./js/310987714.js" async></script>-->


	<script>
		// Picture element HTML shim|v it for old IE (pairs with Picturefill.js)
		document.createElement("picture");
	</script>

</head>

<body class="leaderboard desktop  prod " data-pagetype="leaderboard" data-sport="golf" data-customstylesheet="game-package-golf"
 data-lang="en" data-edition="en-us">
	<div class="ad-slot ad-slot-exclusions" data-slot-type="exclusions" data-slot-kvps="pos=exclusions" data-category-exclusion="true"></div>
	<div class="ad-slot ad-slot-overlay" data-slot-type="overlay" data-slot-kvps="pos=outofpage" data-out-of-page="true"></div>
	<div class="ad-slot ad-slot-wallpaper" data-slot-type="wallpaper" data-exclude-bp="s,m" data-slot-kvps="pos=wallpaper" data-collapse-before-load="true"></div>
	<!-- abtest data object global -->
	<script type="text/javascript">
		var abtestData = {};
	</script>


	<!-- optimizely - initialize Optimizely object: temp hard-coded -->
	<script type="text/javascript">
		window['optimizely'] = window['optimizely'] || [];
	</script>


	<div id="fb-root"></div>


	<div id="global-viewport" data-behavior="global_nav_condensed global_nav_full" class=" interior secondary">

		<nav id="global-nav-mobile" data-loadtype="server"></nav>

		<div class="menu-overlay-primary"></div>

		<div id="header-wrapper" data-behavior="global_header" class="hidden-print">



			<header id="global-header" class="espn-en user-account-management has-search">
				<div class="menu-overlay-secondary"></div>

				<div class="container">
					<a id="global-nav-mobile-trigger" href="#" data-route="false"><span>Menu</span></a>
					<h1><a href="/" name="&lpos=sitenavdefault&lid=sitenav_main-logo">Juanma</a></h1>
					<ul class="tools">

						<li class="search">
							<a href="#" class="icon-font-after icon-search-solid-after" id="global-search-trigger"></a>
							<div id="global-search" class="global-search">
								<input type="text" class="search-box" placeholder="Search Sports, Teams or Players..."><input type="submit" class="btn-search">
							</div>
						</li>


						<li class="user" data-behavior="favorites_mgmt"></li>

						<li><a href="#" id="global-scoreboard-trigger" data-route="false">Scores</a></li>
					</ul>


				</div>


				<nav id="global-nav" data-loadtype="server">
					<ul itemscope="" itemtype="http://www.schema.org/SiteNavigationElement">
						<li itemprop="name"><a itemprop="url" href="/nfl/">NFL</a></li>
						<li itemprop="name"><a itemprop="url" href="/nba/">NBA</a></li>
						<li itemprop="name"><a itemprop="url" href="/mlb/">MLB</a></li>
						<li itemprop="name"><a itemprop="url" href="/mens-college-basketball/">NCAAM</a></li>
						<li itemprop="name"><a itemprop="url" href="/college-football/">NCAAF</a></li>
						<li itemprop="name"><a itemprop="url" href="http://www.espnfc.com">Soccer</a></li>
						<li itemprop="name"><a itemprop="url" href="#">&hellip;</a>
							<div>
								<ul class="split">
									<li itemprop="name"><a itemprop="url" href="/nhl/">NHL</a></li>
									<li itemprop="name"><a itemprop="url" href="/golf/">Golf</a></li>
									<li itemprop="name"><a itemprop="url" href="/tennis/">Tennis</a></li>
									<li itemprop="name"><a itemprop="url" href="/mma/">MMA</a></li>
									<li itemprop="name"><a itemprop="url" href="/wwe/">WWE</a></li>
									<li itemprop="name"><a itemprop="url" href="/boxing/">Boxing</a></li>
									<li itemprop="name"><a itemprop="url" href="/esports/">esports</a></li>
									<li itemprop="name"><a itemprop="url" href="/chalk/">Chalk</a></li>
									<li itemprop="name"><a itemprop="url" href="/analytics/">Analytics</a></li>
									<li itemprop="name"><a itemprop="url" href="/womens-basketball/">NCAAW</a></li>
									<li itemprop="name"><a itemprop="url" href="/womens-basketball/">WNBA</a></li>
									<li itemprop="name"><a itemprop="url" href="/racing/nascar/">NASCAR</a></li>
									<li itemprop="name"><a itemprop="url" href="/jayski/">Jayski </a></li>
									<li itemprop="name"><a itemprop="url" href="/racing/">Racing</a></li>
									<li itemprop="name"><a itemprop="url" href="/horse-racing/">Horse</a></li>
									<li itemprop="name"><a itemprop="url" href="http://www.espn.com/college-sports/football/recruiting/">RN FB</a></li>
									<li itemprop="name"><a itemprop="url" href="http://www.espn.com/college-sports/basketball/recruiting/index">RN BB</a></li>
									<li itemprop="name"><a itemprop="url" href="/college-sports/">NCAA</a></li>
									<li itemprop="name"><a itemprop="url" href="http://www.espn.com/moresports/story/_/page/LittleLeagueWorldSeries/little-league-world-series-espn">LLWS</a></li>
									<li itemprop="name"><a itemprop="url" href="/olympics/">Olympic Sports</a></li>
									<li itemprop="name"><a itemprop="url" href="http://www.espn.com/extra/specialolympics/">Special Olympics</a></li>
									<li itemprop="name"><a itemprop="url" href="http://xgames.com/">X Games</a></li>
									<li itemprop="name"><a itemprop="url" href="http://espncricinfo.com/">Cricket</a></li>
									<li itemprop="name"><a itemprop="url" href="http://www.espnscrum.com/">Rugby</a></li>
									<li itemprop="name"><a itemprop="url" href="/endurance/">Endurance</a></li>
									<li itemprop="name"><a itemprop="url" href="http://www.tsn.ca/cfl">CFL</a></li>
								</ul>
							</div>
						</li>
						<li class="pillar more-espn"><a href="#">More ESPN</a></li>
						<li class="pillar fantasy"><a href="/fantasy/">Fantasy</a></li>
						<li class="pillar listen"><a href="http://www.espn.com/espnradio/index">Listen</a></li>
						<li class="pillar watch"><a href="http://www.espn.com/watchespn/index">Watch</a></li>
					</ul>

				</nav>




				<nav id="global-nav-secondary" data-loadtype="server">
					<div class="global-nav-container">
						<ul class="first-group">
							<li class="sports" itemprop="name"><span class="positioning"><a href="/golf/"><span class="brand-logo "><img src="./imagenes/i.png"></span>
								<span class="link-text">Golf</span>
								</a>
								</span>
							</li>
							<li class="sub"><a href="/golf/" data-breakpoints="desktop,desktop-lg,mobile,tablet"><span class="link-text">Home</span><span class="link-text-short">Home</span></a></li>
							<li class="sub"><a href="/golf/leaderboard"><span class="link-text">Scores</span><span class="link-text-short">Scores</span></a></li>
							<li class="sub"><a href="http://www.espn.com/golf/schedule" data-mobile="false"><span class="link-text">Schedule</span><span class="link-text-short">Schedule</span></a></li>
							<li class="sub"><a href="http://www.espn.com/golf/leaders" data-mobile="false"><span class="link-text">Stats</span><span class="link-text-short">Stats</span></a></li>
							<li class="sub"><a href="http://www.espn.com/golf/players" data-mobile="false"><span class="link-text">Players</span><span class="link-text-short">Players</span></a></li>
							<li class="sub external"><a href="http://www.masters.com/"><span class="link-text">Masters.com</span><span class="link-text-short">Masters.com</span></a></li>
							<li class="sub"><a href="http://www.espn.com/golf/statistics/_/year/2016/sort/cupPoints" data-mobile="false"><span class="link-text">FedEx Cup</span><span class="link-text-short">FedEx Cup</span></a></li>
							<li class="sub external"><a href="http://www.vividseats.com/pga-golf?wsUser=717&wsVar=Golfindexnav"><span class="link-text">Tickets</span><span class="link-text-short">Tickets</span></a></li>
						</ul>
						<script type="text/javascript">
							var espn = espn || {};
							espn.nav = espn.nav || {};
							espn.nav.navId = 11929946;
							espn.nav.isFallback = false;
						</script>
					</div>
				</nav>

			</header>
		</div>

		<section id="pane-main" class="post">

			<div id="custom-nav" data-id="leaderboard"></div>

			<!--            	<div class="ad-slot ad-slot-banner ad-wrapper" data-slot-type="banner" data-slot-kvps="pos=banner"></div>
-->
			<section id="main-container">

				<div class="main-content layout-bc">
					<section class="col-b">
						<header class="matchup-header">
							<div class="tab-container alt">
								<ul class="tabs alt" data-behavior="tabs_transform">
									<li class="active"><a itemprop="url" href="/golf/leaderboard"><span>Las Rozas PGA Tour</span></a></li>
									<li><a itemprop="url" href="/golf/leaderboard/_/tour/eur"><span>Euro</span></a></li>
									<li><a itemprop="url" href="/golf/leaderboard/_/tour/lpga"><span>LPGA</span></a></li>
									<li><a itemprop="url" href="/golf/leaderboard/_/tour/champions-tour"><span>Champions</span></a></li>
									<li><a itemprop="url" href="/golf/leaderboard/_/tour/ntw"><span>Web.com</span></a></li>
								</ul>
							</div>
							<div class="tab-content">
								<div class="tab-pane active">
									<div class="filters">
										<div class="dropdown-wrapper hoverable width-auto" data-behavior="button_dropdown"><button class="season button-filter med dropdown-toggle" type="button">2016-17</button>
											<ul class="season dropdown-menu med" role="menu">
												<li><a data-season="2017">2016-17</a></li>
												<li><a data-season="2016">2015-16</a></li>
												<li><a data-season="2015">2014-15</a></li>
												<li><a data-season="2014">2013-14</a></li>
												<li><a data-season="2013">2013</a></li>
												<li><a data-season="2012">2012</a></li>
												<li><a data-season="2011">2011</a></li>
												<li><a data-season="2010">2010</a></li>
												<li><a data-season="2009">2009</a></li>
												<li><a data-season="2008">2008</a></li>
												<li><a data-season="2007">2007</a></li>
												<li><a data-season="2006">2006</a></li>
												<li><a data-season="2005">2005</a></li>
												<li><a data-season="2004">2004</a></li>
												<li><a data-season="2003">2003</a></li>
												<li><a data-season="2002">2002</a></li>
												<li><a data-season="2001">2001</a></li>
											</ul>
										</div>
										<div class="season mobile-dropdown caption"><span class="mobile-arrow" ></span><select id="mobile-season-toggle"><option data-season="2017" selected="selected">2016-17</option><option data-season="2016">2015-16</option><option data-season="2015">2014-15</option><option data-season="2014">2013-14</option><option data-season="2013">2013</option><option data-season="2012">2012</option><option data-season="2011">2011</option><option data-season="2010">2010</option><option data-season="2009">2009</option><option data-season="2008">2008</option><option data-season="2007">2007</option><option data-season="2006">2006</option><option data-season="2005">2005</option><option data-season="2004">2004</option><option data-season="2003">2003</option><option data-season="2002">2002</option><option data-season="2001">2001</option></select></div>

										<div class="dropdown-wrapper hoverable width-auto" data-behavior="button_dropdown"><button class="tournament button-filter med dropdown-toggle" type="button">Campos</button>
											<ul class="tour dropdown-menu med" role="menu">

														<li><a href="/espn.asp?tournamentId=0">Últimos partidos</a></li>
														<li><a href="/espn.asp?tournamentId=1">Las Rejas corto</a></li>
														<li><a href="/espn.asp?tournamentId=2">Las Rejas Largo</a></li>
														<li><a href="/espn.asp?tournamentId=3">Cocotal</a></li>
														<li><a href="/espn.asp?tournamentId=4">Don Cayo</a></li>
														<li><a href="/espn.asp?tournamentId=5">Lerma</a></li>
														<li><a href="/espn.asp?tournamentId=6">Riocerezo</a></li>
														<li><a href="/espn.asp?tournamentId=7">C.D.M. La Dehesa</a></li>
														<li><a href="/espn.asp?tournamentId=8">El Fresnillo</a></li>
														<li><a href="/espn.asp?tournamentId=9">Club de Campo - Amarillo</a></li>
														<li><a href="/espn.asp?tournamentId=10">Club de Campo - Negro</a></li>
														<li><a href="/espn.asp?tournamentId=11">Barberan</a></li>
														<li><a href="/espn.asp?tournamentId=12">La Llorea</a></li>
														<li><a href="/espn.asp?tournamentId=13">Olivar de la Hinojosa Corto</a></li>
														<li><a href="/espn.asp?tournamentId=14">Campo de Layos</a></li>
														<li><a href="/espn.asp?tournamentId=15">Navaluenga</a></li>
														<li><a href="/espn.asp?tournamentId=16">Sancti Petri - A - Mar y Pinos</a></li>
														<li><a href="/espn.asp?tournamentId=17">León</a></li>
														<li><a href="/espn.asp?tournamentId=18">Cabopino</a></li>
														<li><a href="/espn.asp?tournamentId=19">Flamingos</a></li>
														<li><a href="/espn.asp?tournamentId=20">Centro Nacional de la RFEG</a></li>
														<li><a href="/espn.asp?tournamentId=21">Real Sociedad Hípica - Sur</a></li>
												<ul>
										</div>

										<div class="tournament toggle mobile-dropdown caption"><span class="mobile-arrow"></span><select id="mobile-tournament-toggle"><option>Tournaments</option><optgroup label="--- March 2017 ---"><option data-link="/golf/leaderboard?tournamentId=2719">Valspar Championship</option><option data-link="/golf/leaderboard?tournamentId=3735">World Golf Championships-Mexico Championship</option></optgroup><optgroup label="--- February 2017 ---"><option data-link="/golf/leaderboard?tournamentId=2696">The Honda Classic</option><option data-link="/golf/leaderboard?tournamentId=2695">Genesis Open</option><option data-link="/golf/leaderboard?tournamentId=2692">AT&T Pebble Beach Pro-Am</option><option data-link="/golf/leaderboard?tournamentId=2691">Waste Management Phoenix Open</option></optgroup><optgroup label="--- January 2017 ---"><option data-link="/golf/leaderboard?tournamentId=2693">Farmers Insurance Open</option><option data-link="/golf/leaderboard?tournamentId=2690">CareerBuilder Challenge</option><option data-link="/golf/leaderboard?tournamentId=2694">Sony Open in Hawaii</option><option data-link="/golf/leaderboard?tournamentId=2689">SBS Tournament of Champions</option></optgroup><optgroup label="--- December 2016 ---"><option data-link="/golf/leaderboard?tournamentId=3069">Hero World Challenge</option></optgroup><optgroup label="--- November 2016 ---"><option data-link="/golf/leaderboard?tournamentId=2729">The RSM Classic</option><option data-link="/golf/leaderboard?tournamentId=2724">OHL Classic</option><option data-link="/golf/leaderboard?tournamentId=2716">Shriners Hospitals for Children Open</option></optgroup><optgroup label="--- October 2016 ---"><option data-link="/golf/leaderboard?tournamentId=2711">Sanderson Farms Championship</option><option data-link="/golf/leaderboard?tournamentId=2722">World Golf Championships-HSBC Champions</option><option data-link="/golf/leaderboard?tournamentId=2728">CIMB Classic</option><option data-link="/golf/leaderboard?tournamentId=2725">Safeway Open</option></optgroup></select></div>
									</div>
									<h1>World Golf Championships-Marqués Tuñón Cup</h1>
									<ul class="matchup-detail">
										<li>
											<div class="date">Últimos Partidos</div>
											<div class="network">NBC/TGC</div>
										</li>
									</ul>
<!--
									<div id="collapse-two" class="accordion-content collapse">
										<div class="content">
											<ul class="matchup-detail">
												<li>
													<div class="location">Club de Golf Chapultepec</div>
												</li>
												<li>
													<div class="course-detail">
														<div class="type"><span class="light-txt">Par</span> 71</div>
														<div class="type"><span class="light-txt">Yards</span> 7,330</div>
													</div>
												</li>
												<li>
													<div class="course-detail">
														<div class="type"><span class="light-txt">Purse</span> $9,750,000</div>
														<div class="type"><span class="light-txt">Defending Champion</span> Dustin Johnson</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
-->
									<div class="show-hide">
										<a role="button" data-toggle="collapse" href="#collapse-two" aria-expanded="false" aria-controls="collapse-one" class="collapsed"><span class="show-hide-txt"></span> Tournament Details</a>
									</div>
								</div>
							</div>
						</header>
						<section class="matchup-content">
							<div class="filter form-group button-group">
								<div class="wrap"><a class="button-filter-alt lg active webview-internal" href="#" data-tab="leaderboard"><span>Partidos</span></a>
									<a class="button-filter-alt lg webview-internal" href="#" data-tab="course-player-stats"><span>Course Player Stats</span></a>
									<a class="button-filter-alt lg webview-internal" href="#" data-tab="player-stats"><span>Player Stats</span></a>
									<a class="button-filter-alt lg webview-internal" href="#" data-tab="course-stats"><span>Course Stats</span></a>
								</div>
							</div>
							<h2><span class="tournament-status">Final</span></h2>
							<div class="multi-course-container"></div>
							<div id="leaderboard-view" class="leaderboard-container">
								<div class="playoff-wrapper">
									<div class="playoff-container">
										<div class="region-2"></div>
										<div class="region-1"></div>
									</div>
								</div>
								<table class="leaderboard-table round-4" cellspacing="0" cellpadding="0" data-text-contract="Contract table" data-text-expand="Expand table"
								 data-fix-cols="0" data-behavior="responsive_table" ng-app="partidos" ng-controller="partCtrl">
									<colgroup>
										<col class="position in post">
										<col class="movement in">
										<col class="lastName pre in post">
										<col class="relativeScore sorted in post">
										<col class="currentRoundScore in">
										<col class="thru in">
										<col class="round1 in post">
										<col class="round2 in post">
										<col class="round3 in post">
										<col class="round4 in post">
										<col class="totalScore in post">
										<col class="officialAmount post">
										<col class="cupPoints post">
										<col class="teeTime sorted pre">
									</colgroup>
									<thead>
										<tr>
											<th data-sortable="true" data-field="position" data-sort-direction="asc" data-sort-type="integer" class="position sm align-left in post"><span>POS</span></th>
											<th data-sortable="true" data-field="movement" data-sort-direction="asc" data-sort-type="integer" class="movement movement mov-col sm align-left in"><span></span></th>
											<th data-sortable="true" data-field="lastName" data-sort-direction="asc" data-sort-type="string" class="lastName md align-left pre in post"><span>CAMPO</span></th>
											<th data-sortable="true" data-field="relativeScore" data-sort-direction="asc" data-sort-type="integer" class="relativeScore sm asc in post"><span>Día Partido</span></th>
											<th data-sortable="true" data-field="currentRoundScore" data-sort-direction="asc" data-sort-type="integer" class="currentRoundScore today in"><span>TODAY</span></th>
											<th data-sortable="true" data-field="thru" data-sort-direction="desc" data-sort-type="custom:thru" class="thru  in"><span>THRU</span></th>
											<th data-sortable="true" data-field="round1" data-sort-direction="asc" data-sort-type="integer" class="round1  in post"><span>1</span></th>
											<th data-sortable="true" data-field="round2" data-sort-direction="asc" data-sort-type="integer" class="round2  in post"><span>2</span></th>
											<th data-sortable="true" data-field="round3" data-sort-direction="asc" data-sort-type="integer" class="round3  in post"><span>3</span></th>
											<th data-sortable="true" data-field="round4" data-sort-direction="asc" data-sort-type="integer" class="round4  in post"><span>4</span></th>
											<th data-sortable="true" data-field="totalScore" data-sort-direction="asc" data-sort-type="integer" class="totalScore  in post"><span>Hoyos</span></th>
											<th data-sortable="true" data-field="officialAmount" data-sort-direction="desc" data-sort-type="double" class="officialAmount  post"><span>EARNINGS</span></th>
											<th data-sortable="true" data-field="cupPoints" data-sort-direction="desc" data-sort-type="integer" class="cupPoints cup-points align-center post"><span>FEDEX PTS</span></th>
											<th data-sortable="true" data-field="teeTime" data-sort-direction="asc" data-sort-type="integer" class="teeTime tee-time asc pre"><span>TEE TIME</span></th>
										</tr>
									</thead>



									

									<tbody id="leaderboard-model-{{x.id}}" ng-repeat="x in myData">
										<tr class="player-overview player-overview-{{x.id}}">
											<td class="position sm align-left in post">{{x.id}}</td>
											<td class="movement movement mov-col sm align-left in icon-font-before icon-arrow-up-solid-before positive">1</td>
											<td class="playerName md align-left pre in post">
												<span class="team-logo"><img  src="./imagenes/esp.png" alt="{{x.pais}}" title="{{x.pais}}" /></span>
												<a role="button" class="full-name">{{x.nombre_campo}} </a><a role="button" class="short-name">{{x.nombre_campo}}</a></td>
											<td class="relativeScore sm asc in post">{{ x.dia + '-' + x.mes + '-' + x.ano }}</td>
											<td class="currentRoundScore today in">-3</td>
											<td class="thru  in">F</td>
											<td class="round1  in post">{{x.jug1}}</td>
											<td class="round2  in post">{{x.jug2}}</td>
											<td class="round3  in post">{{x.jug3}}</td>
											<td class="round4  in post">{{x.jug4}}</td>
											<td class="totalScore  in post">{{x.num_hoyos}}</td>
											<td class="officialAmount  post">$1,660,000.00</td>
											<td class="cupPoints cup-points align-center post">550</td>
											<td class="teeTime tee-time asc pre">-</td>
										</tr>
										<tr class="player-details results collapse odd" style="height: 320px;">
											<td class="content">
												<div class="loading"></div>
											</td>
										</tr>
									</tbody>



									<script>
										var miApp = angular.module('partidos', []);
										miApp.controller('partCtrl',function ($scope, $http) {
											$http.get("golfapi/api/partidos_json/<%=request.querystring("tournamentId")%>").then(function (response) {	
												$scope.nombre = "Juanma";
												$scope.myData = response.data;
											}, function (response) {
												$scope.nombre = "FALLO";
											});
										});
									</script>



									
								</table>
								<div id="chivato" name="chivato"></div>
							</div>
							
							<div id="stats-container" class="loading leaderboard-container"></div>
						</section>
						<!--							<div class="sponsored-headlines">
					<h2>Sponsored Headlines</h2>
					<div class="OUTBRAIN" data-widget-id="AR_49" data-src="http://www.espn.com/golf/leaderboard"
					 data-ob-template="espn"></div>
				</div>

-->

					</section>
					<section class="col-three">
						<section id="now-feed" data-module="now" class="col-c" data-behavior="index_now_feed" data-now-max-stories="25" data-now-offset="25">

						</section>
					</section>
				</div>



			</section>


		</section>
	</div>

	<script>
								var espn_ui = window.espn_ui || {};
								espn_ui.staticRef = "http://a.espncdn.com/redesign/0.338.4";
								espn_ui.imgRef = "http://a.espncdn.com/redesign/assets/img/";
								espn_ui.insertRef = "http://a.espncdn.com";
								espn_ui.deviceType = "desktop";
								espn_ui.pageShell = false;
								espn.api = {};
								/*	espn.api.disasterURLs = {
										"cdn.espn.com": "cdn.espn.com",
										"sports.core.api.espn.com": "sports.core.api.espn.com",
										"site.api.espn.com": "site.api.espn.com",
										"site.api.prod.espninfra.starwave.com": "site.api.prod.espninfra.starwave.com",
										"fan.core.api.espn.com": "fan.core.api.espn.com",
										"site.core.api.prod.espninfra.starwave.com": "site.core.api.prod.espninfra.starwave.com",
										"api.espn.com": "api.espn.com",
										"api-app.espn.com": "api-app.espn.com",
										"api.prod.espninfra.starwave.com": "api.prod.espninfra.starwave.com"
									};
									*/
								espn_ui.webview = false;
								espn_ui.useNativeBridge = false;
								espn_ui.onefeed = false;
								espn_ui.onefeedControl = false;
								espn_ui.personalizedOneFeedTest1 = false;
								espn_ui.personalizedOneFeedTest2 = false;
								espn_ui.personalizedOneFeedControl = false;
	</script>

	<script src="./js/espn-critical.js"></script>

	<script type="text/javascript">
		var espn = espn || {};
		/* building skeleton for namespace */
		espn.scoreboard = {
			topics: {
				scoreboard: "",
				scoreboxes: []
			},
			models: {},
			views: {},
			collections: {},
			timezoneOffset: 0,
			favorites: {},
			editData: {},
			activeLeague: "topEvents",
			hiddenLeague: "pga",
			settings: {
				activeLeagues: [{ "sportId": 0, "displayName": "Top Events", "league": "topEvents" }, { "sportId": 700, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "http://www.espnfc.com/scores?cc=5901" }, "top25Only": true, "sport": "soccer", "displayName": "English Premier League", "league": "eng.1" }, { "sportId": 41, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "http://www.espn.com/mens-college-basketball/scoreboard" }, "top25Only": true, "sport": "basketball", "displayName": "NCAAM", "league": "mens-college-basketball" }, { "sportId": 54, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "/womens-college-basketball/scoreboard" }, "top25Only": true, "sport": "basketball", "displayName": "NCAAW", "league": "womens-college-basketball" }, { "sportId": 46, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "http://www.espn.com/nba/scoreboard" }, "top25Only": true, "sport": "basketball", "displayName": "NBA", "league": "nba" }, { "sportId": 90, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "http://www.espn.com/nhl/scoreboard" }, "top25Only": true, "sport": "hockey", "displayName": "NHL", "league": "nhl" }, { "sportId": 740, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "http://www.espnfc.com/scores?cc=5901" }, "top25Only": true, "sport": "soccer", "displayName": "La Liga", "league": "esp.1" }, { "sportId": 10, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "http://www.espn.com/mlb/scoreboard" }, "top25Only": true, "sport": "baseball", "displayName": "MLB", "league": "mlb" }, { "sportId": 3454, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "http://www.espn.com/extra/baseball/wbbc/scoreboard" }, "top25Only": true, "sport": "baseball", "displayName": "World Baseball Classic", "league": "world-baseball-classic" }, { "sportId": 770, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "http://www.espnfc.com/scores?cc=5901" }, "top25Only": true, "sport": "soccer", "displayName": "MLS", "league": "usa.1" }, { "sportId": 760, "link": { "title": "Full Scoreboard &#187;", "onclick": "", "href": "http://www.espnfc.com/scores?cc=5901" }, "top25Only": true, "sport": "soccer", "displayName": "Mexican Liga MX", "league": "mex.1" }, { "sportId": 1106, "link": { "title": "Full Leaderboard &#187;", "onclick": "", "href": "http://www.espn.com/golf/leaderboard" }, "top25Only": true, "sport": "golf", "displayName": "Golf (M)", "league": "pga" }, { "sportId": 851, "link": { "title": "Full Results &#187;", "onclick": "", "href": "http://www.espn.com/tennis/dailyResults" }, "top25Only": true, "sport": "tennis", "displayName": "Tennis (M)", "league": "atp" }, { "sportId": 900, "link": { "title": "Full Results &#187;", "onclick": "", "href": "http://www.espn.com/tennis/dailyResults" }, "top25Only": true, "sport": "tennis", "displayName": "Tennis (W)", "league": "wta" }, { "sportId": 2021, "link": { "title": "Full results &#187;", "onclick": "", "href": "http://www.espn.com/racing/schedule" }, "top25Only": true, "sport": "racing", "displayName": "NASCAR", "league": "premier" }],
				useStatic: false,
				initialTopic: "event-topevents",
				version: 2,
				topEventsId: 4379198
			},
			data: (function () {
				var json = null;
				$.ajax({
					'async': true,
					'global': false,
					'url': "/json/basurilla.json",
					'dataType': "json",
					'success': function (data) {
						json = data;
					}
				});
				return json;
			})(),
			queue: []
		};
	</script>

	<script type="text/javascript">
		jQuery.subscribe("espn.defer.end", function () { espn.scoreboard.init(); })
	</script>

	<script>
			(function () {
				var deferLoaded = false;
				function loadDefer() {
					if (!deferLoaded) {
						var deferScripts = ['./js/espn-defer.js', './js/espn-defer-low.js'];
						for (var s = 0; s < deferScripts.length; s++) {
							var script = document.createElement('script');
							script.src = deferScripts[s];
							document.getElementsByTagName('head')[0].appendChild(script);
						}
						deferLoaded = true;
					}
				}
				if (window.espn.loadType === "loadEnd" && (espn_ui.device.isMobile === true || espn_ui.device.isTablet === true)) {
					$(window).load(function () {
						setTimeout(loadDefer, 0)
					});
					setTimeout(loadDefer, 10000)
				} else {
					loadDefer();
				}

			})();
	</script>

	<script>
		espn_ui.Helpers.translate.init();
	</script>
	
	<script type="text/javascript">
		(function ($) {

			var espn = window.espn || {};
			espn.leaderboard = espn.leaderboard || {};
			espn.leaderboard.data = (function () {
				var json = null;
				$.ajax({
					'async': false,
					'global': false,
					'url': "golfapi/api/datos_partidos/<%=request.querystring("tournamentId")%>",
					'dataType': "json",
					'success': function (data) {
							json = data;
					}
				});
				return json;
			})();
			//document.getElementById("chivato").innerHTML = String(espn.leaderboard.data);
			espn.leaderboard.tour = "pga";
			espn.leaderboard.tournamentId = 3735;
			espn.leaderboard.tournamentName = "World Golf Championships-Marqués Tuñón Cup";
			espn.leaderboard.season = 2017;
			espn.leaderboard.status = "post";
			espn.leaderboard.date = "2017-03-02T05:00Z";
			//Golpes en la competición, por jugador. En mi caso, los resultados de los cuatro jugadores, si los hay, por partido.
			//espn.leaderboard.competitorDetailsUrl = "http://site.api.espn.com/apis/site/v2/sports/golf/pga/leaderboard/3735/playersummary?player={{playerId}}&season={{season}}&lang=en&region=us";
			//espn.leaderboard.competitorDetailsUrl = "http://golf.cruzdegorbea.com/apis/playersummary.asp?prtd={{playerId}}&season={{season}}&lang=en&region=us";
			espn.leaderboard.competitorDetailsUrl = "http://golf.cruzdegorbea.com/golfapi/api/playersummary/{{playerId}}/?season={{season}}&lang=en&region=us";

			//Player Stats
			espn.leaderboard.playerStatsUrl = "http://site.api.espn.com/apis/site/v2/sports/golf/pga/leaderboard/players?event=3735&lang=en&region=us";

			//Course Stats
			espn.leaderboard.courseStatsUrl = "http://site.api.espn.com/apis/site/v2/sports/golf/pga/leaderboard/course?event=3735&lang=en&region=us";

			
			espn.leaderboard.tourScheduleUrl = "http://site.api.espn.com/apis/site/v2/sports/golf/pga/tourschedule?season={{season}}&lang=en&region=us";




			function init() {
				espn.leaderboard.init();
			}

			function loadScripts() {

				var _loadScripts = !espn.leaderboard.scriptsLoaded,
					eventName = "espn.leaderboard.loaded";

				if (_loadScripts === true) {
					$.subscribe(eventName, init);

					var script = document.createElement("script");
					script.type = "text/javascript";
					script.src = "./js/leaderboard.min.js";
					document.getElementsByTagName("head")[0].appendChild(script);
				} else {
					init();
				}
			}

			if (window.espn_ui.deferReady === true) {
				loadScripts();
			} else {
				$.subscribe("espn.defer.end", loadScripts);
			}
		})(jQuery);
	</script>

	<script type="text/javascript">
		/*var data = { "cto": true, "chartbeat": { "authors": "leaderboard", "title": "World Golf Championships-Mexico Championship Golf Leaderboard and Results- ESPN", "sections": "golf", "loadVidJS": true, "loadPubJS": false, "path": "/golf/leaderboard", "domain": "www.espn.com", "zone": "www.espn.com.us.golf" }, "device": "desktop", "omniture": { "site": "espn", "pageName": "golf:leaderboard", "premium": "premium-no", "sport": "golf", "hier1": "golf:leaderboard", "contentType": "leaderboard", "convrSport": "golf", "league": "pga", "lang": "en_us", "pageURL": "www.espn.com/golf/leaderboard", "section": "golf", "countryRegion": "en-us", "sections": "golf:leaderboard", "prop58": "isIndex=false", "account": "wdgespcom" }, "isFeaturePhone": false, "ABTest": { "raw": "{\"optimizely\":{\"enabled\":true,\"placements\":[{\"site\":\"espn.co.uk\",\"flag\":true,\"regexp\":\"(/)\"},{\"site\":\"www.espn.com\",\"flag\":true,\"regexp\":\"(/.*)\"},{\"site\":\"www.espnqa.com\",\"flag\":true,\"regexp\":\"(/.*)\"}],\"oldscript\":\"http://a.espncdn.com/sports/optimizely.js\",\"script\":\"http://cdn.optimizely.com/js/310987714.js\"},\"target\":{\"enabled\":false,\"placements\":[{\"site\":\"espn.co.uk\",\"flag\":false,\"regexp\":\"(/)\"},{\"site\":\"www.espn.com\",\"flag\":false,\"regexp\":\"(/.*)\"}],\"script\":\"http://a.espncdn.com/prod/scripts/analytics/target.20.r2.js\"},\"fastcast\":{\"enabled\":false,\"placements\":{\"/ncf/index\":\"true\",\"/nba/index\":\"true\",\"^/.+$\":\"false\"},\"script\":\"http://a.espncdn.com/combiner/c?js=analytics/espn.fastcastTracking.4.js\"}}", "optimizelyURLs": [{ "site": "espn.co.uk", "flag": true, "regexp": "(/)" }, { "site": "www.espn.com", "flag": true, "regexp": "(/.*)" }, { "site": "www.espnqa.com", "flag": true, "regexp": "(/.*)" }], "tScript": "http://a.espncdn.com/prod/scripts/analytics/target.20.r2.js", "host": "espn.com", "environment": "prod", "optimizely": true, "target": false, "domain": "www.espn.com", "isOptimizied": true, "targetURLs": [{ "site": "espn.co.uk", "flag": false, "regexp": "(/)" }, { "site": "www.espn.com", "flag": false, "regexp": "(/.*)" }], "oScript": "http://cdn.optimizely.com/js/310987714.js" } };
		var espn = espn || {};
		espn.track = espn.track || {};
		espn.track.data = data;
	*/
	</script>

	<!--BEGIN QUALTRICS SITE INTERCEPT-->
	<!--	
	<div id='SI_9EvK9r52GPmgW9v'>
-->
	<!--DO NOT REMOVE-CONTENTS PLACED HERE-->
	<!--	</div>
-->
	<!--END SITE INTERCEPT-->

	<!-- SiteCatalyst code version: AppMeasurement 1.0 Copyright 1996-2013 Adobe, Inc. -->

	<!--
	<script type="text/javascript">
		if (typeof s_omni === 'undefined') {
			jQuery(function ($) {
				$.getScriptCache("http://a.espncdn.com/redesign/0.338.4/js/espn-analytics.js", function () {
					var deferEvent = "user.parsed", trackInit = false;
					function initTrack() { if (!trackInit && espn.track && (typeof espn.track.init) === 'function') { espn.track.init(data); trackInit = true; } } $.subscribe(deferEvent, initTrack); setTimeout(initTrack, 5000); if (window.espn_ui.userParsed) { $.publish('user.parsed'); }
				});
			});
		}
	</script>
-->
	<!-- End SiteCatalyst code version: AppMeasurement 1.0 -->
	<script>
		var sbDebug = window.espn && window.espn.flow && window.espn.flow.debug === true;
		if (!sbDebug) {
			if (espn_ui.device.isMobile === true || espn_ui.device.isTablet === true) {
				jQuery.subscribe("espn.defer.end", function () {
					if (DTSS_WebSockets) {
						DTSS_WebSockets.PRECONNECT = true;
						DTSS_WebSockets.FORCE_PRECONNECT = true;
					}
				})
			} else {
				if (DTSS_WebSockets) {
					DTSS_WebSockets.PRECONNECT = true;
					DTSS_WebSockets.FORCE_PRECONNECT = true;
				}
			}
		}
	</script>
</body>

</html>