<HTML>
<object id=ChartSpace1 classid=CLSID:0002E556-0000-0000-C000-000000000046 style="width:100%;height:350"></object>
<object id=ADOConnection1 classid=CLSID:00000514-0000-0010-8000-00AA006D2EA4></object>

<script language=vbscript>
Sub Window_OnLoad()
    Dim rs, categories, values, c
   
    ' This example connects to the Northwind sample database
    ' and charts the Query "Category Sales for 1995". ADO is used to open a
    ' connection to the database and return the entire recordset. The query
    ' contains two columns: Category Name and Category Sales.
    ' The records are then extracted into strings.

    categories = ""
    values = ""

    ' Open the connection and execute the query.
    ADOConnection1.Open "DRIVER={Microsoft Access Driver (*.mdb)};DBQ=C:\TEMP\golf\GolfWeb\golf.mdb"
    Set rs = ADOConnection1.Execute("SELECT * FROM golpes where id_partido=42 and (id_jugador=1 or id_jugador=2)")
    
    'sql="select id, nombre from jugadores"
	'Set Rs = Server.CreateObject("ADODB.Recordset")
	'Rs.Open Sql, Application("conn"),2,3

    ' Start at the first record and move through the entire recordset.
    ' Field 0 is the category name, Field 1 is the sales value.
    ' Create a tab-delimited string for the names and one for the values.
    rs.MoveFirst
    Do While Not rs.EOF
		if rs.fields("id_jugador").value = 1 then 
			categories = categories & rs.Fields("id_hoyo").Value & Chr(9)
			values = values & rs.Fields("golpes").Value & Chr(9)
		else
			categories1 = categories1 & rs.Fields("id_hoyo").Value & Chr(9)
			values1 = values1 & rs.Fields("golpes").Value & Chr(9)		
		end if
        rs.MoveNext
    Loop
    rs.Close
    ADOConnection1.Close
   
    ' Remove the leftover tab character at the end of the strings.
    'categories = Left(categories, Len(categories) - 1)
    'values = Left(values, Len(values) - 1)
   
    ' Create a chart with one series (called "Sales").
    ChartSpace1.Clear
    ChartSpace1.Charts.Add
    ChartSpace1.Charts(0).SeriesCollection.Add
    ChartSpace1.Charts(0).SeriesCollection(0).Caption = "Susana"
    ChartSpace1.Charts(0).SeriesCollection.Add
    ChartSpace1.Charts(0).SeriesCollection(1).Caption = "Juanma"
    

    'Set the series categories and values using the strings created from the recordset.
    Set c = ChartSpace1.Constants
    ChartSpace1.Charts(0).SeriesCollection(0).SetData c.chDimCategories, c.chDataLiteral, categories
    ChartSpace1.Charts(0).SeriesCollection(0).SetData c.chDimValues, c.chDataLiteral, values
    
    ChartSpace1.Charts(0).SeriesCollection(1).SetData c.chDimCategories, c.chDataLiteral, categories
    ChartSpace1.Charts(0).SeriesCollection(1).SetData c.chDimValues, c.chDataLiteral, values1
   
    ' Set the chart type and format the axis as U.S. dollars.
    'ChartSpace1.Charts(0).Type = c.chChartTypeBarClustered
    ChartSpace1.Charts(0).Type = 7
    ChartSpace1.Charts(0).Axes(c.chAxisPositionBottom).NumberFormat = "##"
End Sub
</script>
<SELECT id=lstChartType SIZE=1>
 <OPTION selected value=0>Clustered Column</OPTION>
 <OPTION value=1>Stacked Column</OPTION>
 <OPTION value=2>100% Stacked Column</OPTION>
 <OPTION value=3>Clustered Bar</OPTION>
 <OPTION value=4>Stacked Bar</OPTION>
 <OPTION value=5>100% Stacked Bar</OPTION>
 <OPTION value=6>Line</OPTION>
 <OPTION value=8>Stacked Line</OPTION>
 <OPTION value=10>100% Stacked Line</OPTION>
 <OPTION value=7>Line with Markers</OPTION>
 <OPTION value=9>Stacked Line with Markers</OPTION>
 <OPTION value=11>100% Stacked Line with Markers</OPTION>
 <OPTION value=12>Smooth Line</OPTION>
 <OPTION value=14>Stacked Smooth Line</OPTION>
 <OPTION value=16>100% Stacked Smooth Line</OPTION>
 <OPTION value=13>Smooth Line with Markers</OPTION>
 <OPTION value=15>Stacked Smooth Line with Markers</OPTION>
 <OPTION value=17>100% Stacked Smooth Line with Markers</OPTION>
 <OPTION value=18>Pie</OPTION>
 <OPTION value=19>Pie Exploded</OPTION>
 <OPTION value=20>Stacked Pie</OPTION>
 <OPTION value=21>Scatter</OPTION>
 <OPTION value=25>Scatter with Lines</OPTION>
 <OPTION value=24>Scatter with Markers and Lines</OPTION>
 <OPTION value=26>Filled Scatter</OPTION>
 <OPTION value=23>Scatter with Smooth Lines</OPTION>
 <OPTION value=22>Scatter with Markers and Smooth Lines</OPTION>
 <OPTION value=27>Bubble</OPTION>
 <OPTION value=28>Bubble with Lines</OPTION>
 <OPTION value=29>Area</OPTION>
 <OPTION value=30>Stacked Area</OPTION>
 <OPTION value=31>100% Stacked Area</OPTION>
 <OPTION value=32>Doughnut</OPTION>
 <OPTION value=33>Exploded Doughnut</OPTION>
 <OPTION value=34>Radar with Lines</OPTION>
 <OPTION value=35>Radar with Lines and Markers</OPTION>
 <OPTION value=36>Filled Radar</OPTION>
 <OPTION value=37>Radar with Smooth Lines</OPTION>
 <OPTION value=38>Radar with Smooth Lines and Markers</OPTION>
 <OPTION value=39>High-Low-Close</OPTION>
 <OPTION value=40>Open-High-Low-Close</OPTION>
 <OPTION value=41>Polar</OPTION>
 <OPTION value=42>Polar with Lines</OPTION>
 <OPTION value=43>Polar with Lines and Markers</OPTION>
 <OPTION value=44>Polar with Smooth Lines</OPTION>
 <OPTION value=45>Polar with Smooth Lines and Markers</OPTION>
</SELECT>
<SCRIPT language=vbscript>


Sub lstChartType_onChange()
    nChartType = CLng(lstChartType.value)
  '  msgbox (ChartSpace_WebChart.Charts.Item(0).Type)
ChartSpace1.Charts.Item(0).Type = nChartType
end sub
</script>
</HTML>
