<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>Estadísticas MAJADAHONDA PGA TOUR</TITLE>
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<META content="" name=description>
<META content="" name=keywords>
<LINK href="estadisticas_archivos/default.css" type=TEXT/CSS rel=stylesheet>
<META content="MSHTML 6.00.2800.1170" name=GENERATOR></HEAD>
<BODY link=#000175 bgColor=#29598c leftMargin=0 topMargin=0 marginheight="0" 
marginwidth="0">
<TABLE cellSpacing=0 cellPadding=0 width=770 bgColor=#213829 border=0>
  <TBODY>
  <TR>
    <TD width=283><MAP name=link><AREA shape=RECT alt="" coords=5,3,56,67 
        href="http://www.pgatour.com/"><AREA shape=RECT alt="" 
        coords=62,39,164,70 href="http://www.golfweb.com/"></MAP><IMG height=70 
      alt="PGA TOUR" src="estadisticas_archivos/lower_hdr.jpg" width=283 
      useMap=#link border=0></TD>
    <TD align=middle width=487>
<%
function fnSort(aSort, intAsc, cual, cuantoscampos, cuantosjugadores)
    Dim intTempStore(13)
    Dim i, j 
    'For i = 1 To UBound(aSort) - 1 'cambiado el inicio a 1 desde 0
    For i = 1 To cuantosjugadores
		'For j = i To UBound(aSort)
		For j = i To cuantosjugadores
			if aSort(i,cual)<>"" and aSort(j,cual)<>"" then
				'Sort Ascending
				if intAsc = 0 Then
						if cdbl(aSort(i,cual)) > cdbl(aSort(j,cual)) Then
							for x=1 to cuantoscampos
								intTempStore(x) = aSort(i,x)    
								aSort(i,x) = aSort(j,x)
								aSort(j,x) = intTempStore(x)
							next
					end if 'i > j
				'Sort Descending, que es lo que voy a usar de momento
				Else
						if cdbl(aSort(i,cual)) < cdbl(aSort(j,cual)) Then
							for x=1 to cuantoscampos
								intTempStore(x) = aSort(i,x)    
								aSort(i,x) = aSort(j,x)
								aSort(j,x) = intTempStore(x)
							next
					End if 'i < j
				End if 'intAsc = 1
			end if
		Next 'j
    Next 'i
    fnSort = aSort
 End function 'fnSort
%>
          <SCRIPT language=JavaScript id=_hbc>
if (!_rew) {var _rew="1";}
if (!_bon) {var _bon="";}
if (!_uid) {var _uid="";}
if (!_seg) {var _seg="";}
var _pn="http:/" + location.pathname;
var _cmp="";
var _gp="";
var _gpn="";
var _cmpn="";
var _cp="null";
var _pndef="title";
var _ctdef="full";
var _dlf="n";
var _elf="n";
var _epg="n";
var _hcv=65;
var _mn="we70";
var _gn="";
var _mlc="";
</SCRIPT>
<SCRIPT language=javascript1.1 
   src="estadisticas_archivos/hbe-v65-no10-aff.js" defer></SCRIPT>
 </TD></TR></TBODY></TABLE><NOSCRIPT><IMG height=1 
src="estadisticas_archivos/HG.gif" width=1 border=0> </NOSCRIPT>
<SCRIPT language=JavaScript1.2>
<!--
function ShowLayer(){return};
function OutLayer(){return};
//-->
</SCRIPT>

<TABLE cellSpacing=0 cellPadding=0 width=770 
background=estadisticas_archivos/gn_bg.gif border=0>
  <TBODY>
  <TR>
    <TD align=left height=25>
      <TABLE cellSpacing=0 cellPadding=0 
      background=estadisticas_archivos/stats.htm border=0>
        <TBODY>
        <TR>
          <TD class=gnsbtxt>&nbsp;&nbsp;<A class=gnsbtxt2 id=gnddhomeA 
            onmouseover="ShowLayer('gnddhome', event)" onmouseout=OutLayer(); 
            href="http://www.pgatour.com/index.html" name=gnddhomeA>Home</A></TD>
          <TD class=gnsbtxt>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddplaA 
            onmouseover="ShowLayer('gnddpla', event)" onmouseout=OutLayer(); 
            href="http://www.pgatour.com/players/index.html" 
            name=gnddplaA>Players</A></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddtouA 
            onmouseover="ShowLayer('gnddtou', event)" onmouseout=OutLayer(); 
            href="http://www.pgatour.com/tournaments/index.html" 
            name=gnddtouA>Partidos</A></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddscoA 
            onmouseover="ShowLayer('gnddsco', event)" onmouseout=OutLayer(); 
            href="./resultados.htm" 
            name=gnddscoA>Resultados</A></TD>
          <TD class=gnsbtxt noWrap height=25>&nbsp;&nbsp;<IMG height=25 alt="" 
            hspace=0 src="estadisticas_archivos/gn_left.gif" width=6 
            align=middle border=0></TD>
          <TD class=gnsbtxt2 align=middle 
          background=estadisticas_archivos/gn_middle.gif><A id=gnddstaA 
            onmouseover="ShowLayer('gnddsta', event)" onmouseout=OutLayer(); 
            href="estadisticas.htm" 
            name=gnddstaA>Estadísticas</A></TD>
          <TD class=gnsbtxt noWrap height=25><IMG height=25 alt="" hspace=0 
            src="estadisticas_archivos/gn_right.gif" width=6 align=middle 
            border=0>&nbsp;&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddshpA 
            onmouseover="ShowLayer('gnddshp', event)" onmouseout=OutLayer(); 
            href="http://www.sportsline.com/redir?url=981" 
          name=gnddshpA>Shop</A></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddmltA 
            onmouseover="ShowLayer('gnddmlt', event)" onmouseout=OutLayer(); 
            href="http://www.pgatour.com/mm/index.html" 
            name=gnddmltA>Multimedia</A></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<SCRIPT language=JavaScript1.2 src="estadisticas_archivos/pd.js" 
type=text/javascript></SCRIPT>

<TABLE class=bk cellSpacing=0 cellPadding=0 width=770 border=0>
  <%if Request.QueryString("glp")<>"" then
		golpes_recorrido = cint(Request.QueryString("glp"))
	else
		golpes_recorrido = 27
	end if%>
  <TR>
    <TD vAlign=top width=600><FONT class=a4><B> MAJADAHONDA TOUR 2003&nbsp;    
    </B><BR> Media para recorrido de <%=golpes_recorrido%> golpes</FONT> 
      <TABLE cellSpacing=0 cellPadding=0 width=590 border=0>
        
        <TR>
          <TD align=left><SPAN class=v1><B>
</B></SPAN></TD>
          <TD align=right><FONT color=#0000ff><!--
<img src="/images/234x60_sponsor.jpg" width="234" height="60" border="0">
--></FONT></TD></TR></TABLE>
      <DIV class=spacer10><FONT color=#0000ff></FONT></DIV><!-- Do NOT edit by hand, automatically generated in sunday night process -->
      <TABLE cellSpacing=1 cellPadding=2 width="100%" border=0>
        
        <TR>
          <TD align=left><FONT class=a1>Y-T-D statistics through:&nbsp;the 
            Memorial Tournament&nbsp;Jun 01, 2003</FONT></TD></TR></TABLE>
      <DIV class=Table1>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        
        <TR vAlign=top>
          <TD width="100%">
            <TABLE cellSpacing=1 cellPadding=2 width="100%" border=0>
              
              <TR class=tb1>
                <TD colSpan=10><FONT class=v1><B>
                  <TABLE>
                    
                    <TR>
                      <TD><B>Archive:</B></TD>
                      <TD><B>03</B> ·<A href="/stats/2002/r_127.html"><FONT 
                        color=#0000ff>02</FONT></A> ·<A 
                        href="/stats/2001/r_127.html"><FONT 
                        color=#0000ff>01</FONT></A> ·<A 
                        href="/stats/2000/r_127.html"><FONT 
                        color=#0000ff>00</FONT></A>  </TD></TR></TABLE><!--end--></B></FONT></TD></TR>
              <TR class=tb0>
                <%
                sql_campo = "select * from campos"
				Set RsCampo= Server.CreateObject("ADODB.Recordset")
				Set RsJugadores= Server.CreateObject("ADODB.Recordset")
				sql="select id, nombre from jugadores"
				RsJugadores.Open Sql, Application("conn"),2,3
				RsCampo.Open sql_campo, Application("conn"),2,3
				RsCampo.MoveFirst 
                %>
                <TD class=tb0font>Posición</TD>
                <TD class=tb0font>Jugador</TD>
               <!-- <TD class=tb0font align=middle>Ida</TD>                
                <TD class=tb0font align=middle>Vuelta</TD>-->
                <TD class=tb0font align=middle>Med.P</TD>                
                <TD class=tb0font align=middle>Mej.P</TD>
                <TD class=tb0font align=middle>PeorP</TD>
                <%if golpes_recorrido > 45 then%>
                <TD class=tb0font align=middle>Med.V</TD>
                <TD class=tb0font align=middle>Mej.V</TD>
                <TD class=tb0font align=middle>PeorV</TD>
                <%end if%>
                <TD class=tb0font align=middle>Partidos Jugados</TD>
                <TD class=tb0font align=middle>Golpes Totales</TD>                
                <%
				Dim Aux(10,13)
				'1.- Nombre
				'2.- Media Recorrido completo
				'3.- partidos jugados (completos o no)
				'4.- Golpes en total
				'5.- Peor Partido completo
				'6.- Mejor Partido completo
				'7.- Golpes en recorridos completos
				'8.- Recorridos completos
				'9.- Media Vuelta
				'10.- Peor vuelta
				'11.- Mejor vuelta
				'12.- Golpes en vueltas
				'13.- Vueltas hechas
				for x=1 to session("cuantosjugadores")
					for y=1 to 13
						Aux(x,y)=0
						if y=6 then Aux(x,y)=1000
						if y=11 then Aux(x,y)=1000
					next
				next
                contador=0
                Response.Write "<font color=#FF0000>Campos : </font> <BR>"
                do while not Rscampo.EOF 'Para cada uno de los campos
					if RsCampo.Fields("par_campo").Value = golpes_recorrido then
						Response.Write RsCampo.Fields("nombre").Value & " - "
						set Rsgolpes=Server.CreateObject("ADODB.Recordset")
						RsJugadores.MoveFirst 
						do while not RsJugadores.EOF 
							SQL = "SELECT sum(Golpes.golpes) as suma FROM hoyos INNER JOIN Golpes ON hoyos.Id = Golpes.id_hoyo WHERE"& _
								" hoyos.id_campo=" & RsCampo.Fields("id").Value & " AND Golpes.id_jugador"&_
								"=" & RsJugadores.Fields("id").Value
							set RsPartidos = Server.CreateObject("ADODB.Recordset")
							sql_partidos = "SELECT Count(golpes.id_partido) AS Rondas FROM partidos INNER JOIN"& _
											" (hoyos INNER JOIN Golpes ON hoyos.Id = Golpes.id_hoyo) ON "& _
											"partidos.Id = Golpes.id_partido WHERE (((hoyos.numero)=1) AND "& _
											"((Golpes.id_jugador)=" & Rsjugadores.Fields("id").Value  & ") and"& _
											" partidos.id_campo="& RsCampo.Fields("id").Value  &")"
							RsPartidos.Open sql_partidos, Application("conn"),2,3
							Rsgolpes.Open Sql, Application("conn"),2,3
							if RsGolpes.Fields("suma").Value <> "" then
								Aux(Rsjugadores.Fields("id"),1)=Rsjugadores.Fields("nombre").value
								Aux(Rsjugadores.Fields("id"),3)=cint(Aux(Rsjugadores.Fields("id"),3)) + cint(RsPartidos.Fields("Rondas").Value)
								Aux(Rsjugadores.Fields("id"),4)=cint(Aux(Rsjugadores.Fields("id"),4)) + cint(RsGolpes.Fields("suma").Value)
							end if
							RsPartidos.close
							RsGolpes.Close 
							SQL = "SELECT id FROM partidos WHERE id_campo=" & RsCampo.Fields("id").Value
							RsPartidos.Open SQL, Application("conn"),2,3
							do while not RsPartidos.EOF 
								'El mejor y peor resultados y media para partidos COMPLETOS.
								' Vemos para cada jugador y cada campo cuales son
								SQL = "SELECT sum(Golpes.golpes) as suma_partido, count(golpes.id_hoyo) as hoyos_jugados FROM Golpes WHERE id_partido"&_
									"=" & RsPartidos.Fields("id").Value & " AND Golpes.id_jugador=" & RsJugadores.Fields("id").Value & " AND golpes.golpes"&_
									"<>0"
								Rsgolpes.Open Sql, Application("conn"),2,3
								Hoyos_campo = RsCampo.Fields("n_hoyos").Value 
								set Rsgolpes1=Server.CreateObject("ADODB.Recordset")
								if Hoyos_campo > 18 then Hoyos_campo = 18
								if RsGolpes.Fields("suma_partido").Value <> "" and cint(RsGolpes.Fields("hoyos_jugados").Value) = cint(Hoyos_campo) then
									Aux(Rsjugadores.Fields("id"),7) = cint(Aux(Rsjugadores.Fields("id"),7)) +cint(RsGolpes.Fields("suma_partido").Value)
									Aux(Rsjugadores.Fields("id"),8) = Aux(Rsjugadores.Fields("id"),8) + 1
									if cint(RsGolpes.Fields("suma_partido").Value) < Aux(Rsjugadores.Fields("id"),6) then
										Aux(Rsjugadores.Fields("id"),6)=cint(RsGolpes.Fields("suma_partido").Value)
									end if
									if cint(RsGolpes.Fields("suma_partido").Value) > Aux(Rsjugadores.Fields("id"),5) then
										Aux(Rsjugadores.Fields("id"),5)=cint(RsGolpes.Fields("suma_partido").Value)	
									end if 
									'Ahora las vueltas por separado
									SQL = "SELECT Golpes.golpes, golpes.id_hoyo FROM Golpes WHERE id_partido"&_
										"=" & RsPartidos.Fields("id").Value & " AND Golpes.id_jugador=" & RsJugadores.Fields("id").Value & " AND golpes.golpes"&_
										"<>0"
									Rsgolpes1.Open Sql, Application("conn"),2,3
									for y=1 to cdbl(RsGolpes.Fields("hoyos_jugados").Value)/9
										Aux9=0
										for x=1 to 9
											Aux(Rsjugadores.Fields("id"),12) = Aux(Rsjugadores.Fields("id"),12) + RsGolpes1.Fields("golpes").Value
											Aux9=Aux9 + RsGolpes1.Fields("golpes").Value
											RsGolpes1.MoveNext 
										next
										Aux(Rsjugadores.Fields("id"),13) = Aux(Rsjugadores.Fields("id"),13) + 1
										if cint(Aux9) < Aux(Rsjugadores.Fields("id"),11) then
											Aux(Rsjugadores.Fields("id"),11)=cint(Aux9)
										end if
										if cint(Aux9) > Aux(Rsjugadores.Fields("id"),10) then
											Aux(Rsjugadores.Fields("id"),10)=cint(Aux9)
										end if
									next
									RsGolpes1.Close
								end if 
								'Para vueltas sueltas.(9 hoyos jugados en campos de 18)
								if RsGolpes.Fields("suma_partido").Value <> "" and cint(RsGolpes.Fields("hoyos_jugados").Value) = 9 then
									SQL = "SELECT Golpes.golpes, golpes.id_hoyo FROM Golpes WHERE id_partido"&_
										"=" & RsPartidos.Fields("id").Value & " AND Golpes.id_jugador=" & RsJugadores.Fields("id").Value & " AND golpes.golpes"&_
										"<>0"
									'Response.Write sql
									Rsgolpes1.Open SQL, Application("conn"),2,3
									Aux9=0
									'if RsGolpes1.RecordCount > 0 then
										for x=1 to 9
											Aux(Rsjugadores.Fields("id"),12) = Aux(Rsjugadores.Fields("id"),12) + RsGolpes1.Fields("golpes").Value
											Aux9=Aux9 + RsGolpes1.Fields("golpes").Value
											RsGolpes1.MoveNext 
										next
										Aux(Rsjugadores.Fields("id"),13) = Aux(Rsjugadores.Fields("id"),13) + 1
										if cint(Aux9) < Aux(Rsjugadores.Fields("id"),11) then
											Aux(Rsjugadores.Fields("id"),11)=cint(Aux9)
										end if
										if cint(Aux9) > Aux(Rsjugadores.Fields("id"),10) then
											Aux(Rsjugadores.Fields("id"),10)=cint(Aux9)
										end if
										RsGolpes1.Close							
									'end if
								end if								
								RsGolpes.Close 
								RsPartidos.MoveNext
							loop
							RsPartidos.Close 
							contador=contador+1
							if partidos_completos <> 0 then
								Aux(Rsjugadores.Fields("id"),2) = FormatNumber(Cdbl(Suma_Golpes_total/partidos_completos), 2, -1)
							end if
						    rsjugadores.MoveNext 
					    loop
					end if
					RsCampo.MoveNext 
				loop
	            set RsCampo=nothing
			    set rsgolpes=nothing
	            set rsjugadores=nothing
			    set rsgolpes1=nothing
	            for x=1 to session("cuantosjugadores")
					if Aux(x,8) <> 0 then
						Aux(x,2) = FormatNumber(Cdbl(Aux(x,7)/Aux(x,8)), 2, -1)
					end if
					if Aux(x,13) <> 0 then
						Aux(x,9) = FormatNumber(Cdbl(Aux(x,12)/Aux(x,13)), 2, -1)
					end if
				next
	            '*****************
	            Ordenada=fnsort(Aux,0,2,13,session("cuantosjugadores"))'function fnSort(aSort, intAsc, cual, cuantoscampos, cuantosjugadores)
				'*******************
				Aux1=1
	            For x=1 to session("cuantosjugadores")
					if Ordenada(x,1)<>"0" then
						if par=0 then	
							par=1%>
							<TR class=tb1 vAlign=top>
						<%else
							par=0%>
							<TR class=tb2 vAlign=top>
						<%end if%>	
						<TD><%=aux1%></TD>
						<TD><%=Ordenada(x,1)%></TD>
						<TD align=center><FONT color=#0000ff><%=Ordenada(x,2)%></FONT></TD>
						<TD align=middle>
						<%if Ordenada(x,6)=1000 then 
							Response.Write "-"
						  else
							Response.Write Ordenada(x,6)
						  end if%></TD>
						<TD align=middle><%=Ordenada(x,5)%></TD>
		                <%if golpes_recorrido > 45 then%>						
						<TD align=middle><%=Ordenada(x,9)%></TD>
						<TD align=middle><%=Ordenada(x,11)%></TD>
						<TD align=middle><%=Ordenada(x,10)%></TD>
						<%end if%>
						<TD align=middle><%=Ordenada(x,3)%></TD>
						<TD align=middle><%=Ordenada(x,4)%></TD>
						</TR>
						<%Aux1=Aux1+1	              
					end if
			    next%>
				</TABLE>
	            <TABLE cellSpacing=1 cellPadding=2 width="100%" border=0>
	              
              <TR class=tb0>
                <TD align=left>Med.P - Media de partidos completos (18 hoyos o 9, según el campo)<br>
				Mej.P - Mejor partido<br>
				PeorP - Pues eso<br>
				Med.V - Media de vueltas de 9 hoyos. En campos de 9 hoyos coincide con Med.P<br>
				Mej.V y Peor.V, no hace falta mucha imaginación...</TD></TR>
              <TR class=tb0>
                <TD align=left>Titleist logo signifies Titleist golfball 
                usage</TD></TR></TABLE>
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              
              <TR bgColor=#eef2f2>
                <TD align=right></TD></TR></TABLE></TD></TR></TABLE></DIV><!--end--></TD>
    <TD width=10>&nbsp;</TD>
    <TD class=bk vAlign=top align=middle width=150>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        
        <TR>
          <TD height=10></TD></TR></TABLE> 
      <P></P>
</TD>
    <TD width=10>&nbsp;</TD></TR></TABLE>
<TABLE class=bk cellSpacing=0 cellPadding=0 width=770 border=0>
  
  <TR>
    <TD width=10>&nbsp;</TD>
    <TD vAlign=bottom align=middle width=590></TD>
    <TD width=170>&nbsp;</TD></TR></TABLE>
<TABLE class=bk cellSpacing=0 cellPadding=0 width=770 border=0>
  
  <TR>
    <TD colSpan=3></TD></TR>
  <TR vAlign=top>
    <TD align=middle width=127><A href="http://www.ibm.com/linux" 
      target=_blank><IMG height=42 alt=IBM 
      src="http://images.pgatour.com/images/ibm_footer.gif" width=107 
      border=0></A></TD>
    <TD width=10>&nbsp;</TD>
    <TD class=ftrlink align=middle 
      width=633><STRONG>&nbsp;&nbsp;&nbsp;</STRONG><A 
      href="/information/index.html"><FONT color=#0000ff><STRONG>About 
      Us</STRONG></FONT></A><STRONG> | </STRONG><A href="/tos.html"><FONT 
      color=#0000ff><STRONG>Terms of Service</STRONG></FONT></A><STRONG> | 
      </STRONG><A href="/information/partner.html"><FONT 
      color=#0000ff><STRONG>TOUR Partners</STRONG></FONT></A><STRONG> | 
      </STRONG><A href="/information/licensees.html"><FONT 
      color=#0000ff><STRONG>Licensees</STRONG></FONT></A><STRONG> | </STRONG><A 
      href="/information/privacy.html"><FONT color=#0000ff><STRONG>Privacy 
      Statement</STRONG></FONT></A><STRONG> | </STRONG><A 
      href="/help/index.html"><FONT 
      color=#0000ff><STRONG>Feedback</STRONG></FONT></A><STRONG> | </STRONG><A 
      href="http://www.sportsline.com/redir?url=990" target=_blank><FONT 
      color=#0000ff><STRONG>PGA TOUR Partners Club</STRONG></FONT></A><BR><FONT 
      class=ftrtxt><STRONG>    © 2003, MAJADAHONDA&nbsp;PGA TOUR 
      </STRONG>
      <P></P></FONT></TD></TR></TABLE><!-- /html/pgatour/includes/ads/T_PGAstats_ftr.inc  -->
<STRONG>
</STRONG> <NOSCRIPT><IFRAME marginWidth=0 marginHeight=0 
src="http://ads.specificpop.com/code?pid=279&amp;gid=17" frameBorder=0 width=0 
scrolling=no height=0>
</IFRAME></IFRAME></IFRAME></IFRAME></IFRAME></IFRAME></NOSCRIPT><!--end--></BODY></HTML>
