<html>
<head>
<TITLE>MAJADAHONDA PGA TOUR Scoring</TITLE>
<link rel="stylesheet" type="TEXT/CSS" href="./imagenes/default.css">
<link rel="stylesheet" type="TEXT/CSS" href="./imagenes/scorecard.css">
<script language="JavaScript">
<!--
function openParent(numero, campo){
	var contacto;
	var abre = 'hoyos_Sueltos.asp?p=' + numero + '&c=' + campo;
	contacto = window.open(abre,'hoyo','scrollbars=no,status=no,Height=500,Width=800,screenX=60,screenY=60,titlebar=yes');
	contacto.focus();
}
//-->
</script>
</head>
<body bgcolor="#eef2f2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#000175">
<div align="center">
<table width="590" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top" width="96"><img src="./imagenes/<%=Request.QueryString("id")%>.jpg" border="0" width="96" height="109" vspace="3"></td><td>
<table align="left" valign="top" border="0" cellpadding="2" cellspacing="1">
<tr>
<%	
	quien=Request.QueryString("id")
	partido=Request.QueryString("p")
	
	SqlJugador= "SELECT nombre, handicap, tees FROM jugadores where id=" & quien
	Set RsJugador=Server.CreateObject("ADODB.Recordset")
	RsJugador.Open SqlJugador, Application("conn"),3,2
	Nombre_Jugador=RsJugador.Fields("nombre").Value 
	Handicap_jugador=RsJugador.Fields("handicap").Value
	tees_jugador=RsJugador.Fields("tees").Value
	RsJugador.Close 
	set RsJugador=nothing
	
	SqlPartidos= "SELECT id_campo, dia, mes, ano FROM partidos where id=" & partido
	Set RsPartidos=Server.CreateObject("ADODB.Recordset")
	RsPartidos.Open SqlPartidos, Application("conn"),3,2
	id_campo=RsPartidos.Fields("id_campo").Value 
	dia_partido=RsPartidos.Fields("dia").Value & "/" & RsPartidos.Fields("mes").Value & "/" & RsPartidos.Fields("ano").Value
	RsPartidos.Close 
	set RsPartidos=nothing
	
	SqlCampos= "SELECT * FROM campos where id=" & id_campo
	Set RsCampos=Server.CreateObject("ADODB.Recordset")
	RsCampos.Open SqlCampos, Application("conn"),3,2
	nombre_campo=RsCampos.Fields("nombre").Value	
	numero_hoyos=RsCampos.Fields("n_hoyos").Value
	par_campo=RsCampos.Fields("par_campo").Value
	if tees_jugador="m" then
		Slope=RsCampos.Fields("slope_m").Value
		VC=RsCampos.Fields("VC_m").Value
	else	
		Slope=RsCampos.Fields("slope_h").Value
		VC=RsCampos.Fields("VC_h").Value
	end if
	RsCampos.Close 
	set RsCampos=nothing
	
	Dim Mat_Golpes(19), Mat_putts(18)
	SqlGolpes= "SELECT * FROM golpes where id_partido=" & partido & " and id_jugador=" & quien
	Set RsGolpes=Server.CreateObject("ADODB.Recordset")
	RsGolpes.Open SqlGolpes, Application("conn"),3,2
	RsGolpes.MoveFirst 	
	x=1
	while not RsGolpes.EOF 
		Mat_Golpes(x)=cint(RsGolpes.Fields("golpes").Value)		
		Mat_Putts(x)=cint(RsGolpes.Fields("putts").Value)
		RsGolpes.MoveNext 
		x=x+1
	wend
	RsGolpes.MoveFirst
	
	Dim Mat_Pares(27)	
	Dim handicap_hoyos(27)
	Dim stbf(27)
	SqlHoyos= "SELECT id, par, hcp FROM hoyos where id_campo=" & id_campo & " and tees='" & tees_jugador &"' order by id"
	Set RsHoyos=Server.CreateObject("ADODB.Recordset")
	RsHoyos.Open SqlHoyos, Application("conn"),3,2
	RsHoyos.MoveFirst 	
	x=1
	while not RsHoyos.EOF 
		Mat_Pares(x)=cint(RsHoyos.Fields("par").Value)
		handicap_hoyos(x)=cint(RsHoyos.Fields("hcp").Value)
		RsHoyos.MoveNext 
		x=x+1
	wend
	RsHoyos.MoveFirst


%>
<td colspan="2" class="v4"><b><%=Nombre_Jugador%></b></td>
</tr>
<tr>
<td colspan="2" class="v2"><b><%=nombre_campo%></b></td>
</tr>
<tr>
<td class="a1" align="left">
                Handicap: <%=handicap_Jugador%><br>
                Partido: <%=dia_partido%><br>
                &nbsp<br>
</td><td class="a1" align="left">
                &nbsp <br>
                &nbsp<br>
</td>
</tr>
<tr>
<td class="a1" colspan="2">Last updated: <font color="red">8:21 AM ET. 09/01/2003</font></td>
</tr>
</table>
</td><td width="190"><img src="./imagenes/sc_side_190x109.gif" border="0" alt="PGA TOUR Scorecard" width="190" height="109"></td>
</tr>
<tr>
<td class="v1" colspan="3">&nbsp;<b><a href="javascript:openParent('http://www.pgatour.com/players/02/45/02/index.html');">Intro</a> |
            <a href="javascript:openParent('http://www.pgatour.com/players/02/45/02/bio.html');">Bio</a> |
            <a href="javascript:openParent('http://www.pgatour.com/players/02/45/02/stats.html');">Stats</a> |
            <a href="javascript:openParent('http://www.pgatour.com/players/02/45/02/results.html');">Results</a> |
            <a href="javascript:openParent('http://www.pgatour.com/players/02/45/02/sc/index.html');">Scorecards</a> |
            <a href="javascript:openParent('http://www.pgatour.com/scoring/r/index.html?2003,r505,4,r');">Leaderboard</a>
                 | <a href="http://www.pgatour.com/tourcast/index.html" target="_blank">Follow this player - TOURCast</a></b></td>
</tr>
</table>
<div class="leaderboard">
<table width="590" cellpadding="0" cellspacing="0" border="0" class="outline">
<tr>
<td>
<table border="0" cellspacing="1" cellpadding="3" width="590">
<tr>
<td colspan="22" class="headA" align="center"><%=nombre_campo%> - <%=dia_partido%></td>
</tr>
<tr class="headB">
<td class="headA">Hole</td>
<%for x=1 to 9 %>
<th><A href="javascript:openParent('<%=x%>','<%=id_campo%>')"><%=x%></A>
</th>
<!--</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th><th>8</th><th>9</th>-->
<%
RsHoyos.MoveNext 
next%>
<th><font color=#0000FF>OUT</font></th>
<% 	for x=10 to 18%>
		<th>
<%		if RsHoyos.RecordCount > 9 then %>		
			<A href="javascript:openParent('<%=x%>','<%=id_campo%>')"><%=x%></A>
			<%
			RsHoyos.MoveNext
		else
			Response.write x
		end if
		Response.Write("</th>")
	next
RsHoyos.MoveFirst %>
<!--<th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th><th>18</th>-->
<th><font color=#0000FF>IN</font></th><th><font color=#FF0000>TOT</font></th>
</tr>

<tr class="headC">
<td class="headA">Handicap</td>
<%
for x=1 to 9
Response.Write "<th><font color=#727272>"
Response.Write handicap_hoyos(x) 
Response.Write "</font></th>"
next
Response.Write "<th>&nbsp;</th>"
if cint(numero_hoyos) > 9 then
	for x=10 to 18
		Response.Write "<th><font color=#727272>"
		Response.Write handicap_hoyos(x) 
		Response.Write "</font></th>"
	next
	Response.Write "<th>&nbsp;</th>"
else
	Response.Write "<th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th>"
end if
Response.Write "<th>&nbsp;</th>"
%>


<tr class="headC">
<td class="headA">Par</td>
<%
Suma_Ida=0
Suma_Vuelta=0
for x=1 to 9
Response.Write "<th>"
Response.Write Mat_Pares(x) 
Response.Write "</th>"
Suma_Ida=Suma_Ida + Mat_Pares(x)
next
Response.Write "<th> <font color=#0000FF>" & Suma_Ida & "</font></th>"
if cint(numero_hoyos) > 9 then
	for x=10 to 18
		Response.Write "<th>"
		Response.Write Mat_Pares(x) 
		Response.Write "</th>"
		Suma_Vuelta=suma_vuelta + Mat_Pares(x)
	next
	Response.Write "<th> <font color=#0000FF>" & Suma_Vuelta & "</font></th>"
else
	Response.Write "<th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th>"
end if
Suma_par=suma_ida + suma_vuelta
Response.Write "<th> <font color=#FF0000>" & Suma_par & "</font></th>"
%>
</tr>
<tr class="data">
<td class="headA">Golpes</td>
<%
Suma_Ida=0
Suma_Vuelta=0
dim suma(2,7)'1-Albatros/2-eagle/3-birdie/4-Par/5-Bogey/6-Doble Bogey/7-Triple Bogey o peor
for x= 1 to 7
	suma(1,x)=0
	suma(2,x)=0
next
for x=1 to 9
	Response.Write "<th"
	if Mat_Golpes(x)<>0 then
		select case (Mat_Golpes(x) - Mat_Pares(x))
		case -3
			Response.Write " class=ace>"
			suma(1,1)=suma(1,1)+1
		case -2
			Response.Write " class=eag>"
			suma(1,2)=suma(1,2)+1
		case -1
			Response.Write " class=bir>"
			suma(1,3)=suma(1,3)+1
		case 0
			Response.Write " class=par_>"
			suma(1,4)=suma(1,4)+1
		case 1
			Response.Write " class=bog>"
			suma(1,5)=suma(1,5)+1
		case 2
			Response.Write " class=dbo>"
			suma(1,6)=suma(1,6)+1
		case else
			Response.Write " class=tbo>"
			suma(1,7)=suma(1,7)+1
		end select
	else
		Response.Write " class=tbo>"
		suma(1,7)=suma(1,7)+1
	end if
	Response.Write Mat_Golpes(x) 
	Response.Write "</th>"
	Suma_Ida=Suma_Ida + Mat_Golpes(x)
next
Response.Write "<th><font color=#0000FF>" & Suma_Ida & "</font></th>"
DosVueltas=false
for y= 10 to 18
	if Mat_Golpes(y)<> 0 then
		DosVueltas=true
	end if
next
if cint(numero_hoyos) > 9 and DosVueltas = true then
	for x=10 to 18
	Response.Write "<th"
	if Mat_Golpes(x)<>0 then
		select case (Mat_Golpes(x) - Mat_Pares(x))
		case -3
			Response.Write " class=ace>"
			suma(1,1)=suma(1,1)+1
		case -2
			Response.Write " class=eag>"
			suma(1,2)=suma(1,2)+1
		case -1
			Response.Write " class=bir>"
			suma(1,3)=suma(1,3)+1
		case 0
			Response.Write " class=par_>"
			suma(1,4)=suma(1,4)+1
		case 1
			Response.Write " class=bog>"
			suma(1,5)=suma(1,5)+1
		case 2
			Response.Write " class=dbo>"
			suma(1,6)=suma(1,6)+1
		case else
			Response.Write " class=tbo>"
			suma(1,7)=suma(1,7)+1
		end select
	else
		Response.Write " class=tbo>"
		suma(1,7)=suma(1,7)+1
	end if
	Response.Write Mat_Golpes(x) 
	Response.Write "</th>"
	Suma_Vuelta=suma_vuelta + Mat_Golpes(x)
	next
	Response.Write "<th><font color=#0000FF>" & Suma_Vuelta & "</font></th>"
else
	if numero_hoyos=18 then numero_hoyos = 9
	Response.Write "<th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th>"
end if
suma_golpes=suma_ida + suma_vuelta
Response.Write "<th><font color=#FF0000>" & suma_golpes & "</font></th>"
%>
</tr>
<tr class="data">
<td class="headA">Putts</td>
<%
Suma_Ida=0
Suma_Vuelta=0
RsGolpes.MoveFirst
for x=1 to 9
	Response.Write "<th"
	if RsGolpes.Fields("putts").Value <= 1 then
		Response.write " class=bir>"
	else
		if RsGolpes.Fields("putts").Value >= 3 then
			Response.write " class=bog>"
		else
			response.write ">"
		end if
	end if
Response.Write RsGolpes.Fields("putts").Value 
Response.Write "</th>"
Suma_Ida=Suma_Ida + RsGolpes.Fields("putts").Value
RsGolpes.MoveNext
next
Response.Write "<th><font color=#0000FF>" & Suma_Ida & "</font></th>"
if cint(numero_hoyos) > 9 then
	for x=1 to 9
	Response.Write "<th"
	if RsGolpes.Fields("putts").Value <= 1 then
		Response.write " class=bir>"
	else
		if RsGolpes.Fields("putts").Value >= 3 then
			Response.write " class=bog>"
		else
			response.write ">"
		end if
	end if
	Response.Write RsGolpes.Fields("putts").Value 
	Response.Write "</th>"
	Suma_Vuelta=suma_vuelta + RsGolpes.Fields("putts").Value
	RsGolpes.MoveNext
	next
	Response.Write "<th><font color=#0000FF>" & Suma_Vuelta & "</font></th>"
else
	Response.Write "<th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th>"
end if
Response.Write "<th><font color=#FF0000>" & suma_ida + suma_vuelta & "</font></th>"
%>
<!--<th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>&nbsp;</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>-</th><th>&nbsp;</th><th>E</th>-->
</tr>
<tr class="data">
<td class="headA">Stableford</td>
<%
    num_hoyos = numero_hoyos
    suma_par = par_campo
    dim Mat_Pares_stbf(18)
    for x= 1 to num_hoyos
		Mat_Pares_stbf(x) = Mat_pares(x)
    next
    'Aqu� va lo que calcula el handicap para ese campo en particular
    'HJE = H_EGA*(VS/113)+(VC-Par)
    if RsGolpes.RecordCount = 9 then Handicap_jugador=Round(Handicap_jugador/2)
    contador = Round(Handicap_jugador * (Slope / 113) + VC - suma_par)
    Response.Write contador
    Contador_hoyos = 0
    For x = 1 To contador
        Contador_hoyos = Contador_hoyos + 1
        If Contador_hoyos = 19 Then Contador_hoyos = 1        
        while Mat_golpes(Contador_hoyos) = 0 or Mat_golpes(Contador_hoyos) = ""
            Contador_hoyos = Contador_hoyos + 1
            If Contador_hoyos = 19 Then Contador_hoyos = 1
        wend
        For y = 1 To num_hoyos
            'If handicap_hoyos(y) = Contador_hoyos Then Mat_Pares_stbf(handicap_hoyos(y)) = Mat_Pares_stbf(handicap_hoyos(y)) + 1
            If handicap_hoyos(y) = Contador_hoyos Then Mat_Pares_stbf(y) = Mat_Pares_stbf(y) + 1
        Next
    Next
    For x = 1 To num_hoyos
		stbf(x)=0
	next    
    For x = 1 To num_hoyos
		if mat_golpes(x)<> 0 then
			If mat_golpes(x) = Mat_Pares_stbf(x) + 1 Then
				stbf(x) = stbf(x) + 1
	        ElseIf mat_golpes(x) = Mat_Pares_stbf(x) Then
		        stbf(x) = stbf(x) + 2
			ElseIf mat_golpes(x) = Mat_Pares_stbf(x) - 1 Then
				stbf(x) = stbf(x) + 3
	        ElseIf mat_golpes(x) = Mat_Pares_stbf(x) - 2 Then
		        stbf(x) = stbf(x) + 4
			ElseIf mat_golpes(x) = Mat_Pares_stbf(x) - 3 Then
				stbf(x) = stbf(x) + 5
	        ElseIf mat_golpes(x) = Mat_Pares_stbf(x) - 4 Then
		        stbf(x) = stbf(x) + 6
			ElseIf mat_golpes(x) = Mat_Pares_stbf(x) - 5 Then
				stbf(x) = stbf(x) + 7
	        ElseIf mat_golpes(x) = Mat_Pares_stbf(x) - 6 Then
		        stbf(x) = stbf(x) + 8
			End If
		end if
    Next
	suma_ida_stbf=0
	suma_vuelta_stbf=0
	for x=1 to 9
		Response.Write("<th>" & stbf(x) & "</th>")
		suma_ida_stbf = suma_ida_stbf + stbf(x)
	next
	Response.Write("<th>" & suma_ida_stbf & "</th>")	
	for x=10 to 18
		Response.Write("<th>" & stbf(x) & "</th>")
		suma_vuelta_stbf = suma_vuelta_stbf + stbf(x)
	next
	Response.Write("<th>" & suma_vuelta_stbf & "</th>")	
	Response.Write("<th>" & suma_ida_stbf + suma_vuelta_stbf & "</th>")	
%>
</th>
</tr>
<tr>
<td colspan="22" align="right"><img src="./imagenes/sclegend.gif" border="0"></td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<%
RsHoyos.Close
set RsHoyos=nothing
RsGolpes.Close
set RsGolpes=nothing
%>


<table width="590" cellpadding="2" cellspacing="0" border="0">
<tr>
<td aligh="left" class="v2" valign="bottom"><b>JUANMA.CRUZDEGORBEA.COM - Get connected to the game</b></td><td align="right"><img src="./imagenes/poweredbyshotlink_105x30.gif" border="0" width="105" height="30" alt="Powered by ShotLink"></td>
</tr>
</table><!-- <b>Scorecard stats may be incomplete.</b> -->
<table width="590" cellpadding="0" cellspacing="0" border="0" class="outline">
<tr>
<td>
<div class="leaderboard">
<table border="0" cellspacing="1" cellpadding="3" width="590">
<tr>
<td class="headA">STATS</td><th class="headA">IDA</th><th class="headA">VUELTA</th><th class="headA">TOT</th><th class="headA">RANK*</th>
</tr>
<tr class="data">
<td class="headA">ALBATROS</td><th><%=suma(1,1)%></th><th><%=suma(2,1)%></th><th><%=suma(1,1)+suma(2,1)%></th><th>N/A</th>
</tr>
<tr class="data">
<td class="headA">EAGLES</td><th><%=suma(1,2)%></th><th><%=suma(2,2)%></th><th><%=suma(1,2)+suma(2,2)%></th><th>N/A</th>
</tr>
<tr class="data">
<td class="headA">BIRDIES</td><th><%=suma(1,3)%></th><th><%=suma(2,3)%></th><th><%=suma(1,3)+suma(2,3)%></th><th>N/A</th>
</tr>
<tr class="data">
<td class="headA">PARES</td><th><%=suma(1,4)%></th><th><%=suma(2,4)%></th><th><%=suma(1,4)+suma(2,4)%></th><th>N/A</th>
</tr>
<tr class="data">
<td class="headA">BOGEYS</td><th><%=suma(1,5)%></th><th><%=suma(2,5)%></th><th><%=suma(1,5)+suma(2,5)%></th><th>N/A</th>
</tr>
<tr class="data">
<td class="headA">DOBLE BOGEYS</td><th><%=suma(1,6)%></th><th><%=suma(2,6)%></th><th><%=suma(1,6)+suma(2,6)%></th><th>N/A</th>
</tr>
<tr class="data">
<td class="headA">OTROS</td><th><%=suma(1,7)%></th><th><%=suma(2,7)%></th><th><%=suma(1,7)+suma(2,7)%></th><th>N/A</th>
</tr>
<tr class="data">
<td class="headA">PUTTS PER ROUND</td><th><%=suma_ida%></th><th><%=suma_vuelta%></th><th><%=suma_ida + suma_vuelta%></th><th>N/A</th>
</tr>
<%
GIR1 = 0
GIR2 = 0
for x=1 to 9
	if (Mat_Golpes(x)-Mat_Putts(x)) <= (Mat_pares(x)-2) and (Mat_Golpes(x) <> 0) then
		GIR1 = GIR1 + 1
	end if	
next
if numero_hoyos > 9 then
	for x=10 to 18
		if (Mat_Golpes(x)-Mat_Putts(x)) <= (Mat_pares(x)-2) and (Mat_Golpes(x) <> 0) then
			GIR2 = GIR2 + 1
		end if
	next
end if
%>
<tr class="data">
<td class="headA">GREENS IN REG</td><th><%=FormatNumber(GIR1/9*100, 2, -1)%>%</th><th><%=FormatNumber(GIR2/9*100, 2, -1)%>%</th><th><%=FormatNumber((GIR1 + GIR2)/numero_hoyos*100, 2, -1)%>%</th><th>T2</th>
</tr>
<tr class="data">
<td class="headA">PUTTS PER GIR</td><th>N/A</th><th>N/A</th><th>N/A</th><th>N/A</th>
</tr>
<tr>
<td align="right" colspan="6">  * Ranks are computed at the end of each round.</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</div>
<DIV style="position:absolute; width:1; height:1; left:-50; top:-50; z-index:5;"><table width="0" height="0" border="0" cellspacing="0" cellpadding="0"><tr><td bgcolor="#213829">
<!-- SiteCatalyst code version: G.2-Custom.
Copyright 1997-2003 Omniture, Inc. More info available at
http://www.omniture.com --><script language="JavaScript"><!--
/*************** DO NOT ALTER THE FOLLOWING 2 LINES ! ***************/
var s_code=' '//--></script>
<script language="JavaScript"><!--
/************************ ADDITIONAL FEATURES ************************
     Style Sheet Usage
     Dynamic Account Selection
     Plugins
	Custom:
		Removal Of NO-JavaScript Support
		Addition Of s_account Variable Support
*/
/************************** CONFIG SECTION **************************/
/* You may add or alter any code config here.                       */
/* Specify the Report Suite ID(s) to track here */
var s_account="cbspgatour"
//var s_account="devcbspgatour"
var s_dynamicAccountSelection=false
var s_dynamicAccountList=""
/* E-commerce Config */
var s_eVarCFG=""
/* Link Tracking Config */
var s_trackDownloadLinks=true
var s_trackExternalLinks=true
var s_trackInlineStats=true
var s_linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,doc,pdf,xls,ram"
var s_linkInternalFilters="javascript:,pgatour,golfweb,worldgolfchampionships,presidentscup,tpc,playatpc"
var s_linkExternalFilters="sportsline,nfl,ncaasports"
var s_linkLeaveQueryString=false
/* Plugin Config */
var s_usePlugins=true
function s_doPlugins() {
	/* Add calls to plugins here */
}

/************************** PLUGINS SECTION *************************/
/* You may insert any plugins you wish to use here.                 */
/*
 * Plugin: Get Plugin Modified Value
 */
function s_vp_getValue(vs)
	{var k=vs.substring(0,2)=='s_'?vs.substring(2):vs;return s_wd[
	's_vpm_'+k]?s_wd['s_vpv_'+k]:s_gg(k)}
/*
 * Plugin: Get Query String CGI Variable Value
 */
function s_vp_getCGI(vs,k)
	{var v='';if(k&&s_wd.location.search){var q=s_wd.location.search,
	qq=q.indexOf('?');q=qq<0?q:q.substring(qq+1);v=s_pt(q,'&',s_cgif,
	k)}s_vpr(vs,v)}function s_cgif(t,k){if(t){var te=t.indexOf('='),
	sk=te<0?t:t.substring(0,te),sv=te<0?'True':t.substring(te+1);if(
	sk==k)return s_epa(sv)}return ''}
/*
 * Plugin: Get State Of Form On Abandon
 */
function s_vp_getFormAbandonState(vs,lt,ln)
	{if(!s_wd.s_vp_faolr){s_wd.s_vp_faol=s_wd.onload;s_wd.onload=
	s_vp_fas;s_wd.s_vp_favs=vs;s_wd.s_vp_falt=lt;s_wd.s_vp_faln=ln
	s_wd.s_vp_faolr=1}}var s_vp_faolr=0;function s_vp_fas(e){var r=
	true;if(s_wd.s_vp_faol)r=s_wd.s_vp_faol(e);if(s_d.forms&&
	s_d.forms.length>0){var vs=s_wd.s_vp_favs,p=s_gg('pageName'),fn,
	f,en,el,t,oc,och;for(fn=0;fn<s_d.forms.length;fn++){f=s_d.forms[
	fn];if(vs&&!s_wd[vs])s_wd[vs]=(f.name?f.name:(p?p:''))
	+':No Data Entered';for(en=0;en<f.elements.length;en++){el=
	f.elements[en];t=el.type;if(t&&t.toUpperCase){t=t.toUpperCase()
	oc=el.onclick?el.onclick.toString():'';och=el.onchange?
	el.onchange.toString():'';if(oc.indexOf("s_vp_fac(")<0&&
	och.indexOf("s_vp_fac(")<0){if(t=='BUTTON'||t=='CHECKBOX'||t==
	'RADIO'||t=='RESET'||t=='SUBMIT'||t=='IMAGE'){el.s_vp_faoc=
	el.onclick;if(((f.name&&oc.indexOf(f.name)>=0)||(oc.indexOf(
	'form.')>=0))&&oc.indexOf("submit(")>=0)el.onclick=s_vp_fasu;else
	el.onclick=s_vp_fac}else{el.s_vp_faoch=el.onchange;if(((f.name&&
	och.indexOf(f.name)>=0)||(och.indexOf('form.')>=0))&&och.indexOf(
	"submit(")>=0)el.onchange=s_vp_fasu;else el.onchange=s_vp_fac }}}
	}f.s_vp_faos=f.onsubmit;f.onsubmit=s_vp_fasu}s_wd.s_vp_faul=
	s_wd.onunload;s_wd.onunload=s_vp_fa}return r}function s_vp_fa(e){
	var vs=s_wd.s_vp_favs;if(vs&&s_wd[vs]){s_lnk=new Object
	s_linkType=s_wd.s_vp_falt;s_linkName=s_wd.s_vp_faln;s_gs('')
	s_lnk=''}if(s_wd.s_ful)return s_wd.s_ful(e);return true}
	function s_vp_fasu(e){var vs=s_wd.s_vp_favs;if(vs)s_wd[vs]='';if(
	this.s_vp_faos)return this.s_vp_faos(e);if(this.s_vp_faoc)
	return this.s_vp_faoc(e);if(this.s_vp_faoch)
	return this.s_vp_faoch(e);return true}function s_vp_fac(e){var b=
	"s_gs(",vs=s_wd.s_vp_favs,f=this.form,p=s_gg('pageName');if(vs)
	s_wd[vs]=(f.name?f.name:(p?p:''))+':'+(this.name?this.name:'')
	if(this.s_vp_faoc)return this.s_vp_faoc(e);if(this.s_vp_faoch)
	return this.s_vp_faoch(e);return true}
/*
 * Plugin Utilities v2.0 (Required For All Plugins)
 */
function s_vpr(vs,v){if(s_wd[vs])s_wd[vs]=s_wd[vs];else s_wd[vs]=''
if(vs.substring(0,2) == 's_')vs=vs.substring(2);s_wd['s_vpv_'+vs]=v
s_wd['s_vpm_'+vs]=1}function s_dt(tz,t){var d=new Date;if(t)d.setTime(
t);d=new Date(d.getTime()+(d.getTimezoneOffset()*60*1000))
return new Date(Math.floor(d.getTime()+(tz*60*60*1000)))}
function s_vh_gt(k,v){var vh='|'+s_c_r('s_vh_'+k),vi=vh.indexOf('|'+v
+'='),ti=vi<0?vi:vi+2+v.length,pi=vh.indexOf('|',ti),t=ti<0?'':
vh.substring(ti,pi<0?vh.length:pi);return t}function s_vh_gl(k){var
vh=s_c_r('s_vh_'+k),e=vh?vh.indexOf('='):0;return vh?(vh.substring(0,
e?e:vh.length)):''}function s_vh_s(k,v){if(k&&v){var e=new Date,st=
e.getTime(),y=e.getYear(),c='s_vh_'+k,vh='|'+s_c_r(c)+'|',t=s_vh_gt(k,
v);e.setYear((y<1900?y+1900:y)+5);if(t)vh=s_rep(vh,'|'+v+'='+t+'|','|'
);if(vh.substring(0,1)=='|')vh=vh.substring(1);if(vh.substring(
vh.length-1,vh.length)=='|')vh=vh.substring(0,vh.length-1);vh=v
+'=[PCC]'+(vh?'|'+vh:'');s_c_w(c,vh,e);if(s_vh_gt(k,v)!='[PCC]')
return 0;vh=s_rep(vh,'[PCC]',st);s_c_w(c,vh,e)}return 1}

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_un,s_ios=0,s_csss=0,s_q='',s_code='',code='',s_bcr=0,s_lnk='',
s_eo='',s_vb,s_tfs=0,s_etfs=0,s_wd=window,s_ssl=(
s_wd.location.protocol.toLowerCase().indexOf('https')>=0),s_d=
s_wd.document,s_n=navigator,s_u=s_n.userAgent,s_apn=s_n.appName,s_v=
s_n.appVersion,s_apv,s_i,s_ie=s_v.indexOf('MSIE '),s_ns6=s_u.indexOf(
'Netscape6/');if(s_v.indexOf('Opera')>=0||s_u.indexOf('Opera')>=0)
s_apn='Opera';var s_isie=(s_apn=='Microsoft Internet Explorer'),
s_isns=(s_apn=='Netscape'),s_isopera=(s_apn=='Opera'),s_ismac=(
s_u.indexOf('Mac')>=0);if(s_ie>0){s_apv=parseInt(s_i=s_v.substring(
s_ie+5));if(s_apv>3)s_apv=parseFloat(s_i)}else if(s_ns6>0)s_apv=
parseFloat(s_u.substring(s_ns6+10));else s_apv=parseFloat(s_v)
function s_co(o){if(!o)return o;var n=new Object;for(x in o)n[x]=o[x]
return n}function s_num(x){var s=x.toString(),g='0123456789',p,d;for(
p=0;p<s.length;p++){d=s.substring(p,p+1);if(g.indexOf(d)<0)return 0}
return 1}function s_rep(s,o,n){var i=s.indexOf(o),l=n.length>0?
n.length:1;while(s&&i>=0){s=s.substring(0,i)+n+s.substring(i+o.length)
i=s.indexOf(o,i+l)}return s}function s_ape(s){return s_rep(escape(s),
'+','%2B')}function s_epa(s){return unescape(s_rep(s,'+',' '))}
function s_pt(s,d,f,a){var t=s,x=0,y,r;while(t){y=t.indexOf(d);y=y<0?
t.length:y;t=t.substring(0,y);r=f(t,a);if(r)return r;x+=y+d.length;t=
s.substring(x,s.length);t=x<s.length?t:''}return ''}function s_fl(s,l)
{return (s+'').substring(0,l)}var s_c_d='';function s_c_gdf(t,a){if(
!s_num(t))return 1;return 0}function s_c_gd(){var d=
s_wd.location.hostname,p;if(d&&!s_c_d){p=d.indexOf('.');while(p>=0&&
d.substring(p+1).indexOf('.')>=0){d=d.substring(p+1);p=d.indexOf('.')}
s_c_d=d.indexOf('.')>=0&&s_pt(d,'.',s_c_gdf,0)?'.'+d:''}return s_c_d}
function s_c_r(k){k=s_ape(k);var c=' '+s_d.cookie,s=c.indexOf(' '+k
+'='),e=s<0?s:c.indexOf(';',s),v=s<0?'':s_epa(c.substring(s+2
+k.length,e<0?c.length:e));return v}function s_c_w(k,v,e){var d=
s_c_gd();if(k)s_d.cookie=k+'='+s_ape(v)+'; '+'path=/;'+(e?' expires='
+e.toGMTString()+';':'')+(d?' domain='+d+';':'');return s_c_r(k)==v}
function s_cet(f,a,et,oe,fb){var r,d=0
/*@cc_on@if(@_jscript_version>=5){try{return f(a)}catch(e){return et(e)}d=1}@end@*/
if(!d){if(s_ismac&&s_u.indexOf('MSIE 4')>=0)return fb(a);else{
s_wd.s_oe=s_wd.onerror;s_wd.onerror=oe;r=f(a);s_wd.onerror=s_wd.s_oe
return r}}}function s_gtfset(e){return s_tfs}function s_gtfsoe(e){
s_wd.onerror=s_wd.s_oe;s_etfs=1;var code=s_gs(s_un);if(code)s_d.write(
code);s_etfs=0;return true}function s_gtfsfb(a){return s_wd}
function s_gtfsf(w){var p=w.parent,l=w.location;s_tfs=w;if(p&&
p.location!=l&&p.location.host==l.host){s_tfs=p;return s_gtfsf(s_tfs)}
return s_tfs}function s_gtfs(){if(!s_tfs){s_tfs=s_wd;if(!s_etfs)s_tfs=
s_cet(s_gtfsf,s_tfs,s_gtfset,s_gtfsoe,s_gtfsfb)}return s_tfs}
function s_ca(un){un=un.toLowerCase()
if(!s_csss&&s_d.styleSheets&&s_isie&&!s_ismac&&s_apv>=5)s_csss=1;if(
s_csss){if(s_d.styleSheets.length<1)s_d.write('<st'+'yle type="text/c'
+'ss"></st'+'yle>');if(!s_d.styleSheets[0]||!s_d.styleSheets[0
].addImport)s_csss=0}
var ci=un.indexOf(','),fun=ci<0?un:un.substring(0,ci),imn='s_i_'+fun
if(!s_ios&&s_d.images&&s_apv>=3&&!s_isopera&&(s_ns6<0||s_apv>=6.1))
s_ios=1;if(!s_csss&&s_ios&&!s_d.images[imn]){s_d.write('<im'
+'g name="'+imn+'" height=1 width=1 border=0 alt="">');if(!s_d.images[
imn])s_ios=0}}function s_it(un){s_ca(un)}function s_mr(un,sess,q,ta){
un=un.toLowerCase();var ci=un.indexOf(','),fun=ci<0?un:un.substring(0,
ci),imn='s_i_'+fun,unc=s_rep(fun,'_','-'),rs='http'+(s_ssl?'s':'')
+'://'+(s_ssl?'102':unc)+'.112.2O7.net/b/ss/'+un+'/'+(s_csss?0:1)+'/G.3-XPd-F/'
+sess+'?'+'[AQB]&ndh=1'+(q?q:'')+(s_q?s_q:'')+'&[AQE]'
if(s_csss)s_d.styleSheets[0].addImport(rs);else
if(s_ios){if(!s_d.images[imn]){var im=new Image;im.src=rs}else
s_d.images[imn].src=rs}if(s_csss||s_ios){if(rs.indexOf('&pe=')>=0&&(
!ta||ta=='_self'||ta=='_top'||(s_wd.name&&ta==s_wd.name))){var b=
new Date,e=new Date;while(e.getTime()-b.getTime()<500)e=new Date}
return ''}return '<im'+'g sr'+'c="'+rs
+'" width=1 height=1 border=0 alt="">'}function s_gg(v){var g='s_'+v
return s_wd[g]||s_wd.s_disableLegacyVars ? s_wd[g] : s_wd[v]}var
s_qav='';function s_havf(t,a){var b=t.substring(0,4),s=t.substring(4),
n=parseInt(s),k='s_g_'+t,m='s_vpm_'+t,q=t;if(!s_wd['s_'+t])s_wd['s_'+t
]='';s_wd[k]=s_wd[m]?s_wd['s_vpv_'+t]:s_gg(t);s_wd[m]=0;if(t==
'charSet')q='ce';else if(t=='cookieDomainPeriods')q='cdp';else if(t==
'cookieLifetime')q='cl';else if(t=='channel')q='ch';else if(t==
'campaign')q='v0';else if(s_num(s)){if(b=='prop')q='c'+n;else if(b==
'eVar')q='v'+n}if(s_wd[k]&&t!='linkName'&&t!='linkType')s_qav+='&'+q
+'='+s_ape(s_wd[k]);return ''}function s_hav(){var n,av='charSet,cook'
+'ieDomainPeriods,cookieLifetime,pageName,channel,server,pageType,cam'
+'paign,state,zip,events,products,purchaseID,eVarCFG,linkName,linkTyp'
+'e';for(n=1;n<26;n++)av+=',prop'+n+',eVar'+n;s_qav='';s_pt(av,',',
s_havf,0);return s_qav}function s_lnf(t,h){t=t?t.toLowerCase():'';h=h?
h.toLowerCase():'';var te=t.indexOf('=');if(t&&te>0&&h.indexOf(
t.substring(te+1))>=0)return t.substring(0,te);return ''}
function s_ln(h){if(s_gg('linkNames'))return s_pt(s_gg('linkNames'),
',',s_lnf,h);return ''}function s_ltdf(t,h){t=t?t.toLowerCase():'';h=
h?h.toLowerCase():'';var qi=h.indexOf('?');h=qi>=0?h.substring(0,qi):h
if(t&&h.substring(h.length-(t.length+1))=='.'+t)return 1;return 0}
function s_ltef(t,h){t=t?t.toLowerCase():'';h=h?h.toLowerCase():'';if(
t&&h.indexOf(t)>=0)return 1;return 0}function s_lt(h){var lft=s_gg(
'linkDownloadFileTypes'),lef=s_gg('linkExternalFilters'),lif=s_gg(
'linkInternalFilters')?s_gg('linkInternalFilters'):
s_wd.location.hostname;h=h.toLowerCase();if(s_gg('trackDownloadLinks'
)&&lft&&s_pt(lft,',',s_ltdf,h))return 'd';if(s_gg('trackExternalLinks'
)&&(lef||lif)&&(!lef||s_pt(lef,',',s_ltef,h))&&(!lif||!s_pt(lif,',',
s_ltef,h)))return 'e';return ''}function s_lc(e){s_lnk=s_co(this)
s_gs('');s_lnk='';if(this.s_oc)return this.s_oc(e);return true}
function s_ls(){var l,ln,oc;for(ln=0;ln<s_d.links.length;ln++){l=
s_d.links[ln];oc=l.onclick?l.onclick.toString():'';if(oc.indexOf(
"s_gs(")<0&&oc.indexOf("s_lc(")<0){l.s_oc=l.onclick;l.onclick=s_lc}}}
function s_bc(e){s_eo=e.srcElement?e.srcElement:e.target;s_gs('')
s_eo=''}function s_ot(o){var x=o.type,y=o.tagName;return (x&&
x.toUpperCase?x:y&&y.toUpperCase?y:o.href?'A':'').toUpperCase()}
function s_oid(o){var t=s_ot(o),p=o.protocol,c=o.onclick,n='',x=0;if(
!o.s_oid){if(o.href&&(t=='A'||t=='AREA')&&(!c||!p||p.toLowerCase(
).indexOf('javascript')<0))n=o.href;else if(c){n=s_rep(s_rep(s_rep(
s_rep(c.toString(),"\r",''),"\n",''),"\t",''),' ','');x=2}else if(
o.value&&(t=='INPUT'||t=='SUBMIT')){n=o.value;x=3}else if(o.src&&t==
'IMAGE')n=o.src;if(n){o.s_oid=s_fl(n,100);o.s_oidt=x}}return o.s_oid}
function s_rqf(t,un){var e=t.indexOf('='),u=e>=0?','+t.substring(0,e)
+',':'';return u&&u.indexOf(','+un+',')>=0?s_epa(t.substring(e+1)):''}
function s_rq(un){var c=un.indexOf(','),v=s_c_r('s_sq'),q='';if(c<0)
return s_pt(v,'&',s_rqf,un);return s_pt(un,',',s_rq,0)}var s_sqq,s_squ
function s_sqp(t,a){var e=t.indexOf('='),q=e<0?'':s_epa(t.substring(e
+1));s_sqq[q]='';if(e>=0)s_pt(t.substring(0,e),',',s_sqs,q);return 0}
function s_sqs(un,q){s_squ[un]=q;return 0}function s_sq(un,q){s_sqq=
new Object;s_squ=new Object;s_sqq[q]='';var k='s_sq',v=s_c_r(k),x,c=0
s_pt(v,'&',s_sqp,0);s_pt(un,',',s_sqs,q);v='';for(x in s_squ)s_sqq[
s_squ[x]]+=(s_sqq[s_squ[x]]?',':'')+x;for(x in s_sqq)if(x&&s_sqq[x]&&(
x==q||c<2)){v+=(v?'&':'')+s_sqq[x]+'='+s_ape(x);c++}return s_c_w(k,v,0
)}function s_wdl(e){s_wd.s_wd_l=1;var r=true;if(s_wd.s_ol)r=s_wd.s_ol(
e);if(s_wd.s_ls)s_wd.s_ls();return r}function s_wds(un){un=
un.toLowerCase();s_wd.s_wd_l=1;if(s_apv>3&&(!s_isie||!s_ismac||s_apv>=
5)){s_wd.s_wd_l=0;if(!s_wd.s_unl)s_wd.s_unl=new Array;s_wd.s_unl[
s_wd.s_unl.length]=un;if(s_d.body&&s_d.body.attachEvent){if(
!s_wd.s_bcr&&s_d.body.attachEvent('onclick',s_bc))s_wd.s_bcr=1}
else if(s_d.body&&s_d.body.addEventListener){if(!s_wd.s_bcr&&
s_d.body.addEventListener('click',s_bc,false))s_wd.s_bcr=1}else{var
ol=s_wd.onload?s_wd.onload.toString():'';if(ol.indexOf("s_wdl(")<0){
s_wd.s_ol=s_wd.onload;s_wd.onload=s_wdl}}}}function s_vs(un,x){var s=
s_gg('visitorSampling'),g=s_gg('visitorSamplingGroup'),k='s_vsn_'+un+(
g?'_'+g:''),n=s_c_r(k),e=new Date,y=e.getYear();e.setYear(y+10+(y<
1900?1900:0));if(s){s*=100;if(!n){if(!s_c_w(k,x,e))return 0;n=x}if(n%
10000>s)return 0}return 1}
function s_gs(un){un=un.toLowerCase()
s_un=un;var trk=1,tm=new Date,sed=Math&&Math.random?Math.floor(
Math.random()*10000000000000):tm.getTime(),sess='s'+Math.floor(
tm.getTime()/10800000)%10+sed,yr=tm.getYear(),t,ta='',q='',qs='';yr=
yr<1900?yr+1900:yr;t=tm.getDate()+'/'+tm.getMonth()+'/'+yr+' '
+tm.getHours()+':'+tm.getMinutes()+':'+tm.getSeconds()+' '+tm.getDay()
+' '+tm.getTimezoneOffset();if(!s_q){s_d.cookie='s_cc=true; path=/'
var tfs=s_gtfs(),tl=tfs.location,r=tfs.document.referrer,s='',c='',v=
'',p='',bw='',bh='',j='1.0',vb=s_vb?s_vb:'',g=s_wd.location,k=
s_d.cookie.indexOf('s_cc=')>=0?'Y':'N',hp='',ct='';if(s_apv>=4)s=
screen.width+'x'+screen.height;if(s_isns||s_isopera){if(s_apv>=3){j=
'1.1';var i1=0,i2=0,sta;while(i2<30&&i1<navigator.plugins.length){sta=
navigator.plugins[i1].name;if(sta.length>100)sta=sta.substring(0,100)
sta+=';';if(p.indexOf(sta)<0)p+=sta;i1++;i2++}v=navigator.javaEnabled(
)?'Y':'N'}if(s_apv>=4){j='1.2';c=screen.pixelDepth;bw=s_wd.innerWidth
bh=s_wd.innerHeight}if(s_apv>=4.06)j='1.3'}else if(s_isie){if(s_apv<4)
r='';if(s_apv>=4){v=navigator.javaEnabled()?'Y':'N';j='1.2';c=
screen.colorDepth}if(s_apv>=5){bw=s_d.documentElement.offsetWidth;bh=
s_d.documentElement.offsetHeight;j='1.3';if(!s_ismac&&s_d.body){
s_d.body.addBehavior("#default#homePage");hp=s_d.body.isHomePage(tl)?
"Y":"N";s_d.body.addBehavior("#default#clientCaps");ct=
s_d.body.connectionType}}}s_q=(g?'&g='+s_ape(s_fl(g,255)):'')+(r?'&r='
+s_ape(s_fl(r,255)):'')+(s?'&s='+s_ape(s):'')+(c?'&c='+s_ape(c):'')+(
j?'&j='+j:'')+(v?'&v='+v:'')+(k?'&k='+k:'')+(bw?'&bw='+bw:'')+(bh?
'&bh='+bh:'')+(vb?'&vb='+vb:'')+(ct?'&ct='+s_ape(ct):'')+(hp?'&hp='
+hp:'')+(p?'&p='+s_ape(p):'')}if(s_gg('usePlugins'))s_wd.s_doPlugins()
q+=(t?'&t='+s_ape(t):'')+s_hav();if(s_lnk||s_eo){var o=s_eo?s_eo:s_lnk
if(!o)return '';var p=s_wd.s_g_pageName,w=1,t=s_ot(o),n=s_oid(o),x=
o.s_oidt,h,l,i,oc;if(s_eo&&o==s_eo){while(o&&!n&&t!='BODY'){o=
o.parentElement?o.parentElement:o.parentNode;if(!o)return '';t=s_ot(o)
n=s_oid(o);x=o.s_oidt}oc=o.onclick?o.onclick.toString():'';if(
oc.indexOf("s_gs(")>=0)return ''}ta=o.target;h=o.href?o.href:'';i=
h.indexOf('?');h=s_gg('linkLeaveQueryString')||i<0?h:h.substring(0,i)
l=s_gg('linkName')?s_gg('linkName'):s_ln(h);t=s_gg('linkType')?s_gg(
'linkType').toLowerCase():s_lt(h);if(t&&(h||l))q+='&pe=lnk_'+(t=='d'||
t=='e'?s_ape(t):'o')+(h?'&pev1='+s_ape(h):'')+(l?'&pev2='+s_ape(l):'')
else trk=0;if(s_gg('trackInlineStats')){if(!p){p=s_wd.location.href;w=
0}p=p?s_fl(p,255):'';t=s_ot(o);i=o.sourceIndex;if(s_gg('objectID')){n=
s_gg('objectID');x=1;i=1}if(p&&n&&t)qs='&pid='+s_ape(p)+(w?'&pidt='+w:
'')+'&oid='+s_ape(n)+(x?'&oidt='+x:'')+'&ot='+s_ape(t)+(i?'&oi='+i:'')
}s_wd.s_linkName=s_wd.s_linkType=s_wd.s_objectID=s_lnk=s_eo='';if(
!s_wd.s_disableLegacyVars)s_wd.linkName=s_wd.linkType=s_wd.objectID=''
}if(!trk&&!qs)return '';var code='';if(un){if(trk&&s_vs(un,sed))code+=
s_mr(un,sess,q+(qs?qs:s_rq(un)),ta);s_sq(un,trk?'':qs)}else if(
s_wd.s_unl)for(var unn=0;unn<s_wd.s_unl.length;unn++){un=s_wd.s_unl[
unn];if(trk&&s_vs(un,sed))code+=s_mr(un,sess,q+(qs?qs:s_rq(un)),ta)
s_sq(un,trk?'':qs)}return code}function s_dc(un){un=un.toLowerCase()
s_wds(un);s_ca(un);return s_gs(un)}
s_code=s_dc(s_account);if(s_code)document.write(s_code)
//--></script><script language="JavaScript"><!--
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
//--></script><noscript></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: G.3. --></td></tr></table></div>



</body>
</html><!--end-->
