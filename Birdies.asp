<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE>Estadísticas MAJADAHONDA PGA TOUR</TITLE>
<META http-equiv=Content-Type content="text/html; charset=utf-8">
<META content="" name=description>
<META content="" name=keywords>
<LINK href="estadisticas_archivos/default.css" type=TEXT/CSS rel=stylesheet>
<META content="MSHTML 6.00.2800.1170" name=GENERATOR></HEAD>
<BODY link=#000175 bgColor=#29598c leftMargin=0 topMargin=0 marginheight="0" 
marginwidth="0">
<TABLE cellSpacing=0 cellPadding=0 width=770 bgColor=#213829 border=0>
  <TBODY>
  <TR>
    <TD width=283><MAP name=link><AREA shape=RECT alt="" coords=5,3,56,67 
        href="http://www.pgatour.com/"><AREA shape=RECT alt="" 
        coords=62,39,164,70 href="http://www.golfweb.com/"></MAP><IMG height=70 
      alt="PGA TOUR" src="estadisticas_archivos/lower_hdr.jpg" width=283 
      useMap=#link border=0></TD>
    <TD align=middle width=487>
<%
function fnSort(aSort, intAsc, cual, cuantoscampos, cuantosjugadores)
    Dim intTempStore(5)
    Dim i, j 
    'For i = 1 To UBound(aSort) - 1 'cambiado el inicio a 1 desde 0
    For i = 1 To cuantosjugadores
		'For j = i To UBound(aSort)
		For j = i To cuantosjugadores
			if aSort(i,cual)<>"" and aSort(j,cual)<>"" then
				'Sort Ascending
				if intAsc = 0 Then
						if cdbl(aSort(i,cual)) > cdbl(aSort(j,cual)) Then
							for x=1 to cuantoscampos
								intTempStore(x) = aSort(i,x)    
								aSort(i,x) = aSort(j,x)
								aSort(j,x) = intTempStore(x)
							next
					end if 'i > j
				'Sort Descending, que es lo que voy a usar de momento
				Else
						if cdbl(aSort(i,cual)) < cdbl(aSort(j,cual)) Then
							for x=1 to cuantoscampos
								intTempStore(x) = aSort(i,x)    
								aSort(i,x) = aSort(j,x)
								aSort(j,x) = intTempStore(x)
							next
					End if 'i < j
				End if 'intAsc = 1
			end if
		Next 'j
    Next 'i
    fnSort = aSort
 End function 'fnSort         
         
%>
          <SCRIPT language=JavaScript id=_hbc>
if (!_rew) {var _rew="1";}
if (!_bon) {var _bon="";}
if (!_uid) {var _uid="";}
if (!_seg) {var _seg="";}
var _pn="http:/" + location.pathname;
var _cmp="";
var _gp="";
var _gpn="";
var _cmpn="";
var _cp="null";
var _pndef="title";
var _ctdef="full";
var _dlf="n";
var _elf="n";
var _epg="n";
var _hcv=65;
var _mn="we70";
var _gn="";
var _mlc="";
</SCRIPT>
<SCRIPT language=javascript1.1 
   src="estadisticas_archivos/hbe-v65-no10-aff.js" defer></SCRIPT>
 </TD></TR></TBODY></TABLE><NOSCRIPT><IMG height=1 
src="estadisticas_archivos/HG.gif" width=1 border=0> </NOSCRIPT>
<SCRIPT language=JavaScript1.2>
<!--
function ShowLayer(){return};
function OutLayer(){return};
//-->
</SCRIPT>

<TABLE cellSpacing=0 cellPadding=0 width=770 
background=estadisticas_archivos/gn_bg.gif border=0>
  <TBODY>
  <TR>
    <TD align=left height=25>
      <TABLE cellSpacing=0 cellPadding=0 
      background=estadisticas_archivos/stats.htm border=0>
        <TBODY>
        <TR>
          <TD class=gnsbtxt>&nbsp;&nbsp;<A class=gnsbtxt2 id=gnddhomeA 
            onmouseover="ShowLayer('gnddhome', event)" onmouseout=OutLayer(); 
            href="http://www.pgatour.com/index.html" name=gnddhomeA>Home</A></TD>
          <TD class=gnsbtxt>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddplaA 
            onmouseover="ShowLayer('gnddpla', event)" onmouseout=OutLayer(); 
            href="http://www.pgatour.com/players/index.html" 
            name=gnddplaA>Players</A></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddtouA 
            onmouseover="ShowLayer('gnddtou', event)" onmouseout=OutLayer(); 
            href="http://www.pgatour.com/tournaments/index.html" 
            name=gnddtouA>Partidos</A></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddscoA 
            onmouseover="ShowLayer('gnddsco', event)" onmouseout=OutLayer(); 
            href="./resultados.htm" 
            name=gnddscoA>Resultados</A></TD>
          <TD class=gnsbtxt noWrap height=25>&nbsp;&nbsp;<IMG height=25 alt="" 
            hspace=0 src="estadisticas_archivos/gn_left.gif" width=6 
            align=middle border=0></TD>
          <TD class=gnsbtxt2 align=middle 
          background=estadisticas_archivos/gn_middle.gif><A id=gnddstaA 
            onmouseover="ShowLayer('gnddsta', event)" onmouseout=OutLayer(); 
            href="estadisticas.htm" 
            name=gnddstaA>Estadísticas</A></TD>
          <TD class=gnsbtxt noWrap height=25><IMG height=25 alt="" hspace=0 
            src="estadisticas_archivos/gn_right.gif" width=6 align=middle 
            border=0>&nbsp;&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddshpA 
            onmouseover="ShowLayer('gnddshp', event)" onmouseout=OutLayer(); 
            href="http://www.sportsline.com/redir?url=981" 
          name=gnddshpA>Shop</A></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt><A id=gnddmltA 
            onmouseover="ShowLayer('gnddmlt', event)" onmouseout=OutLayer(); 
            href="http://www.pgatour.com/mm/index.html" 
            name=gnddmltA>Multimedia</A></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD>
          <TD class=gnsbtxt>&nbsp;<B>·</B>&nbsp;</TD>
          <TD class=gnsbtxt></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
<SCRIPT language=JavaScript1.2 src="estadisticas_archivos/pd.js" 
type=text/javascript></SCRIPT>

<TABLE class=bk cellSpacing=0 cellPadding=0 width=770 border=0>
  
  <TR>
    <TD vAlign=top width=600><FONT class=a4><B> MAJADAHONDA TOUR 2003&nbsp;
<%              if Request.QueryString("p")<>"" then
					par=Request.QueryString("p")
				else
					par=3
				end if%>
    
    </B><BR> Birdies  
    <%if par<> 0 then
		Response.write "en pares " & par
	else
		Response.Write "totales"
	end if%></FONT> 
      <TABLE cellSpacing=0 cellPadding=0 width=590 border=0>
        
        <TR>
          <TD align=left><SPAN class=v1><B>
</B></SPAN></TD>
          <TD align=right><FONT color=#0000ff><!--
<img src="/images/234x60_sponsor.jpg" width="234" height="60" border="0">
--></FONT></TD></TR></TABLE>
      <DIV class=spacer10><FONT color=#0000ff></FONT></DIV><!-- Do NOT edit by hand, automatically generated in sunday night process -->
      <TABLE cellSpacing=1 cellPadding=2 width="100%" border=0>
        
        <TR>
          <TD align=left><FONT class=a1>Y-T-D statistics through:&nbsp;the 
            Memorial Tournament&nbsp;Jun 01, 2003</FONT></TD></TR></TABLE>
      <DIV class=Table1>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        
        <TR vAlign=top>
          <TD width="100%">
            <TABLE cellSpacing=1 cellPadding=2 width="100%" border=0>
              
              <TR class=tb1>
                <TD colSpan=5><FONT class=v1><B>
                  <TABLE>
                    
                    <TR>
                      <TD><B>Archive:</B></TD>
                      <TD><B>03</B> ·<A href="/stats/2002/r_127.html"><FONT 
                        color=#0000ff>02</FONT></A> ·<A 
                        href="/stats/2001/r_127.html"><FONT 
                        color=#0000ff>01</FONT></A> ·<A 
                        href="/stats/2000/r_127.html"><FONT 
                        color=#0000ff>00</FONT></A> 
                      </TD></TR></TABLE><!--end--></B></FONT></TD></TR>
              <TR class=tb0>
                <%
				sql="select id, nombre from jugadores"
				Set RsJugadores= Server.CreateObject("ADODB.Recordset")
				RsJugadores.Open Sql, Application("conn"),2,3
				RsJugadores.MoveFirst 
                %>
                <TD class=tb0font>Posición</TD>
                <TD class=tb0font>Jugador</TD>
                <TD class=tb0font align=middle>%</TD>                
                <TD class=tb0font align=middle>Birdies</TD>
                <TD class=tb0font align=middle>
                <%if par <> 0 then
					Response.write "Pares " & par &  " jugados"
				else
					Response.Write "hoyos totales"
				end if
					%></TD>
                <%
                contador=0
                if par <> 0 then
					SQL = "SELECT count(hoyos.id)as birdies FROM hoyos INNER JOIN Golpes ON hoyos.Id = Golpes.id_hoyo WHERE"& _
						" (hoyos.par-golpes.golpes=1) AND (hoyos.par=" & par & ") and Golpes.id_jugador=" 
	                SQL1 = "SELECT count(hoyos.id)as hoyos FROM hoyos INNER JOIN Golpes ON hoyos.Id = Golpes.id_hoyo WHERE"& _
					  " (hoyos.par=" & par & ") AND Golpes.id_jugador=" 
				else
					SQL = "SELECT count(hoyos.id)as birdies FROM hoyos INNER JOIN Golpes ON hoyos.Id = Golpes.id_hoyo WHERE"& _
						" (hoyos.par-golpes.golpes=1) and Golpes.id_jugador=" 					
	                SQL1 = "SELECT count(hoyos.id)as hoyos FROM hoyos INNER JOIN Golpes ON hoyos.Id = Golpes.id_hoyo WHERE"& _
					   " Golpes.id_jugador=" 
				end if
				'Response.Write sql
                set Rsgolpes=Server.CreateObject("ADODB.Recordset")
                set Rsgolpes1=Server.CreateObject("ADODB.Recordset")
                Dim Aux(10,5)
                do while not RsJugadores.EOF 
					Rsgolpes.Open Sql & RsJugadores.Fields("id").Value & " and (golpes.putts<>0" &_
					" or golpes.golpes<>0)", Application("conn"),2,3
					Rsgolpes1.Open Sql1 & RsJugadores.Fields("id").Value & " and (golpes.putts<>0" &_
					" or golpes.golpes<>0)", Application("conn"),2,3
					Aux(contador+1,1)=Rsjugadores.Fields("nombre").value
					Aux(contador+1,3)=RsGolpes.Fields("birdies").Value
					Aux(contador+1,4)=RsGolpes1.Fields("hoyos").Value
					if cint(Aux(cint(contador)+1,4)) <> 0 then
						Aux(cint(contador)+1,2) = FormatNumber(Cdbl(Aux(contador+1,3))/Cdbl(Aux(contador+1,4))*100, 2, -1)
					end if
					RsGolpes.Close 
					RsGolpes1.Close 
					contador=contador+1
			        rsjugadores.MoveNext 
                loop
		      set rsgolpes=nothing
		      set rsgolpes1=nothing
              set rsjugadores=nothing
              '*****************
              Ordenada=fnsort(Aux,1,2,4,session("cuantosjugadores"))'function fnSort(aSort, intAsc, cual, cuantoscampos, cuantosjugadores)
			  '*******************
              For x=1 to contador
              if par=0 then
					par=1%>
					<TR class=tb1 vAlign=top>
				<%else
					par=0%>
						<TR class=tb2 vAlign=top>
				<%end if%>	
						<TD><%=x%></TD>
						<TD><%=Ordenada(x,1)%></TD>
						<TD align=center><FONT 
		                  color=#0000ff><%=Ordenada(x,2)%></FONT></TD>
						<TD align=middle><%=Ordenada(x,3)%></TD>              
						<TD align=middle><%=Ordenada(x,4)%></TD></TR>              
		      <%next%>
			</TABLE>
            <TABLE cellSpacing=1 cellPadding=2 width="100%" border=0>
              
              <TR class=tb0>
                <TD align=left>The All Around statistic is computed by 
                  totalling a player's rank in each of the following statistics: 
                  Scoring Leaders, Putting Leaders, Eagle Leaders, Birdie 
                  Leaders, Sand Saves, Greens in Regulation, Driving Distance 
                  and Driving Accuracy.</TD></TR>
              <TR class=tb0>
                <TD align=left>Titleist logo signifies Titleist golfball 
                usage</TD></TR></TABLE>
            <TABLE cellSpacing=0 cellPadding=4 width="100%" border=0>
              
              <TR bgColor=#eef2f2>
                <TD align=right></TD></TR></TABLE></TD></TR></TABLE></DIV><!--end--></TD>
    <TD width=10>&nbsp;</TD>
    <TD class=bk vAlign=top align=middle width=150>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
        
        <TR>
          <TD height=10></TD></TR></TABLE> 
      <P></P>
</TD>
    <TD width=10>&nbsp;</TD></TR></TABLE>
<TABLE class=bk cellSpacing=0 cellPadding=0 width=770 border=0>
  
  <TR>
    <TD width=10>&nbsp;</TD>
    <TD vAlign=bottom align=middle width=590></TD>
    <TD width=170>&nbsp;</TD></TR></TABLE>
<TABLE class=bk cellSpacing=0 cellPadding=0 width=770 border=0>
  
  <TR>
    <TD colSpan=3></TD></TR>
  <TR vAlign=top>
    <TD align=middle width=127><A href="http://www.ibm.com/linux" 
      target=_blank><IMG height=42 alt=IBM 
      src="./imagenes/ibm_footer.gif" width=107 
      border=0></A></TD>
    <TD width=10>&nbsp;</TD>
    <TD class=ftrlink align=middle 
      width=633><STRONG>&nbsp;&nbsp;&nbsp;</STRONG><A 
      href="/information/index.html"><FONT color=#0000ff><STRONG>About 
      Us</STRONG></FONT></A><STRONG> | </STRONG><A href="/tos.html"><FONT 
      color=#0000ff><STRONG>Terms of Service</STRONG></FONT></A><STRONG> | 
      </STRONG><A href="/information/partner.html"><FONT 
      color=#0000ff><STRONG>TOUR Partners</STRONG></FONT></A><STRONG> | 
      </STRONG><A href="/information/licensees.html"><FONT 
      color=#0000ff><STRONG>Licensees</STRONG></FONT></A><STRONG> | </STRONG><A 
      href="/information/privacy.html"><FONT color=#0000ff><STRONG>Privacy 
      Statement</STRONG></FONT></A><STRONG> | </STRONG><A 
      href="/help/index.html"><FONT 
      color=#0000ff><STRONG>Feedback</STRONG></FONT></A><STRONG> | </STRONG><A 
      href="http://www.sportsline.com/redir?url=990" target=_blank><FONT 
      color=#0000ff><STRONG>PGA TOUR Partners Club</STRONG></FONT></A><BR><FONT 
      class=ftrtxt><STRONG>    © 2003, MAJADAHONDA&nbsp;PGA TOUR 
      </STRONG>
      <P></P></FONT></TD></TR></TABLE><!-- /html/pgatour/includes/ads/T_PGAstats_ftr.inc  -->
<STRONG>
</STRONG> <NOSCRIPT><IFRAME marginWidth=0 marginHeight=0 
src="http://ads.specificpop.com/code?pid=279&amp;gid=17" frameBorder=0 width=0 
scrolling=no height=0>
</IFRAME></IFRAME></IFRAME></IFRAME></IFRAME></IFRAME></NOSCRIPT><!--end--></BODY></HTML>
