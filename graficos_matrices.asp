<HTML>
<object id=ChartSpace1 classid=CLSID:0002E556-0000-0000-C000-000000000046 style="width:100%;height:350"></object>

<script language=vbs>
Sub Window_OnLoad()

    Dim categories(3)
    Dim values(3)
    Dim chConstants

    ' Create an array of strings representing the categories.
    ' The categories will be the same for all three series.
    categories(0) = "Car"
    categories(1) = "Sport-Utility"
    categories(2) = "Truck"
    categories(3) = "Minivan"

    ' Clear the contents of the chart workspace. This removes
    ' any old charts that may already exist and leaves the chart workspace
    ' completely empty. One chart object is then added.
    ChartSpace1.Clear
    ChartSpace1.Charts.Add
    Set chConstants = ChartSpace1.Constants

    ' Add three series to the chart.
    ChartSpace1.Charts(0).SeriesCollection.Add
    ChartSpace1.Charts(0).SeriesCollection.Add
    ChartSpace1.Charts(0).SeriesCollection.Add

    ' Series one contains sales growth data for 1998.
    ' Set the series caption (the text that appears in the legend).
    ChartSpace1.Charts(0).SeriesCollection(0).Caption = "1998"

    ' Set the categories for the first series (this collection is zero-based)
    ChartSpace1.Charts(0).SeriesCollection(0).SetData chConstants.chDimCategories, chConstants.chDataLiteral, categories

    values(0) = 0.2
    values(1) = 0.06
    values(2) = 0.17
    values(3) = 0.13

    ChartSpace1.Charts(0).SeriesCollection(0).Caption = "1998"
    ChartSpace1.Charts(0).SeriesCollection(0).SetData chConstants.chDimCategories, chConstants.chDataLiteral, categories
    ChartSpace1.Charts(0).SeriesCollection(0).SetData chConstants.chDimValues, chConstants.chDataLiteral, values

    ' Series two contains sales growth data for 1999.
    ' Update the values array, then set the chart data.
    values(0) = 0.38
    values(1) = 0.82
    values(2) = 0.28
    values(3) = 0.62

    ChartSpace1.Charts(0).SeriesCollection(1).Caption = "1999"
    ChartSpace1.Charts(0).SeriesCollection(1).SetData chConstants.chDimCategories, chConstants.chDataLiteral, categories
    ChartSpace1.Charts(0).SeriesCollection(1).SetData chConstants.chDimValues, chConstants.chDataLiteral, values

    ' Series two contains sales growth data for 2000.
    ' Update the values array, and then set the chart data.
    values(0) = 0.42
    values(1) = 0.12
    values(2) = 0.55
    values(3) = 0.25

    ChartSpace1.Charts(0).SeriesCollection(2).Caption = "2000"
    ChartSpace1.Charts(0).SeriesCollection(2).SetData chConstants.chDimCategories, chConstants.chDataLiteral, categories
    ChartSpace1.Charts(0).SeriesCollection(2).SetData chConstants.chDimValues, chConstants.chDataLiteral, values

    ' Make the chart legend visible, format the left value axis as percentage,
    ' and specify that value gridlines are at 10% intervals.
    ChartSpace1.Charts(0).HasLegend = True
    ChartSpace1.Charts(0).Axes(chConstants.chAxisPositionLeft).NumberFormat = "0%"
    ChartSpace1.Charts(0).Axes(chConstants.chAxisPositionLeft).MajorUnit = 0.1
End Sub
</script>

</HTML>
