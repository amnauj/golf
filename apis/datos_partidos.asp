<%
        'Hay que actualizar aquí y en partidos_json.asp (core.webapi/partidos_json)
        if request.querystring("idTournament") = "" then 
            sqlPartidos = "select nombre, c.id as id_campo, p.id as id_partido from campos c inner join partidos p on p.id_campo = c.id order by p.id desc"
        else
            sqlPartidos = "select nombre, c.id as id_campo, p.id as id_partido from campos c inner join partidos p on p.id_campo = c.id where c.id= " & request.querystring("idTournament") & " order by p.id desc"        
        end if
        set RsPartidos = Application("conn").execute(sqlPartidos)
        Nombre_campo = RsPartidos("nombre").value
        idCampo = RsPartidos("id_campo").value
        
        
        Mensaje_Json = "{""uid"": ""s:1100~l:1106~e:3735"",""tournament"": {""numberOfRounds"": 4,""id"": ""241"",""cutCount"": 0,""cutScore"": 0," &_
        """displayName"": ""Las Rozas World Golf Championships"",""cutRound"": 0,""major"": false},""displayPurse"": ""$9,750,000"",""courses"": ["
        
       contadorPartidos = 0
       ultimo_partido = 0
       id_campo_actual = 0
       do while (not rsPartidos.eof and contadorPartidos < 21)
            
            'Primero cargar los campos, después los datos de los hoyos de cada campo.

                if ultimo_partido <> RsPartidos.Fields("id_partido") then
                    ultimo_partido = RsPartidos.Fields("id_partido")
                    contadorPartidos = contadorPartidos + 1
                end if
                
                if id_campo_actual <> cint(RsPartidos.Fields("id_campo")) then
                    sql="select sum(metros) as m_total, c.id as id_campo, c.nombre as nombre, c.par_campo as par from campos c inner join hoyos h on h.id_campo = c.id  where c.id=" & RsPartidos.Fields("id_campo") & " and h.tees = 'h' group by c.id, c.nombre, c.par_campo" 
                            
                    RsCampoInfo = Application("conn").execute(sql)
                    nombreCampo = RsCampoInfo("nombre").value
                    idCampo = RsCampoInfo("id_campo").value
                    mTotal = RsCampoInfo("m_total").value
                    parCampo = RsCampoInfo("par").value
                
                    sql="select p_out = case when sum(par) is null then 0 else sum(par) end from campos c inner join hoyos h on h.id_campo = c.id  where c.id=" & RsPartidos.Fields("id_campo") & " and h.tees = 'h' and h.numero < 10" 

                    RsCampoInfo = Application("conn").execute(sql)
                    parOut = RsCampoInfo("p_out").value

                    sql="select p_in = case when sum(par) is null then 0 else sum(par) end from campos c inner join hoyos h on h.id_campo = c.id  where c.id=" & RsPartidos.Fields("id_campo") & " and h.tees = 'h' and h.numero > 9" 
                    
                    RsCampoInfo = Application("conn").execute(sql)
                    parIn = RsCampoInfo("p_in").value

                    id_campo_actual = cint(RsPartidos.Fields("id_campo"))
                end if

                Mensaje_Json = Mensaje_Json & "{""totalYards"": " & mTotal & ",""id"": """ & idCampo & """,""shotsToPar"": " & parCampo & "," &_
                """parIn"": " & parIn & ",""name"": """ & nombreCampo & """,""parOut"": "& parOut &",""holes"": ["
              
                
                sql = "select numero, par, metros from hoyos where id_campo=" & RsPartidos.Fields("id_campo") & " and tees = 'h' order by numero asc"
                
                set RsCamposHoyos = Application("conn").execute(sql)
                
                do while not RsCamposHoyos.eof
                    Mensaje_Json = Mensaje_Json & "{ ""totalYards"": " & RsCamposHoyos.fields("metros") & ",""shotsToPar"": " & RsCamposHoyos.fields("par") & ",""number"": " & RsCamposHoyos.fields("numero") &" },"
                    RsCamposHoyos.moveNext
                loop    
            RsPartidos.moveNext
            Mensaje_Json = Left(Mensaje_Json, len(Mensaje_Json) - 1)
            Mensaje_Json = Mensaje_Json & "]},"
		loop
        Mensaje_Json = Left(Mensaje_Json, len(Mensaje_Json) - 1) & "],"

    Mensaje_Json = Mensaje_Json & """playoffType"": { ""id"": 0,""minimumHoles"": 1,""description"": ""Sudden Death""},""purse"": 9750000," &_
    """status"": {""type"": {""id"": ""3"",""description"": ""Final"",""name"": ""STATUS_FINAL"",""state"": ""post"",""completed"": true}}," &_
    """winner"": {""id"": ""3448"",""displayName"": ""Dustin Johnson""},""endDate"": ""2017-03-05T05:00Z"",""links"": [{""text"": ""Summary""," &_
    """isPremium"": false,""shortText"": ""Summary"",""rel"": [""summary"",""desktop"",""event""],""language"": ""en""," &_
    """href"": ""http://www.espn.com/golf/leaderboard?tournamentId=3735"",""isExternal"": false}],""date"": ""2017-03-02T05:00Z""," &_
    """league"": {""id"": ""1106"",""name"": ""Pro Golfer's Association"",""slug"": ""pga"",""shortName"": ""PGA Tour"",""abbreviation"": ""PGA""" &_
    "},""hasCourseStats"": true,""hasPlayerStats"": true,""id"": ""3735"",""competitions"": [{""onWatchESPN"": false,""scoringSystem"": {""id"": ""3""," &_
    """name"": ""Medal""},""status"": {""type"": {""shortDetail"": ""Complete"",""detail"": ""Complete"",""description"": ""Final"",""state"": ""post""" &_
    "},""period"": 4},"
    Mensaje_Json = Mensaje_Json & """competitors"": [ "

            'Ahora para recuperar los partidos
            set RsPartidos = Application("conn").execute(sqlPartidos)
            Do while not RsPartidos.eof
                Mensaje_Json = Mensaje_Json & "{""uid"": ""s:1100~l:1106~a:" & RsPartidos.Fields("id_partido").value & """,""id"": """ &_
                 RsPartidos.Fields("id_partido").value & """,""status"": {""position"": {""id"": ""2"",""isTie"": false,""displayName"": ""2""" &_
                "},""playoff"": false,""thru"": 18,""behindCurrentRound"": false,""teeTime"": ""2017-03-05T17:48Z"",""displayValue"": ""F""," &_
                """type"": {""id"": ""2"",""shortDetail"": ""F"",""detail"": ""Finish"",""description"": ""Finish"",""name"": ""STATUS_FINISH""," &_
                """state"": ""post"",""completed"": true},""period"": 4,""displayThru"": ""18"",""startHole"": 1,""hole"": 18},""sortOrder"": 2," &_
                """movement"": -5,""linescores"": ["
                 For x = 1 to 4 
                    Mensaje_Json = Mensaje_Json & "{""teeTime"": ""2017-03-02T20:47Z"",""hasStream"": false,""value"": 69,""currentPosition"": 14," &_
                """outScore"": 33,""period"": " & x & ",""displayValue"": ""-2"",""inScore"": 36,""startPosition"": 14},"
                next
                Mensaje_Json = Left(Mensaje_Json, len(Mensaje_Json)-1)
                Mensaje_Json = Mensaje_Json & "],""score"": {""value"": 271,""displayValue"": ""-13""},""featured"": false,""athlete"": {" &_
                """id"": """ & RsPartidos.Fields("id_partido").value & """,""flag"": {""alt"": """ & RsPartidos.Fields("nombre").value & """," &_
                """href"": ""/imagenes/" & RsPartidos.Fields("id_campo").value & ".gif""},""links"": [{"&_ 
                """href"": ""http://www.espn.com/golf/player/_/id/5539/tommy-fleetwood""}],""displayName"": """ & RsPartidos.Fields("nombre").value & """" &_
                "},""statistics"": [{""name"": ""scoreToPar"",""displayValue"": ""-13""},{""name"": ""officialAmount"",""displayValue"": ""$1,045,000.00""" &_
                "},{""name"": ""cupPoints"",""displayValue"": ""0""}]},"
            RsPartidos.moveNext
        loop

        Mensaje_Json = left(Mensaje_Json, len(Mensaje_Json)-1)

        Mensaje_Json = Mensaje_Json & "],""dataFormat"": ""API"",""leaders"": [{""shortDisplayName"": ""Driving Distance"",""name"": ""driveDistAvg""," &_
        """abbreviation"": ""YDS/DRV"",""displayName"": ""Driving Distance"",""leaders"": [{""value"": 333.79998779296875," &_
        """displayValue"": ""333.8"",""athlete"": {""id"": ""780"",""displayName"": ""Bubba Watson""}}]},{""shortDisplayName"": ""Driving Accuracy""," &_
        """name"": ""fwysHit"",""abbreviation"": ""DRV ACC"",""displayName"": ""Driving Accuracy"",""leaders"": [{""value"": 80.36000061035156,"&_
        """displayValue"": ""80.4"",""athlete"": {""id"": ""9037"",""displayName"": ""Matthew Fitzpatrick""}}]},{""shortDisplayName"": ""Greens in Regulation""," &_
        """name"": ""gir"",""abbreviation"": ""GIR"",""displayName"": ""Greens in Regulation"",""leaders"": [{""value"": 80.55999755859375,""displayValue"": ""80.6""," &_
        """athlete"": {""id"": ""4831"",""displayName"": ""Joost Luiten""}}]},{""shortDisplayName"": ""Putts per GIR"",""name"": ""puttsGirAvg""," &_
        """abbreviation"": ""PP GIR"",""displayName"": ""Putts per GIR"",""leaders"": [{""value"": 1.5219999551773071,""displayValue"": ""1.522""," &_
        """athlete"": {""id"": ""4848"",""displayName"": ""Justin Thomas""}}]}],""broadcasts"": [{""region"": ""us"",""lang"": ""en"",""media"": {""id"": ""5""," &_
        """name"": ""NBC"",""callLetters"": ""NBC"",""shortName"": ""NBC""}},{""region"": ""us"",""lang"": ""en"",""media"": {""id"": ""9"",""name"": ""TGC""," &_
        """callLetters"": ""TGC"",""shortName"": ""TGC""}}]}],""defendingChampion"": {""season"": 2016,""athlete"": {""id"": ""388"",""displayName"": ""Adam Scott""" &_
        "}},""season"": {""year"": 2017},""primary"": false,""name"": ""World Golf Championships"",""shortName"": ""World Golf Championships""" &_ 
        "}"


    set RsPartidos=nothing	
    set RsCampoInfo=nothing
    set RsCamposHoyos=nothing
    response.Clear()
	Response.ContentType = "application/json; charset=utf-8"
    response.Write Mensaje_Json
%>