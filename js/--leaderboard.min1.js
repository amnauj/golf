! function (root, factory) {
    if ("function" == typeof define && define.amd) define(["backbone", "underscore"], function (Backbone, _) {
        return root.Marionette = root.Mn = factory(root, Backbone, _)
    });
    else if ("undefined" != typeof exports) {
        var Backbone = require("backbone"),
            _ = require("underscore");
        module.exports = factory(root, Backbone, _)
    } else root.Marionette = root.Mn = factory(root, root.Backbone, root._)
}(this, function (root, Backbone, _) {
    "use strict";
    ! function (Backbone, _) {
        var previousChildViewContainer = Backbone.ChildViewContainer;
        return Backbone.ChildViewContainer = function (Backbone, _) {
            var Container = function (views) {
                this._views = {}, this._indexByModel = {}, this._indexByCustom = {}, this._updateLength(), _.each(views, this.add, this)
            };
            _.extend(Container.prototype, {
                add: function (view, customIndex) {
                    var viewCid = view.cid;
                    return this._views[viewCid] = view, view.model && (this._indexByModel[view.model.cid] = viewCid), customIndex && (this._indexByCustom[customIndex] = viewCid), this._updateLength(), this
                },
                findByModel: function (model) {
                    return this.findByModelCid(model.cid)
                },
                findByModelCid: function (modelCid) {
                    var viewCid = this._indexByModel[modelCid];
                    return this.findByCid(viewCid)
                },
                findByCustom: function (index) {
                    var viewCid = this._indexByCustom[index];
                    return this.findByCid(viewCid)
                },
                findByIndex: function (index) {
                    return _.values(this._views)[index]
                },
                findByCid: function (cid) {
                    return this._views[cid]
                },
                remove: function (view) {
                    var viewCid = view.cid;
                    return view.model && delete this._indexByModel[view.model.cid], _.any(this._indexByCustom, function (cid, key) {
                        if (cid === viewCid) return delete this._indexByCustom[key], !0
                    }, this), delete this._views[viewCid], this._updateLength(), this
                },
                call: function (method) {
                    this.apply(method, _.tail(arguments))
                },
                apply: function (method, args) {
                    _.each(this._views, function (view) {
                        _.isFunction(view[method]) && view[method].apply(view, args || [])
                    })
                },
                _updateLength: function () {
                    this.length = _.size(this._views)
                }
            });
            var methods = ["forEach", "each", "map", "find", "detect", "filter", "select", "reject", "every", "all", "some", "any", "include", "contains", "invoke", "toArray", "first", "initial", "rest", "last", "without", "isEmpty", "pluck", "reduce"];
            return _.each(methods, function (method) {
                Container.prototype[method] = function () {
                    var views = _.values(this._views),
                        args = [views].concat(_.toArray(arguments));
                    return _[method].apply(_, args)
                }
            }), Container
        }(Backbone, _), Backbone.ChildViewContainer.VERSION = "0.1.7", Backbone.ChildViewContainer.noConflict = function () {
            return Backbone.ChildViewContainer = previousChildViewContainer, this
        }, Backbone.ChildViewContainer
    }(Backbone, _),
        function (Backbone, _) {
            var previousWreqr = Backbone.Wreqr,
                Wreqr = Backbone.Wreqr = {};
            return Backbone.Wreqr.VERSION = "1.3.3", Backbone.Wreqr.noConflict = function () {
                return Backbone.Wreqr = previousWreqr, this
            }, Wreqr.Handlers = function (Backbone, _) {
                var Handlers = function (options) {
                    this.options = options, this._wreqrHandlers = {}, _.isFunction(this.initialize) && this.initialize(options)
                };
                return Handlers.extend = Backbone.Model.extend, _.extend(Handlers.prototype, Backbone.Events, {
                    setHandlers: function (handlers) {
                        _.each(handlers, function (handler, name) {
                            var context = null;
                            _.isObject(handler) && !_.isFunction(handler) && (context = handler.context, handler = handler.callback), this.setHandler(name, handler, context)
                        }, this)
                    },
                    setHandler: function (name, handler, context) {
                        var config = {
                            callback: handler,
                            context: context
                        };
                        this._wreqrHandlers[name] = config, this.trigger("handler:add", name, handler, context)
                    },
                    hasHandler: function (name) {
                        return !!this._wreqrHandlers[name]
                    },
                    getHandler: function (name) {
                        var config = this._wreqrHandlers[name];
                        if (config) return function () {
                            return config.callback.apply(config.context, arguments)
                        }
                    },
                    removeHandler: function (name) {
                        delete this._wreqrHandlers[name]
                    },
                    removeAllHandlers: function () {
                        this._wreqrHandlers = {}
                    }
                }), Handlers
            }(Backbone, _), Wreqr.CommandStorage = function () {
                var CommandStorage = function (options) {
                    this.options = options, this._commands = {}, _.isFunction(this.initialize) && this.initialize(options)
                };
                return _.extend(CommandStorage.prototype, Backbone.Events, {
                    getCommands: function (commandName) {
                        var commands = this._commands[commandName];
                        return commands || (commands = {
                            command: commandName,
                            instances: []
                        }, this._commands[commandName] = commands), commands
                    },
                    addCommand: function (commandName, args) {
                        var command = this.getCommands(commandName);
                        command.instances.push(args)
                    },
                    clearCommands: function (commandName) {
                        var command = this.getCommands(commandName);
                        command.instances = []
                    }
                }), CommandStorage
            }(), Wreqr.Commands = function (Wreqr, _) {
                return Wreqr.Handlers.extend({
                    storageType: Wreqr.CommandStorage,
                    constructor: function (options) {
                        this.options = options || {}, this._initializeStorage(this.options), this.on("handler:add", this._executeCommands, this), Wreqr.Handlers.prototype.constructor.apply(this, arguments)
                    },
                    execute: function (name) {
                        name = arguments[0];
                        var args = _.rest(arguments);
                        this.hasHandler(name) ? this.getHandler(name).apply(this, args) : this.storage.addCommand(name, args)
                    },
                    _executeCommands: function (name, handler, context) {
                        var command = this.storage.getCommands(name);
                        _.each(command.instances, function (args) {
                            handler.apply(context, args)
                        }), this.storage.clearCommands(name)
                    },
                    _initializeStorage: function (options) {
                        var storage, StorageType = options.storageType || this.storageType;
                        storage = _.isFunction(StorageType) ? new StorageType : StorageType, this.storage = storage
                    }
                })
            }(Wreqr, _), Wreqr.RequestResponse = function (Wreqr, _) {
                return Wreqr.Handlers.extend({
                    request: function (name) {
                        if (this.hasHandler(name)) return this.getHandler(name).apply(this, _.rest(arguments))
                    }
                })
            }(Wreqr, _), Wreqr.EventAggregator = function (Backbone, _) {
                var EA = function () { };
                return EA.extend = Backbone.Model.extend, _.extend(EA.prototype, Backbone.Events), EA
            }(Backbone, _), Wreqr.Channel = function (Wreqr) {
                var Channel = function (channelName) {
                    this.vent = new Backbone.Wreqr.EventAggregator, this.reqres = new Backbone.Wreqr.RequestResponse, this.commands = new Backbone.Wreqr.Commands, this.channelName = channelName
                };
                return _.extend(Channel.prototype, {
                    reset: function () {
                        return this.vent.off(), this.vent.stopListening(), this.reqres.removeAllHandlers(), this.commands.removeAllHandlers(), this
                    },
                    connectEvents: function (hash, context) {
                        return this._connect("vent", hash, context), this
                    },
                    connectCommands: function (hash, context) {
                        return this._connect("commands", hash, context), this
                    },
                    connectRequests: function (hash, context) {
                        return this._connect("reqres", hash, context), this
                    },
                    _connect: function (type, hash, context) {
                        if (hash) {
                            context = context || this;
                            var method = "vent" === type ? "on" : "setHandler";
                            _.each(hash, function (fn, eventName) {
                                this[type][method](eventName, _.bind(fn, context))
                            }, this)
                        }
                    }
                }), Channel
            }(Wreqr), Wreqr.radio = function (Wreqr, _) {
                var Radio = function () {
                    this._channels = {}, this.vent = {}, this.commands = {}, this.reqres = {}, this._proxyMethods()
                };
                _.extend(Radio.prototype, {
                    channel: function (channelName) {
                        if (!channelName) throw new Error("Channel must receive a name");
                        return this._getChannel(channelName)
                    },
                    _getChannel: function (channelName) {
                        var channel = this._channels[channelName];
                        return channel || (channel = new Wreqr.Channel(channelName), this._channels[channelName] = channel), channel
                    },
                    _proxyMethods: function () {
                        _.each(["vent", "commands", "reqres"], function (system) {
                            _.each(messageSystems[system], function (method) {
                                this[system][method] = proxyMethod(this, system, method)
                            }, this)
                        }, this)
                    }
                });
                var messageSystems = {
                    vent: ["on", "off", "trigger", "once", "stopListening", "listenTo", "listenToOnce"],
                    commands: ["execute", "setHandler", "setHandlers", "removeHandler", "removeAllHandlers"],
                    reqres: ["request", "setHandler", "setHandlers", "removeHandler", "removeAllHandlers"]
                },
                    proxyMethod = function (radio, system, method) {
                        return function (channelName) {
                            var messageSystem = radio._getChannel(channelName)[system];
                            return messageSystem[method].apply(messageSystem, _.rest(arguments))
                        }
                    };
                return new Radio
            }(Wreqr, _), Backbone.Wreqr
        }(Backbone, _);
    var previousMarionette = root.Marionette,
        previousMn = root.Mn,
        Marionette = Backbone.Marionette = {};
    Marionette.VERSION = "2.4.2", Marionette.noConflict = function () {
        return root.Marionette = previousMarionette, root.Mn = previousMn, this
    }, Backbone.Marionette = Marionette, Marionette.Deferred = Backbone.$.Deferred, Marionette.extend = Backbone.Model.extend, Marionette.isNodeAttached = function (el) {
        return Backbone.$.contains(document.documentElement, el)
    }, Marionette.mergeOptions = function (options, keys) {
        options && _.extend(this, _.pick(options, keys))
    }, Marionette.getOption = function (target, optionName) {
        if (target && optionName) return target.options && void 0 !== target.options[optionName] ? target.options[optionName] : target[optionName]
    }, Marionette.proxyGetOption = function (optionName) {
        return Marionette.getOption(this, optionName)
    }, Marionette._getValue = function (value, context, params) {
        return _.isFunction(value) && (value = params ? value.apply(context, params) : value.call(context)), value
    }, Marionette.normalizeMethods = function (hash) {
        return _.reduce(hash, function (normalizedHash, method, name) {
            return _.isFunction(method) || (method = this[method]), method && (normalizedHash[name] = method), normalizedHash
        }, {}, this)
    }, Marionette.normalizeUIString = function (uiString, ui) {
        return uiString.replace(/@ui\.[a-zA-Z_$0-9]*/g, function (r) {
            return ui[r.slice(4)]
        })
    }, Marionette.normalizeUIKeys = function (hash, ui) {
        return _.reduce(hash, function (memo, val, key) {
            var normalizedKey = Marionette.normalizeUIString(key, ui);
            return memo[normalizedKey] = val, memo
        }, {})
    }, Marionette.normalizeUIValues = function (hash, ui, properties) {
        return _.each(hash, function (val, key) {
            _.isString(val) ? hash[key] = Marionette.normalizeUIString(val, ui) : _.isObject(val) && _.isArray(properties) && (_.extend(val, Marionette.normalizeUIValues(_.pick(val, properties), ui)), _.each(properties, function (property) {
                var propertyVal = val[property];
                _.isString(propertyVal) && (val[property] = Marionette.normalizeUIString(propertyVal, ui))
            }))
        }), hash
    }, Marionette.actAsCollection = function (object, listProperty) {
        var methods = ["forEach", "each", "map", "find", "detect", "filter", "select", "reject", "every", "all", "some", "any", "include", "contains", "invoke", "toArray", "first", "initial", "rest", "last", "without", "isEmpty", "pluck"];
        _.each(methods, function (method) {
            object[method] = function () {
                var list = _.values(_.result(this, listProperty)),
                    args = [list].concat(_.toArray(arguments));
                return _[method].apply(_, args)
            }
        })
    };
    var deprecate = Marionette.deprecate = function (message, test) {
        _.isObject(message) && (message = message.prev + " is going to be removed in the future. Please use " + message.next + " instead." + (message.url ? " See: " + message.url : "")), void 0 !== test && test || deprecate._cache[message] || (deprecate._warn("Deprecation warning: " + message), deprecate._cache[message] = !0)
    };
    deprecate._warn = "undefined" != typeof console && (console.warn || console.log) || function () { }, deprecate._cache = {}, Marionette._triggerMethod = function () {
        function getEventName(match, prefix, eventName) {
            return eventName.toUpperCase()
        }
        var splitter = /(^|:)(\w)/gi;
        return function (context, event, args) {
            var noEventArg = arguments.length < 3;
            noEventArg && (args = event, event = args[0]);
            var result, methodName = "on" + event.replace(splitter, getEventName),
                method = context[methodName];
            return _.isFunction(method) && (result = method.apply(context, noEventArg ? _.rest(args) : args)), _.isFunction(context.trigger) && (noEventArg + args.length > 1 ? context.trigger.apply(context, noEventArg ? args : [event].concat(_.drop(args, 0))) : context.trigger(event)), result
        }
    }(), Marionette.triggerMethod = function (event) {
        return Marionette._triggerMethod(this, arguments)
    }, Marionette.triggerMethodOn = function (context) {
        var fnc = _.isFunction(context.triggerMethod) ? context.triggerMethod : Marionette.triggerMethod;
        return fnc.apply(context, _.rest(arguments))
    }, Marionette.MonitorDOMRefresh = function (view) {
        function handleShow() {
            view._isShown = !0, triggerDOMRefresh()
        }

        function handleRender() {
            view._isRendered = !0, triggerDOMRefresh()
        }

        function triggerDOMRefresh() {
            view._isShown && view._isRendered && Marionette.isNodeAttached(view.el) && _.isFunction(view.triggerMethod) && view.triggerMethod("dom:refresh")
        }
        view.on({
            show: handleShow,
            render: handleRender
        })
    },
        function (Marionette) {
            function bindFromStrings(target, entity, evt, methods) {
                var methodNames = methods.split(/\s+/);
                _.each(methodNames, function (methodName) {
                    var method = target[methodName];
                    if (!method) throw new Marionette.Error('Method "' + methodName + '" was configured as an event handler, but does not exist.');
                    target.listenTo(entity, evt, method)
                })
            }

            function bindToFunction(target, entity, evt, method) {
                target.listenTo(entity, evt, method)
            }

            function unbindFromStrings(target, entity, evt, methods) {
                var methodNames = methods.split(/\s+/);
                _.each(methodNames, function (methodName) {
                    var method = target[methodName];
                    target.stopListening(entity, evt, method)
                })
            }

            function unbindToFunction(target, entity, evt, method) {
                target.stopListening(entity, evt, method)
            }

            function iterateEvents(target, entity, bindings, functionCallback, stringCallback) {
                if (entity && bindings) {
                    if (!_.isObject(bindings)) throw new Marionette.Error({
                        message: "Bindings must be an object or function.",
                        url: "marionette.functions.html#marionettebindentityevents"
                    });
                    bindings = Marionette._getValue(bindings, target), _.each(bindings, function (methods, evt) {
                        _.isFunction(methods) ? functionCallback(target, entity, evt, methods) : stringCallback(target, entity, evt, methods)
                    })
                }
            }
            Marionette.bindEntityEvents = function (target, entity, bindings) {
                iterateEvents(target, entity, bindings, bindToFunction, bindFromStrings)
            }, Marionette.unbindEntityEvents = function (target, entity, bindings) {
                iterateEvents(target, entity, bindings, unbindToFunction, unbindFromStrings)
            }, Marionette.proxyBindEntityEvents = function (entity, bindings) {
                return Marionette.bindEntityEvents(this, entity, bindings)
            }, Marionette.proxyUnbindEntityEvents = function (entity, bindings) {
                return Marionette.unbindEntityEvents(this, entity, bindings)
            }
        }(Marionette);
    var errorProps = ["description", "fileName", "lineNumber", "name", "message", "number"];
    return Marionette.Error = Marionette.extend.call(Error, {
        urlRoot: "http://marionettejs.com/docs/v" + Marionette.VERSION + "/",
        constructor: function (message, options) {
            _.isObject(message) ? (options = message, message = options.message) : options || (options = {});
            var error = Error.call(this, message);
            _.extend(this, _.pick(error, errorProps), _.pick(options, errorProps)), this.captureStackTrace(), options.url && (this.url = this.urlRoot + options.url)
        },
        captureStackTrace: function () {
            Error.captureStackTrace && Error.captureStackTrace(this, Marionette.Error)
        },
        toString: function () {
            return this.name + ": " + this.message + (this.url ? " See: " + this.url : "")
        }
    }), Marionette.Error.extend = Marionette.extend, Marionette.Callbacks = function () {
        this._deferred = Marionette.Deferred(), this._callbacks = []
    }, _.extend(Marionette.Callbacks.prototype, {
        add: function (callback, contextOverride) {
            var promise = _.result(this._deferred, "promise");
            this._callbacks.push({
                cb: callback,
                ctx: contextOverride
            }), promise.then(function (args) {
                contextOverride && (args.context = contextOverride), callback.call(args.context, args.options)
            })
        },
        run: function (options, context) {
            this._deferred.resolve({
                options: options,
                context: context
            })
        },
        reset: function () {
            var callbacks = this._callbacks;
            this._deferred = Marionette.Deferred(), this._callbacks = [], _.each(callbacks, function (cb) {
                this.add(cb.cb, cb.ctx)
            }, this)
        }
    }), Marionette.Controller = function (options) {
        this.options = options || {}, _.isFunction(this.initialize) && this.initialize(this.options)
    }, Marionette.Controller.extend = Marionette.extend, _.extend(Marionette.Controller.prototype, Backbone.Events, {
        destroy: function () {
            return Marionette._triggerMethod(this, "before:destroy", arguments), Marionette._triggerMethod(this, "destroy", arguments), this.stopListening(), this.off(), this
        },
        triggerMethod: Marionette.triggerMethod,
        mergeOptions: Marionette.mergeOptions,
        getOption: Marionette.proxyGetOption
    }), Marionette.Object = function (options) {
        this.options = _.extend({}, _.result(this, "options"), options), this.initialize.apply(this, arguments)
    }, Marionette.Object.extend = Marionette.extend, _.extend(Marionette.Object.prototype, Backbone.Events, {
        initialize: function () { },
        destroy: function () {
            return this.triggerMethod("before:destroy"), this.triggerMethod("destroy"), this.stopListening(), this
        },
        triggerMethod: Marionette.triggerMethod,
        mergeOptions: Marionette.mergeOptions,
        getOption: Marionette.proxyGetOption,
        bindEntityEvents: Marionette.proxyBindEntityEvents,
        unbindEntityEvents: Marionette.proxyUnbindEntityEvents
    }), Marionette.Region = Marionette.Object.extend({
        constructor: function (options) {
            if (this.options = options || {}, this.el = this.getOption("el"), this.el = this.el instanceof Backbone.$ ? this.el[0] : this.el, !this.el) throw new Marionette.Error({
                name: "NoElError",
                message: 'An "el" must be specified for a region.'
            });
            this.$el = this.getEl(this.el), Marionette.Object.call(this, options)
        },
        show: function (view, options) {
            if (this._ensureElement()) {
                this._ensureViewIsIntact(view);
                var showOptions = options || {},
                    isDifferentView = view !== this.currentView,
                    preventDestroy = !!showOptions.preventDestroy,
                    forceShow = !!showOptions.forceShow,
                    isChangingView = !!this.currentView,
                    _shouldDestroyView = isDifferentView && !preventDestroy,
                    _shouldShowView = isDifferentView || forceShow;
                if (isChangingView && this.triggerMethod("before:swapOut", this.currentView, this, options), this.currentView && delete this.currentView._parent, _shouldDestroyView ? this.empty() : isChangingView && _shouldShowView && this.currentView.off("destroy", this.empty, this), _shouldShowView) {
                    view.once("destroy", this.empty, this), view.render(), view._parent = this, isChangingView && this.triggerMethod("before:swap", view, this, options), this.triggerMethod("before:show", view, this, options), Marionette.triggerMethodOn(view, "before:show", view, this, options), isChangingView && this.triggerMethod("swapOut", this.currentView, this, options);
                    var attachedRegion = Marionette.isNodeAttached(this.el),
                        displayedViews = [],
                        attachOptions = _.extend({
                            triggerBeforeAttach: this.triggerBeforeAttach,
                            triggerAttach: this.triggerAttach
                        }, showOptions);
                    return attachedRegion && attachOptions.triggerBeforeAttach && (displayedViews = this._displayedViews(view), this._triggerAttach(displayedViews, "before:")), this.attachHtml(view), this.currentView = view, attachedRegion && attachOptions.triggerAttach && (displayedViews = this._displayedViews(view), this._triggerAttach(displayedViews)), isChangingView && this.triggerMethod("swap", view, this, options), this.triggerMethod("show", view, this, options), Marionette.triggerMethodOn(view, "show", view, this, options), this
                }
                return this
            }
        },
        triggerBeforeAttach: !0,
        triggerAttach: !0,
        _triggerAttach: function (views, prefix) {
            var eventName = (prefix || "") + "attach";
            _.each(views, function (view) {
                Marionette.triggerMethodOn(view, eventName, view, this)
            }, this)
        },
        _displayedViews: function (view) {
            return _.union([view], _.result(view, "_getNestedViews") || [])
        },
        _ensureElement: function () {
            if (_.isObject(this.el) || (this.$el = this.getEl(this.el), this.el = this.$el[0]), !this.$el || 0 === this.$el.length) {
                if (this.getOption("allowMissingEl")) return !1;
                throw new Marionette.Error('An "el" ' + this.$el.selector + " must exist in DOM")
            }
            return !0
        },
        _ensureViewIsIntact: function (view) {
            if (!view) throw new Marionette.Error({
                name: "ViewNotValid",
                message: "The view passed is undefined and therefore invalid. You must pass a view instance to show."
            });
            if (view.isDestroyed) throw new Marionette.Error({
                name: "ViewDestroyedError",
                message: 'View (cid: "' + view.cid + '") has already been destroyed and cannot be used.'
            })
        },
        getEl: function (el) {
            return Backbone.$(el, Marionette._getValue(this.options.parentEl, this))
        },
        attachHtml: function (view) {
            this.$el.contents().detach(), this.el.appendChild(view.el)
        },
        empty: function (options) {
            var view = this.currentView,
                preventDestroy = Marionette._getValue(options, "preventDestroy", this);
            if (view) return view.off("destroy", this.empty, this), this.triggerMethod("before:empty", view), preventDestroy || this._destroyView(), this.triggerMethod("empty", view), delete this.currentView, preventDestroy && this.$el.contents().detach(), this
        },
        _destroyView: function () {
            var view = this.currentView;
            view.destroy && !view.isDestroyed ? view.destroy() : view.remove && (view.remove(), view.isDestroyed = !0)
        },
        attachView: function (view) {
            return this.currentView = view, this
        },
        hasView: function () {
            return !!this.currentView
        },
        reset: function () {
            return this.empty(), this.$el && (this.el = this.$el.selector), delete this.$el, this
        }
    }, {
            buildRegion: function (regionConfig, DefaultRegionClass) {
                if (_.isString(regionConfig)) return this._buildRegionFromSelector(regionConfig, DefaultRegionClass);
                if (regionConfig.selector || regionConfig.el || regionConfig.regionClass) return this._buildRegionFromObject(regionConfig, DefaultRegionClass);
                if (_.isFunction(regionConfig)) return this._buildRegionFromRegionClass(regionConfig);
                throw new Marionette.Error({
                    message: "Improper region configuration type.",
                    url: "marionette.region.html#region-configuration-types"
                })
            },
            _buildRegionFromSelector: function (selector, DefaultRegionClass) {
                return new DefaultRegionClass({
                    el: selector
                })
            },
            _buildRegionFromObject: function (regionConfig, DefaultRegionClass) {
                var RegionClass = regionConfig.regionClass || DefaultRegionClass,
                    options = _.omit(regionConfig, "selector", "regionClass");
                return regionConfig.selector && !options.el && (options.el = regionConfig.selector), new RegionClass(options)
            },
            _buildRegionFromRegionClass: function (RegionClass) {
                return new RegionClass
            }
        }), Marionette.RegionManager = Marionette.Controller.extend({
            constructor: function (options) {
                this._regions = {}, this.length = 0, Marionette.Controller.call(this, options), this.addRegions(this.getOption("regions"))
            },
            addRegions: function (regionDefinitions, defaults) {
                return regionDefinitions = Marionette._getValue(regionDefinitions, this, arguments), _.reduce(regionDefinitions, function (regions, definition, name) {
                    return _.isString(definition) && (definition = {
                        selector: definition
                    }), definition.selector && (definition = _.defaults({}, definition, defaults)), regions[name] = this.addRegion(name, definition), regions
                }, {}, this)
            },
            addRegion: function (name, definition) {
                var region;
                return region = definition instanceof Marionette.Region ? definition : Marionette.Region.buildRegion(definition, Marionette.Region), this.triggerMethod("before:add:region", name, region), region._parent = this, this._store(name, region), this.triggerMethod("add:region", name, region), region
            },
            get: function (name) {
                return this._regions[name]
            },
            getRegions: function () {
                return _.clone(this._regions)
            },
            removeRegion: function (name) {
                var region = this._regions[name];
                return this._remove(name, region), region
            },
            removeRegions: function () {
                var regions = this.getRegions();
                return _.each(this._regions, function (region, name) {
                    this._remove(name, region)
                }, this), regions
            },
            emptyRegions: function () {
                var regions = this.getRegions();
                return _.invoke(regions, "empty"), regions
            },
            destroy: function () {
                return this.removeRegions(), Marionette.Controller.prototype.destroy.apply(this, arguments)
            },
            _store: function (name, region) {
                this._regions[name] || this.length++ , this._regions[name] = region
            },
            _remove: function (name, region) {
                this.triggerMethod("before:remove:region", name, region), region.empty(), region.stopListening(), delete region._parent, delete this._regions[name], this.length-- , this.triggerMethod("remove:region", name, region)
            }
        }), Marionette.actAsCollection(Marionette.RegionManager.prototype, "_regions"), Marionette.TemplateCache = function (templateId) {
            this.templateId = templateId
        }, _.extend(Marionette.TemplateCache, {
            templateCaches: {},
            get: function (templateId, options) {
                var cachedTemplate = this.templateCaches[templateId];
                return cachedTemplate || (cachedTemplate = new Marionette.TemplateCache(templateId), this.templateCaches[templateId] = cachedTemplate), cachedTemplate.load(options)
            },
            clear: function () {
                var i, args = _.toArray(arguments),
                    length = args.length;
                if (length > 0)
                    for (i = 0; i < length; i++) delete this.templateCaches[args[i]];
                else this.templateCaches = {}
            }
        }), _.extend(Marionette.TemplateCache.prototype, {
            load: function (options) {
                if (this.compiledTemplate) return this.compiledTemplate;
                var template = this.loadTemplate(this.templateId, options);
                return this.compiledTemplate = this.compileTemplate(template, options), this.compiledTemplate
            },
            loadTemplate: function (templateId, options) {
                var template = Backbone.$(templateId).html();
                if (!template || 0 === template.length) throw new Marionette.Error({
                    name: "NoTemplateError",
                    message: 'Could not find template: "' + templateId + '"'
                });
                return template
            },
            compileTemplate: function (rawTemplate, options) {
                return _.template(rawTemplate, options)
            }
        }), Marionette.Renderer = {
            render: function (template, data) {
                if (!template) throw new Marionette.Error({
                    name: "TemplateNotFoundError",
                    message: "Cannot render the template since its false, null or undefined."
                });
                var templateFunc = _.isFunction(template) ? template : Marionette.TemplateCache.get(template);
                return templateFunc(data)
            }
        }, Marionette.View = Backbone.View.extend({
            isDestroyed: !1,
            constructor: function (options) {
                _.bindAll(this, "render"), options = Marionette._getValue(options, this), this.options = _.extend({}, _.result(this, "options"), options), this._behaviors = Marionette.Behaviors(this), Backbone.View.call(this, this.options), Marionette.MonitorDOMRefresh(this)
            },
            getTemplate: function () {
                return this.getOption("template")
            },
            serializeModel: function (model) {
                return model.toJSON.apply(model, _.rest(arguments))
            },
            mixinTemplateHelpers: function (target) {
                target = target || {};
                var templateHelpers = this.getOption("templateHelpers");
                return templateHelpers = Marionette._getValue(templateHelpers, this), _.extend(target, templateHelpers)
            },
            normalizeUIKeys: function (hash) {
                var uiBindings = _.result(this, "_uiBindings");
                return Marionette.normalizeUIKeys(hash, uiBindings || _.result(this, "ui"))
            },
            normalizeUIValues: function (hash, properties) {
                var ui = _.result(this, "ui"),
                    uiBindings = _.result(this, "_uiBindings");
                return Marionette.normalizeUIValues(hash, uiBindings || ui, properties)
            },
            configureTriggers: function () {
                if (this.triggers) {
                    var triggers = this.normalizeUIKeys(_.result(this, "triggers"));
                    return _.reduce(triggers, function (events, value, key) {
                        return events[key] = this._buildViewTrigger(value), events
                    }, {}, this)
                }
            },
            delegateEvents: function (events) {
                return this._delegateDOMEvents(events), this.bindEntityEvents(this.model, this.getOption("modelEvents")), this.bindEntityEvents(this.collection, this.getOption("collectionEvents")), _.each(this._behaviors, function (behavior) {
                    behavior.bindEntityEvents(this.model, behavior.getOption("modelEvents")), behavior.bindEntityEvents(this.collection, behavior.getOption("collectionEvents"))
                }, this), this
            },
            _delegateDOMEvents: function (eventsArg) {
                var events = Marionette._getValue(eventsArg || this.events, this);
                events = this.normalizeUIKeys(events), _.isUndefined(eventsArg) && (this.events = events);
                var combinedEvents = {},
                    behaviorEvents = _.result(this, "behaviorEvents") || {},
                    triggers = this.configureTriggers(),
                    behaviorTriggers = _.result(this, "behaviorTriggers") || {};
                _.extend(combinedEvents, behaviorEvents, events, triggers, behaviorTriggers), Backbone.View.prototype.delegateEvents.call(this, combinedEvents)
            },
            undelegateEvents: function () {
                return Backbone.View.prototype.undelegateEvents.apply(this, arguments), this.unbindEntityEvents(this.model, this.getOption("modelEvents")), this.unbindEntityEvents(this.collection, this.getOption("collectionEvents")), _.each(this._behaviors, function (behavior) {
                    behavior.unbindEntityEvents(this.model, behavior.getOption("modelEvents")), behavior.unbindEntityEvents(this.collection, behavior.getOption("collectionEvents"))
                }, this), this
            },
            _ensureViewIsIntact: function () {
                if (this.isDestroyed) throw new Marionette.Error({
                    name: "ViewDestroyedError",
                    message: 'View (cid: "' + this.cid + '") has already been destroyed and cannot be used.'
                })
            },
            destroy: function () {
                if (this.isDestroyed) return this;
                var args = _.toArray(arguments);
                return this.triggerMethod.apply(this, ["before:destroy"].concat(args)), this.isDestroyed = !0, this.triggerMethod.apply(this, ["destroy"].concat(args)), this.unbindUIElements(), this.isRendered = !1, this.remove(), _.invoke(this._behaviors, "destroy", args), this
            },
            bindUIElements: function () {
                this._bindUIElements(), _.invoke(this._behaviors, this._bindUIElements)
            },
            _bindUIElements: function () {
                if (this.ui) {
                    this._uiBindings || (this._uiBindings = this.ui);
                    var bindings = _.result(this, "_uiBindings");
                    this.ui = {}, _.each(bindings, function (selector, key) {
                        this.ui[key] = this.$(selector)
                    }, this)
                }
            },
            unbindUIElements: function () {
                this._unbindUIElements(), _.invoke(this._behaviors, this._unbindUIElements)
            },
            _unbindUIElements: function () {
                this.ui && this._uiBindings && (_.each(this.ui, function ($el, name) {
                    delete this.ui[name]
                }, this), this.ui = this._uiBindings, delete this._uiBindings)
            },
            _buildViewTrigger: function (triggerDef) {
                var hasOptions = _.isObject(triggerDef),
                    options = _.defaults({}, hasOptions ? triggerDef : {}, {
                        preventDefault: !0,
                        stopPropagation: !0
                    }),
                    eventName = hasOptions ? options.event : triggerDef;
                return function (e) {
                    e && (e.preventDefault && options.preventDefault && e.preventDefault(), e.stopPropagation && options.stopPropagation && e.stopPropagation());
                    var args = {
                        view: this,
                        model: this.model,
                        collection: this.collection
                    };
                    this.triggerMethod(eventName, args)
                }
            },
            setElement: function () {
                var ret = Backbone.View.prototype.setElement.apply(this, arguments);
                return _.invoke(this._behaviors, "proxyViewProperties", this), ret
            },
            triggerMethod: function () {
                var ret = Marionette._triggerMethod(this, arguments);
                return this._triggerEventOnBehaviors(arguments), this._triggerEventOnParentLayout(arguments[0], _.rest(arguments)), ret
            },
            _triggerEventOnBehaviors: function (args) {
                for (var triggerMethod = Marionette._triggerMethod, behaviors = this._behaviors, i = 0, length = behaviors && behaviors.length; i < length; i++) triggerMethod(behaviors[i], args)
            },
            _triggerEventOnParentLayout: function (eventName, args) {
                var layoutView = this._parentLayoutView();
                if (layoutView) {
                    var eventPrefix = Marionette.getOption(layoutView, "childViewEventPrefix"),
                        prefixedEventName = eventPrefix + ":" + eventName;
                    Marionette._triggerMethod(layoutView, [prefixedEventName, this].concat(args));
                    var childEvents = Marionette.getOption(layoutView, "childEvents"),
                        normalizedChildEvents = layoutView.normalizeMethods(childEvents);
                    normalizedChildEvents && _.isFunction(normalizedChildEvents[eventName]) && normalizedChildEvents[eventName].apply(layoutView, [this].concat(args))
                }
            },
            _getImmediateChildren: function () {
                return []
            },
            _getNestedViews: function () {
                var children = this._getImmediateChildren();
                return children.length ? _.reduce(children, function (memo, view) {
                    return view._getNestedViews ? memo.concat(view._getNestedViews()) : memo
                }, children) : children
            },
            _getAncestors: function () {
                for (var ancestors = [], parent = this._parent; parent;) ancestors.push(parent), parent = parent._parent;
                return ancestors
            },
            _parentLayoutView: function () {
                var ancestors = this._getAncestors();
                return _.find(ancestors, function (parent) {
                    return parent instanceof Marionette.LayoutView
                })
            },
            normalizeMethods: Marionette.normalizeMethods,
            mergeOptions: Marionette.mergeOptions,
            getOption: Marionette.proxyGetOption,
            bindEntityEvents: Marionette.proxyBindEntityEvents,
            unbindEntityEvents: Marionette.proxyUnbindEntityEvents
        }), Marionette.ItemView = Marionette.View.extend({
            constructor: function () {
                Marionette.View.apply(this, arguments)
            },
            serializeData: function () {
                if (!this.model && !this.collection) return {};
                var args = [this.model || this.collection];
                return arguments.length && args.push.apply(args, arguments), this.model ? this.serializeModel.apply(this, args) : {
                    items: this.serializeCollection.apply(this, args)
                }
            },
            serializeCollection: function (collection) {
                return collection.toJSON.apply(collection, _.rest(arguments))
            },
            render: function () {
                return this._ensureViewIsIntact(), this.triggerMethod("before:render", this), this._renderTemplate(), this.isRendered = !0, this.bindUIElements(), this.triggerMethod("render", this), this
            },
            _renderTemplate: function () {
                var template = this.getTemplate();
                if (template !== !1) {
                    if (!template) throw new Marionette.Error({
                        name: "UndefinedTemplateError",
                        message: "Cannot render the template since it is null or undefined."
                    });
                    var data = this.mixinTemplateHelpers(this.serializeData()),
                        html = Marionette.Renderer.render(template, data, this);
                    return this.attachElContent(html), this
                }
            },
            attachElContent: function (html) {
                return this.$el.html(html), this
            }
        }), Marionette.CollectionView = Marionette.View.extend({
            childViewEventPrefix: "childview",
            sort: !0,
            constructor: function (options) {
                this.once("render", this._initialEvents), this._initChildViewStorage(), Marionette.View.apply(this, arguments), this.on({
                    "before:show": this._onBeforeShowCalled,
                    show: this._onShowCalled,
                    "before:attach": this._onBeforeAttachCalled,
                    attach: this._onAttachCalled
                }), this.initRenderBuffer()
            },
            initRenderBuffer: function () {
                this._bufferedChildren = []
            },
            startBuffering: function () {
                this.initRenderBuffer(), this.isBuffering = !0
            },
            endBuffering: function () {
                var nestedViews, canTriggerAttach = this._isShown && Marionette.isNodeAttached(this.el);
                this.isBuffering = !1, this._isShown && this._triggerMethodMany(this._bufferedChildren, this, "before:show"), canTriggerAttach && this._triggerBeforeAttach && (nestedViews = this._getNestedViews(), this._triggerMethodMany(nestedViews, this, "before:attach")), this.attachBuffer(this, this._createBuffer()),
                    canTriggerAttach && this._triggerAttach && (nestedViews = this._getNestedViews(), this._triggerMethodMany(nestedViews, this, "attach")), this._isShown && this._triggerMethodMany(this._bufferedChildren, this, "show"), this.initRenderBuffer()
            },
            _triggerMethodMany: function (targets, source, eventName) {
                var args = _.drop(arguments, 3);
                _.each(targets, function (target) {
                    Marionette.triggerMethodOn.apply(target, [target, eventName, target, source].concat(args))
                })
            },
            _initialEvents: function () {
                this.collection && (this.listenTo(this.collection, "add", this._onCollectionAdd), this.listenTo(this.collection, "remove", this._onCollectionRemove), this.listenTo(this.collection, "reset", this.render), this.getOption("sort") && this.listenTo(this.collection, "sort", this._sortViews))
            },
            _onCollectionAdd: function (child, collection, opts) {
                var index;
                if (index = void 0 !== opts.at ? opts.at : _.indexOf(this._filteredSortedModels(), child), this._shouldAddChild(child, index)) {
                    this.destroyEmptyView();
                    var ChildView = this.getChildView(child);
                    this.addChild(child, ChildView, index)
                }
            },
            _onCollectionRemove: function (model) {
                var view = this.children.findByModel(model);
                this.removeChildView(view), this.checkEmpty()
            },
            _onBeforeShowCalled: function () {
                this._triggerBeforeAttach = this._triggerAttach = !1, this.children.each(function (childView) {
                    Marionette.triggerMethodOn(childView, "before:show", childView)
                })
            },
            _onShowCalled: function () {
                this.children.each(function (childView) {
                    Marionette.triggerMethodOn(childView, "show", childView)
                })
            },
            _onBeforeAttachCalled: function () {
                this._triggerBeforeAttach = !0
            },
            _onAttachCalled: function () {
                this._triggerAttach = !0
            },
            render: function () {
                return this._ensureViewIsIntact(), this.triggerMethod("before:render", this), this._renderChildren(), this.isRendered = !0, this.triggerMethod("render", this), this
            },
            reorder: function () {
                var children = this.children,
                    models = this._filteredSortedModels(),
                    modelsChanged = _.find(models, function (model) {
                        return !children.findByModel(model)
                    });
                if (modelsChanged) this.render();
                else {
                    var els = _.map(models, function (model, index) {
                        var view = children.findByModel(model);
                        return view._index = index, view.el
                    });
                    this.triggerMethod("before:reorder"), this._appendReorderedChildren(els), this.triggerMethod("reorder")
                }
            },
            resortView: function () {
                Marionette.getOption(this, "reorderOnSort") ? this.reorder() : this.render()
            },
            _sortViews: function () {
                var models = this._filteredSortedModels(),
                    orderChanged = _.find(models, function (item, index) {
                        var view = this.children.findByModel(item);
                        return !view || view._index !== index
                    }, this);
                orderChanged && this.resortView()
            },
            _emptyViewIndex: -1,
            _appendReorderedChildren: function (children) {
                this.$el.append(children)
            },
            _renderChildren: function () {
                this.destroyEmptyView(), this.destroyChildren({
                    checkEmpty: !1
                }), this.isEmpty(this.collection) ? this.showEmptyView() : (this.triggerMethod("before:render:collection", this), this.startBuffering(), this.showCollection(), this.endBuffering(), this.triggerMethod("render:collection", this), this.children.isEmpty() && this.showEmptyView())
            },
            showCollection: function () {
                var ChildView, models = this._filteredSortedModels();
                _.each(models, function (child, index) {
                    ChildView = this.getChildView(child), this.addChild(child, ChildView, index)
                }, this)
            },
            _filteredSortedModels: function () {
                var models, viewComparator = this.getViewComparator();
                return models = viewComparator ? _.isString(viewComparator) || 1 === viewComparator.length ? this.collection.sortBy(viewComparator, this) : _.clone(this.collection.models).sort(_.bind(viewComparator, this)) : this.collection.models, this.getOption("filter") && (models = _.filter(models, function (model, index) {
                    return this._shouldAddChild(model, index)
                }, this)), models
            },
            showEmptyView: function () {
                var EmptyView = this.getEmptyView();
                if (EmptyView && !this._showingEmptyView) {
                    this.triggerMethod("before:render:empty"), this._showingEmptyView = !0;
                    var model = new Backbone.Model;
                    this.addEmptyView(model, EmptyView), this.triggerMethod("render:empty")
                }
            },
            destroyEmptyView: function () {
                this._showingEmptyView && (this.triggerMethod("before:remove:empty"), this.destroyChildren(), delete this._showingEmptyView, this.triggerMethod("remove:empty"))
            },
            getEmptyView: function () {
                return this.getOption("emptyView")
            },
            addEmptyView: function (child, EmptyView) {
                var nestedViews, canTriggerAttach = this._isShown && !this.isBuffering && Marionette.isNodeAttached(this.el),
                    emptyViewOptions = this.getOption("emptyViewOptions") || this.getOption("childViewOptions");
                _.isFunction(emptyViewOptions) && (emptyViewOptions = emptyViewOptions.call(this, child, this._emptyViewIndex));
                var view = this.buildChildView(child, EmptyView, emptyViewOptions);
                view._parent = this, this.proxyChildEvents(view), this._isShown && Marionette.triggerMethodOn(view, "before:show", view), this.children.add(view), canTriggerAttach && this._triggerBeforeAttach && (nestedViews = [view].concat(view._getNestedViews()), view.once("render", function () {
                    this._triggerMethodMany(nestedViews, this, "before:attach")
                }, this)), this.renderChildView(view, this._emptyViewIndex), canTriggerAttach && this._triggerAttach && (nestedViews = [view].concat(view._getNestedViews()), this._triggerMethodMany(nestedViews, this, "attach")), this._isShown && Marionette.triggerMethodOn(view, "show", view)
            },
            getChildView: function (child) {
                var childView = this.getOption("childView");
                if (!childView) throw new Marionette.Error({
                    name: "NoChildViewError",
                    message: 'A "childView" must be specified'
                });
                return childView
            },
            addChild: function (child, ChildView, index) {
                var childViewOptions = this.getOption("childViewOptions");
                childViewOptions = Marionette._getValue(childViewOptions, this, [child, index]);
                var view = this.buildChildView(child, ChildView, childViewOptions);
                return this._updateIndices(view, !0, index), this.triggerMethod("before:add:child", view), this._addChildView(view, index), this.triggerMethod("add:child", view), view._parent = this, view
            },
            _updateIndices: function (view, increment, index) {
                this.getOption("sort") && (increment && (view._index = index), this.children.each(function (laterView) {
                    laterView._index >= view._index && (laterView._index += increment ? 1 : -1)
                }))
            },
            _addChildView: function (view, index) {
                var nestedViews, canTriggerAttach = this._isShown && !this.isBuffering && Marionette.isNodeAttached(this.el);
                this.proxyChildEvents(view), this._isShown && !this.isBuffering && Marionette.triggerMethodOn(view, "before:show", view), this.children.add(view), canTriggerAttach && this._triggerBeforeAttach && (nestedViews = [view].concat(view._getNestedViews()), view.once("render", function () {
                    this._triggerMethodMany(nestedViews, this, "before:attach")
                }, this)), this.renderChildView(view, index), canTriggerAttach && this._triggerAttach && (nestedViews = [view].concat(view._getNestedViews()), this._triggerMethodMany(nestedViews, this, "attach")), this._isShown && !this.isBuffering && Marionette.triggerMethodOn(view, "show", view)
            },
            renderChildView: function (view, index) {
                return view.render(), this.attachHtml(this, view, index), view
            },
            buildChildView: function (child, ChildViewClass, childViewOptions) {
                var options = _.extend({
                    model: child
                }, childViewOptions);
                return new ChildViewClass(options)
            },
            removeChildView: function (view) {
                return view && (this.triggerMethod("before:remove:child", view), view.destroy ? view.destroy() : view.remove && view.remove(), delete view._parent, this.stopListening(view), this.children.remove(view), this.triggerMethod("remove:child", view), this._updateIndices(view, !1)), view
            },
            isEmpty: function () {
                return !this.collection || 0 === this.collection.length
            },
            checkEmpty: function () {
                this.isEmpty(this.collection) && this.showEmptyView()
            },
            attachBuffer: function (collectionView, buffer) {
                collectionView.$el.append(buffer)
            },
            _createBuffer: function () {
                var elBuffer = document.createDocumentFragment();
                return _.each(this._bufferedChildren, function (b) {
                    elBuffer.appendChild(b.el)
                }), elBuffer
            },
            attachHtml: function (collectionView, childView, index) {
                collectionView.isBuffering ? collectionView._bufferedChildren.splice(index, 0, childView) : collectionView._insertBefore(childView, index) || collectionView._insertAfter(childView)
            },
            _insertBefore: function (childView, index) {
                var currentView, findPosition = this.getOption("sort") && index < this.children.length - 1;
                return findPosition && (currentView = this.children.find(function (view) {
                    return view._index === index + 1
                })), !!currentView && (currentView.$el.before(childView.el), !0)
            },
            _insertAfter: function (childView) {
                this.$el.append(childView.el)
            },
            _initChildViewStorage: function () {
                this.children = new Backbone.ChildViewContainer
            },
            destroy: function () {
                return this.isDestroyed ? this : (this.triggerMethod("before:destroy:collection"), this.destroyChildren({
                    checkEmpty: !1
                }), this.triggerMethod("destroy:collection"), Marionette.View.prototype.destroy.apply(this, arguments))
            },
            destroyChildren: function (options) {
                var destroyOptions = options || {},
                    shouldCheckEmpty = !0,
                    childViews = this.children.map(_.identity);
                return _.isUndefined(destroyOptions.checkEmpty) || (shouldCheckEmpty = destroyOptions.checkEmpty), this.children.each(this.removeChildView, this), shouldCheckEmpty && this.checkEmpty(), childViews
            },
            _shouldAddChild: function (child, index) {
                var filter = this.getOption("filter");
                return !_.isFunction(filter) || filter.call(this, child, index, this.collection)
            },
            proxyChildEvents: function (view) {
                var prefix = this.getOption("childViewEventPrefix");
                this.listenTo(view, "all", function () {
                    var args = _.toArray(arguments),
                        rootEvent = args[0],
                        childEvents = this.normalizeMethods(_.result(this, "childEvents"));
                    args[0] = prefix + ":" + rootEvent, args.splice(1, 0, view), "undefined" != typeof childEvents && _.isFunction(childEvents[rootEvent]) && childEvents[rootEvent].apply(this, args.slice(1)), this.triggerMethod.apply(this, args)
                })
            },
            _getImmediateChildren: function () {
                return _.values(this.children._views)
            },
            getViewComparator: function () {
                return this.getOption("viewComparator")
            }
        }), Marionette.CompositeView = Marionette.CollectionView.extend({
            constructor: function () {
                Marionette.CollectionView.apply(this, arguments)
            },
            _initialEvents: function () {
                this.collection && (this.listenTo(this.collection, "add", this._onCollectionAdd), this.listenTo(this.collection, "remove", this._onCollectionRemove), this.listenTo(this.collection, "reset", this._renderChildren), this.getOption("sort") && this.listenTo(this.collection, "sort", this._sortViews))
            },
            getChildView: function (child) {
                var childView = this.getOption("childView") || this.constructor;
                return childView
            },
            serializeData: function () {
                var data = {};
                return this.model && (data = _.partial(this.serializeModel, this.model).apply(this, arguments)), data
            },
            render: function () {
                return this._ensureViewIsIntact(), this._isRendering = !0, this.resetChildViewContainer(), this.triggerMethod("before:render", this), this._renderTemplate(), this._renderChildren(), this._isRendering = !1, this.isRendered = !0, this.triggerMethod("render", this), this
            },
            _renderChildren: function () {
                (this.isRendered || this._isRendering) && Marionette.CollectionView.prototype._renderChildren.call(this)
            },
            _renderTemplate: function () {
                var data = {};
                data = this.serializeData(), data = this.mixinTemplateHelpers(data), this.triggerMethod("before:render:template");
                var template = this.getTemplate(),
                    html = Marionette.Renderer.render(template, data, this);
                this.attachElContent(html), this.bindUIElements(), this.triggerMethod("render:template")
            },
            attachElContent: function (html) {
                return this.$el.html(html), this
            },
            attachBuffer: function (compositeView, buffer) {
                var $container = this.getChildViewContainer(compositeView);
                $container.append(buffer)
            },
            _insertAfter: function (childView) {
                var $container = this.getChildViewContainer(this, childView);
                $container.append(childView.el)
            },
            _appendReorderedChildren: function (children) {
                var $container = this.getChildViewContainer(this);
                $container.append(children)
            },
            getChildViewContainer: function (containerView, childView) {
                if (containerView.$childViewContainer) return containerView.$childViewContainer;
                var container, childViewContainer = Marionette.getOption(containerView, "childViewContainer");
                if (childViewContainer) {
                    var selector = Marionette._getValue(childViewContainer, containerView);
                    if (container = "@" === selector.charAt(0) && containerView.ui ? containerView.ui[selector.substr(4)] : containerView.$(selector), container.length <= 0) throw new Marionette.Error({
                        name: "ChildViewContainerMissingError",
                        message: 'The specified "childViewContainer" was not found: ' + containerView.childViewContainer
                    })
                } else container = containerView.$el;
                return containerView.$childViewContainer = container, container
            },
            resetChildViewContainer: function () {
                this.$childViewContainer && (this.$childViewContainer = void 0)
            }
        }), Marionette.LayoutView = Marionette.ItemView.extend({
            regionClass: Marionette.Region,
            options: {
                destroyImmediate: !1
            },
            childViewEventPrefix: "childview",
            constructor: function (options) {
                options = options || {}, this._firstRender = !0, this._initializeRegions(options), Marionette.ItemView.call(this, options)
            },
            render: function () {
                return this._ensureViewIsIntact(), this._firstRender ? this._firstRender = !1 : this._reInitializeRegions(), Marionette.ItemView.prototype.render.apply(this, arguments)
            },
            destroy: function () {
                return this.isDestroyed ? this : (this.getOption("destroyImmediate") === !0 && this.$el.remove(), this.regionManager.destroy(), Marionette.ItemView.prototype.destroy.apply(this, arguments))
            },
            showChildView: function (regionName, view) {
                return this.getRegion(regionName).show(view)
            },
            getChildView: function (regionName) {
                return this.getRegion(regionName).currentView
            },
            addRegion: function (name, definition) {
                var regions = {};
                return regions[name] = definition, this._buildRegions(regions)[name]
            },
            addRegions: function (regions) {
                return this.regions = _.extend({}, this.regions, regions), this._buildRegions(regions)
            },
            removeRegion: function (name) {
                return delete this.regions[name], this.regionManager.removeRegion(name)
            },
            getRegion: function (region) {
                return this.regionManager.get(region)
            },
            getRegions: function () {
                return this.regionManager.getRegions()
            },
            _buildRegions: function (regions) {
                var defaults = {
                    regionClass: this.getOption("regionClass"),
                    parentEl: _.partial(_.result, this, "el")
                };
                return this.regionManager.addRegions(regions, defaults)
            },
            _initializeRegions: function (options) {
                var regions;
                this._initRegionManager(), regions = Marionette._getValue(this.regions, this, [options]) || {};
                var regionOptions = this.getOption.call(options, "regions");
                regionOptions = Marionette._getValue(regionOptions, this, [options]), _.extend(regions, regionOptions), regions = this.normalizeUIValues(regions, ["selector", "el"]), this.addRegions(regions)
            },
            _reInitializeRegions: function () {
                this.regionManager.invoke("reset")
            },
            getRegionManager: function () {
                return new Marionette.RegionManager
            },
            _initRegionManager: function () {
                this.regionManager = this.getRegionManager(), this.regionManager._parent = this, this.listenTo(this.regionManager, "before:add:region", function (name) {
                    this.triggerMethod("before:add:region", name)
                }), this.listenTo(this.regionManager, "add:region", function (name, region) {
                    this[name] = region, this.triggerMethod("add:region", name, region)
                }), this.listenTo(this.regionManager, "before:remove:region", function (name) {
                    this.triggerMethod("before:remove:region", name)
                }), this.listenTo(this.regionManager, "remove:region", function (name, region) {
                    delete this[name], this.triggerMethod("remove:region", name, region)
                })
            },
            _getImmediateChildren: function () {
                return _.chain(this.regionManager.getRegions()).pluck("currentView").compact().value()
            }
        }), Marionette.Behavior = Marionette.Object.extend({
            constructor: function (options, view) {
                this.view = view, this.defaults = _.result(this, "defaults") || {}, this.options = _.extend({}, this.defaults, options), this.ui = _.extend({}, _.result(view, "ui"), _.result(this, "ui")), Marionette.Object.apply(this, arguments)
            },
            $: function () {
                return this.view.$.apply(this.view, arguments)
            },
            destroy: function () {
                return this.stopListening(), this
            },
            proxyViewProperties: function (view) {
                this.$el = view.$el, this.el = view.el
            }
        }), Marionette.Behaviors = function (Marionette, _) {
            function Behaviors(view, behaviors) {
                return _.isObject(view.behaviors) ? (behaviors = Behaviors.parseBehaviors(view, behaviors || _.result(view, "behaviors")), Behaviors.wrap(view, behaviors, _.keys(methods)), behaviors) : {}
            }

            function BehaviorTriggersBuilder(view, behaviors) {
                this._view = view, this._behaviors = behaviors, this._triggers = {}
            }

            function getBehaviorsUI(behavior) {
                return behavior._uiBindings || behavior.ui
            }
            var delegateEventSplitter = /^(\S+)\s*(.*)$/,
                methods = {
                    behaviorTriggers: function (behaviorTriggers, behaviors) {
                        var triggerBuilder = new BehaviorTriggersBuilder(this, behaviors);
                        return triggerBuilder.buildBehaviorTriggers()
                    },
                    behaviorEvents: function (behaviorEvents, behaviors) {
                        var _behaviorsEvents = {};
                        return _.each(behaviors, function (b, i) {
                            var _events = {},
                                behaviorEvents = _.clone(_.result(b, "events")) || {};
                            behaviorEvents = Marionette.normalizeUIKeys(behaviorEvents, getBehaviorsUI(b));
                            var j = 0;
                            _.each(behaviorEvents, function (behaviour, key) {
                                var match = key.match(delegateEventSplitter),
                                    eventName = match[1] + "." + [this.cid, i, j++, " "].join(""),
                                    selector = match[2],
                                    eventKey = eventName + selector,
                                    handler = _.isFunction(behaviour) ? behaviour : b[behaviour];
                                _events[eventKey] = _.bind(handler, b)
                            }, this), _behaviorsEvents = _.extend(_behaviorsEvents, _events)
                        }, this), _behaviorsEvents
                    }
                };
            return _.extend(Behaviors, {
                behaviorsLookup: function () {
                    throw new Marionette.Error({
                        message: "You must define where your behaviors are stored.",
                        url: "marionette.behaviors.html#behaviorslookup"
                    })
                },
                getBehaviorClass: function (options, key) {
                    return options.behaviorClass ? options.behaviorClass : Marionette._getValue(Behaviors.behaviorsLookup, this, [options, key])[key]
                },
                parseBehaviors: function (view, behaviors) {
                    return _.chain(behaviors).map(function (options, key) {
                        var BehaviorClass = Behaviors.getBehaviorClass(options, key),
                            behavior = new BehaviorClass(options, view),
                            nestedBehaviors = Behaviors.parseBehaviors(view, _.result(behavior, "behaviors"));
                        return [behavior].concat(nestedBehaviors)
                    }).flatten().value()
                },
                wrap: function (view, behaviors, methodNames) {
                    _.each(methodNames, function (methodName) {
                        view[methodName] = _.partial(methods[methodName], view[methodName], behaviors)
                    })
                }
            }), _.extend(BehaviorTriggersBuilder.prototype, {
                buildBehaviorTriggers: function () {
                    return _.each(this._behaviors, this._buildTriggerHandlersForBehavior, this), this._triggers
                },
                _buildTriggerHandlersForBehavior: function (behavior, i) {
                    var triggersHash = _.clone(_.result(behavior, "triggers")) || {};
                    triggersHash = Marionette.normalizeUIKeys(triggersHash, getBehaviorsUI(behavior)), _.each(triggersHash, _.bind(this._setHandlerForBehavior, this, behavior, i))
                },
                _setHandlerForBehavior: function (behavior, i, eventName, trigger) {
                    var triggerKey = trigger.replace(/^\S+/, function (triggerName) {
                        return triggerName + ".behaviortriggers" + i
                    });
                    this._triggers[triggerKey] = this._view._buildViewTrigger(eventName)
                }
            }), Behaviors
        }(Marionette, _), Marionette.AppRouter = Backbone.Router.extend({
            constructor: function (options) {
                this.options = options || {}, Backbone.Router.apply(this, arguments);
                var appRoutes = this.getOption("appRoutes"),
                    controller = this._getController();
                this.processAppRoutes(controller, appRoutes), this.on("route", this._processOnRoute, this)
            },
            appRoute: function (route, methodName) {
                var controller = this._getController();
                this._addAppRoute(controller, route, methodName)
            },
            _processOnRoute: function (routeName, routeArgs) {
                if (_.isFunction(this.onRoute)) {
                    var routePath = _.invert(this.getOption("appRoutes"))[routeName];
                    this.onRoute(routeName, routePath, routeArgs)
                }
            },
            processAppRoutes: function (controller, appRoutes) {
                if (appRoutes) {
                    var routeNames = _.keys(appRoutes).reverse();
                    _.each(routeNames, function (route) {
                        this._addAppRoute(controller, route, appRoutes[route])
                    }, this)
                }
            },
            _getController: function () {
                return this.getOption("controller")
            },
            _addAppRoute: function (controller, route, methodName) {
                var method = controller[methodName];
                if (!method) throw new Marionette.Error('Method "' + methodName + '" was not found on the controller');
                this.route(route, methodName, _.bind(method, controller))
            },
            mergeOptions: Marionette.mergeOptions,
            getOption: Marionette.proxyGetOption,
            triggerMethod: Marionette.triggerMethod,
            bindEntityEvents: Marionette.proxyBindEntityEvents,
            unbindEntityEvents: Marionette.proxyUnbindEntityEvents
        }), Marionette.Application = Marionette.Object.extend({
            constructor: function (options) {
                this._initializeRegions(options), this._initCallbacks = new Marionette.Callbacks, this.submodules = {}, _.extend(this, options), this._initChannel(), Marionette.Object.call(this, options)
            },
            execute: function () {
                this.commands.execute.apply(this.commands, arguments)
            },
            request: function () {
                return this.reqres.request.apply(this.reqres, arguments)
            },
            addInitializer: function (initializer) {
                this._initCallbacks.add(initializer)
            },
            start: function (options) {
                this.triggerMethod("before:start", options), this._initCallbacks.run(options, this), this.triggerMethod("start", options)
            },
            addRegions: function (regions) {
                return this._regionManager.addRegions(regions)
            },
            emptyRegions: function () {
                return this._regionManager.emptyRegions()
            },
            removeRegion: function (region) {
                return this._regionManager.removeRegion(region)
            },
            getRegion: function (region) {
                return this._regionManager.get(region)
            },
            getRegions: function () {
                return this._regionManager.getRegions()
            },
            module: function (moduleNames, moduleDefinition) {
                var ModuleClass = Marionette.Module.getClass(moduleDefinition),
                    args = _.toArray(arguments);
                return args.unshift(this), ModuleClass.create.apply(ModuleClass, args)
            },
            getRegionManager: function () {
                return new Marionette.RegionManager
            },
            _initializeRegions: function (options) {
                var regions = _.isFunction(this.regions) ? this.regions(options) : this.regions || {};
                this._initRegionManager();
                var optionRegions = Marionette.getOption(options, "regions");
                return _.isFunction(optionRegions) && (optionRegions = optionRegions.call(this, options)), _.extend(regions, optionRegions), this.addRegions(regions), this
            },
            _initRegionManager: function () {
                this._regionManager = this.getRegionManager(), this._regionManager._parent = this, this.listenTo(this._regionManager, "before:add:region", function () {
                    Marionette._triggerMethod(this, "before:add:region", arguments)
                }), this.listenTo(this._regionManager, "add:region", function (name, region) {
                    this[name] = region, Marionette._triggerMethod(this, "add:region", arguments)
                }), this.listenTo(this._regionManager, "before:remove:region", function () {
                    Marionette._triggerMethod(this, "before:remove:region", arguments)
                }), this.listenTo(this._regionManager, "remove:region", function (name) {
                    delete this[name], Marionette._triggerMethod(this, "remove:region", arguments)
                })
            },
            _initChannel: function () {
                this.channelName = _.result(this, "channelName") || "global", this.channel = _.result(this, "channel") || Backbone.Wreqr.radio.channel(this.channelName), this.vent = _.result(this, "vent") || this.channel.vent, this.commands = _.result(this, "commands") || this.channel.commands, this.reqres = _.result(this, "reqres") || this.channel.reqres
            }
        }), Marionette.Module = function (moduleName, app, options) {
            this.moduleName = moduleName, this.options = _.extend({}, this.options, options), this.initialize = options.initialize || this.initialize, this.submodules = {}, this._setupInitializersAndFinalizers(), this.app = app, _.isFunction(this.initialize) && this.initialize(moduleName, app, this.options)
        }, Marionette.Module.extend = Marionette.extend, _.extend(Marionette.Module.prototype, Backbone.Events, {
            startWithParent: !0,
            initialize: function () { },
            addInitializer: function (callback) {
                this._initializerCallbacks.add(callback)
            },
            addFinalizer: function (callback) {
                this._finalizerCallbacks.add(callback)
            },
            start: function (options) {
                this._isInitialized || (_.each(this.submodules, function (mod) {
                    mod.startWithParent && mod.start(options)
                }), this.triggerMethod("before:start", options), this._initializerCallbacks.run(options, this), this._isInitialized = !0, this.triggerMethod("start", options))
            },
            stop: function () {
                this._isInitialized && (this._isInitialized = !1, this.triggerMethod("before:stop"), _.invoke(this.submodules, "stop"), this._finalizerCallbacks.run(void 0, this), this._initializerCallbacks.reset(), this._finalizerCallbacks.reset(), this.triggerMethod("stop"))
            },
            addDefinition: function (moduleDefinition, customArgs) {
                this._runModuleDefinition(moduleDefinition, customArgs)
            },
            _runModuleDefinition: function (definition, customArgs) {
                if (definition) {
                    var args = _.flatten([this, this.app, Backbone, Marionette, Backbone.$, _, customArgs]);
                    definition.apply(this, args)
                }
            },
            _setupInitializersAndFinalizers: function () {
                this._initializerCallbacks = new Marionette.Callbacks, this._finalizerCallbacks = new Marionette.Callbacks
            },
            triggerMethod: Marionette.triggerMethod
        }), _.extend(Marionette.Module, {
            create: function (app, moduleNames, moduleDefinition) {
                var module = app,
                    customArgs = _.drop(arguments, 3);
                moduleNames = moduleNames.split(".");
                var length = moduleNames.length,
                    moduleDefinitions = [];
                return moduleDefinitions[length - 1] = moduleDefinition, _.each(moduleNames, function (moduleName, i) {
                    var parentModule = module;
                    module = this._getModule(parentModule, moduleName, app, moduleDefinition), this._addModuleDefinition(parentModule, module, moduleDefinitions[i], customArgs)
                }, this), module
            },
            _getModule: function (parentModule, moduleName, app, def, args) {
                var options = _.extend({}, def),
                    ModuleClass = this.getClass(def),
                    module = parentModule[moduleName];
                return module || (module = new ModuleClass(moduleName, app, options), parentModule[moduleName] = module, parentModule.submodules[moduleName] = module), module
            },
            getClass: function (moduleDefinition) {
                var ModuleClass = Marionette.Module;
                return moduleDefinition ? moduleDefinition.prototype instanceof ModuleClass ? moduleDefinition : moduleDefinition.moduleClass || ModuleClass : ModuleClass
            },
            _addModuleDefinition: function (parentModule, module, def, args) {
                var fn = this._getDefine(def),
                    startWithParent = this._getStartWithParent(def, module);
                fn && module.addDefinition(fn, args), this._addStartWithParent(parentModule, module, startWithParent)
            },
            _getStartWithParent: function (def, module) {
                var swp;
                return _.isFunction(def) && def.prototype instanceof Marionette.Module ? (swp = module.constructor.prototype.startWithParent, !!_.isUndefined(swp) || swp) : !_.isObject(def) || (swp = def.startWithParent, !!_.isUndefined(swp) || swp)
            },
            _getDefine: function (def) {
                return !_.isFunction(def) || def.prototype instanceof Marionette.Module ? _.isObject(def) ? def.define : null : def
            },
            _addStartWithParent: function (parentModule, module, startWithParent) {
                module.startWithParent = module.startWithParent && startWithParent, module.startWithParent && !module.startWithParentIsConfigured && (module.startWithParentIsConfigured = !0, parentModule.addInitializer(function (options) {
                    module.startWithParent && module.start(options)
                }))
            }
        }), Marionette
}), this.JST = this.JST || {}, this.JST["leaderboard/competitor-stats"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        var keys = ["driveDistAvg", "driveAccuracyPct", "gir", "puttsGirAvg", "sandSaves", "eagles", "birdies", "bogeys"];
        __p += "\n", 0 === Object.keys(obj).length ? __p += "\n\t<div>" + (null == (__t = _.translate("No Stats Available")) ? "" : __t) + "</div>\n" : (__p += '\n\t<table class="golf">\n\t\t<thead>\n\t\t\t<tr>\n\t\t\t\t', _.each(keys, function (key) {
            __p += "\n\t\t\t\t\t", obj[key] && (__p += '\n\t\t\t\t\t\t<th class="align-center">\n\t\t\t\t\t\t\t', void 0 !== obj[key].displayName && (__p += "\n\t\t\t\t\t\t\t\t" + (null == (__t = obj[key].displayName || "") ? "" : __t) + "\n\t\t\t\t\t\t\t"), __p += "\n\t\t\t\t\t\t</th>\n\t\t\t\t\t"), __p += "\n\t\t\t\t"
        }), __p += "\n\t\t\t</tr>\n\t\t</thead>\n\t\t<tbody>\n\t\t\t<tr>\n\t\t\t\t", _.each(keys, function (key) {
            if (__p += "\n\t\t\t\t\t", obj[key]) {
                __p += "\n\t\t\t\t\t";
                var symbol = "driveAccuracyPct" === key || "gir" === key || "sandSaves" === key ? "%" : "";
                __p += '\n\t\t\t\t\t\t<td class="align-center">\n\t\t\t\t\t\t\t', void 0 !== obj[key].displayValue && (__p += "\n\t\t\t\t\t\t\t\t" + (null == (__t = obj[key].displayValue + symbol || "") ? "" : __t) + "\n\t\t\t\t\t\t\t"), __p += "\n\t\t\t\t\t\t</td>\n\t\t\t\t\t"
            }
            __p += "\n\t\t\t\t"
        }), __p += "\n\t\t\t</tr>\n\t\t</tbody>\n\t</table>\n")
    }
    return __p
}, this.JST["leaderboard/course-stats-table"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) __p += '<table class="course-stats-table" cellspacing="0" cellpadding="0" data-text-contract="Contract table" data-mobile-force-responsive="true" data-text-expand="Expand table" data-fix-cols="0" data-behavior="responsive_table">\n\t<colgroup>\n\t\t', config.forEach(function (entry) {
        __p += "\n\t\t\t\t<col " + (null == (__t = getColumnAttributes(entry)) ? "" : __t) + ">\n\t\t"
    }), __p += "\n\t</colgroup>\n\t<thead>\n\t\t<tr>\n\t\t\t", config.forEach(function (entry) {
        __p += "\n\t\t\t\t<th " + (null == (__t = getHeaderAttributes(entry)) ? "" : __t) + ">\n\t\t\t\t\t<span>" + (null == (__t = _.translate(entry.name)) ? "" : __t) + "</span>\n\t\t\t\t</th>\n\t\t\t"
    }), __p += "\n\t\t</tr>\n\t</thead>\n\t<tbody></tbody>\n</table>";
    return __p
}, this.JST["leaderboard/course-stats"] = function (obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape;
    with (obj) __p += '<div class="holes-wrapper"></div>';
    return __p
}, this.JST["leaderboard/courses-header"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) __p += '<div class="dropdown-wrapper hoverable width-auto caption" data-behavior="button_dropdown">\n\t<button class="coursebutton button-filter med dropdown-toggle" type="button">' + (null == (__t = defaultCourseName) ? "" : __t) + '</button>\n\t<ul class="coursetoggle dropdown-menu med" role="menu">\n\t\t', courses.forEach(function (course) {
        __p += '\n\t\t\t<li><a data-id="' + (null == (__t = course.id) ? "" : __t) + '" href="#">' + (null == (__t = course.name) ? "" : __t) + "</a></li>\n\t\t"
    }), __p += '\n\t</ul>\n</div>\n<div class="coursetoggle mobile-dropdown caption">\n\t<span class="mobile-arrow"></span>\n\t<select>\n\t\t', courses.forEach(function (course) {
        __p += '\n\t\t\t<option data-id="' + (null == (__t = course.id) ? "" : __t) + '" datclass="active">' + (null == (__t = course.name) ? "" : __t) + "</option>\n\t\t"
    }), __p += "\n\t\t}\n\t\t\n\t</select>\n</div>";
    return __p
}, this.JST["leaderboard/dropdown"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) tournamentGroups.forEach(function (tournamentGroup) {
        __p += '\n\t<li>\n\t\t<span class="dropdown__group-title">--- ' + (null == (__t = _.translate(tournamentGroup.month) + " " + tournamentGroup.year) ? "" : __t) + " ---</span>\n\t\t<ul>\n\t\t\t", tournamentGroup.tournaments.forEach(function (tournament) {
            __p += '\n\t\t\t\t<li>\n\t\t\t\t\t<a href="' + (null == (__t = tournament.link) ? "" : __t) + '">\n\t\t\t\t\t\t' + (null == (__t = tournament.label) ? "" : __t) + "\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t"
        }), __p += "\n\t\t</ul>\n\t</li>\n"
    });
    return __p
}, this.JST["leaderboard/full-playoff-competitor"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        __p += '<td class="' + (null == (__t = getWinnerClass()) ? "" : __t) + '">\n\t<span class="team-logo">\n\t\t', flag && (__p += '\n\t\t\t<img src="' + (null == (__t = getFlagUrl()) ? "" : __t) + '" alt="' + (null == (__t = country || "flag") ? "" : __t) + '">\n\t\t'), __p += "\n\t</span>\n\t" + (null == (__t = displayName) ? "" : __t) + "\n</td>\n\n", __p += isFull18 ? "\n\t<td>" + (null == (__t = getRelativeScore()) ? "" : __t) + "</td>\n" : "\n\t<td></td>\n", __p += "\n";
        var holes = getHolesPlayed();
        __p += "\n";
        for (var i = 0; i < holes; i++) {
            __p += "\n\t";
            var score = getScoreForHoleNumber(i);
            __p += '\n\t<td class="' + (null == (__t = getClassForHoleNumber(i)) ? "" : __t) + '"><span>' + (null == (__t = score) ? "" : __t) + "</span></td>\n\t", 8 === i && isFull18 ? __p += "\n\t\t<td>" + (null == (__t = getOutScore()) ? "" : __t) + "</td>\n\t" : 17 === i && isFull18 && (__p += "\n\t\t<td>" + (null == (__t = getInScore()) ? "" : __t) + "</td>\n\t\t<td>" + (null == (__t = getTotalScore()) ? "" : __t) + "</td>\n\t"), __p += "\n"
        }
    }
    return __p
}, this.JST["leaderboard/full-playoff"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        __p += '<div class="status">' + (null == (__t = getStatusText()) ? "" : __t) + '</div>\n<div class="table-key golf">\n\t<ul>\n\t\t<li class="yellow">Eagle</li>\n\t\t<li class="green">Birdie</li>\n\t\t<li class="red">Bogey</li>\n\t\t<li class="blue">Double Bogey or Worse</li>\n\t</ul>\n</div>\n<table class="inline golf" data-behavior="responsive_table">\n\t<colgroup>\n\t\t<col>\n\t\t<col>\n\t\t', isFull18 && (__p += '\n\t\t\t<col class="bordered">\n\t\t'), __p += "\n\t";
        var holes = getHolesToDisplay();
        __p += "\n\t";
        for (var i = 0; i < holes; i++) __p += "\n\t\t", __p += 8 === i && isFull18 ? '\n\t\t\t<col>\n\t\t\t<col class="bordered">\n\t\t' : 17 === i && isFull18 ? '\n\t\t\t<col>\n\t\t\t<col class="bordered">\n\t\t\t<col class="bordered">\n\t\t' : "\n\t\t\t<col>\n\t\t", __p += "\n\t";
        __p += "\n\t</colgroup>\n\t<thead>\n\t\t<tr>\n\t\t\t<th></th>\n\t\t\t<th></th>\n\t\t\t<th>Hole</th>\n\t\t\t";
        for (var i = 0; i < holes; i++) __p += "\n\t\t\t\t<th>" + (null == (__t = getHoleNumberForIndex(i)) ? "" : __t) + "</th>\n\t\t\t\t", 8 === i && isFull18 ? __p += "\n\t\t\t\t\t<th>" + (null == (__t = _.translate("OUT")) ? "" : __t) + "</th>\n\t\t\t\t" : 17 === i && isFull18 && (__p += "\n\t\t\t\t\t<th>" + (null == (__t = _.translate("IN")) ? "" : __t) + "</th>\n\t\t\t\t\t<th>" + (null == (__t = _.translate("TOT")) ? "" : __t) + "</th>\n\t\t\t\t"),
            __p += "\n\t\t\t";
        __p += "\n\t\t</tr>\n\t\t<tr>\n\t\t\t<th>PLAYER</th>\n\t\t\t<th>PAR</th>\n\t\t\t";
        for (var i = 0; i < holes; i++) __p += "\n\t\t\t\t<th>" + (null == (__t = getParForIndex(i)) ? "" : __t) + "</th>\n\t\t\t\t", 8 === i && isFull18 ? __p += "\n\t\t\t\t\t<th>" + (null == (__t = getParOut()) ? "" : __t) + "</th>\n\t\t\t\t" : 17 === i && isFull18 && (__p += "\n\t\t\t\t\t<th>" + (null == (__t = getParIn()) ? "" : __t) + "</th>\n\t\t\t\t\t<th>" + (null == (__t = getTotalPar()) ? "" : __t) + "</th>\n\t\t\t\t"), __p += "\n\t\t\t";
        __p += "\n\t\t</tr>\n\t</thead>\n\t<tbody></tbody>\n</table>"
    }
    return __p
}, this.JST["leaderboard/hole"] = function (obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape;
    with (obj) __p += "<td>" + (null == (__t = number) ? "" : __t) + "</td>\n<td>" + (null == (__t = par) ? "" : __t) + "</td>\n<td>" + (null == (__t = yards) ? "" : __t) + '</td>\n<td class="avgScore">' + (null == (__t = avgScore) ? "" : __t) + '</td>\n<td class="eagles">' + (null == (__t = eagles) ? "" : __t) + '</td>\n<td class="birdies">' + (null == (__t = birdies) ? "" : __t) + '</td>\n<td class="pars">' + (null == (__t = pars) ? "" : __t) + '</td>\n<td class="bogeys">' + (null == (__t = bogeys) ? "" : __t) + '</td>\n<td class="dBogey">' + (null == (__t = dBogey) ? "" : __t) + '</td>\n<td class="dBogeyPlus">' + (null == (__t = dBogeyPlus) ? "" : __t) + '</td>\n<td class="scoreToPar ' + (null == (__t = getMovementClass(scoreToPar)) ? "" : __t) + '">' + (null == (__t = formatScore(scoreToPar)) ? "" : __t) + "</td>\n";
    return __p
}, this.JST["leaderboard/linescores"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        if (showRoundDropdown() === !0) {
            __p += '\n\t<div class="dropdown-wrapper hoverable width-auto">\n\t\t<button class="button-filter med dropdown-toggle" type="button">' + (null == (__t = getMostRecentRoundText()) ? "" : __t) + '</button>\n\t\t<ul class="dropdown-menu med" role="menu">\n\t\t\t';
            for (var i = getNumRounds(); i > 0; i--) __p += '\n\t\t\t\t<li>\n\t\t\t\t\t<a href="#" class="scorecard-round" data-round="' + (null == (__t = i) ? "" : __t) + '">' + (null == (__t = _.translate("Round")) ? "" : __t) + " " + (null == (__t = i) ? "" : __t) + "</a>\n\t\t\t\t</li>\n\t\t\t";
            __p += '\n\t\t</ul>\n\t</div>\n\t<div class="mobile-dropdown">\n\t\t<span class="mobile-arrow"></span>\n\t\t<select class="scorecard-round-mobile">\n\t\t\t';
            for (var i = getNumRounds(); i > 0; i--) __p += '\n\t\t\t\t<option class="active" value="' + (null == (__t = i) ? "" : __t) + '">' + (null == (__t = _.translate("Round")) ? "" : __t) + " " + (null == (__t = i) ? "" : __t) + "</option>\n\t\t\t";
            __p += "\n\t\t</select>\n\t</div>\n"
        }
        __p += '\n\n<div class="table-key golf">\n\t<ul>\n\t\t<li class="yellow">' + (null == (__t = _.translate("Eagle")) ? "" : __t) + '</li>\n\t\t<li class="green">' + (null == (__t = _.translate("Birdie")) ? "" : __t) + '</li>\n\t\t<li class="red">' + (null == (__t = _.translate("Bogey")) ? "" : __t) + '</li>\n\t\t<li class="blue">' + (null == (__t = _.translate("Double Bogey or Worse")) ? "" : __t) + '</li>\n\n\t</ul>\n</div>\n<div class="scorecard"></div>'
    }
    return __p
}, this.JST["leaderboard/mobile-tournament-dropdown"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) tournamentGroups.forEach(function (tournamentGroup) {
        __p += "\n\t<option>" + (null == (__t = _.translate("Tournaments")) ? "" : __t) + '</option>\n\t<optgroup label="--- ' + (null == (__t = _.translate(tournamentGroup.month) + " " + tournamentGroup.year) ? "" : __t) + ' ---">\n\t\t', tournamentGroup.tournaments.forEach(function (tournament) {
            __p += '\n\t\t\t<option data-link="' + (null == (__t = tournament.link) ? "" : __t) + '">' + (null == (__t = tournament.label) ? "" : __t) + "</option>\n\t\t"
        }), __p += "\n\t</optgroup>\n"
    });
    return __p
}, this.JST["leaderboard/player-stat"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        __p += '<tr class="player-overview">\n\t<td class="rank">' + (null == (__t = position) ? "" : __t) + '</td>\n\t<td class="playerName align-left">\n\t\t<span class="team-logo">\n\t\t\t', flag && (__p += '\n\t\t\t\t<img src="' + (null == (__t = getFlag()) ? "" : __t) + '" alt="' + (null == (__t = country) ? "" : __t) + '" title="' + (null == (__t = country) ? "" : __t) + '"/>\n\t\t\t'), __p += "\n\t\t</span>\n\t\t";
        var showLink = showPlayerLink();
        __p += "\n\t\t", showLink && (__p += '\n\t\t<a role="button" class="full-name">\n\t\t'), __p += "\n\t\t\t" + (null == (__t = displayName) ? "" : __t) + "\n\t\t", showLink && (__p += "\n\t\t</a>\n\t\t"), __p += "\n\t\t", displayName && (__p += "\n\t\t\t", showLink && (__p += '\n\t\t\t\t<a role="button" class="short-name">\n\t\t\t'), __p += "\n\t\t\t\t\t" + (null == (__t = espn_ui.Helpers.util.shortenPlayerName({
            displayName: displayName,
            type: "firstInitial"
        })) ? "" : __t) + "\n\t\t\t", showLink && (__p += "\n\t\t\t\t</a>\n\t\t\t"), __p += "\n\t\t"), __p += '\n\t</td>\n\t<td class="driveDistAvg">' + (null == (__t = driveDistAvg || "-") ? "" : __t) + '</td>\n\t<td class="fwysHit">' + (null == (__t = fwysHit + "%") ? "" : __t) + '</td>\n\t<td class="gir">' + (null == (__t = gir + "%") ? "" : __t) + '</td>\n\t<td class="puttsGirAvg">' + (null == (__t = puttsGirAvg) ? "" : __t) + '</td>\n\t<td class="eagles">' + (null == (__t = eagles) ? "" : __t) + '</td>\n\t<td class="birdies">' + (null == (__t = birdies) ? "" : __t) + '</td>\n\t<td class="pars">' + (null == (__t = pars) ? "" : __t) + '</td>\n\t<td class="bogeys">' + (null == (__t = bogeys) ? "" : __t) + '</td>\n\t<td class="doubles">' + (null == (__t = doubles) ? "" : __t) + '</td>\n\t<td class="scoreToPar">' + (null == (__t = formatScore(scoreToPar)) ? "" : __t) + '</td>\n</tr>\n<tr class="player-details results collapse odd">\n\t<td colspan="14" class="content">\n\t\t<div class="loading"></div>\n\t</td>\n</tr>\n'
    }
    return __p
}, this.JST["leaderboard/player-stats-leaders"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) __p += '<div class="player-details-table-wrap">\n\t<table class="player-details-table" cellspacing="0" cellpadding="0" data-text-contract="Contract table" data-text-expand="Expand table" data-mobile-force-responsive="true" data-fix-cols="0">\n\t\t<tbody>\n\t\t\t<tr id="stat-leaders" class="player-details">\n\t\t\t\t', categories.forEach(function (cat) {
        __p += '\n\t\t\t\t\t<td class="' + (null == (__t = cat) ? "" : __t) + '">\n\t\t\t\t\t\t<div class="player-cell">\n\t\t\t\t\t\t\t<div class="title">' + (null == (__t = getNameForCategory(cat)) ? "" : __t) + '</div>\n\t\t\t\t\t\t\t<div class="player-wrap">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</td>\n\t\t\t\t'
    }), __p += "\n\t\t\t</tr>\n\t\t</tbody>\n\t</table>\n</div>";
    return __p
}, this.JST["leaderboard/player-stats-table"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) __p += '<table class="player-stats-table" cellspacing="0" cellpadding="0" data-text-contract="Contract table" data-mobile-force-responsive="true" data-text-expand="Expand table" data-fix-cols="0" data-behavior="responsive_table">\n\t<colgroup>\n\t\t', config.forEach(function (entry) {
        __p += "\n\t\t\t\t<col " + (null == (__t = getColumnAttributes(entry)) ? "" : __t) + ">\n\t\t"
    }), __p += "\n\t</colgroup>\n\t<thead>\n\t\t<tr>\n\t\t\t", config.forEach(function (entry) {
        __p += "\n\t\t\t\t<th " + (null == (__t = getHeaderAttributes(entry)) ? "" : __t) + ">\n\t\t\t\t\t<span>" + (null == (__t = entry.name) ? "" : __t) + "</span>\n\t\t\t\t</th>\n\t\t\t"
    }), __p += "\n\t\t</tr>\n\t</thead>\n\t<tbody></tbody>\n</table>";
    return __p
}, this.JST["leaderboard/player-stats"] = function (obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape;
    with (obj) __p += '<div class="player-stats-leaders-wrapper"></div>\n<div class="player-stats-table-wrapper"></div>';
    return __p
}, this.JST["leaderboard/playoff"] = function (obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape;
    with (obj) __p += '<div class="region-2"></div>\n<div class="region-1"></div>';
    return __p
}, this.JST["leaderboard/profile"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) (dateOfBirth || birthPlace || age) && (__p += '\n<ul class="profile"> \n\t', dateOfBirth && (__p += '\n\t<li class="date-container" data-behavior="date_time" data-date="' + (null == (__t = dateOfBirth) ? "" : __t) + '">' + (null == (__t = _.translate("Birth Date")) ? "" : __t) + ': <span data-dateFormat="date12" class="date"></span></li>\n\t'), __p += "\n\t", birthPlace && (__p += "\n\t<li>" + (null == (__t = _.translate("Birth Place")) ? "" : __t) + ": <span>" + (null == (__t = birthPlace || "") ? "" : __t) + "</span></li>\n\t"), __p += "\n\t", age && (__p += "\n\t<li>" + (null == (__t = _.translate("Age")) ? "" : __t) + ": <span>" + (null == (__t = age) ? "" : __t) + "</span></li>\n\t"), __p += "\n</ul>\n"), __p += "\n", (obj.hand || obj.college) && (__p += '\n<ul class="profile">\n\t', obj.hand && (__p += "\n\t<li>" + (null == (__t = _.translate("Swings")) ? "" : __t) + ": <span>" + (null == (__t = hand || "-") ? "" : __t) + "</span></li>\n\t"), __p += "\n\t", obj.college && (__p += "\n\t<li>" + (null == (__t = _.translate("College")) ? "" : __t) + ": <span>" + (null == (__t = college || "-") ? "" : __t) + "</span></li>\n\t"), __p += " \n</ul>\n"), __p += '\n<ul class="profile">\n\t<li class="title"><span>' + (null == (__t = _.translate("PGA Money List")) ? "" : __t) + "</span></li>\n\t<li>" + (null == (__t = _.translate("Rank")) ? "" : __t) + ': <span class="rank">' + (null == (__t = rank || "-") ? "" : __t) + "</span></li>\n\t<li>" + (null == (__t = _.translate("Earnings")) ? "" : __t) + ': <span class="earnings">' + (null == (__t = earnings || "-") ? "" : __t) + '</span></li>\n</ul>\n<div class="user-meta">\n\t<a href="' + (null == (__t = link) ? "" : __t) + '" class="button-alt sm">' + (null == (__t = _.translate("Full Player Profile")) ? "" : __t) + "</a>\n</div>";
    return __p
}, this.JST["leaderboard/scorecard"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        __p += "<colgroup>\n\t";
        for (var i = 0; i <= 21; i++) __p += "\n\t\t", __p += 0 === i || 10 === i || 20 === i || 21 === i ? '\n\t\t\t<col class="bordered">\n\t\t' : "\n\t\t\t<col>\n\t\t", __p += "\n\t";
        __p += '\n\t</colgroup>\n<thead>\n\t<tr>\n\t\t<th class="align-right">' + (null == (__t = _.translate("Hole")) ? "" : __t) + "</th>\n\t\t";
        for (var i = 1; i <= 18; i++) __p += "\n\t\t\t<th>" + (null == (__t = i) ? "" : __t) + "</th>\n\t\t\t", 9 === i ? __p += "\n\t\t\t\t<th>OUT</th>\n\t\t\t" : 18 === i && (__p += "\n\t\t\t\t<th>" + (null == (__t = _.translate("IN")) ? "" : __t) + "</th>\n\t\t\t\t<th>" + (null == (__t = _.translate("TOT")) ? "" : __t) + "</th>\n\t\t\t"), __p += "\n\t\t";
        if (__p += "\n\t</tr>\n</thead>\n<tbody>\n\t", hasYardages() === !0) {
            __p += '\n\t\t<tr class="yardages">\n\t\t\t<td class="align-right">' + (null == (__t = _.translate("Yards")) ? "" : __t) + "</td>\n\t\t\t";
            var holes = getHoles();
            __p += "\n\t\t\t", holes && holes.length > 0 && (__p += "\n\t\t\t\t", _.each(holes, function (hole, i) {
                __p += "\n\t\t\t\t\t<td>" + (null == (__t = hole.yards) ? "" : __t) + "</td>\n\t\t\t\t\t", 8 === i ? __p += '\n\t\t\t\t\t\t<td class="out">' + (null == (__t = getYardsOut()) ? "" : __t) + "</td>\n\t\t\t\t\t" : 17 === i && (__p += '\n\t\t\t\t\t\t<td class="in">' + (null == (__t = getYardsIn()) ? "" : __t) + '</td>\n\t\t\t\t\t\t<td class="total">' + (null == (__t = getTotalYards()) ? "" : __t) + "</td>\n\t\t\t\t\t"), __p += "\n\t\t\t\t"
            }), __p += "\n\t\t\t"), __p += "\n\t\t</tr>\n\t"
        }
        __p += '\n\t<tr>\n\t\t<td class="align-right">' + (null == (__t = _.translate("Par")) ? "" : __t) + "</td>\n\t\t";
        var holes = getHoles();
        if (__p += "\n\t\t", holes && holes.length > 0) __p += "\n\t\t\t", _.each(holes, function (hole, i) {
            __p += "\n\t\t\t\t<td>" + (null == (__t = hole.par) ? "" : __t) + "</td>\n\t\t\t\t", 8 === i ? __p += '\n\t\t\t\t\t<td class="out">' + (null == (__t = getParOut()) ? "" : __t) + "</td>\n\t\t\t\t" : 17 === i && (__p += '\n\t\t\t\t\t<td class="in">' + (null == (__t = getParIn()) ? "" : __t) + '</td>\n\t\t\t\t\t<td class="total">' + (null == (__t = getPar()) ? "" : __t) + "</td>\n\t\t\t\t"), __p += "\n\t\t\t"
        }), __p += "\n\t\t";
        else {
            if (__p += "\n\t\t\t", obj.scores && scores.length > 0) __p += "\n\t\t\t\t", _.each(scores, function (score) {
                __p += "\n\t\t\t\t\t<td>" + (null == (__t = score.par) ? "" : __t) + "</td>\n\t\t\t\t\t", 9 === score.holeNumber ? __p += "\n\t\t\t\t\t\t<td>" + (null == (__t = getParOut()) ? "" : __t) + "</td>\n\t\t\t\t\t" : 18 === score.holeNumber && (__p += "\n\t\t\t\t\t\t<td>" + (null == (__t = getParIn()) ? "" : __t) + "</td>\n\t\t\t\t\t\t<td>" + (null == (__t = getPar()) ? "" : __t) + "</td>\n\t\t\t\t\t"), __p += "\n\t\t\t\t"
            }), __p += "\n\t\t\t";
            else {
                __p += "\n\t\t\t\t";
                for (var i = 1; i <= 18; i++) __p += "\n\t\t\t\t\t<td><span>-</span></td>\n\t\t\t\t\t", 9 === i ? __p += "\n\t\t\t\t\t\t<td>-</td>\n\t\t\t\t\t" : 18 === i && (__p += "\n\t\t\t\t\t\t<td>-</td>\n\t\t\t\t\t\t<td>-</td>\n\t\t\t\t\t"), __p += "\n\t\t\t\t";
                __p += "\n\t\t\t"
            }
            __p += "\n\t\t"
        }
        if (__p += '\n\t</tr>\n\t<tr>\n\t\t<td class="align-right">' + (null == (__t = _.translate("Score")) ? "" : __t) + "</td>\n\t\t", obj.scores && scores.length > 0) __p += "\n\t\t\t", _.each(scores, function (score) {
            __p += '\n\t\t\t\t<td class="' + (null == (__t = getClassForScore(score)) ? "" : __t) + '"><span>' + (null == (__t = score.score) ? "" : __t) + "</span></td>\n\t\t\t\t", 9 === score.holeNumber ? __p += "\n\t\t\t\t\t<td>" + (null == (__t = outScore) ? "" : __t) + "</td>\n\t\t\t\t" : 18 === score.holeNumber && (__p += "\n\t\t\t\t\t<td>" + (null == (__t = inScore) ? "" : __t) + "</td>\n\t\t\t\t\t<td>" + (null == (__t = aggregateScore) ? "" : __t) + "</td>\n\t\t\t\t"), __p += "\n\t\t\t"
        }), __p += "\n\t\t";
        else {
            __p += "\n\t\t\t";
            for (var i = 1; i <= 18; i++) __p += "\n\t\t\t\t<td><span>-</span></td>\n\t\t\t\t", 9 === i ? __p += "\n\t\t\t\t\t<td>-</td>\n\t\t\t\t" : 18 === i && (__p += "\n\t\t\t\t\t<td>-</td>\n\t\t\t\t\t<td>-</td>\n\t\t\t\t"), __p += "\n\t\t\t";
            __p += "\n\t\t"
        }
        __p += "\n\t</tr>\n</tbody>"
    }
    return __p
}, this.JST["leaderboard/stat-leader"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        __p += '<div class="player">\n\t<div class="headshot-md athlete">\n\t\t<img src="' + (null == (__t = getHeadshot()) ? "" : __t) + '" alt="' + (null == (__t = displayName) ? "" : __t) + '">\n\t</div>\n\t<div class="details">\n\t\t<div class="name">\n\t\t\t', flag && (__p += '\n\t\t\t\t<img src="' + (null == (__t = getFlag()) ? "" : __t) + '" alt="' + (null == (__t = !!flag && flag.alt) ? "" : __t) + '"/>\n\t\t\t'), __p += "\n\t\t\t" + (null == (__t = displayName) ? "" : __t) + '\n\t\t</div>\n\t\t<div class="points">\n\t\t\t';
        var category = getCategory();
        __p += "\n\t\t\t", __p += "fwysHit" == category || "gir" == category ? "\n\t\t\t\t" + (null == (__t = getFeaturedStat() + "%") ? "" : __t) + "\n\t\t\t" : "\n\t\t\t\t" + (null == (__t = getFeaturedStat()) ? "" : __t) + "\n\t\t\t", __p += '\n\t\t</div>\n\t</div>\n\t<div class="meta">\n\t\t<span class="scoreToPar">' + (null == (__t = getScoreToPar()) ? "" : __t) + '</span>\n\t\t<span class="position">' + (null == (__t = getPositionText()) ? "" : __t) + "</span>\n\t</div>\n</div>\n"
    }
    return __p
}, this.JST["leaderboard/stat-leaders"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) __p += '<div class="player-details-table-wrap">\n\t<table class="player-details-table" cellspacing="0" cellpadding="0" data-text-contract="Contract table" data-text-expand="Expand table" data-mobile-force-responsive="true" data-fix-cols="0">\n\t\t<tbody>\n\t\t\t<tr id="stat-leaders" class="player-details">\n\t\t\t\t', categories.forEach(function (cat) {
        __p += '\n\t\t\t\t\t<td class="' + (null == (__t = cat) ? "" : __t) + '">\n\t\t\t\t\t\t<div class="player-cell">\n\t\t\t\t\t\t\t<div class="title">' + (null == (__t = getNameForCategory(cat)) ? "" : __t) + '</div>\n\t\t\t\t\t\t\t<div class="player-wrap"></div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</td>\n\t\t\t\t'
    }), __p += "\n\t\t\t</tr>\n\t\t</tbody>\n\t</table>\n</div>";
    return __p
}, this.JST["leaderboard/sudden-death-playoff-competitor"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        __p += '<td class="' + (null == (__t = getWinnerClass()) ? "" : __t) + '">\n\t<span class="team-logo">\n\t\t', obj.flag && (__p += '\n\t\t\t<img src="' + (null == (__t = getFlagUrl()) ? "" : __t) + '" alt="' + (null == (__t = country || "flag") ? "" : __t) + '" title="' + (null == (__t = country || "") ? "" : __t) + '">\n\t\t'), __p += "\n\t</span>\n\t" + (null == (__t = displayName) ? "" : __t) + "\n</td>\n\n";
        for (var i = 0; i < holesPlayed; i++) {
            __p += "\n\t";
            var hole = linescore.get("scores").at(i);
            __p += "\n\t", __p += hole && hole.get("score") ? "\n\t\t<td>" + (null == (__t = hole.get("score")) ? "" : __t) + "</td>\n\t" : "\n\t\t<td>--</td>\n\t", __p += "\n"
        }
    }
    return __p
}, this.JST["leaderboard/sudden-death-playoff"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) __p += '<div class="status">' + (null == (__t = getStatusText()) ? "" : __t) + '</div>\n<table data-behavior="responsive_table">\n\t<thead>\n\t\t<tr>\n\t\t\t<th>PLAYER</th>\n\t\t\t', getHoleNumbers().forEach(function (holeNumber) {
        __p += "\n\t\t\t\t<th>" + (null == (__t = _.translate("Hole") + " " + holeNumber) ? "" : __t) + "</th>\n\t\t\t"
    }), __p += "\n\t\t\t\n\t\t</tr>\n\t</thead>\n\t<tbody></tbody>\n</table>";
    return __p
}, this.JST["leaderboard/suddendeath"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        __p += '<div class="status">' + (null == (__t = status) ? "" : __t) + '</div>\n<table data-behavior="responsive_table">\n\t<thead>\n\t\t<tr>\n\t\t\t<th>' + (null == (__t = _.translate("PLAYER")) ? "" : __t) + "</th>\n\t\t\t";
        var holes = getHoles();
        __p += "\n\t\t\t", _.each(holes, function (holeNumber) {
            __p += "\n\t\t\t\t<th>" + (null == (__t = holeNumber) ? "" : __t) + "</th>\n\t\t\t"
        }), __p += '\n\t\t</tr>\n\t</thead>\n\t<tbody class="competitors"></tbody>\n</table>\n'
    }
    return __p
}, this.JST["leaderboard/tournament-details-error"] = function (obj) {
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape;
    with (obj) __p += '<tbody>\n\t<tr class="">\n\t\t<td>\n\t\t\t<div class="error-message" style="text-align:center;">' + (null == (__t = _.translate("Could not fetch player details.  Please try again later.")) ? "" : __t) + "</div>\n\t\t</td>\n\t</tr>\n</tbody>";
    return __p
}, this.JST["leaderboard/tournament-details"] = function (obj) {
    function print() {
        __p += __j.call(arguments, "")
    }
    obj || (obj = {});
    var __t, __p = "",
        __e = _.escape,
        __j = Array.prototype.join;
    with (obj) {
        __p += '<tbody>\n\t<tr class="">\n\t\t<td>\n\t\t\t<div class="headshot">\n\t\t\t\t<div class="headshot-xl athlete">\n\t\t\t\t\t';
        var headshotUrl = getHeadshotUrl();
        __p += "\n\t\t\t\t\t", headshotUrl && (__p += '\n\t\t\t\t\t\t<img src="' + (null == (__t = headshotUrl) ? "" : __t) + '"/>\n\t\t\t\t\t'), __p += '\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="detail">\n\t\t\t\t<div class="tab-container alt">\n\t\t\t\t\t<ul class="tabs alt">\n\t\t\t\t\t\t', showScorecard() && (__p += '\n\t\t\t\t\t\t\t<li class="' + (null == (__t = getClassName("linescores")) ? "" : __t) + '" data-tab="linescores"><span>' + (null == (__t = _.translate("Scorecard")) ? "" : __t) + "</span></li>\n\t\t\t\t\t\t"), __p += "\n\t\t\t\t\t\t", showStats() && (__p += '\n\t\t\t\t\t\t\t<li data-tab="stats"><span>' + (null == (__t = _.translate("Tournament Stats")) ? "" : __t) + "</span></li>\n\t\t\t\t\t\t"), __p += "\n\t\t\t\t\t\t", showProfile() && (__p += '\n\t\t\t\t\t\t\t<li class="' + (null == (__t = getClassName("profile")) ? "" : __t) + '" data-tab="profile"><span>' + (null == (__t = _.translate("Profile &amp; Ranking")) ? "" : __t) + "</span></li>\n\t\t\t\t\t\t"), __p += '\n\t\t\t\t\t</ul>\n\t\t\t\t</div>\n\t\t\t\t<div class="tab-content">\n\t\t\t\t\t<div class="tab-pane active"></div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</td>\n\t</tr>\n</tbody>'
    }
    return __p
},
    function e(t, n, r) {
        function s(o, u) {
            if (!n[o]) {
                if (!t[o]) {
                    var a = "function" == typeof require && require;
                    if (!u && a) return a(o, !0);
                    if (i) return i(o, !0);
                    var f = new Error("Cannot find module '" + o + "'");
                    throw f.code = "MODULE_NOT_FOUND", f
                }
                var l = n[o] = {
                    exports: {}
                };
                t[o][0].call(l.exports, function (e) {
                    var n = t[o][1][e];
                    return s(n ? n : e)
                }, l, l.exports, e, t, n, r)
            }
            return n[o].exports
        }
        for (var i = "function" == typeof require && require, o = 0; o < r.length; o++) s(r[o]);
        return s
    }({
        1: [function (require, module, exports) {
            function getOptions(data) {
                var currentRound = espn_util.find(data, ["competitions", 0, "status", "period"]),
                    numberOfRounds = espn_util.find(data, ["tournament", "numberOfRounds"]);
                return currentRound > numberOfRounds && (currentRound = numberOfRounds), {
                    currentRound: currentRound,
                    numberOfRounds: numberOfRounds,
                    parse: !0
                }
            }

            function getId(player) {
                return player.id ? player.id : player.name ? player.name.toLowerCase().replace(/[\s\.']/g, "") : void 0
            }
            var CompetitorModel = require("../models/Competitor"),
                _ = window._,
                $ = window.$,
                espn_util = window.espn_util,
                CompetitorsCollection = window.Backbone.Collection.extend({
                    comparator: function (model) {
                        return parseInt(model.get("sortOrder"), 10)
                    },
                    parse: function (data) {
                        var models = [],
                            competitors = espn_util.find(data, ["competitions", 0, "competitors"]),
                            options = getOptions(data);
                        return _.isArray(competitors) && competitors.length > 0 && _.each(competitors, function (competitor) {
                            competitor && models.push(new CompetitorModel(competitor, options))
                        }), models
                    },
                    update: function (data) {
                        var that = this,
                            options = getOptions(data);
                        if (data && data.competitions && data.competitions.length > 0) {
                            var competitors = data.competitions[0].competitors;
                            _.isArray(competitors) && competitors.length > 0 && _.each(competitors, function (competitor) {
                                var id = getId(competitor),
                                    competitorModel = that.get(id);
                                competitorModel && competitorModel.update(competitor, options)
                            })
                        }
                    },
                    addStats: function (callback) {
                        return callback = callback || function () { }, $.ajax({
                            url: espn.leaderboard.playerStatsUrl + "&rand=" + Math.random(),
                            success: function (data) {
                                this.updatePlayerStats(data, callback)
                            }.bind(this),
                            error: function () {
                                callback("Something went wrong")
                            }.bind(this)
                        })
                    },
                    updatePlayerStats: function (data, callback) {
                        callback = callback || function () { };
                        var that = this;
                        data && data.leaderboard && !_.isEmpty(data.leaderboard) && _.each(data.leaderboard, function (leader) {
                            var thisCompetitor = that.get(leader.id);
                            void 0 === thisCompetitor && (thisCompetitor = that.findWhere({
                                displayName: leader.displayName
                            })), thisCompetitor && thisCompetitor.updatePlayerStats(leader.stats)
                        }), callback(null)
                    }
                });
            module.exports = CompetitorsCollection
        }, {
            "../models/Competitor": 12
        }],
        2: [function (require, module, exports) {
            var CourseModel = require("../models/Course"),
                _ = window._,
                $ = window.$,
                CoursesCollection = window.Backbone.Collection.extend({
                    parse: function (data) {
                        var models = [];
                        return data && data.courses && _.isArray(data.courses) && data.courses.length > 0 && _.each(data.courses, function (course) {
                            models.push(new CourseModel(course, {
                                parse: !0
                            }))
                        }), models
                    },
                    update: function (callback) {
                        return callback = callback || function () { }, $.ajax({
                            url: espn.leaderboard.courseStatsUrl + "&rand=" + Math.random(),
                            success: function (data) {
                                if (data && data.courses && _.isArray(data.courses) && data.courses.length > 0) {
                                    var model;
                                    _.each(data.courses, function (course) {
                                        model = this.findWhere({
                                            id: course.courseId
                                        }), model && model.get("holes").addStats(course.holes)
                                    }.bind(this))
                                }
                                callback(null, this)
                            }.bind(this),
                            error: function () {
                                callback("Something went wrong")
                            }.bind(this)
                        })
                    }
                });
            module.exports = CoursesCollection
        }, {
            "../models/Course": 13
        }],
        3: [function (require, module, exports) {
            var HoleModel = require("../models/Hole"),
                _ = window._,
                HolesCollection = window.Backbone.Collection.extend({
                    comparator: function (model) {
                        return parseInt(model.get("number"), 10)
                    },
                    parse: function (holes) {
                        var models = [];
                        return holes && holes.length > 0 && _.each(holes, function (hole) {
                            models.push(new HoleModel(hole, {
                                parse: !0
                            }))
                        }), models
                    },
                    addStats: function (holes) {
                        if (holes && holes.length > 0) {
                            var model;
                            holes.forEach(function (hole) {
                                model = this.findWhere({
                                    id: hole.holeNumber
                                }), model && model.addStats(hole)
                            }.bind(this))
                        }
                    }
                });
            module.exports = HolesCollection
        }, {
            "../models/Hole": 15
        }],
        4: [function (require, module, exports) {
            var PlayoffCompetitorModel = require("../models/PlayoffCompetitor"),
                _ = window._,
                PlayoffCompetitorsCollection = window.Backbone.Collection.extend({
                    parse: function (competitors, options) {
                        var models = [];
                        return _.each(competitors, function (competitor) {
                            models.push(new PlayoffCompetitorModel(competitor, options))
                        }), models
                    },
                    update: function (competitors, options) {
                        var that = this;
                        _.each(competitors, function (competitor) {
                            that.get(competitor.id) && that.get(competitor.id).update(competitor, options)
                        })
                    },
                    getHoleNumbers: function () {
                        var linescore = this.getRepresentativeLinescore();
                        return linescore && linescore.get("scores") ? linescore.get("scores").map(function (hole) {
                            return hole.get("holeNumber")
                        }) : []
                    },
                    getRepresentativeLinescore: function () {
                        var representative, linescore, scores, max = 0;
                        return _.each(this.models, function (competitor) {
                            linescore = competitor.get("linescore"), scores = linescore && linescore.get("scores"), linescore && scores.length > max && (representative = linescore, max = scores.length)
                        }), representative
                    }
                });
            module.exports = PlayoffCompetitorsCollection
        }, {
            "../models/PlayoffCompetitor": 19
        }],
        5: [function (require, module, exports) {
            function getRoundKeys(competitors) {
                var roundKeys = [];
                return competitors.forEach(function (competitor) {
                    competitor.linescores.forEach(function (linescore) {
                        linescore.isPlayoff === !0 && roundKeys.indexOf(linescore.period) === -1 && roundKeys.push(linescore.period)
                    })
                }), roundKeys
            }
            var PlayoffRoundModel = require("../models/PlayoffRound"),
                PlayoffRoundsCollection = window.Backbone.Collection.extend({
                    parse: function (competitors) {
                        var roundKeys = getRoundKeys(competitors),
                            models = [];
                        return roundKeys.forEach(function (key) {
                            models.push(new PlayoffRoundModel({
                                key: key,
                                competitors: competitors
                            }, {
                                    parse: !0
                                }))
                        }), models
                    },
                    update: function (competitors) {
                        var roundKeys = getRoundKeys(competitors),
                            that = this;
                        roundKeys.forEach(function (key) {
                            that.findWhere({
                                key: key
                            }) ? that.findWhere({
                                key: key
                            }).update({
                                key: key,
                                competitors: competitors
                            }) : that.add(new PlayoffRoundModel({
                                key: key,
                                competitors: competitors
                            }, {
                                    parse: !0
                                }))
                        })
                    }
                });
            module.exports = PlayoffRoundsCollection
        }, {
            "../models/PlayoffRound": 22
        }],
        6: [function (require, module, exports) {
            var PlayoffHoleModel = require("../models/PlayoffHole"),
                _ = window._,
                PlayoffScoresCollection = window.Backbone.Collection.extend({
                    parse: function (data) {
                        var models = [],
                            linescores = data.linescores.filter(function (hole) {
                                return 0 !== hole.period
                            });
                        return _.each(linescores, function (hole, i) {
                            hole.id = i + 1, models.push(new PlayoffHoleModel(hole, {
                                parse: !0
                            }))
                        }), models
                    },
                    update: function (data) {
                        var model, linescores = data.linescores.filter(function (hole) {
                            return 0 !== hole.period
                        });
                        _.each(linescores, function (hole, i) {
                            hole.id = i + 1, model = this.get(hole.id), model ? model.update(hole) : this.add(new PlayoffHoleModel(hole, {
                                parse: !0
                            }))
                        }.bind(this))
                    }
                });
            module.exports = PlayoffScoresCollection
        }, {
            "../models/PlayoffHole": 20
        }],
        7: [function (require, module, exports) {
            var RoundModel = require("../models/tournament-details/Round"),
                _ = window._,
                RoundsCollection = window.Backbone.Collection.extend({
                    comparator: "roundNumber",
                    parse: function (data) {
                        var models = [];
                        return data && _.isArray(data) && _.each(data, function (round) {
                            round && round.linescores && round.linescores.length > 0 && models.push(new RoundModel(round, {
                                parse: !0
                            }))
                        }), models
                    },
                    update: function (data) {
                        var that = this;
                        data && _.isArray(data) && data.length > 0 && _.each(data, function (round) {
                            if (round && round.linescores && round.linescores.length > 0) {
                                var roundNumber = round.period,
                                    roundModel = that.findWhere({
                                        roundNumber: roundNumber
                                    });
                                roundModel ? roundModel.update(round) : that.add(new RoundModel(round, {
                                    parse: !0
                                }))
                            }
                        })
                    }
                });
            module.exports = RoundsCollection
        }, {
            "../models/tournament-details/Round": 25
        }],
        8: [function (require, module, exports) {
            var HoleScoreModel = window.Backbone.Model.extend({}),
                _ = window._,
                ScoresCollection = window.Backbone.Collection.extend({
                    parse: function (data) {
                        var scoresByHole = {};
                        data.linescores && data.linescores.length > 0 && _.each(data.linescores, function (hole) {
                            scoresByHole[hole.period] = hole
                        });
                        for (var hole, models = [], i = 1; i <= 18; i++) hole = scoresByHole[i], hole ? models.push(new HoleScoreModel({
                            holeNumber: hole.period,
                            score: hole.value,
                            scoreName: hole.scoreType.displayName,
                            scoreType: hole.scoreType.name,
                            par: hole.par || "-"
                        })) : models.push(new HoleScoreModel({
                            holeNumber: i,
                            score: "-",
                            scoreName: "Par",
                            scoreType: "PAR",
                            par: "-"
                        }));
                        return models
                    },
                    update: function (data) {
                        var model;
                        _.each(data.linescores, function (hole) {
                            hole && (model = this.findWhere({
                                holeNumber: hole.period
                            }), model && model.set({
                                holeNumber: hole.period,
                                score: hole.value,
                                scoreName: hole.scoreType.displayName,
                                scoreType: hole.scoreType.name,
                                par: hole.par || "-"
                            }))
                        }.bind(this))
                    }
                });
            module.exports = ScoresCollection
        }, {}],
        9: [function (require, module, exports) {
            var _ = window._;
            module.exports = {
                ANIMATE_SPEED: 1e3,
                MISSING_HEADSHOT: "http://a.espncdn.com/combiner/i?img=/i/headshots/nophoto.png",
                CLASSNAME_ARROW_UP: "icon-arrow-up-solid-before positive",
                CLASSNAME_ARROW_DOWN: "icon-arrow-down-solid-before negative",
                LOADING_TEXT: _.translate("Loading") + "...",
                TOURNAMENTS_TEXT: _.translate("Tournaments"),
                SCORECARD_COLOR_CODES: {
                    ACE: "yellow",
                    DOUBLE_EAGLE: "yellow",
                    EAGLE: "yellow",
                    BIRDIE: "green",
                    BOGEY: "red",
                    DOUBLE_BOGEY: "blue",
                    TRIPLE_BOGEY: "blue",
                    OTHER: "blue"
                },
                PLAYER_STATS_COLUMN_CONFIG: [{
                    key: "position",
                    sortDirection: "asc",
                    sortType: "integer",
                    name: _.translate("Pos"),
                    className: "sm asc",
                    sorted: !0
                }, {
                    key: "playerName",
                    sortKey: "lastName",
                    sortDirection: "asc",
                    sortType: "string",
                    name: _.translate("Player"),
                    className: "md align-left"
                }, {
                    key: "driveDistAvg",
                    sortDirection: "desc",
                    sortType: "double",
                    name: _.translate("yds/drv"),
                    className: ""
                }, {
                    key: "fwysHit",
                    sortDirection: "desc",
                    sortType: "double",
                    name: _.translate("drv acc"),
                    className: ""
                }, {
                    key: "gir",
                    sortDirection: "desc",
                    sortType: "double",
                    name: _.translate("gir"),
                    className: ""
                }, {
                    key: "puttsGirAvg",
                    sortDirection: "asc",
                    sortType: "double",
                    name: _.translate("pp gir"),
                    className: ""
                }, {
                    key: "eagles",
                    sortDirection: "desc",
                    sortType: "integer",
                    name: _.translate("eagle"),
                    className: ""
                }, {
                    key: "birdies",
                    sortDirection: "desc",
                    sortType: "integer",
                    name: _.translate("birdie"),
                    className: ""
                }, {
                    key: "pars",
                    sortDirection: "desc",
                    sortType: "integer",
                    name: _.translate("pars"),
                    className: ""
                }, {
                    key: "bogeys",
                    sortDirection: "desc",
                    sortType: "integer",
                    name: _.translate("bogey"),
                    className: ""
                }, {
                    key: "doubles",
                    sortDirection: "desc",
                    sortType: "integer",
                    name: _.translate("dbl+"),
                    className: ""
                }, {
                    key: "scoreToPar",
                    sortDirection: "asc",
                    sortType: "integer",
                    name: _.translate("to par")
                }],
                COURSE_STATS_COLUMN_CONFIG: [{
                    key: "number",
                    name: _.translate("hole"),
                    sortDirection: "asc",
                    sortType: "integer",
                    numberFormat: "#",
                    sorted: !0,
                    className: "asc"
                }, {
                    key: "par",
                    name: _.translate("par"),
                    sortDirection: "asc",
                    sortType: "integer",
                    numberFormat: "#"
                }, {
                    key: "yards",
                    name: _.translate("yards"),
                    sortDirection: "desc",
                    sortType: "integer",
                    numberFormat: "#"
                }, {
                    key: "avgScore",
                    name: _.translate("avg. score"),
                    sortDirection: "asc",
                    sortType: "double",
                    numberFormat: "#.00"
                }, {
                    key: "eagles",
                    name: _.translate("eagles"),
                    sortDirection: "desc",
                    sortType: "integer",
                    numberFormat: "#"
                }, {
                    key: "birdies",
                    name: _.translate("birdies"),
                    sortDirection: "desc",
                    sortType: "integer",
                    numberFormat: "#"
                }, {
                    key: "pars",
                    name: _.translate("pars"),
                    sortDirection: "desc",
                    sortType: "integer",
                    numberFormat: "#"
                }, {
                    key: "bogeys",
                    name: _.translate("bogeys"),
                    sortDirection: "desc",
                    sortType: "integer",
                    numberFormat: "#"
                }, {
                    key: "dBogey",
                    name: _.translate("doubles"),
                    sortDirection: "desc",
                    sortType: "integer",
                    numberFormat: "#"
                }, {
                    key: "dBogeyPlus",
                    name: _.translate("other"),
                    sortDirection: "desc",
                    sortType: "integer",
                    numberFormat: "#"
                }, {
                    key: "scoreToPar",
                    name: _.translate("+/- avg."),
                    sortDirection: "asc",
                    sortType: "double",
                    numberFormat: "0.00"
                }],
                PROJECTED_CUT_TEXT: _.translate("Projected Cut"),
                CUT_TEXT: _.translate("The following players failed to make the cut at")
            }
        }, {}],
        10: [function (require, module, exports) {
            function BufferedFastCastHandler() {
                var processUpdates = _.throttle(function (updatedFeed, callback) {
                    callback(updatedFeed)
                }, 3e3);
                return {
                    handleCasterMessage: function (callback) {
                        return function (updatedFeed) {
                            processUpdates(updatedFeed, callback)
                        }
                    }
                }
            }

            function _getMinutesUntilStart(status, timestamp) {
                var tmpDate, tmpConnectTime, timeToConnect = 1440,
                    currentDate = moment();
                return "in" === status ? timeToConnect = 0 : "pregame" !== status && "pre" !== status || (tmpDate = moment(timestamp), tmpDate.isValid() && (tmpConnectTime = tmpDate.diff(currentDate, "minutes"), tmpConnectTime < timeToConnect && timeToConnect > 0 && (timeToConnect = tmpConnectTime))), timeToConnect
            }

            function _getConnectionDelayTimestamp(milliseconds) {
                var d = new Date;
                return d.setSeconds(d.getSeconds() + milliseconds / 1e3)
            }

            function _subscribeToFastCastTopic(obj) {
                var topic = obj.topic,
                    callback = obj.callback,
                    optUrl = obj.url,
                    status = obj.status,
                    timestamp = obj.timestamp,
                    timeToConnect = 0,
                    timeBeforeWeCanConnectToNextGame = 0,
                    timeBeforeWeCanConnectToAnything = 0,
                    fastCastConnectionGroup = fastCastConnectionGroups[topic];
                return fastCastConnectionGroup ? fastCastConnectionGroup : (fastCastConnectionGroup = {
                    connected: !1,
                    topic: topic,
                    connectTimeout: null,
                    waitingSince: null,
                    timeToConnect: 1 / 0
                }, fastCastConnectionGroups[topic] = fastCastConnectionGroup, fastCastConnectionGroup.connected ? fastCastConnectionGroup : ("in" === status ? timeToConnect = 0 : (timeBeforeWeCanConnectToNextGame = 6e4 * Math.max(_getMinutesUntilStart(status, timestamp) - FASTCAST_CONNECT_MINUTES_AWAY, 0), timeBeforeWeCanConnectToAnything = CONNECTION_DELAY_TIMESTAMP - +new Date, timeToConnect = Math.max(timeBeforeWeCanConnectToNextGame, timeBeforeWeCanConnectToAnything)), null !== fastCastConnectionGroup.connectTimeout && timeToConnect < fastCastConnectionGroup.timeToConnect - fastCastConnectionGroup.waitingSince && (clearTimeout(fastCastConnectionGroup.connectTimeout), fastCastConnectionGroup.connectTimeout = null, sbDebug && window.console.log("stop waiting to connect to", fastCastConnectionGroup.topic, "because we shouldnt wait that long")),
                    sbDebug && window.console.log("connecting to", topic, timeToConnect < 45e3 ? "in " + timeToConnect / 1e3 + " seconds" : moment(+new Date + timeToConnect).fromNow(), "(", timeBeforeWeCanConnectToNextGame, ",", timeBeforeWeCanConnectToAnything, ")"), void (null === fastCastConnectionGroup.connectTimeout && (fastCastConnectionGroup.timeToConnect = timeToConnect, fastCastConnectionGroup.waitingSince = +new Date, fastCastConnectionGroup.callback = (new BufferedFastCastHandler).handleCasterMessage(function (bufferedUpdatedFeed) {
                        callback(bufferedUpdatedFeed)
                    }), fastCastConnectionGroup.connectTimeout = setTimeout(function () {
                        clearTimeout(fastCastConnectionGroup.connectTimeout), fastCastConnectionGroup.connectTimeout = null, fastCastConnectionGroup.connected = !0, sbDebug && window.console.log("connect to", fastCastConnectionGroup.topic);
                        var options = {
                            url: optUrl,
                            error: fastCastConnectionGroup.callback
                        };
                        espn.flow.connect(fastCastConnectionGroup.topic, fastCastConnectionGroup.callback, options)
                    }, timeToConnect)))))
            }

            function _unsubscribeFromFastCastTopic(obj) {
                var topic = obj.topic,
                    fastCastConnectionGroup = fastCastConnectionGroups[topic];
                return fastCastConnectionGroup ? (null !== fastCastConnectionGroup.connectTimeout && (clearTimeout(fastCastConnectionGroup.connectTimeout), fastCastConnectionGroup.connectTimeout = null), fastCastConnectionGroup.connected ? (sbDebug && window.console.log("disconnect from", fastCastConnectionGroup.topic), espn.flow.disconnect(fastCastConnectionGroup.topic, fastCastConnectionGroup.callback)) : sbDebug && window.console.log("stop waiting to connect to", fastCastConnectionGroup.topic), delete fastCastConnectionGroups[topic], fastCastConnectionGroup) : fastCastConnectionGroup
            }
            var moment = window.moment,
                _ = window._,
                FASTCAST_CONNECT_MINUTES_AWAY = 10,
                CONNECTION_DELAY_TIMESTAMP = _getConnectionDelayTimestamp(15e3),
                fastCastConnectionGroups = {},
                sbDebug = espn.leaderboard && espn.leaderboard.debug === !0;
            module.exports = {
                subscribeToFastCastTopic: function () {
                    _subscribeToFastCastTopic.apply(this, arguments)
                },
                unsubscribeFromFastCastTopic: function () {
                    _unsubscribeFromFastCastTopic.apply(this, arguments)
                }
            }
        }, {}],
        11: [function (require, module, exports) {
            function _init() {
                espn.leaderboard.model = new LeaderboardModel, espn.leaderboard.model.init(window.espn.leaderboard.data || {});
                var contentView;
                espn.leaderboard.App || (espn.leaderboard.App = new window.Backbone.Marionette.Application, espn.leaderboard.App.on("start", function () {
                    espn.leaderboard.App.contentView || (contentView = new ContentView, contentView.render(), espn.leaderboard.App.contentView = contentView)
                }), espn.leaderboard.App.vent.on("connect", onConnect), espn.leaderboard.App.vent.on("disconnect", onDisconnect)), espn.leaderboard.App.start(), $(document).on("page.beforeload", onBeforeLoad)
            }

            function onConnect() {
                var topic = getTopic();
                if (!espn.flow.activeTopics[topic]) {
                    var callback = function (feed) {
                        feed && feed.data && feed.data.events && feed.data.events.length > 0 && espn.leaderboard.model && espn.leaderboard.model.update(feed.data.events[0])
                    };
                    fastcastTimeoutHook = setTimeout(function () {
                        FASTCAST.subscribeToFastCastTopic({
                            topic: topic,
                            status: espn.leaderboard.status,
                            timestamp: espn.leaderboard.date,
                            url: getOptUrl(),
                            callback: callback
                        })
                    }, FC_CONNECT_TIMEOUT)
                }
            }

            function onDisconnect() {
                FASTCAST.unsubscribeFromFastCastTopic({
                    topic: getTopic()
                }), clearTimeout(fastcastTimeoutHook)
            }

            function onBeforeLoad() {
                espn.leaderboard.App && espn.leaderboard.App.contentView && (espn.leaderboard.App.contentView.destroy(), espn.leaderboard.App.contentView = null), espn.leaderboard.model && (espn.leaderboard.model = null), onDisconnect(), $(document).off("page.beforeload", onBeforeLoad)
            }

            function getTopic() {
                return "golf-leaderboard-" + espn.leaderboard.tour
            }

            function getOptUrl() {
                return "http://site.api.espn.com/apis/site/v2/sports/golf/leaderboard?event=" + espn.leaderboard.tournamentId
            }
            var $ = window.jQuery,
                espn = window.espn || {};
            espn.leaderboard = espn.leaderboard || {}, espn.leaderboard.scriptsLoaded = !0;
            var fastcastTimeoutHook, ContentView = require("./views/Content"),
                LeaderboardModel = require("./models/Leaderboard"),
                FASTCAST = require("./fastcast"),
                FC_CONNECT_TIMEOUT = 5e3;
            espn.leaderboard.init = function () {
                setTimeout(function () {
                    _init()
                }, 0)
            }, $.publish("espn.leaderboard.loaded", [])
        }, {
            "./fastcast": 10,
            "./models/Leaderboard": 16,
            "./views/Content": 30
        }],
        12: [function (require, module, exports) {
            function getCompetitorDetailsUrl(playerId) {
                if (espn.leaderboard.competitorDetailsUrl) return espn.leaderboard.competitorDetailsUrl.replace("{{playerId}}", playerId).replace("{{season}}", espn.leaderboard.season)
            }

            function getThruValue(player) {
                var thru = espn_util.find(player, ["status", "thru"]) || espn_util.find(player, ["status", "displayValue"]);
                return "F" === thru && (thru = "18"), "-" === thru && (thru = "0"), void 0 !== thru && (thru = parseInt(thru, 10)), thru
            }

            function getId(player) {
                if (player.id) return player.id;
                var name = player.name || espn_util.find(player, ["athlete", "displayName"]);
                return name ? name.toLowerCase().replace(/[\s\.']/g, "") : void 0
            }

            function getPlayerName(player) {
                return player.athlete && player.athlete.displayName ? player.athlete.displayName : player.name
            }

            function getPlayerLastName(player) {
                var playerName = getPlayerName(player);
                if (playerName) {
                    var index = playerName.indexOf(" ");
                    if (index > -1) return playerName.substring(index)
                }
                return playerName
            }
            var TournamentDetailsModel = require("./TournamentDetails"),
                $ = window.$,
                _ = window._,
                espn_util = window.espn_util,
                CompetitorModel = window.Backbone.Model.extend({
                    defaults: {
                        driveDistAvg: "-",
                        fwysHit: "-",
                        gir: "-",
                        puttsGirAvg: "-",
                        eagles: "-",
                        birdies: "-",
                        pars: "-",
                        bogeys: "-",
                        doubles: "-",
                        scoreToPar: "-",
                        officialAmount: "$.00",
                        country: ""
                    },
                    initialize: function () {
                        this.requestingDetails = !1
                    },
                    parse: function (player, options) {
                        var scoreToPar, officialAmount, cupPoints;
                        player && player.statistics && player.statistics.length > 0 && player.statistics.forEach(function (stat) {
                            "scoreToPar" === stat.name ? (scoreToPar = stat.displayValue, "E" === scoreToPar && (scoreToPar = "0")) : "officialAmount" === stat.name ? officialAmount = stat.displayValue : "cupPoints" === stat.name && (cupPoints = stat.displayValue)
                        });
                        var statusState, statusName, statusShortDetail, position, playerRound;
                        player.status && (player.status.type && (statusState = player.status.type.state, statusName = player.status.type.name, statusShortDetail = player.status.type.shortDetail), player.status.position && (position = player.status.position.displayName, position || (position = "-")), playerRound = player.status.period);
                        var obj = {
                            amateur: player.amateur,
                            displayName: getPlayerName(player),
                            lastName: getPlayerLastName(player),
                            id: getId(player),
                            uid: player.uid,
                            links: player.links,
                            headshot: espn_util.find(player, ["athlete", "headshot", "href"]),
                            flag: espn_util.find(player, ["athlete", "flag", "href"]),
                            country: espn_util.find(player, ["athlete", "flag", "alt"]) || "",
                            featured: player.featured,
                            featuredStream: player.featured && player.stream,
                            movement: player.movement || 0,
                            totalScore: player.score && player.score.value || 0,
                            aggregateScore: player.totalScore,
                            relativeScore: scoreToPar,
                            status: statusState,
                            statusName: statusName,
                            statusShortDetail: statusShortDetail,
                            sortOrder: player.sortOrder,
                            position: position,
                            officialAmount: officialAmount,
                            cupPoints: cupPoints,
                            thru: getThruValue(player),
                            teeTime: espn_util.find(player, ["status", "teeTime"])
                        };
                        player.linescores && player.linescores.length > 0 && _.each(player.linescores, function (l) {
                            l.period && l.value && (l.period < playerRound || "post" === statusState) && (obj["round" + l.period] = l.value)
                        });
                        var currentRoundScore = espn_util.find(player, ["linescores", options.currentRound - 1, "displayValue"]);
                        return "E" === currentRoundScore && (currentRoundScore = "0"), obj.currentRoundScore = currentRoundScore, obj
                    },
                    addCompetitorDetails: function (data) {
                        this.get("details") ? this.get("details").update(data) : this.set("details", new TournamentDetailsModel(data, {
                            parse: !0
                        }))
                    },
                    updatePlayerStats: function (stats) {
                        if (stats && stats.length > 0) {
                            var statMap = {};
                            stats = stats.forEach(function (stat) {
                                "driveAccuracyPct" === stat.name ? stat.name = "fwysHit" : "scoreToPar" === stat.name && "E" === stat.displayValue && (stat.displayValue = "0"), statMap[stat.name] = stat.displayValue
                            }), this.set(statMap)
                        }
                    },
                    getCompetitorDetails: function (callback) {
                        var that = this,
                            url = getCompetitorDetailsUrl(this.get("id"));
                        this.requestingDetails !== !0 && url && (this.requestingDetails = !0, $.ajax({
                            url: url,
                            success: function (data) {
                                that.addCompetitorDetails(data), that.requestingDetails = !1, callback(null)
                            },
                            error: function () {
                                that.requestingDetails = !1, callback("Something went wrong")
                            }
                        }))
                    },
                    update: function (data, options) {
                        this.set(this.parse(data, options))
                    }
                });
            module.exports = CompetitorModel
        }, {
            "./TournamentDetails": 23
        }],
        13: [function (require, module, exports) {
            function calculatePar(holes, type) {
                if (holes && 18 === holes.length) {
                    var pars = holes.map(function (hole) {
                        return hole.shotsToPar
                    });
                    return "in" === type ? pars = pars.slice(9) : "out" === type && (pars = pars.slice(0, 9)), pars.reduce(function (prev, curr) {
                        return prev + curr
                    })
                }
                return "-"
            }
            var HolesCollection = require("../collections/Holes"),
                CourseModel = window.Backbone.Model.extend({
                    parse: function (course) {
                        return {
                            id: course.id,
                            name: course.name,
                            address: course.address,
                            yards: course.totalYards,
                            par: course.shotsToPar || calculatePar(course.holes),
                            parIn: course.parIn || calculatePar(course.holes, "in"),
                            parOut: course.parOut || calculatePar(course.holes, "out"),
                            holes: new HolesCollection(course.holes, {
                                parse: !0
                            })
                        }
                    }
                });
            module.exports = CourseModel
        }, {
            "../collections/Holes": 3
        }],
        14: [function (require, module, exports) {
            var HeaderModel = window.Backbone.Model.extend({
                defaults: {
                    tournamentStatus: "post",
                    tournamentStatusDetail: "",
                    roundStatus: "post",
                    roundStatusDetail: "",
                    hasPlayerStats: !1,
                    hasCourseStats: !1,
                    numberOfRounds: 4
                },
                parse: function (data) {
                    var obj = {};
                    if (data && data.competitions && data.competitions.length > 0) {
                        var competition = data.competitions[0];
                        competition && competition.status && competition.status.type && (obj.roundStatus = competition.status.type.state, obj.roundStatusDetail = competition.status.type.detail, obj.currentRound = competition.status.period, obj.dataFormat = competition.dataFormat)
                    }
                    return data && data.status && data.status.type && (obj.tournamentStatus = data.status.type.state, obj.tournamentStatusDetail = data.status.type.description), data && data.tournament && (obj.cutScore = data.tournament.cutScore, obj.cutRound = data.tournament.cutRound, obj.numberOfRounds = data.tournament.numberOfRounds), obj.hasPlayerStats = data.hasPlayerStats === !0 && "API" === obj.dataFormat, obj.hasCourseStats = data.hasCourseStats === !0 && "API" === obj.dataFormat, obj.winnerId = data.winner && data.winner.id, espn.leaderboard.status !== obj.tournamentStatus && (obj.tournamentStatus = espn.leaderboard.status), obj
                },
                update: function (data) {
                    this.set(this.parse(data))
                },
                getCutCount: function () {
                    var cutCount, cutScore = this.get("cutScore"),
                        competitors = espn.leaderboard.model.get("competitors"),
                        isProjected = this.get("currentRound") === this.get("cutRound") && "in" === this.get("roundStatus");
                    return competitors && competitors.length > 0 && (cutCount = 0, competitors.each(function (model) {
                        if ("STATUS_CUT" !== model.get("statusName"))
                            if (isProjected === !0) {
                                var score = model.get("relativeScore");
                                void 0 !== score && "-" !== score && ("E" === score && (score = "0"), score = parseInt(score, 10), score <= cutScore && cutCount++)
                            } else cutCount++;
                        else "MDF" === model.get("statusShortDetail") && cutCount++
                    })), cutCount
                }
            });
            module.exports = HeaderModel
        }, {}],
        15: [function (require, module, exports) {
            var _ = window._,
                HoleModel = window.Backbone.Model.extend({
                    defaults: {
                        avgScore: "-",
                        eagles: "0",
                        birdies: "0",
                        pars: "0",
                        bogeys: "0",
                        dBogey: "0",
                        dBogeyPlus: "0",
                        scoreToPar: "0",
                        rank: "-"
                    },
                    parse: function (hole) {
                        return {
                            id: hole.number,
                            number: hole.number,
                            par: hole.shotsToPar,
                            yards: hole.totalYards
                        }
                    },
                    addStats: function (hole) {
                        var obj = {};
                        _.each(hole.holeStatistics, function (stat) {
                            stat.name && stat.displayValue && (obj[stat.name] = stat.displayValue)
                        }), this.set(obj)
                    }
                });
            module.exports = HoleModel
        }, {}],
        16: [function (require, module, exports) {
            var HeaderModel = require("./Header"),
                CompetitorsCollection = require("../collections/Competitors"),
                CoursesCollection = require("../collections/Courses"),
                LeadersModel = require("./Leaders"),
                PlayoffModel = require("./Playoff"),
                Leaderboard = window.Backbone.Model.extend({
                    init: function (data) {
                        this.addHeader(data), this.addCourses(data), this.addCompetitors(data), this.addLeaders(data), this.addPlayoff(data)
                    },
                    addHeader: function (data) {
                        this.set("header", new HeaderModel(data, {
                            parse: !0
                        }))
                    },
                    addCourses: function (data) {
                        this.set("courses", new CoursesCollection(data, {
                            parse: !0
                        }))
                    },
                    addCompetitors: function (data) {
                        this.set("competitors", new CompetitorsCollection(data, {
                            parse: !0
                        }))
                    },
                    addLeaders: function (data) {
                        this.set("leaders", new LeadersModel(data, {
                            parse: !0
                        }))
                    },
                    addPlayoff: function (data) {
                        this.set("playoff", new PlayoffModel(data, {
                            parse: !0
                        }))
                    },
                    update: function (data) {
                        this.updateHeader(data), this.updateCompetitors(data), this.updateLeaders(data), this.updatePlayoff(data)
                    },
                    updateHeader: function (data) {
                        this.get("header").update(data)
                    },
                    updateCompetitors: function (data) {
                        this.get("competitors").update(data)
                    },
                    updateLeaders: function (data) {
                        this.get("leaders").update(data)
                    },
                    updatePlayoff: function (data) {
                        this.get("playoff").update(data)
                    }
                });
            module.exports = Leaderboard
        }, {
            "../collections/Competitors": 1,
            "../collections/Courses": 2,
            "./Header": 14,
            "./Leaders": 17,
            "./Playoff": 18
        }],
        17: [function (require, module, exports) {
            var _ = window._,
                LeadersModel = window.Backbone.Model.extend({
                    parse: function (data) {
                        var obj = {};
                        if (data && !_.isEmpty(data.competitions) && data.competitions[0].leaders) {
                            var leaders = data.competitions[0].leaders;
                            _.each(leaders, function (leader) {
                                leader.name && !_.isEmpty(leader.leaders) && leader.leaders[0].athlete && (obj[leader.name] = leader.leaders[0].athlete.id)
                            })
                        }
                        return obj
                    },
                    update: function (data) {
                        this.set(this.parse(data))
                    }
                });
            module.exports = LeadersModel
        }, {}],
        18: [function (require, module, exports) {
            function getPlayoffCompetitors(data) {
                var competitors = espn_util.find(data, ["competitions", 0, "competitors"]);
                return competitors && competitors.length > 0 ? _.where(competitors, {
                    status: {
                        playoff: !0
                    }
                }) : []
            }
            var PlayoffRoundsCollection = require("../collections/PlayoffRounds"),
                _ = window._,
                espn_util = window.espn_util,
                PlayoffModel = window.Backbone.Model.extend({
                    defaults: {
                        hasPlayoff: !1
                    },
                    intialize: function () { },
                    parse: function (data, options) {
                        options = options || {};
                        var obj = {},
                            competitors = getPlayoffCompetitors(data);
                        return competitors && competitors.length > 0 && (obj.hasPlayoff = !0, options.update !== !0 && (obj.rounds = new PlayoffRoundsCollection(competitors, {
                            parse: !0
                        }))), obj
                    },
                    update: function (data) {
                        this.set(this.parse(data, {
                            update: !0
                        })), this.get("rounds") && this.get("rounds").update(getPlayoffCompetitors(data), {
                            parse: !0
                        })
                    }
                });
            module.exports = PlayoffModel
        }, {
            "../collections/PlayoffRounds": 5
        }],
        19: [function (require, module, exports) {
            function getLinescore(competitor, options) {
                return _.findWhere(competitor.linescores, {
                    isPlayoff: !0,
                    period: options.roundKey
                })
            }
            var PlayoffLinescoreModel = require("./PlayoffLinescore"),
                espn_util = window.espn_util,
                _ = window._,
                PlayoffCompetitorModel = window.Backbone.Model.extend({
                    parse: function (competitor, options) {
                        var obj = {
                            amateur: competitor.amateur,
                            displayName: competitor.athlete.displayName,
                            id: competitor.id,
                            uid: competitor.uid,
                            links: competitor.links,
                            headshot: espn_util.find(competitor, ["athlete", "headshot", "href"]),
                            flag: espn_util.find(competitor, ["athlete", "flag", "href"]),
                            country: espn_util.find(competitor, ["athlete", "flag", "alt"]),
                            holesPlayed: options.holesPlayed,
                            isFull18: options.isFull18
                        };
                        if (options.update !== !0) {
                            var linescore = getLinescore(competitor, options);
                            linescore && (obj.linescore = new PlayoffLinescoreModel(linescore, options))
                        }
                        return obj
                    },
                    update: function (competitor, options) {
                        options = options || (options = {}), options.update = !0, this.set(this.parse(competitor, options));
                        var linescore = getLinescore(competitor, options);
                        linescore && this.get("linescore").update(linescore, options)
                    }
                });
            module.exports = PlayoffCompetitorModel
        }, {
            "./PlayoffLinescore": 21
        }],
        20: [function (require, module, exports) {
            var PlayoffHoleModel = window.Backbone.Model.extend({
                parse: function (hole) {
                    return {
                        id: hole.id,
                        holeNumber: hole.period,
                        score: hole.value,
                        scoreName: hole.scoreType && hole.scoreType.displayName,
                        scoreType: hole.scoreType && hole.scoreType.name
                    }
                },
                update: function (hole) {
                    this.set(this.parse(hole))
                }
            });
            module.exports = PlayoffHoleModel
        }, {}],
        21: [function (require, module, exports) {
            var PlayoffScoresCollection = require("../collections/PlayoffScores"),
                PlayoffLinescoreModel = window.Backbone.Model.extend({
                    initialize: function () {
                        this.get("scores").on("add change remove reset", function () {
                            this.trigger("change")
                        }.bind(this))
                    },
                    parse: function (linescore, options) {
                        options = options || (options = {});
                        var obj = {
                            displayValue: linescore.displayValue,
                            outScore: linescore.outScore,
                            inScore: linescore.inScore,
                            period: linescore.period,
                            value: linescore.value
                        };
                        return options.update !== !0 && (obj.scores = new PlayoffScoresCollection(linescore, {
                            parse: !0
                        })), obj
                    },
                    update: function (linescore) {
                        this.set(this.parse(linescore, {
                            update: !0
                        })), this.get("scores").update(linescore)
                    }
                });
            module.exports = PlayoffLinescoreModel
        }, {
            "../collections/PlayoffScores": 6
        }],
        22: [function (require, module, exports) {
            function filterCompetitors(competitors, roundKey) {
                return competitors.filter(function (competitor) {
                    return !!_.findWhere(competitor.linescores, {
                        period: roundKey
                    })
                })
            }

            function isFull18(roundKey) {
                return "U.S. Open Championship" === espn.leaderboard.data.name && roundKey === espn.leaderboard.data.tournament.numberOfRounds + 1
            }

            function getHolesPlayed(competitors, roundKey) {
                var linescores, holesPlayed = 0;
                return isFull18(roundKey) ? 18 : (competitors.forEach(function (competitor) {
                    var linescore = _.findWhere(competitor.linescores, {
                        period: roundKey
                    });
                    linescore && linescore.linescores && (linescores = linescore.linescores.filter(function (hole) {
                        return 0 !== hole.period
                    }), linescores.length > holesPlayed && (holesPlayed = linescores.length))
                }), holesPlayed)
            }
            var PlayoffCompetitorsCollection = require("../collections/PlayoffCompetitors"),
                _ = window._,
                PlayoffRoundModel = window.Backbone.Model.extend({
                    parse: function (data, options) {
                        options = options || (options = {});
                        var roundKey = data.key,
                            competitors = filterCompetitors(data.competitors, roundKey),
                            holesPlayed = getHolesPlayed(competitors, roundKey),
                            type = "sudden-death";
                        holesPlayed > 4 && (type = "aggregate");
                        var obj = {
                            key: roundKey,
                            type: type,
                            holesPlayed: holesPlayed,
                            isFull18: isFull18(roundKey)
                        };
                        return options.update !== !0 && (obj.competitors = new PlayoffCompetitorsCollection(competitors, {
                            parse: !0,
                            roundKey: data.key,
                            holesPlayed: holesPlayed,
                            isFull18: isFull18
                        })), obj
                    },
                    update: function (data) {
                        this.set(this.parse(data, {
                            update: !0
                        }));
                        var competitors = filterCompetitors(data.competitors, data.key),
                            holesPlayed = getHolesPlayed(competitors, data.key);
                        this.get("competitors").update(competitors, {
                            roundKey: data.key,
                            holesPlayed: holesPlayed,
                            isFull18: isFull18(data.key)
                        })
                    },
                    getHoleNumbers: function () {
                        return this.get("competitors").getHoleNumbers()
                    }
                });
            module.exports = PlayoffRoundModel
        }, {
            "../collections/PlayoffCompetitors": 4
        }],
        23: [function (require, module, exports) {
            var StatsModel = require("./tournament-details/Stats"),
                RoundsCollection = require("../collections/Rounds"),
                ProfileModel = require("./tournament-details/Profile"),
                TournamentDetailsModel = window.Backbone.Model.extend({
                    parse: function (obj) {
                        return obj = obj || {}, obj.stats = new StatsModel(obj.stats, {
                            parse: !0
                        }), obj.rounds = new RoundsCollection(obj.rounds, {
                            parse: !0
                        }), obj.profile = new ProfileModel(obj.profile, {
                            parse: !0
                        }), obj
                    },
                    update: function (obj) {
                        obj = obj || {}, this.get("stats") ? this.get("stats").update(obj.stats) : this.set("stats", new StatsModel(obj.stats, {
                            parse: !0
                        })), this.get("rounds") ? this.get("rounds").update(obj.rounds) : this.set("rounds", new RoundsCollection(obj.rounds, {
                            parse: !0
                        })), this.get("profile") ? this.get("profile").update(obj.profile) : this.set("profile", new ProfileModel(obj.profile, {
                            parse: !0
                        }))
                    }
                });
            module.exports = TournamentDetailsModel
        }, {
            "../collections/Rounds": 7,
            "./tournament-details/Profile": 24,
            "./tournament-details/Stats": 26
        }],
        24: [function (require, module, exports) {
            var ProfileModel = window.Backbone.Model.extend({
                defaults: {
                    displayName: "",
                    age: "",
                    dateOfBirth: "",
                    birthPlace: "",
                    college: "",
                    hand: "",
                    headshot: "",
                    link: "",
                    rank: "",
                    earnings: ""
                },
                parse: function (data) {
                    return data && "$.00" === data.earnings && (data.earnings = "$0"), data
                },
                update: function (data) {
                    this.set(this.parse(data))
                }
            });
            module.exports = ProfileModel
        }, {}],
        25: [function (require, module, exports) {
            var ScoresCollection = require("../../collections/Scores"),
                RoundModel = window.Backbone.Model.extend({
                    parse: function (data, options) {
                        var obj = {
                            roundNumber: data.period,
                            inScore: data.inScore,
                            outScore: data.outScore,
                            totalScore: data.displayValue,
                            aggregateScore: data.value,
                            courseId: data.courseId + "",
                            isPlayoff: data.isPlayoff === !0
                        };
                        return options.update !== !0 && (obj.scores = new ScoresCollection(data, {
                            parse: !0
                        })), obj
                    },
                    update: function (data) {
                        this.set(this.parse(data, {
                            update: !0
                        })), this.get("scores").update(data)
                    }
                });
            module.exports = RoundModel
        }, {
            "../../collections/Scores": 8
        }],
        26: [function (require, module, exports) {
            var _ = window._,
                fields = ["driveDistAvg", "gir", "puttsGirAvg", "sandSaves", "eagles", "birdies", "bogeys"],
                StatsModel = window.Backbone.Model.extend({
                    defaults: {
                        hasStats: !1
                    },
                    parse: function (stats) {
                        var obj = {},
                            hasStats = !1;
                        return stats && _.isArray(stats) && stats.length > 0 && _.each(stats, function (stat) {
                            stat.name && (obj[stat.name] = stat, fields.indexOf(stat.name) > -1 && "0" !== stat.displayValue && "0.000" !== stat.displayValue && (hasStats = !0))
                        }), obj.hasStats = hasStats, obj
                    },
                    update: function (stats) {
                        this.set(this.parse(stats))
                    },
                    hasStats: function () {
                        return this.get("hasStats") === !0
                    }
                });
            module.exports = StatsModel
        }, {}],
        27: [function (require, module, exports) {
            var CompetitorOverviewView = require("./CompetitorOverview"),
                CompetitorDetailsView = require("./CompetitorDetails"),
                TournamentDetailsView = require("./TournamentDetails"),
                TournamentDetailsErrorView = require("./TournamentDetailsError"),
                $ = window.$,
                PLAYER_SUMMARY_TIMEOUT = 5e3,
                CompetitorView = window.Backbone.Marionette.LayoutView.extend({
                    template: !1,
                    regions: {
                        overview: ".player-overview",
                        details: ".player-details .content"
                    },
                    modelEvents: {
                        change: "update"
                    },
                    ui: {
                        player: ".playerName a"
                    },
                    events: {
                        "click @ui.player": "toggleDetails",
                        "click .icon-video-camera": "openFeaturedStream"
                    },
                    initialize: function () {
                        this.viewModel = {
                            details: {
                                showing: !1
                            }
                        }
                    },
                    onRender: function () {
                        this.overview.attachView(new CompetitorOverviewView({
                            model: this.model,
                            el: this.$(".player-overview")
                        })), this.details.attachView(new CompetitorDetailsView({
                            model: this.model
                        }))
                    },
                    toggleDetails: function () {
                        var that = this;
                        this.viewModel.details.showing === !0 ? (this.$(".player-details").addClass("collapse"), this.details.empty(), this.model.unset("details"), this.$(".content").append($('<div class="loading"></div>'))) : (this.$(".player-details > .content").attr("colspan", this.$(".player-overview").find("td:visible").length), this.$(".player-details").removeClass("collapse"), this.model.getCompetitorDetails(function (err) {
                            err ? that.details.show(new TournamentDetailsErrorView) : that.details.show(new TournamentDetailsView({
                                model: that.model.get("details")
                            }))
                        })), this.viewModel.details.showing = !this.viewModel.details.showing
                    },
                    openFeaturedStream: function (e) {
                        e.stopPropagation();
                        var stream = this.model.get("featuredStream");
                        stream && window.open(stream, "_blank", "toolbar=0,location=0,menubar=0")
                    },
                    update: function (model) {
                        var changed = this.model.changedAttributes();
                        changed && ("featured" in changed && this.updateFeaturedGroup(model), this.viewModel.details.showing === !0 && ("relativeScore" in changed || "thru" in changed) && (espn.leaderboard.debug === !0 && this.$(".player-overview").addClass("updating"), this.timeoutHook = setTimeout(function () {
                            this.model.getCompetitorDetails(function () {
                                espn.leaderboard.debug === !0 && this.$(".player-overview").removeClass("updating")
                            }.bind(this))
                        }.bind(this), PLAYER_SUMMARY_TIMEOUT)))
                    },
                    updateFeaturedGroup: function () {
                        this.model.get("featured") ? (this.$(".playerName").append('<span class="icon-video-camera link"></span>'), this.$(".playerName").addClass("has-icon")) : (this.$(".playerName").removeClass("has-icon"), this.$("span.icon-video-camera").remove())
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0, clearTimeout(this.timeoutHook)
                    }
                });
            module.exports = CompetitorView
        }, {
            "./CompetitorDetails": 28,
            "./CompetitorOverview": 29,
            "./TournamentDetails": 49,
            "./TournamentDetailsError": 50
        }],
        28: [function (require, module, exports) {
            var CompetitorDetailsView = window.Backbone.Marionette.ItemView.extend({
                template: !1,
                onDestroy: function () {
                    espn.leaderboard.debug === !0
                }
            });
            module.exports = CompetitorDetailsView
        }, {}],
        29: [function (require, module, exports) {
            var utils = require("./utils"),
                config = require("../config"),
                CompetitorOverviewView = window.Backbone.Marionette.ItemView.extend({
                    template: !1,
                    modelEvents: {
                        change: "onModelChanged"
                    },
                    initialize: function () {
                        this.$position = this.$(".position"), this.$relativeScore = this.$(".relativeScore"), this.$movement = this.$(".movement"), this.$status = this.$(".thru"), this.$totalScore = this.$(".totalScore"), this.$today = this.$(".today"), this.$thru = this.$(".thru")
                    },
                    onModelChanged: function () {
                        var changed = this.model.changedAttributes();
                        changed && ("position" in changed && utils.animateChange(this.$position, this.model.get("position")), "relativeScore" in changed && this.onRelativeScoreChanged(), "movement" in changed && this.onMovementChanged(), "status" in changed && utils.animateChange(this.$status, this.model.get("thru")), "totalScore" in changed && utils.animateChange(this.$totalScore, this.model.get("totalScore")), "status" in changed && this.onStatusChanged(), "currentRoundScore" in changed && this.onCurrentRoundScoreChanged(), "thru" in changed && this.onThruChanged(), "statusName" in changed && this.onStatusNameChanged(), "round1" in changed && this.onRoundChanged(1), "round2" in changed && this.onRoundChanged(2), "round3" in changed && this.onRoundChanged(3), "round4" in changed && this.onRoundChanged(4), "round5" in changed && this.onRoundChanged(5), "round6" in changed && this.onRoundChanged(6))
                    },
                    onMovementChanged: function () {
                        var movementVal = parseInt(this.model.get("movement")),
                            classToAdd = "";
                        movementVal > 0 ? classToAdd = config.CLASSNAME_ARROW_DOWN : movementVal < 0 && (classToAdd = config.CLASSNAME_ARROW_UP, movementVal = Math.abs(movementVal)), movementVal = movementVal || "-", utils.animateChange(this.$movement, movementVal, function () {
                            this.$movement.removeClass(config.CLASSNAME_ARROW_UP).removeClass(config.CLASSNAME_ARROW_DOWN).addClass(classToAdd)
                        }.bind(this))
                    },
                    onStatusChanged: function () {
                        if ("post" === this.model.get("status"))
                            for (var roundKey, roundScore, $roundEl, i = 4; i >= 1; i--)
                                if (roundKey = "round" + i, roundScore = this.model.get(roundKey), roundScore && ($roundEl = this.$("." + roundKey), $roundEl.length > 0)) {
                                    utils.animateChange($roundEl, roundScore);
                                    break
                                }
                    },
                    onRoundChanged: function (num) {
                        var $roundEl, roundVal = this.model.get("round" + num);
                        roundVal && "post" === this.model.get("status") && ($roundEl = this.$(".round" + num), $roundEl.length > 0 && utils.animateChange($roundEl, roundVal))
                    },
                    onStatusNameChanged: function () {
                        var statusName = this.model.get("statusName");
                        if ("STATUS_CUT" === statusName) {
                            var statusShortDetail = this.model.get("statusShortDetail");
                            statusShortDetail && utils.animateChange(this.$relativeScore, statusShortDetail)
                        } else void 0 !== this.model.get("relativeScore") && utils.animateChange(this.$relativeScore, utils.formatScore(this.model.get("relativeScore")))
                    },
                    onThruChanged: function () {
                        var thru = this.model.get("thru");
                        18 === thru && (thru = "F"), utils.animateChange(this.$thru, thru)
                    },
                    onRelativeScoreChanged: function () {
                        var score = utils.formatScore(this.model.get("relativeScore"));
                        utils.animateChange(this.$relativeScore, score)
                    },
                    onCurrentRoundScoreChanged: function () {
                        var score = utils.formatScore(this.model.get("currentRoundScore"));
                        utils.animateChange(this.$today, score)
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = CompetitorOverviewView
        }, {
            "../config": 9,
            "./utils": 55
        }],
        30: [function (require, module, exports) {
            var HeaderView = require("./Header"),
                LeaderboardView = require("./Leaderboard"),
                PlayerStatsView = require("./PlayerStats"),
                CourseStatsView = require("./CourseStats"),
                PlayoffView = require("./Playoff"),
                config = require("../config"),
                $ = window.$,
                _ = window._,
                espn_ui = window.espn_ui,
                ContentView = window.Backbone.Marionette.LayoutView.extend({
                    template: !1,
                    el: ".col-b",
                    tournamentsTemplate: window.JST["leaderboard/dropdown"],
                    mobileTournamentsTemplate: window.JST["leaderboard/mobile-tournament-dropdown"],
                    regions: {
                        header: ".matchup-header",
                        leaderboard: "#leaderboard-view",
                        stats: "#stats-container",
                        playoff: ".playoff-wrapper",
                        multiCourseContainer: ".multi-course-container"
                    },
                    initialize: function () {
                        this.viewModel = {
                            currentView: "#leaderboard-view"
                        }, this.$stats = this.$("#stats-container"), this.$leaderboard = this.$("#leaderboard-view"), this.$containers = this.$(".leaderboard-container"), this.$tournaments = this.$(".tournament.button-filter"), this.competitors = espn.leaderboard.model.get("competitors"), this.courses = espn.leaderboard.model.get("courses"), this.playoffModel = espn.leaderboard.model.get("playoff"), this.listenTo(this.playoffModel, "change", this.onPlayoffModelChanged, this)
                    },
                    events: {
                        "click ul.season li a": "changeSeason",
                        "change #mobile-season-toggle": "changeSeason",
                        "change #mobile-tournament-toggle": "changeTournament",
                        "click .button-group a": "changeView"
                    },
                    onRender: function () {
                        var headerView = new HeaderView({
                            model: espn.leaderboard.model.get("header")
                        });
                        this.header.attachView(headerView);
                        var leaderboardView = new LeaderboardView({
                            collection: espn.leaderboard.model.get("competitors")
                        });
                        this.leaderboard.attachView(leaderboardView), "in" === espn.leaderboard.status && this.playoffModel.get("hasPlayoff") === !0 && this.renderPlayoff(), espn.leaderboard.debug === !0 && (window.leaderboardView = leaderboardView)
                    },
                    changeSeason: function (e) {
                        var $target = $(e.currentTarget),
                            isMobile = $target.is("select"),
                            data = isMobile ? $target.find("option:selected").data() : $target.data(),
                            text = isMobile ? $target.find("option:selected").text() : $target.text();
                        if (data.season) {
                            this.$("button.season").text(text), this.$('#mobile-season-toggle option[data-season="' + data.season + '"]').attr("selected", "selected").siblings().removeAttr("selected");
                            var url = espn.leaderboard.tourScheduleUrl.replace("{{season}}", data.season);
                            return this.$tournaments.text(config.LOADING_TEXT).removeClass("dropdown-toggle").parent().removeClass("hoverable"), $.ajax({
                                type: "GET",
                                url: url,
                                success: this.updateDropdown.bind(this),
                                error: function () {
                                    this.$tournaments.text(config.TOURNAMENTS_TEXT).addClass("dropdown-toggle").parent().addClass("hoverable")
                                }.bind(this)
                            })
                        }
                    },
                    changeTournament: function (e) {
                        var $target = $(e.currentTarget),
                            link = $target.find("option:selected").attr("data-link"),
                            $link = $('<a href="' + link + '"></a>');
                        $(document.body).append($link), $link.trigger("click"), $link.remove()
                    },
                    updateDropdown: function (data) {
                        if (data && data.defaultSeason) {
                            var tourSeason = _.findWhere(data.seasons, {
                                year: data.defaultSeason
                            });
                            if (tourSeason && tourSeason.events) {
                                var tournamentGroups = [],
                                    tournamentsByMonth = {},
                                    months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "Octorber", "November", "December"],
                                    SHOW_PRE_TOURNAMENT_WITHIN_DAYS = 2,
                                    MS_IN_DAY = 864e5,
                                    today = Date.now(),
                                    filteredTourSeason = tourSeason.events.filter(function (tournament, i) {
                                        return "pre" !== tournament.status || 0 === i || Math.floor((new Date(tournament.startDate) - today) / MS_IN_DAY) <= SHOW_PRE_TOURNAMENT_WITHIN_DAYS
                                    });
                                filteredTourSeason.reverse().forEach(function (tournament) {
                                    var link = espn_ui.Helpers.links.getRelativeEditionLink({
                                        link: tournament.link
                                    }),
                                        startDate = new Date(tournament.startDate),
                                        month = months[startDate.getMonth()],
                                        year = startDate.getFullYear(),
                                        obj = {
                                            label: tournament.label,
                                            link: link
                                        };
                                    espn.leaderboard.useqa === !0 && (link += "&useqa=true"), tournamentsByMonth[month] ? tournamentsByMonth[month].push(obj) : (tournamentsByMonth[month] = [obj], tournamentGroups.push({
                                        month: month,
                                        year: year,
                                        tournaments: tournamentsByMonth[month]
                                    }))
                                }), this.$tournaments.text(config.TOURNAMENTS_TEXT).addClass("dropdown-toggle").parent().addClass("hoverable"), this.$("ul.tour").html(this.tournamentsTemplate({
                                    tournamentGroups: tournamentGroups
                                })), this.$("#mobile-tournament-toggle").html(this.mobileTournamentsTemplate({
                                    tournamentGroups: tournamentGroups
                                }))
                            }
                        }
                    },
                    changeView: function (e) {
                        e.preventDefault();
                        var $target = $(e.currentTarget),
                            tab = $target.attr("data-tab");
                        this.viewModel.currentView !== tab && (this.$(".matchup-content").addClass("loading-tab"), $target.addClass("active").siblings().removeClass("active"), this.viewModel.currentView = tab, this.$containers.hide(), this.$stats.addClass("loading"), this.stats.empty(), "player-stats" === tab ? (this.$stats.show(), this.competitors.addStats(function () {
                            this.stats.show(new PlayerStatsView), this.$(".matchup-content").removeClass("loading-tab")
                        }.bind(this))) : "course-stats" === tab ? (this.$stats.show(), this.courses.update(function () {
                            this.stats.show(new CourseStatsView({
                                collection: espn.leaderboard.model.get("courses")
                            })), this.$(".matchup-content").removeClass("loading-tab")
                        }.bind(this))) : (this.$leaderboard.show(),
                            this.$(".matchup-content").removeClass("loading-tab")))
                    },
                    onPlayoffModelChanged: function () {
                        var changed = this.playoffModel.changedAttributes();
                        if (changed && "hasPlayoff" in changed) {
                            var hasPlayoff = this.playoffModel.get("hasPlayoff");
                            hasPlayoff && this.renderPlayoff()
                        }
                    },
                    renderPlayoff: function () {
                        this.playoff.show(new PlayoffView({
                            model: this.playoffModel
                        }))
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = ContentView
        }, {
            "../config": 9,
            "./CourseStats": 31,
            "./Header": 38,
            "./Leaderboard": 40,
            "./PlayerStats": 42,
            "./Playoff": 44
        }],
        31: [function (require, module, exports) {
            var CourseStatsTableView = require("./CourseStatsTable"),
                CoursesHeaderView = require("./CoursesHeader"),
                POLLING_INTERVAL = 3e4,
                CourseStatsView = window.Backbone.Marionette.LayoutView.extend({
                    template: window.JST["leaderboard/course-stats"],
                    regions: {
                        holes: ".holes-wrapper"
                    },
                    initialize: function () {
                        espn.leaderboard.App.vent.on("course:change", function (courseId) {
                            var courseModel = espn.leaderboard.model.get("courses").findWhere({
                                id: courseId
                            });
                            courseModel && this.holes.show(new CourseStatsTableView({
                                collection: courseModel.get("holes")
                            }))
                        }.bind(this))
                    },
                    onBeforeShow: function () {
                        if (this.collection.length > 0) {
                            this.collection.length > 1 && espn.leaderboard.App.contentView.multiCourseContainer.show(new CoursesHeaderView({
                                collection: this.collection
                            }));
                            var defaultCourse = this.collection.first();
                            this.holes.show(new CourseStatsTableView({
                                collection: defaultCourse.get("holes")
                            })), "in" === espn.leaderboard.status && (this.pollingInterval = setInterval(function () {
                                this.collection.update()
                            }.bind(this), POLLING_INTERVAL))
                        }
                    },
                    onShow: function () {
                        this.$el.parent().removeClass("loading")
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0, espn.leaderboard.App.vent.off("course:change"), clearInterval(this.pollingInterval), espn.leaderboard.App.contentView.multiCourseContainer.empty()
                    }
                });
            module.exports = CourseStatsView
        }, {
            "./CourseStatsTable": 32,
            "./CoursesHeader": 33
        }],
        32: [function (require, module, exports) {
            var HoleView = require("./Hole"),
                ESPNSortableTableView = require("./ESPNSortableTable"),
                config = require("../config"),
                _ = window._,
                espn_ui = window.espn_ui,
                CourseStatsTableView = ESPNSortableTableView.extend({
                    template: window.JST["leaderboard/course-stats-table"],
                    childView: HoleView,
                    childViewContainer: "tbody",
                    ui: {
                        header: "th"
                    },
                    events: {
                        "click @ui.header": "sortView"
                    },
                    collectionEvents: {
                        change: "reorderIfNecessary"
                    },
                    initialize: function () {
                        this.viewModel = {
                            sort: "number",
                            sortDirection: "asc",
                            displayedSort: "number"
                        }, this.setViewComparator()
                    },
                    onShow: function () {
                        espn_ui.LoadBehavior(this.$el)
                    },
                    templateHelpers: function () {
                        var helpers = {
                            config: config.COURSE_STATS_COLUMN_CONFIG
                        };
                        return _.extend(helpers, ESPNSortableTableView.prototype.templateHelpers.apply(this, arguments))
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0, clearTimeout(this.processingTimeoutHook)
                    }
                });
            module.exports = CourseStatsTableView
        }, {
            "../config": 9,
            "./ESPNSortableTable": 35,
            "./Hole": 39
        }],
        33: [function (require, module, exports) {
            var $ = window.$,
                CoursesHeaderView = window.Backbone.Marionette.ItemView.extend({
                    template: window.JST["leaderboard/courses-header"],
                    events: {
                        "click .coursetoggle li a": "toggleCourse",
                        "change select": "toggleCourse"
                    },
                    initialize: function () {
                        this.$status = $(".matchup-content > h2")
                    },
                    templateHelpers: function () {
                        return {
                            courses: this.collection.toJSON(),
                            defaultCourseName: this.collection.first().get("name")
                        }
                    },
                    onBeforeShow: function () {
                        this.$status.addClass("has-options")
                    },
                    toggleCourse: function (e) {
                        e.preventDefault();
                        var $target = $(e.currentTarget),
                            isMobile = $target.is("select"),
                            courseId = isMobile ? $target.find("option:selected").attr("data-id") : $target.attr("data-id"),
                            text = isMobile ? $target.find("option:selected").text() : $target.text();
                        espn.leaderboard.App.vent.trigger("course:change", courseId), this.$(".coursebutton").text(text), isMobile || (this.$(".dropdown-wrapper").addClass("hide-dropdown"), this.timeoutHook = setTimeout(function () {
                            this.$(".dropdown-wrapper").removeClass("hide-dropdown")
                        }, 500))
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0, clearTimeout(this.timeoutHook), this.$status.removeClass("has-options")
                    }
                });
            module.exports = CoursesHeaderView
        }, {}],
        34: [function (require, module, exports) {
            var ESPNSortableTableView = require("./ESPNSortableTable"),
                _ = window._,
                ExistingCollectionView = ESPNSortableTableView.extend({
                    initialize: function (options) {
                        this._initChildViewStorage(), this.childView = this.childView || options.childView, this.createChildren(options), this._initialEvents()
                    },
                    createChildren: function (options) {
                        this.collection.each(function (model) {
                            var childViewOptions = "function" == typeof this.childViewOptions ? this.childViewOptions(model, options) : {},
                                el = options.selector || this.childPrefix ? this.childPrefix + model.get("id") : model.get("id"),
                                view = new this.childView(_.extend({
                                    el: el,
                                    model: model
                                }, childViewOptions));
                            this.initializeChild(view)
                        }, this)
                    },
                    initializeChild: function (view) {
                        this.proxyChildEvents(view), this.children.add(view), view.onRender && view.onRender()
                    }
                });
            module.exports = ExistingCollectionView
        }, {
            "./ESPNSortableTable": 35
        }],
        35: [function (require, module, exports) {
            var $ = window.$,
                _ = window._,
                ESPNSortableTableView = window.Backbone.Marionette.CompositeView.extend({
                    reorderOnSort: !0,
                    sortView: function (e) {
                        var $header = $(e.currentTarget),
                            data = $header.data(),
                            sortable = data.sortable,
                            sortAttr = data.field,
                            sortType = data.sortType,
                            defaultSortDirection = data.sortDirection,
                            sortDirection = defaultSortDirection,
                            previousSortAttr = this.viewModel.sort,
                            previousSortDirection = this.viewModel.sortDirection;
                        sortable && sortAttr && (this.viewModel.displayedSort === sortAttr && ("asc" === this.viewModel.sortDirection ? sortDirection = "desc" : "desc" === this.viewModel.sortDirection && (sortDirection = "asc")), this.$("th").removeClass("asc desc"), this.$("th." + sortAttr).addClass(sortDirection), this.$("col").removeClass("sorted"), this.$("col." + sortAttr).addClass("sorted"), this.viewModel.displayedSort = sortAttr, this.viewModel.sortDirection = sortDirection, this.viewModel.sort = sortAttr, this.viewModel.sortType = sortType, this.vanitySortAttrs && this.vanitySortAttrs[sortAttr] && (this.viewModel.sort = this.vanitySortAttrs[sortAttr]), this.setViewComparator(), this.viewModel.sort === previousSortAttr && this.viewModel.sortDirection === previousSortDirection || this.reorder(), this.triggerMethod("sort:view"))
                    },
                    reorderIfNecessary: function (model) {
                        this.changedModels = this.changedModels || [], 0 === this.changedModels.length && (this.processingTimeoutHook = setTimeout(function () {
                            var changed, that = this,
                                shouldReorder = !1;
                            _.each(this.changedModels, function (model) {
                                if (changed = model.changedAttributes(), changed && that.viewModel.sort in changed) return shouldReorder = !0, !1
                            }), shouldReorder === !0 && this.reorder(), this.changedModels = []
                        }.bind(this), 0)), this.changedModels.push(model)
                    },
                    templateHelpers: function () {
                        return {
                            getColumnAttributes: function (entry) {
                                var key = entry.sortKey || entry.key || "",
                                    sorted = entry.sorted === !0,
                                    attributes = ' class="';
                                return key && (attributes += " " + key), sorted === !0 && (attributes += " sorted"), attributes += '"'
                            },
                            getHeaderAttributes: function (entry) {
                                var key = entry.sortKey || entry.key || "",
                                    className = ' class="' + key + (entry.className ? " " + entry.className : "") + '"',
                                    attributes = "";
                                return entry.sortable !== !0 && void 0 !== entry.sortable || (attributes += ' data-sortable="true"'), key && (attributes += ' data-field="' + key + '"'), entry.sortDirection && (attributes += ' data-sort-direction="' + entry.sortDirection + '"'), entry.sortType && (attributes += ' data-sort-type="' + entry.sortType + '"'), className && (attributes += className), attributes
                            }
                        }
                    },
                    setViewComparator: function () {
                        var field = this.viewModel.sort,
                            sortDirection = this.viewModel.sortDirection,
                            type = this.viewModel.sortType || "";
                        if (!field || !sortDirection) throw new Error("ESPNSortableTableView requires that a sort field and a sortDirection be specified on the viewModel");
                        switch (type) {
                            case "integer":
                                this.viewComparator = function (model) {
                                    var val = parseInt(model.get(field), 10);
                                    return "asc" === sortDirection ? val : -val
                                };
                                break;
                            case "double":
                                this.viewComparator = function (model) {
                                    var val = model.get(field) || "";
                                    return val = parseFloat(val.replace(/[\$,]/g, "")), "asc" === sortDirection ? val : -val
                                };
                                break;
                            case "custom:thru":
                                this.viewComparator = function (a, b) {
                                    var aVal = a.get("thru"),
                                        bVal = b.get("thru");
                                    return aVal === bVal ? 0 === aVal && (aVal = a.get("teeTime"), bVal = b.get("teeTime"), aVal && bVal && (aVal = new Date(aVal), bVal = new Date(bVal), aVal.getTime() !== bVal.getTime())) ? "asc" === sortDirection ? aVal === bVal ? 0 : aVal > bVal ? 1 : -1 : aVal === bVal ? 0 : aVal > bVal ? -1 : 1 : (aVal = a.get("sortOrder"), bVal = b.get("sortOrder"), aVal === bVal ? 0 : aVal > bVal ? 1 : -1) : void 0 === aVal ? 1 : void 0 === bVal ? -1 : "asc" === sortDirection ? aVal === bVal ? 0 : aVal > bVal ? 1 : -1 : aVal === bVal ? 0 : aVal > bVal ? -1 : 1
                                };
                                break;
                            case "custom:movement":
                                this.viewComparator = function (model) {
                                    var val = parseInt(model.get(field), 10);
                                    return "asc" === sortDirection ? -val : val
                                };
                                break;
                            default:
                                this.viewComparator = function (a, b) {
                                    var aVal = a.get(field),
                                        bVal = b.get(field);
                                    return "asc" === sortDirection ? aVal === bVal ? 0 : aVal > bVal ? 1 : -1 : aVal === bVal ? 0 : aVal > bVal ? -1 : 1
                                }
                        }
                    }
                });
            module.exports = ESPNSortableTableView
        }, {}],
        36: [function (require, module, exports) {
            var FullPlayoffCompetitorView = require("./FullPlayoffCompetitor"),
                _ = window._,
                espn_ui = window.espn_ui,
                FullPlayoffView = window.Backbone.Marionette.CompositeView.extend({
                    template: window.JST["leaderboard/full-playoff"],
                    childView: FullPlayoffCompetitorView,
                    childViewContainer: "tbody",
                    className: "playoff full",
                    initialize: function () {
                        this.collection = this.model.get("competitors"), this.course = espn.leaderboard.model.get("courses").first(), this.listenTo(this.model, "change", this.onChildModelChanged, this), this.listenTo(this.collection, "change", this.onChildModelChanged, this), this.childModelChanged = !1
                    },
                    templateHelpers: function () {
                        var that = this;
                        return {
                            getStatusText: function () {
                                var text = _.translate("Hole Playoff Results");
                                return that.model.get("isFull18") === !0 ? "18 " + text : that.model.get("holesPlayed") + " " + text
                            },
                            getHolesToDisplay: function () {
                                return that.model.get("isFull18") === !0 ? 18 : that.model.get("holesPlayed")
                            },
                            getParForIndex: function (i) {
                                var linescore, hole, holeNumber;
                                return that.model.get("isFull18") === !0 ? holeNumber = i + 1 : (linescore = that.collection.getRepresentativeLinescore(), linescore && linescore.get("scores") && (hole = linescore.get("scores").at(i), hole && (holeNumber = hole.get("holeNumber")))), holeNumber ? that.course.get("holes").get(holeNumber).get("par") : ""
                            },
                            getHoleNumberForIndex: function (i) {
                                var linescore, hole;
                                return that.model.get("isFull18") === !0 ? i + 1 : (linescore = that.collection.getRepresentativeLinescore(), linescore && linescore.get("scores") && (hole = linescore.get("scores").at(i)) ? hole.get("holeNumber") : "-")
                            },
                            getParOut: function () {
                                return that.course.get("parOut") || ""
                            },
                            getParIn: function () {
                                return that.course.get("parIn") || ""
                            },
                            getTotalPar: function () {
                                return that.course.get("par") || ""
                            }
                        }
                    },
                    onShow: function () {
                        espn_ui.LoadBehavior(this.$el)
                    },
                    onChildModelChanged: function () {
                        this.childModelChanged === !1 && (this.childModelChanged = !0, setTimeout(function () {
                            this.render(), this.childModelChanged = !1
                        }.bind(this), 0))
                    }
                });
            module.exports = FullPlayoffView
        }, {
            "./FullPlayoffCompetitor": 37
        }],
        37: [function (require, module, exports) {
            var config = require("../config"),
                FullPlayoffCompetitorView = window.Backbone.Marionette.ItemView.extend({
                    template: window.JST["leaderboard/full-playoff-competitor"],
                    tagName: "tr",
                    initialize: function () {
                        this.linescore = this.model.get("linescore"), this.header = espn.leaderboard.model.get("header"), this.listenTo(this.linescore, "change", this.bubbleUpChange, this), this.listenTo(this.header, "change:winnerId", this.bubbleUpChange, this)
                    },
                    templateHelpers: function () {
                        var that = this;
                        return {
                            getHolesPlayed: function () {
                                return that.model.get("isFull18") === !0 ? 18 : that.model.get("holesPlayed")
                            },
                            getFlagUrl: function () {
                                return window.espn_ui.Helpers.resizeCombinerImage(that.model.get("flag"), {
                                    h: 20,
                                    w: 20
                                })
                            },
                            getWinnerClass: function () {
                                return that.model.get("id") === that.header.get("winnerId") ? "winner" : ""
                            },
                            getRelativeScore: function () {
                                return that.linescore.get("displayValue")
                            },
                            getHoleNumberForIndex: function (i) {
                                var hole = that.linescore.get("scores").at(i);
                                return hole && hole.get("holeNumber") ? hole.get("holeNumber") : "-"
                            },
                            getScoreForHoleNumber: function (i) {
                                var hole = that.linescore.get("scores").at(i);
                                return hole && hole.get("score") ? hole.get("score") : ""
                            },
                            getClassForHoleNumber: function (i) {
                                var hole = that.linescore.get("scores").at(i);
                                return hole && hole.get("scoreType") ? config.SCORECARD_COLOR_CODES[hole.get("scoreType")] || "" : ""
                            },
                            getOutScore: function () {
                                return that.linescore.get("outScore") || "-"
                            },
                            getInScore: function () {
                                return that.linescore.get("inScore") || "-"
                            },
                            getTotalScore: function () {
                                return that.linescore.get("value") || "-"
                            }
                        }
                    },
                    bubbleUpChange: function () {
                        this.model.trigger("change")
                    }
                });
            module.exports = FullPlayoffCompetitorView
        }, {
            "../config": 9
        }],
        38: [function (require, module, exports) {
            var $ = window.$,
                HeaderView = window.Backbone.Marionette.ItemView.extend({
                    template: !1,
                    modelEvents: {
                        "change:tournamentStatus": "onStatusChanged"
                    },
                    onStatusChanged: function () {
                        var status = this.model.get("tournamentStatus");
                        $("#pane-main").removeClass("pre in post").addClass(status), espn.leaderboard.App.vent.trigger("leaderboard:status:changed")
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = HeaderView
        }, {}],
        39: [function (require, module, exports) {
            var utils = require("./utils"),
                HoleView = window.Backbone.Marionette.ItemView.extend({
                    template: window.JST["leaderboard/hole"],
                    tagName: "tr",
                    modelEvents: {
                        change: "update"
                    },
                    dynamicFields: ["avgScore", "eagles", "birdies", "pars", "bogeys", "dBogeys", "dBogeysPlus", "scoreToPar"],
                    onShow: function () {
                        this.dynamicFields.forEach(function (field) {
                            this["$" + field] = this.$("td." + field)
                        }.bind(this))
                    },
                    update: function () {
                        var changed = this.model.changedAttributes();
                        if (changed) {
                            var val;
                            this.dynamicFields.forEach(function (field) {
                                field in changed && (val = this.model.get(field), utils.animateChange(this["$" + field], val))
                            }.bind(this))
                        }
                    },
                    templateHelpers: function () {
                        return {
                            getMovementClass: function (score) {
                                var CLASSNAME_MOVEMENT = "";
                                return isNaN(score) || (parseFloat(score) > 0 ? CLASSNAME_MOVEMENT = "movement positive" : parseFloat(score) < 0 && (CLASSNAME_MOVEMENT = "movement negative")), CLASSNAME_MOVEMENT
                            },
                            formatScore: function (score) {
                                return !isNaN(score) && parseFloat(score) > 0 ? "+" + score : score
                            }
                        }
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = HoleView
        }, {
            "./utils": 55
        }],
        40: [function (require, module, exports) {
            var CompetitorView = require("./Competitor"),
                ESPNCollectionView = require("./ESPNCollectionView"),
                utils = require("./utils"),
                config = require("../config"),
                $ = window.jQuery,
                LeaderboardView = ESPNCollectionView.extend({
                    template: !1,
                    el: ".leaderboard-table",
                    childView: CompetitorView,
                    childPrefix: "#leaderboard-model-",
                    childViewOptions: function (model, index) {
                        return {
                            childIndex: index,
                            parentId: model.get("id")
                        }
                    },
                    vanitySortAttrs: {
                        position: "sortOrder",
                        relativeScore: "sortOrder",
                        teeTime: "sortOrder"
                    },
                    ui: {
                        header: "th"
                    },
                    events: {
                        "click @ui.header": "sortView"
                    },
                    collectionEvents: {
                        change: "reorderIfNecessary"
                    },
                    initialize: function () {
                        ESPNCollectionView.prototype.initialize.apply(this, arguments), this.viewModel = {
                            sort: "sortOrder",
                            sortDirection: "asc",
                            displayedSort: "pre" === espn.leaderboard.status ? "teeTime" : "relativeScore"
                        }, this.setViewComparator(), this.headerModel = espn.leaderboard.model.get("header"), this.listenTo(this.headerModel, "change", this.onHeaderChanged), this.$status = $(".tournament-status"), this.$cutline = this.$(".cutline"), this.$buttonGroup = $(".matchup-content .button-group"), this.$playerStats = $('[data-tab="player-stats"]'), this.$courseStats = $('[data-tab="course-stats"]'), "in" === this.headerModel.get("roundStatus") && espn.leaderboard.App.vent.trigger("connect"), window.location.href.indexOf("#course-stats") > 1 && !this.$courseStats.hasClass("hidden") && this.$courseStats.click(), window.location.href.indexOf("#player-stats") > 1 && !this.$playerStats.hasClass("hidden") && this.$playerStats.click(), this.loadOutbrainModule()
                    },
                    onSortView: function () {
                        "sortOrder" === this.viewModel.sort && "asc" === this.viewModel.sortDirection ? this.$cutline.removeClass("hidden") : this.$cutline.addClass("hidden")
                    },
                    onReorder: function () {
                        "sortOrder" === this.viewModel.sort && "asc" === this.viewModel.sortDirection && this.repositionCutline()
                    },
                    onHeaderChanged: function () {
                        var changed = this.headerModel.changedAttributes();
                        changed && ("cutScore" in changed && this.onCutScoreChanged(), "roundStatus" in changed && this.onRoundStatusChanged(), "roundStatusDetail" in changed && this.onRoundStatusDetailChanged(), "tournamentStatus" in changed && this.onTournamentStatusChanged(), "tournamentStatusDetail" in changed && this.onTournamentStatusDetailChanged(), ("tournamentStatus" in changed || "hasPlayerStats" in changed || "hasCourseStats" in changed) && this.onStatsAvailableChanged())
                    },
                    onRoundStatusChanged: function () {
                        var roundStatus = this.headerModel.get("roundStatus");
                        this.headerModel.get("currentRound") === this.headerModel.get("cutRound") && ("in" === roundStatus ? this.$cutline.find(".msg").text(config.PROJECTED_CUT_TEXT) : "post" === roundStatus && this.$cutline.find(".msg").text(config.CUT_TEXT))
                    },
                    onRoundStatusDetailChanged: function () {
                        var roundStatusDetail = this.headerModel.get("roundStatusDetail");
                        roundStatusDetail && this.$status.html(roundStatusDetail)
                    },
                    onTournamentStatusChanged: function () {
                        var status = this.headerModel.get("tournamentStatus");
                        "post" === status && espn.leaderboard.App.vent.trigger("disconnect")
                    },
                    onTournamentStatusDetailChanged: function () {
                        var statusDetail = this.headerModel.get("tournamentStatusDetail");
                        statusDetail && this.$status.html(statusDetail)
                    },
                    onCutScoreChanged: function () {
                        if (this.headerModel) {
                            var cutScore = this.headerModel.get("cutScore");
                            void 0 !== cutScore && (this.$cutline.find(".cut-score").html(utils.formatScore(cutScore)), this.repositionCutline())
                        }
                    },
                    onStatsAvailableChanged: function () {
                        if (this.headerModel) {
                            var status = this.headerModel.get("tournamentStatus"),
                                hasPlayerStats = this.headerModel.get("hasPlayerStats"),
                                hasCourseStats = this.headerModel.get("hasCourseStats");
                            "in" !== status && "post" !== status || !hasPlayerStats && !hasCourseStats ? (this.$buttonGroup.addClass("hidden"), this.$playerStats.addClass("hidden"), this.$courseStats.addClass("hidden")) : (this.$buttonGroup.removeClass("hidden"), hasPlayerStats ? this.$playerStats.removeClass("hidden") : this.$playerStats.addClass("hidden"), hasCourseStats ? this.$courseStats.removeClass("hidden") : this.$courseStats.addClass("hidden"))
                        }
                    },
                    repositionCutline: function () {
                        if (this.headerModel) {
                            var cutCount = this.headerModel.getCutCount();
                            void 0 !== cutCount && cutCount > 0 ? this.$cutline.insertAfter(".leaderboard-table tbody:not(.cutline):eq(" + (cutCount - 1) + ")").show() : this.$cutline.hide()
                        }
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0, clearTimeout(this.processingTimeoutHook)
                    },
                    loadOutbrainModule: function () {
                        if (!window.espn_ui.webview) {
                            var currentOutbrain = $(".sponsored-links div.OUTBRAIN"),
                                curOutbrainId = currentOutbrain.attr("data-widget-id");
                            "AR_49" === curOutbrainId && currentOutbrain.attr("data-src", window.location.href), window.espn.plugins && window.espn.plugins.getOutbrain(function () {
                                window.OBR.extern.researchWidget()
                            })
                        }
                    }
                });
            module.exports = LeaderboardView
        }, {
            "../config": 9,
            "./Competitor": 27,
            "./ESPNCollectionView": 34,
            "./utils": 55
        }],
        41: [function (require, module, exports) {
            var TournamentDetailsView = require("./TournamentDetails"),
                TournamentDetailsErrorView = require("./TournamentDetailsError"),
                utils = require("./utils"),
                PlayerStatView = window.Backbone.Marionette.LayoutView.extend({
                    template: window.JST["leaderboard/player-stat"],
                    tagName: "tbody",
                    dynamicFields: ["position", "driveDistAvg", "fwysHit", "gir", "puttsGirAvg", "eagles", "birdies", "pars", "bogeys", "doubles", "scoreToPar"],
                    modelEvents: {
                        change: "update"
                    },
                    regions: {
                        details: ".player-details .content"
                    },
                    ui: {
                        player: "a.full-name, a.short-name"
                    },
                    events: {
                        "click @ui.player": "toggleDetails"
                    },
                    initialize: function (options) {
                        this.viewModel = {
                            details: {
                                showing: !1
                            }
                        }, this.dataFormat = options && options.dataFormat
                    },
                    onShow: function () {
                        this.dynamicFields.forEach(function (field) {
                            this["$" + field] = this.$("td." + field)
                        }.bind(this))
                    },
                    templateHelpers: function () {
                        var that = this;
                        return {
                            formatScore: function (score) {
                                return utils.formatScore(score) || "-"
                            },
                            getFlag: function () {
                                return window.espn_ui.Helpers.resizeCombinerImage(that.model.get("flag"), {
                                    h: 20,
                                    w: 20
                                })
                            },
                            showPlayerLink: function () {
                                return "API" === that.dataFormat
                            }
                        }
                    },
                    update: function () {
                        var changed = this.model.changedAttributes();
                        if (changed) {
                            var val;
                            this.dynamicFields.forEach(function (field) {
                                field in changed && (val = this.model.get(field), utils.animateChange(this["$" + field], val))
                            }.bind(this))
                        }
                    },
                    toggleDetails: function (e) {
                        e.preventDefault(), this.viewModel.details.showing === !0 ? this.$(".player-details").addClass("collapse") : (this.$(".player-details").removeClass("collapse"), this.model.getCompetitorDetails(function (err) {
                            err ? this.details.show(new TournamentDetailsErrorView) : this.details.show(new TournamentDetailsView({
                                model: this.model.get("details")
                            }))
                        }.bind(this))), this.viewModel.details.showing = !this.viewModel.details.showing
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = PlayerStatView
        }, {
            "./TournamentDetails": 49,
            "./TournamentDetailsError": 50,
            "./utils": 55
        }],
        42: [function (require, module, exports) {
            var PlayerStatsTableView = require("./PlayerStatsTable"),
                StatLeadersView = require("./StatLeaders"),
                POLLING_INTERVAL = 3e4,
                PlayerStatsView = window.Backbone.Marionette.LayoutView.extend({
                    template: window.JST["leaderboard/player-stats"],
                    regions: {
                        leaders: ".player-stats-leaders-wrapper",
                        stats: ".player-stats-table-wrapper"
                    },
                    initialize: function () {
                        this.leadersModel = espn.leaderboard.model.get("leaders"), this.collection = espn.leaderboard.model.get("competitors")
                    },
                    onBeforeShow: function () {
                        Object.keys(this.leadersModel.toJSON()).length > 0 && this.leaders.show(new StatLeadersView({
                            model: this.leadersModel,
                            collection: this.collection
                        })), this.stats.show(new PlayerStatsTableView({
                            collection: this.collection
                        }))
                    },
                    onShow: function () {
                        this.$el.parent().removeClass("loading"), "in" === espn.leaderboard.status && (this.pollingInterval = setInterval(function () {
                            this.collection.addStats()
                        }.bind(this), POLLING_INTERVAL))
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0, clearInterval(this.pollingInterval)
                    }
                });
            module.exports = PlayerStatsView
        }, {
            "./PlayerStatsTable": 43,
            "./StatLeaders": 46
        }],
        43: [function (require, module, exports) {
            var PlayerStatView = require("./PlayerStat"),
                ESPNSortableTableView = require("./ESPNSortableTable"),
                config = require("../config"),
                _ = window._,
                espn_ui = window.espn_ui,
                PlayerStatsTableView = ESPNSortableTableView.extend({
                    template: window.JST["leaderboard/player-stats-table"],
                    childView: PlayerStatView,
                    childViewContainer: "table",
                    childViewOptions: function () {
                        return {
                            dataFormat: espn.leaderboard.model.get("header").get("dataFormat")
                        }
                    },
                    filter: function (child) {
                        return "STATUS_CUT" !== child.get("statusName")
                    },
                    collectionEvents: {
                        change: "reorderIfNecessary"
                    },
                    vanitySortAttrs: {
                        position: "sortOrder"
                    },
                    ui: {
                        header: "th"
                    },
                    events: {
                        "click @ui.header": "sortView"
                    },
                    initialize: function () {
                        this.viewModel = {
                            sort: "sortOrder",
                            sortDirection: "asc",
                            displayedSort: "position"
                        }, this.setViewComparator()
                    },
                    onShow: function () {
                        espn_ui.LoadBehavior(this.$el)
                    },
                    templateHelpers: function () {
                        var helpers = {
                            config: config.PLAYER_STATS_COLUMN_CONFIG
                        };
                        return _.extend(helpers, ESPNSortableTableView.prototype.templateHelpers.apply(this, arguments))
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0, clearTimeout(this.processingTimeoutHook)
                    }
                });
            module.exports = PlayerStatsTableView
        }, {
            "../config": 9,
            "./ESPNSortableTable": 35,
            "./PlayerStat": 41
        }],
        44: [function (require, module, exports) {
            var SuddenDeathPlayoffView = require("./SuddenDeathPlayoff"),
                FullPlayoffView = require("./FullPlayoff"),
                PlayoffView = window.Backbone.Marionette.LayoutView.extend({
                    template: !1,
                    el: ".playoff-container",
                    regions: {
                        "region-2": ".region-2",
                        "region-1": ".region-1"
                    },
                    initialize: function () {
                        this.listenTo(this.model, "change", this.onModelChanged, this)
                    },
                    onBeforeShow: function () {
                        this.model.get("rounds").each(function (round, i) {
                            var region = this["region-" + (i + 1)];
                            "sudden-death" === round.get("type") ? region.show(new SuddenDeathPlayoffView({
                                model: round
                            })) : region.show(new FullPlayoffView({
                                model: round
                            }))
                        }.bind(this))
                    },
                    onModelChanged: function () {
                        var changed = this.model.changedAttributes();
                        if (changed && "hasPlayoff" in changed) {
                            var hasPlayoff = this.model.get("hasPlayoff");
                            hasPlayoff === !1 && (this["region-1"].empty(), this["region-2"].empty())
                        }
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = PlayoffView
        }, {
            "./FullPlayoff": 36,
            "./SuddenDeathPlayoff": 47
        }],
        45: [function (require, module, exports) {
            function getPositionText(position) {
                return _.translate("Pos") + " " + position
            }
            var utils = require("./utils"),
                config = require("../config"),
                _ = window._,
                StatLeaderView = window.Backbone.Marionette.ItemView.extend({
                    template: window.JST["leaderboard/stat-leader"],
                    modelEvents: {
                        change: "update"
                    },
                    initialize: function () {
                        this.category = this.options.statType
                    },
                    update: function () {
                        var changed = this.model.changedAttributes();
                        changed && (this.category in changed && utils.animateChange(this.$(".points"), this.model.get(this.category)), "position" in changed && utils.animateChange(this.$(".position"), getPositionText(this.model.get("position"))), "scoreToPar" in changed && utils.animateChange(this.$(".scoreToPar"), this.model.get("scoreToPar")))
                    },
                    templateHelpers: function () {
                        var that = this;
                        return {
                            getHeadshot: function () {
                                var headshot = that.model.get("headshot");
                                return headshot ? window.espn_ui.Helpers.resizeCombinerImage(headshot, {
                                    scale: "crop",
                                    w: 51,
                                    h: 51
                                }) : window.espn_ui.Helpers.resizeCombinerImage(config.MISSING_HEADSHOT, {
                                    scale: "crop",
                                    h: 51,
                                    w: 51
                                })
                            },
                            getFeaturedStat: function () {
                                return that.model.get(that.category)
                            },
                            getScoreToPar: function () {
                                return utils.formatScore(that.model.get("scoreToPar")) || "-"
                            },
                            getPositionText: function () {
                                return getPositionText(that.model.get("position"))
                            },
                            getFlag: function () {
                                return window.espn_ui.Helpers.resizeCombinerImage(that.model.get("flag"), {
                                    h: 20,
                                    w: 20
                                })
                            },
                            getCategory: function () {
                                return that.category
                            }
                        }
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = StatLeaderView
        }, {
            "../config": 9,
            "./utils": 55
        }],
        46: [function (require, module, exports) {
            var StatLeaderView = require("./StatLeader"),
                _ = window._,
                StatLeadersView = window.Backbone.Marionette.LayoutView.extend({
                    template: window.JST["leaderboard/stat-leaders"],
                    dynamicFields: ["driveDistAvg", "fwysHit", "gir", "puttsGirAvg"],
                    regions: {
                        driveDistAvg: ".driveDistAvg .player-wrap",
                        fwysHit: ".fwysHit .player-wrap",
                        gir: ".gir .player-wrap",
                        puttsGirAvg: ".puttsGirAvg .player-wrap"
                    },
                    modelEvents: {
                        change: "update"
                    },
                    initialize: function () {
                        this.categories = {
                            driveDistAvg: _.translate("Driving Distance"),
                            fwysHit: _.translate("Driving Accuracy"),
                            gir: _.translate("Greens In Regulation"),
                            puttsGirAvg: _.translate("Putts Per GIR")
                        }
                    },
                    templateHelpers: function () {
                        var that = this;
                        return {
                            categories: Object.keys(that.categories),
                            getNameForCategory: function (cat) {
                                return that.categories[cat]
                            }
                        }
                    },
                    onBeforeShow: function () {
                        this.dynamicFields.forEach(function (field) {
                            var id = this.model.get(field);
                            id && this[field].show(new StatLeaderView({
                                model: this.collection.get(id),
                                statType: field
                            }))
                        }.bind(this))
                    },
                    update: function () {
                        var changed = this.model.changedAttributes();
                        changed && this.dynamicFields.forEach(function (field) {
                            if (field in changed) {
                                var id = this.model.get(field);
                                id && this[field].show(new StatLeaderView({
                                    model: this.collection.get(id),
                                    statType: field
                                }))
                            }
                        }.bind(this))
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = StatLeadersView
        }, {
            "./StatLeader": 45
        }],
        47: [function (require, module, exports) {
            var SuddenDeathPlayoffCompetitorView = require("./SuddenDeathPlayoffCompetitor"),
                espn_ui = window.espn_ui,
                SuddenDeathPlayoffView = window.Backbone.Marionette.CompositeView.extend({
                    template: window.JST["leaderboard/sudden-death-playoff"],
                    className: "playoff sudden-death",
                    childView: SuddenDeathPlayoffCompetitorView,
                    childViewContainer: "tbody",
                    initialize: function () {
                        this.collection = this.model.get("competitors"), this.listenTo(this.model, "change", this.onChildModelChanged, this), this.listenTo(this.collection, "change", this.onChildModelChanged, this), this.childModelChanged = !1
                    },
                    templateHelpers: function () {
                        var that = this;
                        return {
                            getStatusText: function () {
                                return this.getHoleNumbers().length + " Hole Playoff Results"
                            },
                            getHoleNumbers: function () {
                                return that.model.getHoleNumbers()
                            }
                        }
                    },
                    onShow: function () {
                        espn_ui.LoadBehavior(this.$el)
                    },
                    onChildModelChanged: function () {
                        this.childModelChanged === !1 && (this.childModelChanged = !0, setTimeout(function () {
                            this.render(), this.childModelChanged = !1
                        }.bind(this), 0))
                    }
                });
            module.exports = SuddenDeathPlayoffView
        }, {
            "./SuddenDeathPlayoffCompetitor": 48
        }],
        48: [function (require, module, exports) {
            var SuddenDeathPlayoffCompetitorView = window.Backbone.Marionette.ItemView.extend({
                template: window.JST["leaderboard/sudden-death-playoff-competitor"],
                tagName: "tr",
                initialize: function () {
                    this.linescore = this.model.get("linescore"), this.header = espn.leaderboard.model.get("header"), this.listenTo(this.linescore, "change", this.bubbleUpChange, this), this.listenTo(this.header, "change:winnerId", this.bubbleUpChange, this)
                },
                bubbleUpChange: function () {
                    this.model.trigger("change")
                },
                templateHelpers: function () {
                    var that = this;
                    return {
                        getFlagUrl: function () {
                            return window.espn_ui.Helpers.resizeCombinerImage(that.model.get("flag"), {
                                h: 20,
                                w: 20
                            })
                        },
                        getWinnerClass: function () {
                            return that.model.get("id") === that.header.get("winnerId") ? "winner" : ""
                        }
                    }
                }
            });
            module.exports = SuddenDeathPlayoffCompetitorView
        }, {}],
        49: [function (require, module, exports) {
            var LinescoresView = require("./competitor/Linescores"),
                StatsView = require("./competitor/Stats"),
                ProfileView = require("./competitor/Profile"),
                config = require("../config"),
                $ = window.$,
                TournamentDetailsView = window.Backbone.Marionette.LayoutView.extend({
                    template: window.JST["leaderboard/tournament-details"],
                    tagName: "table",
                    regions: {
                        container: ".tab-content .tab-pane"
                    },
                    ui: {
                        tabs: ".tabs li"
                    },
                    events: {
                        "click @ui.tabs": "onTab"
                    },
                    initialize: function () {
                        this.profileModel = this.model.get("profile"), this.statsModel = this.model.get("stats"), this.roundsCollection = this.model.get("rounds"), this.courses = espn.leaderboard.model.get("courses"), this.viewModel = {}
                    },
                    onTab: function (e) {
                        var view, $target = $(e.currentTarget),
                            tab = $target.attr("data-tab");
                        tab && this.viewModel.tab !== tab && ($target.addClass("active").siblings().removeClass("active"), "linescores" === tab ? view = new LinescoresView({
                            collection: this.roundsCollection
                        }) : "stats" === tab ? view = new StatsView({
                            model: this.statsModel
                        }) : "profile" === tab && (view = new ProfileView({
                            model: this.profileModel
                        })), view && (this.container.show(view), this.viewModel.tab = tab))
                    },
                    onBeforeShow: function () {
                        var view;
                        this.roundsCollection && this.roundsCollection.length > 0 && this.courses && this.courses.length > 0 ? (view = new LinescoresView({
                            collection: this.roundsCollection
                        }), this.viewModel.tab = "linescores") : this.statsModel.hasStats() ? (view = new StatsView({
                            model: this.statsModel
                        }), this.viewModel.tab = "stats") : (view = new ProfileView({
                            model: this.profileModel
                        }), this.viewModel.tab = "profile"), view && this.container.show(view)
                    },
                    templateHelpers: function () {
                        var that = this;
                        return {
                            getHeadshotUrl: function () {
                                var headshotUrl = that.profileModel && that.profileModel.get("headshot");
                                return headshotUrl ? window.espn_ui.Helpers.resizeCombinerImage(headshotUrl, {
                                    w: 170,
                                    h: 170,
                                    scale: "crop",
                                    background: "transparent",
                                    cquality: 40
                                }) : window.espn_ui.Helpers.resizeCombinerImage(config.MISSING_HEADSHOT, {
                                    w: 170,
                                    h: 170,
                                    scale: "crop",
                                    background: "transparent",
                                    cquality: 40
                                })
                            },
                            showScorecard: function () {
                                return that.roundsCollection && that.roundsCollection.length > 0 && that.courses && that.courses.length > 0
                            },
                            showStats: function () {
                                return that.statsModel.hasStats()
                            },
                            showProfile: function () {
                                return that.profileModel
                            },
                            getClassName: function (tabName) {
                                if (this.showScorecard()) {
                                    if ("linescores" === tabName) return "active";
                                    if ("profile" === tabName) return ""
                                } else if ("profile" === tabName) return "active"
                            }
                        }
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = TournamentDetailsView
        }, {
            "../config": 9,
            "./competitor/Linescores": 51,
            "./competitor/Profile": 52,
            "./competitor/Stats": 54
        }],
        50: [function (require, module, exports) {
            var TournamentDetailsErrorView = window.Backbone.Marionette.ItemView.extend({
                tagName: "table",
                template: window.JST["leaderboard/tournament-details-error"],
                onDestroy: function () {
                    espn.leaderboard.debug === !0
                }
            });
            module.exports = TournamentDetailsErrorView
        }, {}],
        51: [function (require, module, exports) {
            var ScorecardView = require("./Scorecard"),
                $ = window.$,
                _ = window._,
                espn_ui = window.espn_ui,
                LinescoresView = window.Backbone.Marionette.LayoutView.extend({
                    template: window.JST["leaderboard/linescores"],
                    tagName: "div",
                    className: "tab-pane pane-linescores active",
                    regions: {
                        scorecard: ".scorecard"
                    },
                    ui: {
                        roundTabs: ".scorecard-round",
                        roundTabsMobile: ".scorecard-round-mobile"
                    },
                    events: {
                        "click @ui.roundTabs": "onRoundTab",
                        "change @ui.roundTabsMobile": "onRoundTabMobile"
                    },
                    initialize: function () {
                        this.lastRegulationRound = this.getLastRegulationRound(), this.viewModel = {}, this.lastRegulationRound && (this.viewModel.round = this.lastRegulationRound.get("roundNumber")), this.isTouchscreen = espn_ui.device.isTouchscreen
                    },
                    onBeforeShow: function () {
                        this.scorecard.show(new ScorecardView({
                            model: this.lastRegulationRound
                        })), this.$dropdown = this.$(".dropdown-wrapper"), this.isTouchscreen && (this.$dropdown.removeClass("hoverable"), this.$dropdown.on("click", function (e) {
                            $(e.currentTarget).toggleClass("show-dropdown")
                        }))
                    },
                    templateHelpers: function () {
                        var that = this;
                        return {
                            getMostRecentRoundText: function () {
                                return that.lastRegulationRound ? _.translate("Round") + " " + that.lastRegulationRound.get("roundNumber") : ""
                            },
                            getNumRounds: function () {
                                return that.lastRegulationRound ? that.lastRegulationRound.get("roundNumber") : 0
                            },
                            showRoundDropdown: function () {
                                return this.getNumRounds() > 1
                            }
                        }
                    },
                    getLastRegulationRound: function () {
                        var lastRegulationRound, headerModel = espn.leaderboard.model.get("header"),
                            maxRounds = headerModel.get("numberOfRounds");
                        return this.collection && this.collection.length > 0 && this.collection.each(function (round) {
                            round.get("isPlayoff") !== !0 && round.get("roundNumber") <= maxRounds && (lastRegulationRound = round)
                        }), lastRegulationRound
                    },
                    changeRound: function (roundToShow, roundText) {
                        if (roundToShow = parseInt(roundToShow, 10), roundToShow && roundToShow !== this.viewModel.round) {
                            var model = this.collection.findWhere({
                                roundNumber: roundToShow
                            });
                            model && (this.scorecard.show(new ScorecardView({
                                model: model
                            })), this.setRound(roundToShow, roundText))
                        }
                    },
                    setRound: function (roundToShow, roundText) {
                        this.$(".button-filter").text(roundText), this.$(".scorecard-round-mobile").val(roundToShow), this.viewModel.round = roundToShow
                    },
                    onRoundTab: function (e) {
                        e.preventDefault();
                        var $target = $(e.currentTarget);
                        this.changeRound($target.attr("data-round"), $target.text()), this.isTouchscreen || (this.$dropdown.addClass("hide-dropdown"), setTimeout(function () {
                            this.$dropdown.removeClass("hide-dropdown")
                        }.bind(this), 500))
                    },
                    onRoundTabMobile: function () {
                        var $target = this.$(".scorecard-round-mobile");
                        this.changeRound($target.val(), $target.find("option:selected").text())
                    }
                });
            module.exports = LinescoresView
        }, {
            "./Scorecard": 53
        }],
        52: [function (require, module, exports) {
            var espn_ui = window.espn_ui,
                ProfileView = window.Backbone.Marionette.ItemView.extend({
                    template: window.JST["leaderboard/profile"],
                    tagName: "div",
                    className: "pane-profile",
                    modelEvents: {
                        change: "onModelChanged"
                    },
                    onBeforeShow: function () {
                        espn_ui.LoadBehavior(this.$el), this.$earnings = this.$(".earnings"), this.$rank = this.$(".rank")
                    },
                    onModelChanged: function () {
                        var changed = this.model.changedAttributes();
                        changed && ("earnings" in changed && this.$earnings.html(this.model.get("earnings")), "rank" in changed && this.$rank.html(this.model.get("rank")))
                    }
                });
            module.exports = ProfileView
        }, {}],
        53: [function (require, module, exports) {
            var config = require("../../config"),
                _ = window._,
                ScorecardView = window.Backbone.Marionette.ItemView.extend({
                    template: window.JST["leaderboard/scorecard"],
                    tagName: "table",
                    className: "inline golf",
                    modelEvents: {
                        change: "onModelChanged"
                    },
                    initialize: function () {
                        var courses = espn.leaderboard.model.get("courses");
                        if (courses && 1 === courses.length) this.course = courses.first();
                        else {
                            var course, courseId = this.model.get("courseId");
                            courseId && (course = espn.leaderboard.model.get("courses").findWhere({
                                id: courseId
                            })), this.course = course || courses.first()
                        }
                        if (this.course) {
                            var holes = this.course.get("holes"),
                                yardsIn = 0,
                                yardsOut = 0;
                            holes && holes.length > 0 && (_.each(holes.models, function (hole) {
                                hole.get("id") > 9 ? yardsIn += hole.get("yards") : yardsOut += hole.get("yards")
                            }), this.yardsOut = yardsOut, this.yardsIn = yardsIn)
                        }
                        this.model && this.model.get("scores") && this.listenTo(this.model.get("scores"), "change", this.onModelChanged)
                    },
                    serializeData: function () {
                        var json = this.model.toJSON();
                        return json.scores = this.model.get("scores").toJSON(), json
                    },
                    templateHelpers: function () {
                        var that = this;
                        return {
                            getClassForScore: function (score) {
                                return config.SCORECARD_COLOR_CODES[score.scoreType] || ""
                            },
                            getHoles: function () {
                                return that.course.get("holes").toJSON()
                            },
                            getParOut: function () {
                                return that.course.get("parOut")
                            },
                            getParIn: function () {
                                return that.course.get("parIn")
                            },
                            getPar: function () {
                                return that.course.get("par")
                            },
                            hasYardages: function () {
                                return !1
                            },
                            getYardsOut: function () {
                                return that.yardsOut
                            },
                            getYardsIn: function () {
                                return that.yardsIn
                            },
                            getTotalYards: function () {
                                return that.course.get("yards")
                            }
                        }
                    },
                    onModelChanged: function () {
                        this.render()
                    },
                    onDestroy: function () {
                        espn.leaderboard.debug === !0
                    }
                });
            module.exports = ScorecardView
        }, {
            "../../config": 9
        }],
        54: [function (require, module, exports) {
            var StatsView = window.Backbone.Marionette.ItemView.extend({
                template: window.JST["leaderboard/competitor-stats"],
                tagName: "div",
                className: "pane-stats",
                modelEvents: {
                    change: "render"
                }
            });
            module.exports = StatsView
        }, {}],
        55: [function (require, module, exports) {
            var config = require("../config"),
                $ = window.$;
            module.exports = {
                animateChange: function ($el, val, fn) {
                    fn = fn || function () { }, $el.fadeOut(config.ANIMATE_SPEED, function () {
                        $(this).html(val), fn(), $(this).fadeIn(config.ANIMATE_SPEED)
                    })
                },
                formatScore: function (score) {
                    if (void 0 !== score) {
                        if ("0" === score || 0 === score) return "E";
                        if (parseInt(score, 10) > 0 && "+" !== (score + "").charAt(0)) return "+" + score
                    }
                    return score
                }
            }
        }, {
            "../config": 9
        }]
    }, {}, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 23, 27, 51, 52, 53, 54, 28, 29, 30, 33, 31, 32, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 55]);