//PGATOUR LINKS
var ff_pla = new Array(
['http://www.pgatour.com/players/index.html','Player Index'],
['http://www.pgatour.com/players/pgatour.html','PGA TOUR'],
['http://www.pgatour.com/players/championstour.html','Champions Tour'],
['http://www.pgatour.com/players/htour.html','Nationwide Tour'],
['http://www.golfweb.com/players/lpgatour/index.html','LPGA Tour'],
['http://www.golfweb.com/players/europeantour/index.html','European Tour'],
['http://www.pgatour.com/tournaments/2003_pga_exempt.html','PGA TOUR exemptions'],
['http://www.pgatour.com/players/features.html','Features'],
['http://www.pgatour.com/charity/index.html','Charity']
);


var ff_tou = new Array(
['http://www.pgatour.com/tournaments/index.html','Weekly schedule'],
['http://www.pgatour.com/tournaments/pgatour.html','PGA TOUR'],
['http://www.pgatour.com/tournaments/championstour.html','Champions Tour'],
['http://www.pgatour.com/tournaments/htour.html','Nationwide Tour'],
['http://www.golfweb.com/tournaments/lpgatour/index.html','LPGA Tour'],
['http://www.golfweb.com/tournaments/europeantour/index.html','European Tour'],
['http://www.golfweb.com/tournaments/othertours/index.html','Other Tours'],
['http://www.pgatour.com/partner/wtw/index.html','The Daily Dozen'],
['http://www.pgatour.com/information/ticketmaster.html','Tickets'],
['http://www.pgatour.com/tournaments/saturday/index.html','Saturday Series'],
['http://www.pgatour.com/players/qschool/2002/index.html','Q-School']
);

var ff_sco = new Array(
['http://www.pgatour.com/scoring/index.html','Scoring Central'],
['http://www.pgatour.com/scoring/r/index.html','PGA TOUR'],
['http://www.pgatour.com/scoring/s/index.html','Champions Tour'],
['http://www.pgatour.com/scoring/h/','Nationwide Tour'],
['http://www.golfweb.com/tournaments/lpgatour/leaderboard.html','LPGA Tour'],
['http://www.golfweb.com/tournaments/europeantour/leaderboard.html','European Tour'],
['http://www.pgatour.com/mm/wireless/index.html','Wireless Options']
);

var ff_sta = new Array(
['http://www.pgatour.com/stats/index.html','Stats Central'],
['http://www.pgatour.com/stats/index_r.html','PGA TOUR'],
['http://www.pgatour.com/stats/index_s.html','Champions Tour'],
['http://www.pgatour.com/stats/index_h.html','Nationwide Tour'],
['http://www.pgatour.com/promotions/charlesschwabcup/index.html','Charles Schwab Cup'],
['http://www.europeantour.com/stats/','European Tour'],
['http://www.golfweb.com/stats/index_l.html','LPGA Tour'],
['http://ww1.sportsline.com/u/newsletters/newsletter.cgi?subdir=pgatour','Free Newsletters']
);



var ff_shp = new Array(
['http://www.sportsline.com/redir?url=981','Shopping home'],
['http://members.ebay.com/aboutme/pgatour-officialauctions/&aid=4521','Auctions on eBay'],
['http://www.pgatour.com/information/ticketmaster.html','Buy Tickets'],
['http://www.golfweb.com/practicetee/equipment/index.html','Equipment News'],
['http://www.golfweb.com/realestate/index.html','Real Estate'],
['http://gearguide.golfweb.com/html.cgi?page=index.html','Gear Guide'],
['http://www.sportsline.com/redir?url=969','PGA TOUR Partners Club'],
['http://www.pgatourhome.com/','PGA TOUR Home']
);

var ff_mlt = new Array(
['http://www.pgatour.com/mm/index.html','Multimedia Guide'],
['http://www.pgatour.com/information/tvtimes.html','TV Times'],
['http://www.pgatour.com/mm/video/index.html','Video'],
['http://www.pgatour.com/mm/audio/index.html','Audio'],
['http://www.pgatour.com/interactive/cyv/index.html','Choose Your View'],
['http://www.pgatour.com/mm/liveat17/index.html','LIVE@17'],
['http://www.pgatour.com/mm/video/onlineshow/index.html','TOUR Monday Online'],
['http://www.pgatour.com/mm/audio/pgatourradio/index.html','TOUR Radio'],
['http://www.pgatour.com/mm/audio/outofthebag/index.html','Out of the Bag'],
['http://www.golfweb.com/mm/photogallery/index.html','Photo gallery'],
['http://www.pgatour.com/interactive/index.html','Interactive'],
['http://www.golfweb.com/interactive/games/index.html','Games and Contests']
);


var ff_ins = new Array(
['http://www.pgatour.com/practicetee/index.html','Practice Tee'],
['http://www.pgatour.com/practicetee/touracademy/index.html','TOUR Academy'],
['http://www.pgatour.com/practicetee/tpi/index.html','Tournament Players'],
['http://www.pgatour.com/practicetee/protips/index.html','Pro Tips'],
['http://www.pgatour.com/mm/video/infomercial.html','Natural Golf'],
['http://www.golfweb.com/practicetee/rules/index.html','Rules'],
['http://www.golfweb.com/practicetee/after50/index.html','Golf After 50'],
['http://www.golfweb.com/practicetee/architects/index.html','Architects'],
['http://www.healthygolf.org/','Healthy Golf']
);

var ff_tra = new Array(
['http://www.golfweb.com/travel/index.html','Destinations'],
['http://courseguide.golfweb.com/html.cgi?page=index.html','Course Guide'],
['http://www.wgv.com/','World Golf Village'],
['http://pgatour.book4golf.com/home/','My Tee Time'],
['http://www.tpc.com/','TPC.com'],
['http://www.weather.com/','Weather Channel']
);


var ff_fan = new Array(
['http://golf.fantasy.pgatour.com/splash/golf/pgatour/challenge','Fantasy Games'],
['http://www.pgatour.com/tournaments/fantasy_vault.html','Fantasy Vault']
);

var ff_eve = new Array(
['http://www.pgatour.com/events/index.html','Big Events'],
['http://www.pgatour.com/tournaments/r011/index.html','THE PLAYERS'],
['http://www.pgatour.com/tournaments/r060/index.html','THE TOUR Championship'],
['http://www.presidentscup.com','Presidents Cup'],
['http://www.golfweb.com/tournaments/masters/index.html','Masters'],
['http://www.golfweb.com/tournaments/usopen/index.html','U.S. Open'],
['http://www.worldgolfchampionships.com/','World Golf Championships'],
['http://www.golfweb.com/tournaments/britishopen/2002/index.html','British Open'],
['http://www.golfweb.com/tournaments/pgachampionship/2002/index.html','PGA Championship']
);

var ff_tour = new Array(
['http://www.pgatour.com/tourcast/index.html','TOURCAST home']
);

var more = new Array(
['http://www.sportsline.com/','Homepage'],
['http://www.sportsline.com/nfl','NFL'],
['http://www.sportsline.com/nba','NBA'],
['http://www.sportsline.com/mlb','MLB'],
['http://www.sportsline.com/nhl','NHL'],
['http://www.sportsline.com/collegefootball','NCAA Football'],
['http://www.sportsline.com/autoracing','Auto'],
['http://www.sportsline.com/u/tennis/','Tennis'],
['/info/siteindex/','More Sports']
);

var tt = new Array(
['','',''],
['Players','pla','pla'],
['Tournaments','tou','tou'],
['Scores','sco','sco'],
['Stats','sta','sta'],
['Shop','shp','shp'],
['Multimedia','mlt','mlt'],
['Instructions','ins','ins'],
['Travel','tra','tra'],
['Fantasy','fan','fan'],
['Events','eve','eve'],
['TOURCast','tour','tour'],
['More Sports ','more','more']
);




var dd_ns4 = (document.layers) ? 1 : 0;
var dd_ie4 = (document.all) ? 1 : 0;
var dd_dom = ((document.getElementById) && (!dd_ie4)) ? 1 : 0; // ns6
var isOver = false;
var timer = null;
var cmTop = 0;
var cmLeft = 0;

// dd_ns4 redraw on resize
if (dd_ns4) {
        origWidth = innerWidth;
        origHeight = innerHeight;
}

function reDo() {
        if (innerWidth != origWidth || innerHeight != origHeight) {location.reload();}
}

if (dd_ns4) onresize = reDo;
if (dd_dom) onload = InitObj;


document.write("<div id=\"gnddhome\"></div>");
	
//build pgatour dropdowns
for ( i = 1 ; i < tt.length-1 ; i++ ) {  // runs it threw the loop
	var ff = eval("ff_" + tt[i][1]);       // runs threw and sets the ff_ array # and the first variable in the array.	
	var divNum = "gndd" + tt[i][2];        // runs threw and sets the ff_ array # and the Second variable in the array.	
	var strDiv = "<div class=\"cm\" id=\"" + divNum + "\" onMouseOver=\"OverLayer();\" onMouseOut=\"OutLayer('"+ divNum + "');\">";
	strDiv = strDiv + "<table cellspacing=1 cellpadding=1 bgcolor=Black width=166 border=0><tr valign=top><td nowrap class=cmLff width=166>";	
	for ( j = 0 ; j < ff.length ; j++ ) {   // runs it threw the loop
		var linkString = new String(ff[j][0]);  // Url String going threw the loop starting at 0 
		if (linkString.indexOf('http:') == -1) ff[j][0] = "" + ff[j][0];		//******
		strDiv = strDiv + "&nbsp\;&nbsp\;&nbsp\;&#183\;&nbsp\;<a  href='" + ff[j][0] + "'>" + ff[j][1] + "</a><br>"; // writes out the actual links.
	}
	strDiv = strDiv + "<br></td></tr></table></div>\n";    // makes the ending of the table.
	document.write(strDiv);  // tells it to write it.
}
//End of pgatour dropdowns



//build more SPLN dropdown
var strMore = "<div class=\"cmMore\" id=\"gnddmore\" onMouseOver=\"OverLayer();\" onMouseOut=\"OutLayer('gnddmore');\">";
strMore = strMore + "<table cellspacing=1 cellpadding=3 border=0 width=115><tr valign=top><td nowrap class=cmLff>";
for ( j = 0 ; j < more.length ; j++ ) {
	if ( j == more.length-1 ) 
	var linkString = new String(more[j][0]);
	if (linkString.indexOf('http:') == -1) more[j][0] = "http://www.sportsline.com" + more[j][0];	
	strMore = strMore + "&nbsp\;&nbsp\;&nbsp\;<b>&#183\;</b>&nbsp\;<a href='" + more[j][0] + "'>" + more[j][1] + "</a><br>";
}
strMore = strMore + "<br></td></tr></table></div>\n";
document.write(strMore);
//End of SPLN dropdown .  Note: reason to why sportsline dropdown is separate to the other is the have flexablitly in making that
//that dropdown smaller to fit in a 770 width.


//End of building the dropdowns




function InitObj() {
	for (i = 1; i < tt.length; i++) { 
		whichEl = "gndd" + tt[i][2];
		if (dd_dom) {
			whichEl = document.getElementById(whichEl);
		}
		else {	
			whichEl = (dd_ns4) ? document.layers[whichEl] : document.all[whichEl].style;
		} 
		whichEl.onmouseover = OverLayer;
		whichEl.onmouseout = OutLayer;
	}


}
InitObj();








function ShowLayer(showEl,event) {
	if (showEl == "gnddhome") return;
	clearTimeout(timer);
	HideAllLayers();
	
	if (dd_dom) {
		var whichEl = document.getElementById(showEl);
	

	}
	else {	
		var whichEl = (dd_ns4) ? document.layers[showEl] : document.all[showEl].style;
	
	}
	
	
//POSITIONING OF DIVS	
	whichAnchor = showEl + "A";      //this area set the div into position.  showEl is the name ex. gnddnfl + A =  gnddnflA
	if (dd_ie4) {										// if you are IE the do this
		if (document.all[whichAnchor]){    // is whichAnchor, which is showEl at the point of show 
			cmTop = findy(document.all[whichAnchor]) + 20;   //then go ahead and set it 20 from top 
			cmLeft = findx(document.all[whichAnchor]) - 20;   
			 

		}
	}
	if (dd_ns4) {
		if (document.anchors[whichAnchor]) {
			cmTop = document.anchors[whichAnchor].y + 20;
			cmLeft = document.anchors[whichAnchor].x - 20;
		}
	}
	if (dd_dom){
		if (document.anchors[whichAnchor]){
			var myObject = document.anchors[whichAnchor];
			while (myObject.offsetParent) {
				cmTop  = cmTop  + myObject.offsetTop;
				cmLeft = cmLeft + myObject.offsetLeft;
				myObject = myObject.offsetParent
			}
		}
	}
	
	
	if (dd_dom) {
		whichEl.style.visibility = "visible";
		whichEl.style.top = cmTop + 20;
                if (showEl != 'gnddmore' && showEl) {
                        if (cmLeft > 640) cmLeft = 592;
                }
		whichEl.style.left = cmLeft;		
	}
	
	else {	
		whichEl.visibility = "visible";
		whichEl.top = cmTop;
                if (showEl != 'gnddmore' && showEl) {
                        if (cmLeft > 640) cmLeft = 592;
                }
		whichEl.left = cmLeft;
	} 	
	cmTop = 0; cmLeft = 0;
}



function findy(item) {
	if (item.offsetParent) {
		return item.offsetTop + findy(item.offsetParent);
	}
	else {
		return item.offsetTop;
	}
}

function findx(item) {
	if (item.offsetParent) {
		return item.offsetLeft + findx(item.offsetParent);
	}
	else {
		return item.offsetLeft;
	}
}

//End of POSITIONING




function HideAllLayers() {
	if (dd_dom) {
		var divTemp = document.getElementsByTagName('div');
		for (i = 0; i < divTemp.length; i++) { 
			var divString = new String(divTemp[i].id);
			if (divString.indexOf('gndd') != -1) divTemp[i].style.visibility = "hidden";
		}
	}	
	if (dd_ie4) {
		var divTemp = document.all.tags("div");
		for (i = 0; i < divTemp.length; i++) { 
			var divString = new String(divTemp[i].id);
			if (divString.indexOf('gndd') != -1) divTemp[i].style.visibility = "hidden";
			
		}
	}
	if (dd_ns4) {
		for (i = 0; i < document.layers.length; i++) { 
			var divString = new String(document.layers[i].name);
			if (divString.indexOf('gndd') != -1) document.layers[i].visibility = "hidden";
		}
	}
}

function OverLayer() { clearTimeout(timer); isOver = true; }

function OutLayer() {  // so  when the mouse is off the SPOT, then set time out make variable isOver set False and set time to execute 
clearTimeout(timer);   //function HideAllLayers  in 300 microseconds.
	isOver = false; 
	timer = setTimeout("HideAllLayers()",300);
}

