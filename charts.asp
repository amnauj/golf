<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<TITLE></TITLE>
</HEAD>
<BODY>

<!-- Chart Control -->

<object classid="clsid:0002E500-0000-0000-C000-000000000046"
id="ChartSpace1"
style="HEIGHT: 80%; WIDTH: 100%" width="250" height="200">
</object>




<script language=VBScript>

Sub Window_onLoad()
    'LoadChartWithLiteral ChartSpace1, Array("FY 98", "FY 99"), _
    LoadChartWithLiteral ChartSpace1, Array("Prima","Beneficio"), _
        Array("Beneficio: " & 1 ), _
        Array(Array(300000,300500),Array(500000,500500))
	
End Sub 'Window_onLoad()


Sub LoadChartWithLiteral(cspace, vSeries, vCategories, avValues)

    Dim cht
    Dim ser


	sql="select id, nombre from jugadores"
	Set RsJugadores= Server.CreateObject("ADODB.Recordset")
	RsJugadores.Open Sql, Application("conn"),2,3

	cht.SetData c.chDimValues, 0, avValues
	cht.SetData c.chDimCategories, 0, vCategories 


	cspace.DataSource = RsJugadores
    set c = cspace.Constants
    cspace.Clear
    set cht = cspace.Charts.Add()
	ChartSpace1.HasChartSpaceLegend=true
	ChartSpace1.ChartSpaceLegend.Position=2 
    cht.SetData c.chDimSeriesNames, c.chDataLiteral, vSeries
    cht.SetData c.chDimCategories, c.chDataLiteral, vCategories

    for each ser in cht.SeriesCollection
        ser.SetData c.chDimValues, c.chDataLiteral, avValues(ser.Index)
    next 'ser
	
End Sub 'LoadChartWithLiteral()

</script>

<SELECT id=lstChartType SIZE=1>
 <OPTION selected value=0>Clustered Column</OPTION>
 <OPTION value=1>Stacked Column</OPTION>
 <OPTION value=2>100% Stacked Column</OPTION>
 <OPTION value=3>Clustered Bar</OPTION>
 <OPTION value=4>Stacked Bar</OPTION>
 <OPTION value=5>100% Stacked Bar</OPTION>
 <OPTION value=6>Line</OPTION>
 <OPTION value=8>Stacked Line</OPTION>
 <OPTION value=10>100% Stacked Line</OPTION>
 <OPTION value=7>Line with Markers</OPTION>
 <OPTION value=9>Stacked Line with Markers</OPTION>
 <OPTION value=11>100% Stacked Line with Markers</OPTION>
 <OPTION value=12>Smooth Line</OPTION>
 <OPTION value=14>Stacked Smooth Line</OPTION>
 <OPTION value=16>100% Stacked Smooth Line</OPTION>
 <OPTION value=13>Smooth Line with Markers</OPTION>
 <OPTION value=15>Stacked Smooth Line with Markers</OPTION>
 <OPTION value=17>100% Stacked Smooth Line with Markers</OPTION>
 <OPTION value=18>Pie</OPTION>
 <OPTION value=19>Pie Exploded</OPTION>
 <OPTION value=20>Stacked Pie</OPTION>
 <OPTION value=21>Scatter</OPTION>
 <OPTION value=25>Scatter with Lines</OPTION>
 <OPTION value=24>Scatter with Markers and Lines</OPTION>
 <OPTION value=26>Filled Scatter</OPTION>
 <OPTION value=23>Scatter with Smooth Lines</OPTION>
 <OPTION value=22>Scatter with Markers and Smooth Lines</OPTION>
 <OPTION value=27>Bubble</OPTION>
 <OPTION value=28>Bubble with Lines</OPTION>
 <OPTION value=29>Area</OPTION>
 <OPTION value=30>Stacked Area</OPTION>
 <OPTION value=31>100% Stacked Area</OPTION>
 <OPTION value=32>Doughnut</OPTION>
 <OPTION value=33>Exploded Doughnut</OPTION>
 <OPTION value=34>Radar with Lines</OPTION>
 <OPTION value=35>Radar with Lines and Markers</OPTION>
 <OPTION value=36>Filled Radar</OPTION>
 <OPTION value=37>Radar with Smooth Lines</OPTION>
 <OPTION value=38>Radar with Smooth Lines and Markers</OPTION>
 <OPTION value=39>High-Low-Close</OPTION>
 <OPTION value=40>Open-High-Low-Close</OPTION>
 <OPTION value=41>Polar</OPTION>
 <OPTION value=42>Polar with Lines</OPTION>
 <OPTION value=43>Polar with Lines and Markers</OPTION>
 <OPTION value=44>Polar with Smooth Lines</OPTION>
 <OPTION value=45>Polar with Smooth Lines and Markers</OPTION>
</SELECT>

<SCRIPT language=vbscript>


Sub lstChartType_onChange()
    nChartType = CLng(lstChartType.value)
  '  msgbox (ChartSpace_WebChart.Charts.Item(0).Type)
ChartSpace1.Charts.Item(0).Type = nChartType


end sub
</script>
<P>&nbsp;</P>

</BODY>
</HTML>
